﻿using System;
using ASPPDFLib;

namespace avis.sbair.Classes
{
    public class InvoiceGen
    {

        //public static void ConvertHTMLToPDF(string HTMLCode, string filename)
        //{

        //    //string strFileName = HttpContext.Current.Server.MapPath("test.pdf");
        //    // step 1: creation of a document-object 
        //    Document document = new Document(PageSize.A4, 30, 30, 30, 30);             
        //    // step 2: 
        //    // we create a writer that listens to the document 
        //    PdfWriter.GetInstance(document, new FileStream(filename, FileMode.Create));
        //    StringReader se = new StringReader(HTMLCode);
        //    HTMLWorker obj = new HTMLWorker(document);
        //    document.Open();
        //    obj.Parse(se);
        //    document.Close();
        //    //ShowPdf(strFileName); 

        //    //HttpContext context = HttpContext.Current;
        //    ////Render PlaceHolder to temporary stream
        //    //System.IO.StringWriter stringWrite = new StringWriter();
        //    //System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //    //StringReader reader = new StringReader(HTMLCode);
        //    ////Create PDF document         
        //    //Document doc = new Document(PageSize.A4);       
        //    //HTMLWorker parser = new HTMLWorker(doc);        
        //    //PdfWriter.GetInstance(doc, new FileStream(filename,  FileMode.Create));        
        //    //doc.Open();         
            
        //    //foreach (IElement element in HTMLWorker.ParseToList(new StringReader(HTMLCode), null))       
        //    //{
        //    //    doc.Add(element);
        //    //}        
        //    //doc.Close();
            
        //}     
        


        //public static void createPDF(string html, string filename)
        //{
        //    Document document = new Document();
        //    try
        //    {
        //        PdfWriter.GetInstance(document, new FileStream(filename, FileMode.Create));
        //        document.Open();
        //        List<IElement> htmlarraylist = HTMLWorker.ParseToList(new StringReader(html), null);
        //        for (int k = 0; k < htmlarraylist.Count; k++)
        //        {
        //            document.Add((IElement)htmlarraylist[k]);
        //        }

        //        document.Close();

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //}




        /// <summary>
        /// Creates a PDF file using the specified HTML, and saving the PDF to the specified file name. 
        /// </summary>
        /// <param name="fileName">
        /// The name of the pdf file that will be created, this should be an absolute file path and name.
        /// </param>
        /// <param name="html">
        /// The HTML from which the PDF will be created. Note that the AspPDF component only supports a subset
        /// of standard html.
        /// </param>
        /// 
        private const string RegistrationKey = "IWezKLfzUgLJIAc213KacCXF1UAgYg6v9goKALDDvZxrFIz9sZiyaZhCMCaNmKXzSJ1bnm47UCcA";


        public static string CreatePdfFile(string fileName, string html)
        {
            // Once the html has been rendered, save it to a pdf file.
            IPdfManager objPdf = null;
            IPdfDocument objDoc = null;
            try
            {
                objPdf = new PdfManager();
                objPdf.RegKey = RegistrationKey;
                objDoc = objPdf.CreateDocument(Type.Missing);


                //IPdfPages x = objDoc.Pages;
                //string param;
                //param = "leftmargin=40";
                ////param = "landscape=true";
                //IPdfParam p = objPdf.CreateParam(param);
                //p.Add("rightmargin=20");
                //p.Add("bottommargin=10");
                //p.Add("topmargin=10");
                //p.Add("pagewidth=577.6");
                //p.Add("pageheight=832.4");
                //p.Add("scale=1");

                objDoc.ImportFromUrl(html, Param: "TopMargin = 0;LeftMargin = 40; RightMargin = 0;BottomMargin = 0; PageWidth = 595.3;PageHeight = 841.9", Username: Type.Missing, Password: Type.Missing);

                // put background image on each page.
                //var image = objDoc.OpenImage(@"d:\neil\overlay.jpg",Type.Missing);
                //foreach (IPdfPage page in objDoc.Pages)
                //{
                //    page.Background.DrawImage(image, "x=0,y=0");

                //}                


                /*
                //logo
                //IPdfImage objImage = objDoc.OpenImage(Server.MapPath("painting.jpg"), Missing.Value);
                IPdfPage objPage = objDoc.Pages[1];

                //IPdfImage objImage = objDoc.OpenImage("C:\\neil\\Avis\\Glynn\\WcfRAZOR\\WcfRAZOR\\WcfRAZOR\\WcfRAZOR\\wcfrazor\\wcfrazor\\avislogo.jpg", Type.Missing);

                Bitmap logo = (Bitmap) Bitmap.FromFile(@"C:\neil\Avis\avis.sbair\avis.sbair\Content\Images\avislogo.jpg"); //WcfRAZOR.GenericInvoiceResources.avislogo;
                System.Drawing.Image i = Bitmap.FromFile(@"C:\neil\Avis\avis.sbair\avis.sbair\Content\Images\avislogo.jpg"); //WcfRAZOR.GenericInvoiceResources.avislogo;

                MemoryStream ms = new MemoryStream();
                // Save to memory using the Jpeg format
                logo.Save(ms, ImageFormat.Jpeg);

                // read to end
                byte[] bmpBytes = ms.GetBuffer();
                logo.Dispose();
                ms.Close();


                IPdfImage objImage = objDoc.OpenImageBinary(bmpBytes, Type.Missing);
                IPdfParam objParam = objPdf.CreateParam(Type.Missing);
                //for (int i = 1; i <= 3; i++)
                //{
                //objParam["x"].Value = (objPage.Width - objImage.Width / i) / 2.0f;
                //objParam["y"].Value = objPage.Height - objImage.Height * i / 2.0f - 200;
                //objParam["ScaleX"].Value = 1.0f / i;
                //objParam["ScaleY"].Value = 1.0f / i;

                objParam["x"].Value = 50; //logoPosition.X_Position;
                objParam["y"].Value = 475; // logoPosition.Y_Position;
                //objParam["ScaleX"].Value = 1.0f / i;
                //objParam["ScaleY"].Value = 1.0f / i;

                objPage.Canvas.DrawImage(objImage, objParam);
                //}
                 * */

                return objDoc.Save(fileName, false);


            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("An error occurred saving the PDF document to disk.", ex);
            }
            finally
            {
                if (objDoc != null)
                {
                    objDoc.Close();
                }
            }
        }



    }
}