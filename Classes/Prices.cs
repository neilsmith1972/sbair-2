using System;
using System.Data;
using System.Diagnostics;
using avis.sbair.Models;

namespace avis.sbair.Classes
{
	/// <summary>
	/// Summary description for Prices.
	/// </summary>
	public class Prices
	{
		private Prices _prices = null;
		private String _countryCode = null;

		Prices AllPrices { get { return _prices; } }

		public Prices( String CountryCode )
		{
			this._countryCode = CountryCode;
			LoadPrices();
		}

		private void LoadPrices() 
		{
			DataTable table;

			try 
			{
				String whereCl = String.Format( "CountryCode = '{0}'", _countryCode );

				_prices = new Prices(_countryCode);

				using(Avis.sd.SBAIR.Db.SBAIRDB db = new Avis.sd.SBAIR.Db.SBAIRDB())
				{
					table = db.RateVersionCollection.GetAsDataTable( whereCl, "" );
					_prices.Merge( table );

					table = db.RatePriceCollection.GetAsDataTable( String.Format(" RateVersionID in ( select RateVersionID from [dbo].[RateVersion] where {0} )", whereCl), "" );
					_prices.Merge( table );

					table = db.V_FeePriceCollection.GetAsDataTable( whereCl, "" );
					table.TableName = "FeePrice";
					_prices.Merge( table );

					table = db.V_InsurancePriceCollection.GetAsDataTable( whereCl, "" );
					table.TableName = "InsurancePrice";
					_prices.Merge( table );

					table=db.V_InsuranceVATCollection.GetAsDataTable( whereCl, "" );
                    table.TableName = "InsuranceVAT";
					_prices.Merge( table );

					table=db.VAT_valuesCollection.GetAsDataTable( whereCl, "" );
					_prices.Merge( table );

					table=db.InvoiceFeeCollection.GetAsDataTable( whereCl, "" );
					_prices.Merge( table );

                    table = db.ContractFeeCollection.GetAsDataTable(whereCl, "");
                    _prices.Merge(table);

				}
			}
			catch ( Exception ex ) 
			{
				System.Diagnostics.Trace.WriteLine( ex );
				_prices = null;
				throw new GetPricesException( ResourcesReader.GetResource("Prices.GetPricesException"), ex );
					
			}
		}

		public decimal GetFeePrice( String FeeCode, String CarGroup, DateTime DateRef ) 
		{
			String filter = String.Format( "FeeCode = '{0}' AND CarGroup = '{1}' AND FromDate <= '{2}' AND ( ToDate >= '{2}' OR ToDate Is Null )" , FeeCode, CarGroup, DateRef.ToString( "yyyy-MM-dd" ) );
			try 
			{			
				DataRow[] rows = _prices.Tables["FeePrice"].Select( filter );

				if ( rows.Length == 0 ) throw new ApplicationException("No rows found"); 

				return ((DataType.Prices.FeePriceRow)rows[0]).Price;
			}
			catch ( Exception ex ) 
			{
				System.Diagnostics.Trace.WriteLine( ex );
				throw new FeePriceNotFoundException( filter, FeeCode, CarGroup, DateRef, ex );	
			}
		}

		public RatePrice GetRatePrice( String RateCode, String CarGroup, DateTime DateRef ) 
		{
			DataRow[] rows;
			int RateVersionID = 0;
			int nbDaysWeek = 0;
			int nbDaysMonth = 0;

			String filter=null;
			try 
			{
				 filter = String.Format( "RateCode = '{0}' AND FromDate <= '{1}' AND ( ToDate >= '{1}' OR ToDate Is Null )" , RateCode, DateRef.ToString( "yyyy-MM-dd" ) );
				
				rows = _prices.Tables["RateVersion"].Select( filter );
			
				if ( rows.Length == 0 ) throw new ApplicationException("No rows found"); 
				
				RateVersionID = ((Prices.RateVersionRow)rows[0]).RateVersionID;
				nbDaysWeek = ((Prices.RateVersionRow)rows[0]).NbDaysWeek;
				nbDaysMonth = ((Prices.RateVersionRow)rows[0]).NbDaysMonth;
			}
			catch ( Exception ex ) 
			{
				System.Diagnostics.Trace.WriteLine( ex );
				throw new RateNotFoundException( filter, RateCode, CarGroup, DateRef, ex ); 
			}

			try 
			{
				filter = String.Format( "RateVersionID = {0} AND CarGroup='{1}'", RateVersionID, CarGroup );

				rows = _prices.Tables["RatePrice"].Select( filter );
			
				if ( rows.Length == 0 ) throw new ApplicationException("No rows found"); 

				DataType.RatePrice rate = new RatePrice();

				DataType.Prices.RatePriceRow row = ((DataType.Prices.RatePriceRow)rows[0]);
				rate.PrDay = row.prDay;
				rate.PrWeek = row.prWeek;
				//if ( rate.PrWeek == 0 ) rate.PrWeek = rate.PrDay * nbDaysWeek;
				rate.PrMonth = row.prMonth;
				//if ( rate.PrMonth == 0 ) rate.PrMonth = rate.PrDay * nbDaysMonth;
				rate.PrKm = row.prKm;
				rate.NbDaysWeek = nbDaysWeek;
				rate.NbDaysMonth = nbDaysMonth;
				return rate;
			}
			catch ( Exception ex ) 
			{
				System.Diagnostics.Trace.WriteLine( ex );
				throw new RatePriceNotFoundException( filter, RateCode, CarGroup, DateRef, ex ); 
			}
		}

		public decimal GetInsurancePrice( String InsuranceCode, String CarGroup, DateTime DateRef ) 
		{
			String filter = String.Format( "InsuranceCode = '{0}' AND CarGroup = '{1}' AND FromDate <= '{2}' AND ( ToDate>= '{2}' OR ToDate Is Null )" , InsuranceCode, CarGroup, DateRef.ToString( "yyyy-MM-dd" ) );
			try 
			{			
				DataRow[] rows = _prices.Tables["InsurancePrice"].Select( filter );

				if ( rows.Length == 0 ) throw new ApplicationException("No rows found");

				return ((DataType.Prices.InsurancePriceRow)rows[0]).Price;
			}
			catch ( Exception ex ) 
			{
				System.Diagnostics.Trace.WriteLine( ex );
				throw new InsurancePriceNotFoundException( filter, InsuranceCode, CarGroup, DateRef, ex );	
			}
		}

		public decimal GetInvoiceFeePrice( DateTime DateRef ) 
		{
			String filter = String.Format( "FromDate <= '{0}' AND ( ToDate >= '{0}' OR ToDate Is Null )" , DateRef.ToString( "yyyy-MM-dd" ) );
			try 
			{			
				DataRow[] rows = _prices.Tables["InvoiceFee"].Select( filter );

				if ( rows.Length == 0 ) throw new ApplicationException("No rows found");

				return ((DataType.Prices.InvoiceFeeRow)rows[0]).Price;
			}
			catch ( Exception ex ) 
			{
				System.Diagnostics.Trace.WriteLine( ex );
				throw new InvoiceFeePriceNotFoundException( filter, DateRef, ex );	
			}
		}

        //public decimal GetContractFeePrice(DateTime DateRef)
        //{
        //    String filter = String.Format("FromDate <= '{0}' AND ( ToDate >= '{0}' OR ToDate Is Null )", DateRef.ToString("yyyy-MM-dd"));
        //    try
        //    {
        //        DataRow[] rows = _prices.Tables["ContractFee"].Select(filter);

        //        if (rows.Length == 0) throw new ApplicationException("No rows found");

        //        return ((DataType.Prices.ContractFeeRow)rows[0]).Price;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Trace.WriteLine(ex);
        //        throw new ContractFeePriceNotFoundException(filter, DateRef, ex);
        //    }
        //}

        //public decimal GetVATvalue_Field( String Field, DateTime DateRef ) 
        //{
        //    String filter = String.Format( "Field = '{0}' AND FromDate <= '{1}' AND ( ToDate >= '{1}' OR ToDate Is Null )" , Field, DateRef.ToString( "yyyy-MM-dd" ) );
        //    try 
        //    {

        //        DataRow[] rows = _prices.Tables["VAT_values"].Select( filter );
        //        if ( rows.Length == 0 ) return 0.0m;

        //        return ((DataType.Prices.VAT_valuesRow)rows[0]).pcent;
        //    }
        //    catch ( Exception ex ) 
        //    {
        //        System.Diagnostics.Trace.WriteLine( ex );
        //        throw new PriceNotFoundException("GetVATvalue_Field" , filter, ex );	
        //    }
        //}

        //public decimal GetVAT_Insurance( String InsuranceCode, DateTime DateRef ) 
        //{
        //    String filter = String.Format( "InsuranceCode = '{0}' AND FromDate <= '{1}' AND ( ToDate >= '{1}' OR ToDate Is Null )" , InsuranceCode, DateRef.ToString( "yyyy-MM-dd" ) );
        //    try 
        //    {

        //        DataRow[] rows = _prices.Tables["InsuranceVAT"].Select( filter );
        //        if ( rows.Length == 0 ) return 0.0m; 

        //        return ((DataType.Prices.InsuranceVATRow)rows[0]).pcent;
        //    }
        //    catch ( Exception ex ) 
        //    {
        //        System.Diagnostics.Trace.WriteLine( ex );
        //        throw new PriceNotFoundException("GetVAT_Insurance" , filter, ex );	
        //    }
        //}
	}

	public class PriceNotFoundException : ApplicationException 
	{
		public PriceNotFoundException() : base() {;}
		public PriceNotFoundException( string message) : base(message) {;}
		public PriceNotFoundException( string message, Exception InnerException) : base(message, InnerException) {;}
		public PriceNotFoundException( string method, string filter)  : base(String.Format( "Method: {0}. Filter: {1}", method, filter)) {	; }
		public PriceNotFoundException( string method, string filter, Exception InnerException)  : base(String.Format( "Method: {0}. Filter: {1}", method, filter), InnerException) {	; }
	}

	public class RateNotFoundException : ApplicationException 
	{
		private string _filter = null;
		private string _RateCode = null;
		private string _CarGroup = null;
		private DateTime _DateRef = DateTime.MinValue;

		public string filter { get { return _filter; } }
		public string RateCode { get { return _RateCode; } }
		public string CarGroup { get { return _CarGroup; } }
		public DateTime DateRef { get { return _DateRef; } }

		public RateNotFoundException( string filter, String RateCode, String CarGroup, DateTime DateRef)  : base(String.Format( "Filter: {0}", filter)) {
			this._filter = filter;
			this._RateCode = RateCode;
			this._CarGroup = CarGroup;
			this._DateRef = DateRef;
		}
		
		public RateNotFoundException( string filter, String RateCode, String CarGroup, DateTime DateRef, Exception InnerException)  : base(String.Format( "Filter: {0}", filter), InnerException) {
			this._filter = filter;
			this._RateCode = RateCode;
			this._CarGroup = CarGroup;
			this._DateRef = DateRef;
		}
	}

	public class RatePriceNotFoundException : ApplicationException 
	{
		private string _filter = null;
		private string _RateCode = null;
		private string _CarGroup = null;
		private DateTime _DateRef = DateTime.MinValue;

		public string filter { get { return _filter; } }
		public string RateCode { get { return _RateCode; } }
		public string CarGroup { get { return _CarGroup; } }
		public DateTime DateRef { get { return _DateRef; } }

		public RatePriceNotFoundException( string filter, String RateCode, String CarGroup, DateTime DateRef)  : base(String.Format( "Filter: {0}", filter)) {
			this._filter = filter;
			this._RateCode = RateCode;
			this._CarGroup = CarGroup;
			this._DateRef = DateRef;
		}
		
		public RatePriceNotFoundException( string filter, String RateCode, String CarGroup, DateTime DateRef, Exception InnerException)  : base(String.Format( "Filter: {0}", filter), InnerException) {
			this._filter = filter;
			this._RateCode = RateCode;
			this._CarGroup = CarGroup;
			this._DateRef = DateRef;
		}
	}

	public class FeePriceNotFoundException : ApplicationException 
	{
		private string _filter = null;
		private string _FeeCode = null;
		private string _CarGroup = null;
		private DateTime _DateRef = DateTime.MinValue;

		public string filter { get { return _filter; } }
		public string FeeCode { get { return _FeeCode; } }
		public string CarGroup { get { return _CarGroup; } }
		public DateTime DateRef { get { return _DateRef; } }

		public FeePriceNotFoundException( string filter, String FeeCode, String CarGroup, DateTime DateRef)  : base(String.Format( "Filter: {0}", filter)) 
		{
			this._filter = filter;
			this._FeeCode = FeeCode;
			this._CarGroup = CarGroup;
			this._DateRef = DateRef;
		}
		
		public FeePriceNotFoundException( string filter, String FeeCode, String CarGroup, DateTime DateRef, Exception InnerException)  : base(String.Format( "Filter: {0}", filter), InnerException) 
		{
			this._filter = filter;
			this._FeeCode = FeeCode;
			this._CarGroup = CarGroup;
			this._DateRef = DateRef;
		}
	}

	public class InsurancePriceNotFoundException : ApplicationException 
	{
		private string _filter = null;
		private string _InsuranceCode = null;
		private string _CarGroup = null;
		private DateTime _DateRef = DateTime.MinValue;

		public string filter { get { return _filter; } }
		public string InsuranceCode { get { return _InsuranceCode; } }
		public string CarGroup { get { return _CarGroup; } }
		public DateTime DateRef { get { return _DateRef; } }

		public InsurancePriceNotFoundException( string filter, String InsuranceCode, String CarGroup, DateTime DateRef)  : base(String.Format( "Filter: {0}", filter)) 
		{
			this._filter = filter;
			this._InsuranceCode = InsuranceCode;
			this._CarGroup = CarGroup;
			this._DateRef = DateRef;
		}
		
		public InsurancePriceNotFoundException( string filter, String InsuranceCode, String CarGroup, DateTime DateRef, Exception InnerException)  : base(String.Format( "Filter: {0}", filter), InnerException) 
		{
			this._filter = filter;
			this._InsuranceCode = InsuranceCode;
			this._CarGroup = CarGroup;
			this._DateRef = DateRef;
		}
	}

	public class InvoiceFeePriceNotFoundException : ApplicationException 
	{
		private string _filter = null;
		private DateTime _DateRef = DateTime.MinValue;

		public string filter { get { return _filter; } }
		public DateTime DateRef { get { return _DateRef; } }

		public InvoiceFeePriceNotFoundException( string filter, DateTime DateRef)  : base(String.Format( "Filter: {0}", filter)) 
		{
			this._filter = filter;
			this._DateRef = DateRef;
		}
		
		public InvoiceFeePriceNotFoundException( string filter, DateTime DateRef, Exception InnerException)  : base(String.Format( "Filter: {0}", filter), InnerException) 
		{
			this._filter = filter;
			this._DateRef = DateRef;
		}
	}

    public class ContractFeePriceNotFoundException : ApplicationException
    {
        private string _filter = null;
        private DateTime _DateRef = DateTime.MinValue;

        public string filter { get { return _filter; } }
        public DateTime DateRef { get { return _DateRef; } }

        public ContractFeePriceNotFoundException(string filter, DateTime DateRef)
            : base(String.Format("Filter: {0}", filter))
        {
            this._filter = filter;
            this._DateRef = DateRef;
        }

        public ContractFeePriceNotFoundException(string filter, DateTime DateRef, Exception InnerException)
            : base(String.Format("Filter: {0}", filter), InnerException)
        {
            this._filter = filter;
            this._DateRef = DateRef;
        }
    }

	public class GetPricesException : ApplicationException 
	{
		public GetPricesException() : base() {;}
		public GetPricesException( string message) : base(message) {;}
		public GetPricesException( string message, Exception InnerException) : base(message, InnerException) {;}
	}
}
