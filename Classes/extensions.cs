﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avis.sbair.Classes
{
    public static class extensions
    {
        public static decimal? xRound(decimal o, int s)
        {
            decimal res;
            res = decimal.Round(o, s);

            if (res == 0) return null;

            return res;
        }
    }
}