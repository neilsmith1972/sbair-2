﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avis.sbair.Classes.enums
{
    public class enums
    {
        public enum Status
        {
            Open = 1,
            Pending = 2,
            Invoiced = 3,
            Error = 4
        }
    }
}