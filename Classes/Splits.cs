using System;
using System.Data;
using System.Diagnostics;
using avis.sbair.Models;

namespace avis.sbair.Classes
{
	/// <summary>
	/// Summary description for Splits.
	/// </summary>
	public class Splits
	{
		private Splits _Splits = null;
		private String _countryCode = null;

		Splits AllSplits { get { return _Splits; } }

		public Splits( String CountryCode )
		{
			this._countryCode = CountryCode;
			LoadSplits();
		}

		private void LoadSplits() 
		{
			DataTable table;

			try 
			{
				String whereCl = String.Format( "CountryCode = '{0}'", _countryCode );

				_Splits = new Splits(_countryCode);

				using(Data.DB_Context db = new Data.DB_Context())
				{
					table = db.FeeSplitCollection.GetByCountryCodeAsDataTable( _countryCode );
					_Splits.Merge( table );

					table = db.OtherCostSplitCollection.GetByCountryCodeAsDataTable( _countryCode );
					_Splits.Merge( table );

					table = db.PriceSplitCollection.GetByCountryCodeAsDataTable( _countryCode );
					_Splits.Merge( table );

					table = db.VAT_SplitCollection.GetByCountryCodeAsDataTable( _countryCode );
					_Splits.Merge( table );
				}
			}
			catch ( Exception ex ) 
			{
				System.Diagnostics.Trace.WriteLine( ex );
				_Splits = null;
				throw new ApplicationException( ResourcesReader.GetResource("Splits.GetSplitsException"), ex );
					
			}
		}

		public decimal GetFeeSplitCustomerPart( int FeeSplitID )
		{
			String filter = String.Format( "FeeSplitID = {0}" , FeeSplitID );

			DataRow[] rows = _Splits.Tables["FeeSplit"].Select( filter );

			if ( rows.Length == 0 ) throw new GetSplitException(); 

			return ((Splits.FeeSplitRow)rows[0]).PCentCustomerPart;
		}

		public decimal GetOtherCostSplitCustomerPart( int OtherCostSplitID )
		{
			String filter = String.Format( "OtherCostSplitID = {0}" , OtherCostSplitID );

			DataRow[] rows = _Splits.Tables["OtherCostSplit"].Select( filter );

			if ( rows.Length == 0 ) throw new GetSplitException(); 

			return ((Splits.OtherCostSplitRow)rows[0]).PCentCustomerPart;
		}

		public PriceSplit GetPriceSplit( int PriceSplitID )
		{
			String filter = String.Format( "PriceSplitID = {0}" , PriceSplitID );

			DataRow[] rows = _Splits.Tables["PriceSplit"].Select( filter );

			if ( rows.Length == 0 ) throw new GetSplitException(); 

			DataType.PriceSplit split = new DataType.PriceSplit();
			split.PCentCustomerPartMileage = ((DataType.Splits.PriceSplitRow)rows[0]).PCentCustomerPartMileage;
			split.PCentCustomerPartTime = ((DataType.Splits.PriceSplitRow)rows[0]).PCentCustomerPartTime;
			split.MileageByUnit = ((DataType.Splits.PriceSplitRow)rows[0]).MileageByUnit;
			split.TimeByUnit = ((DataType.Splits.PriceSplitRow)rows[0]).TimeByUnit;
			split.InsurancePayAFixedAmount = ((DataType.Splits.PriceSplitRow)rows[0]).InsurancePayAFixedAmount;
			split.FixedAmountIsVATIncluded = ((DataType.Splits.PriceSplitRow)rows[0]).FixedAmountIsVATIncluded;

			return split;
		}

		public DataType.VAT_Split GetVAT_Split( int VAT_SplitID )
		{
			String filter = String.Format( "VAT_SplitID = {0}" , VAT_SplitID );

			DataRow[] rows = _Splits.Tables["VAT_Split"].Select( filter );

			if ( rows.Length == 0 ) throw new GetSplitException(); 

			DataType.VAT_Split split = new DataType.VAT_Split();

			split.PCentCustomerPart = ((DataType.Splits.VAT_SplitRow)rows[0]).PCentCustomerPart;
			split.SetAppliedTo ( ((DataType.Splits.VAT_SplitRow)rows[0]).AppliedTo );

			return split;
		}

        public System.Collections.Generic.IList<DataType.PriceSplit> GetPriceSplits()
        {            
            DataRow[] rows = _Splits.Tables["PriceSplit"].Select();

            if (rows.Length == 0) throw new GetSplitException();

            var splits = new System.Collections.Generic.List<DataType.PriceSplit>();
            foreach (var row in rows)
            {
                DataType.PriceSplit split = new DataType.PriceSplit();
                split.PCentCustomerPartMileage = ((DataType.Splits.PriceSplitRow)row).PCentCustomerPartMileage;
                split.PCentCustomerPartTime = ((DataType.Splits.PriceSplitRow)row).PCentCustomerPartTime;
                split.MileageByUnit = ((DataType.Splits.PriceSplitRow)row).MileageByUnit;
                split.TimeByUnit = ((DataType.Splits.PriceSplitRow)row).TimeByUnit;
                split.InsurancePayAFixedAmount = ((DataType.Splits.PriceSplitRow)row).InsurancePayAFixedAmount;
                split.FixedAmountIsVATIncluded = ((DataType.Splits.PriceSplitRow)row).FixedAmountIsVATIncluded;
                split.PriceSplitId = ((DataType.Splits.PriceSplitRow)row).PriceSplitID;
                splits.Add(split);
            }

            return splits;
        }

        public System.Collections.Generic.IList<DataType.VAT_Split> GetVAT_Splits()
        {
            
            DataRow[] rows = _Splits.Tables["VAT_Split"].Select();

            if (rows.Length == 0) throw new GetSplitException();

            var splits = new System.Collections.Generic.List<DataType.VAT_Split>();
            foreach (var row in rows)
            {
                DataType.VAT_Split split = new DataType.VAT_Split();

                split.PCentCustomerPart = ((DataType.Splits.VAT_SplitRow)row).PCentCustomerPart;
                split.SetAppliedTo(((DataType.Splits.VAT_SplitRow)row).AppliedTo);
                split.VatSplitId = ((DataType.Splits.VAT_SplitRow)row).VAT_SplitID;
                splits.Add(split);
            }

            return splits;
        }	
	}

	public class GetSplitException : ApplicationException 
	{
		public GetSplitException() : base() {;}
		public GetSplitException( string message) : base(message) {;}
		public GetSplitException( string message, Exception InnerException) : base(message, InnerException) {;}
	}
}
