using System;
using System.Data;
using Avis.sd.SBAIR.DataType;
using Avis.sd.SBAIR.Db;
using System.Reflection;


namespace Avis.sd.SBAIR.Business
{
	/// <summary>
	/// Summary description for CaculationDetails.
	/// </summary>
	public class CalculationDetails : Avis.sd.SBAIR.DataType.CalculationDetails, System.ICloneable
	{	
		private decimal _WizardAmt;
		
		public DataType.Insurances[] _AdditionalInsurances=null; 
		public DataType.Insurances[] AdditionalInsurances { get { return _AdditionalInsurances; } }

		public decimal WizardAmt{
			get {
				if ( this._WizardAmt==0.0m && this.TotalAmt>0.0m ) 
				{
					SetWizardAmt(); // Calculate the WizardAmt
				}
				return _WizardAmt;
			}
		}
		
		public CalculationDetails( )
		{
			this.NbDaysTotal = 0;
			this.I_NbDays = 0;
			this.I_DayPrice = 0.0m;
			this.I_DayAmt = 0.0m;
			this.C_NbDays = 0;
			this.C_DayPrice = 0.0m;
			this.C_DayAmt = 0.0m;
			this.I_NbWeeks = 0;
			this.I_WeekPrice = 0.0m;
			this.I_WeekAmt = 0.0m;
			this.C_NbWeeks = 0;
			this.C_WeekPrice = 0.0m;
			this.C_WeekAmt = 0.0m;
			this.I_NbMonth = 0;
			this.I_MonthPrice = 0.0m;
			this.I_MonthAmt = 0.0m;
			this.C_NbMonth = 0;
			this.C_MonthPrice = 0.0m;
			this.C_MonthAmt = 0.0m;
			this.Time_VATValue = 0.0m;
			this.I_NbKm = 0;
			this.I_KmPrice = 0.0m;
			this.I_KmAmt = 0.0m;
			this.C_NbKm = 0;
			this.C_KmPrice = 0.0m;
			this.C_KmAmt = 0.0m;
			this.Mileage_VATValue = 0.0m;
			this.I_RefuelingAmt = 0.0m;
			this.C_RefuelingAmt = 0.0m;
			this.Refueling_VATValue = 0.0m;
			this.I_DeliveryFeeAmt = 0.0m;
			this.C_DeliveryFeeAmt = 0.0m;
			this.DeliveryFee_VATValue = 0.0m;
			this.I_ReturnFeeAmt = 0.0m;
			this.C_ReturnFeeAmt = 0.0m;
			this.ReturnFee_VATValue = 0.0m;
			this.I_OneWayFeeAmt = 0.0m;
			this.C_OneWayFeeAmt = 0.0m;
			this.OneWayFee_VATValue = 0.0m;
			this.I_AirportFeeAmt = 0.0m;
			this.C_AirportFeeAmt = 0.0m;
			this.AirportFee_VATValue = 0.0m;
			this.I_CongestionFeeAmt = 0.0m;
			this.C_CongestionFeeAmt = 0.0m;
			this.CongestionFee_VATValue = 0.0m;
			this.I_ContractFeeAmt = 0.0m;
			this.C_ContractFeeAmt = 0.0m;
			this.ContractFee_VATValue = 0.0m;
			this.C_InvoiceFeeAmt = 0.0m;
			this.InvoiceFee_VATValue = 0.0m;
			this.OtherCost_VATValue = 0.0m;
			this.C_AmountCoveredByInsurance = 0.0m;
			this.I_AmountCoveredByInsurance = 0.0m;
			this.AmountCoveredByInsurance_VATValue = 0.0m;
			this.I_VATAmt = 0.0m;
			this.C_VATAmt = 0.0m;
			this.I_TotalAmt = 0.0m;
			this.C_TotalAmt = 0.0m;
			this.TotalAmt = 0.0m;

			this._WizardAmt = 0.0m;
			
		}

		/// <summary>
		/// Calculates all amounts for the specified SplitBilling.
		/// All the exceptions are returned as they are to be processed by the calling method.
		/// </summary>
		/// <param name="bill">The split billing.</param>
		/// <param name="prices">References data: Prices.</param>
		/// <param name="splits">References data: Splits.</param>
		public void Calculate(SplitBilling bill, Prices prices, Splits splits) 
		{
			decimal split=0.0m;
			decimal amount = 0.0m;
			//int iRest = 0;
			DateTime DtFrom = bill.CheckOutDate;
			DateTime DtTo = bill.CheckInDate;
			//int NbDaysMonth = 31;
			//int NbDaysWeek = 7;

			decimal C_NonTaxableAmt = 0.0m;
			decimal I_NonTaxableAmt = 0.0m;

			decimal MaxAmountCoveredByInsuranceIncludingVAT=-1.0m;
			decimal MaxAmountCoveredByInsuranceExcludingVAT=-1.0m;

			this.SplitBillingID = bill.SplitBillingID;

			this.NbDaysTotal = bill.CalculateNbDays();
			
			DataType.RatePrice rate = prices.GetRatePrice( bill.InsuranceRateCode, bill.CarGroup, DtFrom );

			DataType.PriceSplit priceSplit = splits.GetPriceSplit( bill.PriceSplitID );

			if ( NbDaysTotal > 0 ) 
			{
				if ( priceSplit.TimeByUnit ) 
				{
					if ( priceSplit.PCentCustomerPartTime > 0 ) 
					{
						// insurance pays a % of a determine amount of days
						bill.NbDaysCoveredByInsurance = (int)Math.Round( ( ( bill.NbDaysCoveredByInsurance * ( 100 - priceSplit.PCentCustomerPartTime ) )/ 100 ), 0 );
					}
					this.I_NbDays = ( bill.NbDaysCoveredByInsurance < NbDaysTotal ) ? bill.NbDaysCoveredByInsurance : NbDaysTotal ;
					this.C_NbDays = ( bill.NbDaysCoveredByInsurance < NbDaysTotal ) ? NbDaysTotal - bill.NbDaysCoveredByInsurance : 0 ;
				}
				else 
				{
					TimeElement elt = GetTimeElement( DtFrom.Month, DtFrom.Year, rate, NbDaysTotal );

					this.I_NbDays = elt.Days;
					this.C_NbDays = elt.Days;

					this.I_NbWeeks = elt.Weeks;
					this.C_NbWeeks = elt.Weeks;

					this.I_NbMonth = elt.Months;
					this.C_NbMonth = elt.Months;	
				}
				
				if ( this.C_NbDays > 0 || this.I_NbDays > 0 ) 
				{
					amount = rate.PrDay;
					if ( priceSplit.TimeByUnit )
					{
						this.C_DayPrice = amount;
						this.I_DayPrice = amount;
					}
					else if ( priceSplit.InsurancePayAFixedAmount ) 
					{
						// The all amount for the renter's part
						this.C_DayPrice = amount;
						this.I_DayPrice = 0.0m;
					}
					else 
					{
						this.C_DayPrice = priceSplit.PCentCustomerPartTime * amount /100;
						this.I_DayPrice = amount - this.C_DayPrice;
					}
					this.C_DayAmt = C_DayPrice * C_NbDays;
					this.I_DayAmt = I_DayPrice * I_NbDays;
				}

				if ( this.C_NbWeeks > 0 || this.I_NbWeeks > 0 ) 
				{
					amount = rate.PrWeek;
					if ( priceSplit.TimeByUnit )
					{
						this.C_WeekPrice = amount;
						this.I_WeekPrice = amount;
					}
					else if ( priceSplit.InsurancePayAFixedAmount ) 
					{
						// The all amount for the renter's part
						this.C_WeekPrice = amount;
						this.I_WeekPrice = 0.0m;
					}
					else 
					{	this.C_WeekPrice = Math.Round(priceSplit.PCentCustomerPartTime * amount /100, 2 );
						this.I_WeekPrice = amount - this.C_WeekPrice;
					}
					this.C_WeekAmt = C_WeekPrice * C_NbWeeks;
					this.I_WeekAmt = I_WeekPrice * I_NbWeeks;

				}
				if ( this.C_NbMonth > 0 || this.I_NbMonth>0 ) 
				{
					amount = rate.PrMonth;
					
					if ( priceSplit.TimeByUnit )
					{
						this.C_MonthPrice = amount;
						this.I_MonthPrice = amount;
					}
					else if ( priceSplit.InsurancePayAFixedAmount ) 
					{
						// The all amount for the renter's part
						this.C_MonthPrice = amount;
						this.I_MonthPrice = 0.0m;
					}
					else 
					{	this.C_MonthPrice = Math.Round(priceSplit.PCentCustomerPartTime * amount /100, 2 );
						this.I_MonthPrice = amount - this.C_MonthPrice;
						
					}
					this.C_MonthAmt = C_MonthPrice * C_NbMonth;
					this.I_MonthAmt = I_MonthPrice * I_NbMonth;
				}

				if ( NbDaysTotal > 0 ) 
				{
					this.C_TimeAmt = (this.C_DayAmt + this.C_WeekAmt + this.C_MonthAmt);
					this.I_TimeAmt = (this.I_DayAmt + this.I_WeekAmt + this.I_MonthAmt);
					this.Time_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.Time ), DtFrom );
					if ( this.Time_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_TimeAmt;
						this.I_TaxableAmt += this.I_TimeAmt;

						this.C_VATAmt += Math.Round( this.C_TimeAmt * this.Time_VATValue / 100, 2 );
						this.I_VATAmt += Math.Round( this.I_TimeAmt * this.Time_VATValue / 100, 2 );
					}
					else 
					{
						C_NonTaxableAmt += this.C_TimeAmt;
						I_NonTaxableAmt += this.I_TimeAmt;
					}
				}

				if ( bill.KmDriven > 0 ) 
				{
					amount = rate.PrKm;

					if ( priceSplit.MileageByUnit ) 
					{
						// The insurance company pay a fixed amount of km
						if ( priceSplit.PCentCustomerPartMileage > 0 ) 
						{
							// insurance pays a % of a determine amount of km
							bill.NbKmCoveredByInsurance = (int)Math.Round( ( ( bill.NbKmCoveredByInsurance * ( 100 - priceSplit.PCentCustomerPartMileage ) )/ 100 ), 0 );
						}
						this.C_KmPrice = amount;
						this.I_KmPrice = amount;
						this.C_NbKm = ( bill.KmDriven > bill.NbKmCoveredByInsurance ) ? bill.KmDriven - bill.NbKmCoveredByInsurance : 0;
						this.I_NbKm = ( bill.KmDriven > bill.NbKmCoveredByInsurance ) ? bill.NbKmCoveredByInsurance : bill.KmDriven;
					}
					else if ( priceSplit.InsurancePayAFixedAmount ) 
					{
						// The insurance company pay a fixed amount of the total bel�p
						// The all amount for the renter's part
						this.C_NbKm = bill.KmDriven;
						this.I_NbKm = bill.KmDriven;
						this.C_KmPrice = amount;
						this.I_KmPrice = 0.0m;
					}
					else 
					{
						// The usual way: The customer and the insurance company pay a percentage of the total sum.
						this.C_NbKm = bill.KmDriven;
						this.I_NbKm = bill.KmDriven;
						this.C_KmPrice = Math.Round( priceSplit.PCentCustomerPartMileage * amount /100, 2 );
						this.I_KmPrice = amount - this.C_KmPrice;
                        
					}

					this.C_KmAmt = C_KmPrice * C_NbKm;
					this.I_KmAmt = I_KmPrice * I_NbKm;
		
					this.Mileage_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.Mileage ), DtFrom );

					if ( this.Mileage_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_KmAmt;
						this.I_TaxableAmt += this.I_KmAmt;

						this.C_VATAmt += Math.Round( this.C_KmAmt * this.Mileage_VATValue / 100, 2 );
						this.I_VATAmt += Math.Round( this.I_KmAmt * this.Mileage_VATValue / 100, 2 );
					}
					else 
					{
						C_NonTaxableAmt += this.C_KmAmt;
						I_NonTaxableAmt += this.I_KmAmt;
					}
				}

				if ( bill.RefuelingSplitID > 0 )
				{
					decimal RefuelingAmount = bill.RefuelingAmount;
					split = splits.GetOtherCostSplitCustomerPart( bill.RefuelingSplitID );

					if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
					{
						throw new MaxPriceCoveredByInsuranceException();
					}

					this.C_RefuelingAmt = Math.Round( split * RefuelingAmount / 100, 2 );
					this.I_RefuelingAmt = RefuelingAmount - this.C_RefuelingAmt;

					this.Refueling_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.Refueling ), DtFrom );

					if ( this.Refueling_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_RefuelingAmt;
						this.I_TaxableAmt += this.I_RefuelingAmt;

						this.C_VATAmt += Math.Round( this.C_RefuelingAmt * this.Refueling_VATValue / 100, 2 );
						this.I_VATAmt += Math.Round( this.I_RefuelingAmt * this.Refueling_VATValue / 100, 2 );
					}
					else 
					{
						C_NonTaxableAmt += this.C_RefuelingAmt;
						I_NonTaxableAmt += this.I_RefuelingAmt;
					}
				}

				if ( bill.DeliveryFeeSplitID > 0 )
				{
					decimal DeliveryFeeAmount = bill.DeliveryFeeAmount;
					split = splits.GetOtherCostSplitCustomerPart( bill.DeliveryFeeSplitID );

					if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
					{
						throw new MaxPriceCoveredByInsuranceException();
					}

					this.C_DeliveryFeeAmt = Math.Round( split * DeliveryFeeAmount / 100, 2 );
					this.I_DeliveryFeeAmt = DeliveryFeeAmount - this.C_DeliveryFeeAmt;

					this.DeliveryFee_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.DeliveryFee ), DtFrom );

					if ( this.DeliveryFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_DeliveryFeeAmt;
						this.I_TaxableAmt += this.I_DeliveryFeeAmt;

						this.C_VATAmt += Math.Round( this.C_DeliveryFeeAmt * this.DeliveryFee_VATValue / 100, 2 );
						this.I_VATAmt += Math.Round( this.I_DeliveryFeeAmt * this.DeliveryFee_VATValue / 100, 2 );
					}
					else 
					{
						C_NonTaxableAmt += this.C_DeliveryFeeAmt;
						I_NonTaxableAmt += this.I_DeliveryFeeAmt;
					}
				}

				if ( bill.ReturnFeeSplitID > 0 )
				{
					decimal ReturnFeeAmount = bill.ReturnFeeAmount;
					split = splits.GetOtherCostSplitCustomerPart( bill.ReturnFeeSplitID );

					if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
					{
						throw new MaxPriceCoveredByInsuranceException();
					}

					this.C_ReturnFeeAmt = Math.Round( split * ReturnFeeAmount / 100 );
					this.I_ReturnFeeAmt = ReturnFeeAmount - this.C_ReturnFeeAmt;

					this.ReturnFee_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.ReturnFee ), DtFrom );

					if ( this.ReturnFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_ReturnFeeAmt;
						this.I_TaxableAmt += this.I_ReturnFeeAmt;

						this.C_VATAmt += Math.Round( this.C_ReturnFeeAmt * this.ReturnFee_VATValue / 100, 2 );
						this.I_VATAmt += Math.Round( this.I_ReturnFeeAmt * this.ReturnFee_VATValue / 100, 2 );
					}
					else
					{
						C_NonTaxableAmt += this.C_ReturnFeeAmt;
						I_NonTaxableAmt += this.I_ReturnFeeAmt;
					}
				}

				if ( bill.AirportFeeSplitID > 0 ) 
				{
					decimal AirportFeeAmount = prices.GetFeePrice( "AIRPORT", bill.CarGroup, DtFrom );
					split = splits.GetFeeSplitCustomerPart( bill.AirportFeeSplitID );

					if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
					{
						throw new MaxPriceCoveredByInsuranceException();
					}

					this.C_AirportFeeAmt = Math.Round( split * AirportFeeAmount / 100, 2 );
					this.I_AirportFeeAmt = AirportFeeAmount - this.C_AirportFeeAmt;

					this.AirportFee_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.AirportFee ), DtFrom );

					if ( this.AirportFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_AirportFeeAmt;
						this.I_TaxableAmt += this.I_AirportFeeAmt;

						this.C_VATAmt += Math.Round( this.C_AirportFeeAmt * this.AirportFee_VATValue / 100, 2 );
						this.I_VATAmt += Math.Round( this.I_AirportFeeAmt * this.AirportFee_VATValue / 100, 2 );
					}
					else
					{
						C_NonTaxableAmt += this.C_AirportFeeAmt;
						I_NonTaxableAmt += this.I_AirportFeeAmt;
					}
				}
			
				if ( bill.OneWayFeeSplitID > 0 ) 
				{
					decimal OneWayFeeAmount=0.0m;
					if ( bill.OneWayFeeAmount > 0 ) 
					{
						OneWayFeeAmount = bill.OneWayFeeAmount;
					}
					else 
					{
						OneWayFeeAmount = prices.GetFeePrice( "ONEWAY", bill.CarGroup, DtFrom );
					}
					split = splits.GetFeeSplitCustomerPart( bill.OneWayFeeSplitID );

					if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
					{
						throw new MaxPriceCoveredByInsuranceException();
					}

					this.C_OneWayFeeAmt = Math.Round( split * OneWayFeeAmount / 100, 2 );
					this.I_OneWayFeeAmt = OneWayFeeAmount - this.C_OneWayFeeAmt;

					this.OneWayFee_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.OneWayFee ), DtFrom );

					if ( this.OneWayFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_OneWayFeeAmt;
						this.I_TaxableAmt += this.I_OneWayFeeAmt;

						this.C_VATAmt += Math.Round( this.C_OneWayFeeAmt * this.OneWayFee_VATValue / 100, 2 );
						this.I_VATAmt += Math.Round( this.I_OneWayFeeAmt * this.OneWayFee_VATValue / 100, 2 );
					}
					else
					{
						C_NonTaxableAmt += this.C_OneWayFeeAmt;
						I_NonTaxableAmt += this.I_OneWayFeeAmt;
					}
				}

				if ( bill.CongestionFeeSplitID > 0 )
				{
					decimal CongestionFeeAmount = bill.CongestionFeeAmount;
					split = splits.GetOtherCostSplitCustomerPart( bill.CongestionFeeSplitID );

					if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
					{
						throw new MaxPriceCoveredByInsuranceException();
					}

					this.C_CongestionFeeAmt = Math.Round( split * CongestionFeeAmount / 100 );
					this.I_CongestionFeeAmt = CongestionFeeAmount - this.C_CongestionFeeAmt;

					this.CongestionFee_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.CongestionFee ), DtFrom );

					if ( this.CongestionFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_CongestionFeeAmt;
						this.I_TaxableAmt += this.I_CongestionFeeAmt;

						this.C_VATAmt += Math.Round( this.C_CongestionFeeAmt * this.CongestionFee_VATValue / 100, 2 );
						this.I_VATAmt += Math.Round( this.I_CongestionFeeAmt * this.CongestionFee_VATValue / 100, 2 );
					}
					else
					{
						C_NonTaxableAmt += this.C_CongestionFeeAmt;
						I_NonTaxableAmt += this.I_CongestionFeeAmt;
					}
				}

				if ( bill.ContractFeeSplitID > 0 )
				{
					decimal ContractFeeAmount = bill.ContractFeeAmount;
					split = splits.GetOtherCostSplitCustomerPart( bill.ContractFeeSplitID );

					if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
					{
						throw new MaxPriceCoveredByInsuranceException();
					}

					this.C_ContractFeeAmt = Math.Round( split * ContractFeeAmount / 100 );
					this.I_ContractFeeAmt = ContractFeeAmount - this.C_ContractFeeAmt;

					this.ContractFee_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.ContractFee ), DtFrom );

					if ( this.ContractFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_ContractFeeAmt;
						this.I_TaxableAmt += this.I_ContractFeeAmt;

						this.C_VATAmt += Math.Round( this.C_ContractFeeAmt * this.ContractFee_VATValue / 100, 2 );
						this.I_VATAmt += Math.Round( this.I_ContractFeeAmt * this.ContractFee_VATValue / 100, 2 );
					}
					else
					{
						C_NonTaxableAmt += this.C_ContractFeeAmt;
						I_NonTaxableAmt += this.I_ContractFeeAmt;
					}
				}

				if ( bill.MiscCostAmount > 0 ) 
				{
					this.OtherCost_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.MiscCost ), DtFrom );

					if ( this.OtherCost_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += bill.MiscCostAmount;
						this.C_VATAmt += Math.Round( bill.MiscCostAmount * this.OtherCost_VATValue / 100, 2 );
					}
					else
					{
						C_NonTaxableAmt += bill.MiscCostAmount;
					}
				}

				if ( bill.GetAdditionalInsurancesCount() > 0 ) 
				{
					this._AdditionalInsurances = new DataType.Insurances[bill.GetAdditionalInsurancesCount()];
					int i=0;
					foreach ( DataType.Insurance insurance in bill.GetAdditionalInsurances() ) 
					{
						if ( priceSplit.InsurancePayAFixedAmount && insurance.ChargedTo == DataType.Insurance.ChargedToEnum.Insurance ) 
						{
							throw new MaxPriceCoveredByInsuranceException();
						}
						this._AdditionalInsurances[i] = new Avis.sd.SBAIR.DataType.Insurances( this.SplitBillingID, insurance.InsuranceCode );
						this._AdditionalInsurances[i].VATValue = prices.GetVAT_Insurance( insurance.InsuranceCode, DtFrom );
						this._AdditionalInsurances[i].Price = prices.GetInsurancePrice( insurance.InsuranceCode, bill.CarGroup, DtFrom ); 
						this._AdditionalInsurances[i].Amount = this._AdditionalInsurances[i].Price * this.NbDaysTotal;
						
						if (insurance.ChargedTo == DataType.Insurance.ChargedToEnum.Customer ) 
						{
							this._AdditionalInsurances[i].ChargedTo = "C";
							if ( this._AdditionalInsurances[i].VATValue > 0.0m ) 
							{
								this.C_TaxableAmt += this._AdditionalInsurances[i].Amount;
								this.C_VATAmt += ( this._AdditionalInsurances[i].Amount * this._AdditionalInsurances[i].VATValue / 100 );
							}
							else
							{
								C_NonTaxableAmt += this._AdditionalInsurances[i].Amount;
							}
						}
						else 
						{
							this._AdditionalInsurances[i].ChargedTo = "I";
							if ( this._AdditionalInsurances[i].VATValue > 0.0m ) 
							{
								this.I_TaxableAmt += this._AdditionalInsurances[i].Amount;
								this.I_VATAmt += ( this._AdditionalInsurances[i].Amount * this._AdditionalInsurances[i].VATValue / 100 );
							}
							else
							{
								I_NonTaxableAmt += this._AdditionalInsurances[i].Amount;
							}
						}
						i++;
					}
				}

				if ( !bill.RenterPaymentMethodHaveInvoiceFee )
				{
					decimal invoiceFeeVATAmount=0.0m;
					this.C_InvoiceFeeAmt = prices.GetInvoiceFeePrice( DtFrom );
					if ( this.C_InvoiceFeeAmt > 0.0m ) 
					{
						this.InvoiceFee_VATValue = prices.GetVATvalue_Field( DataType.CalculationFieldList.Get( DataType.CalculationFields.InvoiceFee ), DtFrom );
					
						if ( this.InvoiceFee_VATValue > 0.0m ) 
						{
							this.C_TaxableAmt += this.C_InvoiceFeeAmt;
							invoiceFeeVATAmount = Math.Round( this.C_InvoiceFeeAmt * this.InvoiceFee_VATValue / 100, 2 );
							this.C_VATAmt += invoiceFeeVATAmount;
						}
						else
						{
							C_NonTaxableAmt += this.C_InvoiceFeeAmt;
						}
					}
				}

				if ( priceSplit.InsurancePayAFixedAmount) 
				{
					this.AmountCoveredByInsurance_VATValue = this.Time_VATValue;

					if (!priceSplit.FixedAmountIsVATIncluded) 
					{
						// Calculate max amount covered with VAT
						MaxAmountCoveredByInsuranceExcludingVAT = bill.AmountCoveredByInsurance;
						MaxAmountCoveredByInsuranceIncludingVAT = Math.Round( MaxAmountCoveredByInsuranceExcludingVAT + ( MaxAmountCoveredByInsuranceExcludingVAT * this.AmountCoveredByInsurance_VATValue / 100 ), 2 );
					}
					else 
					{
						// Calculate max amount covered without VAT
						MaxAmountCoveredByInsuranceIncludingVAT = bill.AmountCoveredByInsurance;
						MaxAmountCoveredByInsuranceExcludingVAT = Math.Round( (MaxAmountCoveredByInsuranceIncludingVAT *100) / ( 100 + this.AmountCoveredByInsurance_VATValue ) , 2 );
					}
				
					// The insurance PayAFixeAmount excluding VAT
					if (  this.C_TaxableAmt > MaxAmountCoveredByInsuranceExcludingVAT ) 
					{
						this.C_TaxableAmt -= MaxAmountCoveredByInsuranceExcludingVAT;
						this.I_TaxableAmt = MaxAmountCoveredByInsuranceExcludingVAT;
						this.C_AmountCoveredByInsurance = MaxAmountCoveredByInsuranceExcludingVAT * (-1);
						this.I_AmountCoveredByInsurance = MaxAmountCoveredByInsuranceExcludingVAT;
					}
					else 
					{
						this.I_TaxableAmt = C_TaxableAmt;
						this.C_TaxableAmt = 0.0m;

						this.C_AmountCoveredByInsurance = I_TaxableAmt * (-1);
						this.I_AmountCoveredByInsurance = I_TaxableAmt;
					}
					// TODO: Add the functionality for the VAT value

					// Re - calculate the VAT Amount base on the VAT for Time and Mileage
					this.C_VATAmt = Math.Round( this.C_TaxableAmt * this.AmountCoveredByInsurance_VATValue / 100, 2 );
					this.I_VATAmt = Math.Round( this.I_TaxableAmt * this.AmountCoveredByInsurance_VATValue / 100, 2 );

				}
				
				/*
				VAT Split
				*/
				if ( ( this.C_VATAmt + this.I_VATAmt ) > 0 ) 
				{
					DataType.VAT_Split vat_Split = splits.GetVAT_Split( bill.VAT_SplitID );

					split = vat_Split.PCentCustomerPart;

					if ( split >= 0 ) 
					{
						decimal TotalVATAmt = this.C_VATAmt + this.I_VATAmt;

						if ( vat_Split.AppliedTo == DataType.VAT_Split.TargetVAT.Insurance ) 
						{
							// The split is about the insurance company's VAT
							// The customer (renter) pays his own VAT + a % of the VAT of the insurance company
							this.C_SplittedVATAmt = this.C_VATAmt + Math.Round( split * this.I_VATAmt / 100, 2 );
							this.I_SplittedVATAmt = TotalVATAmt - this.C_SplittedVATAmt;
						}
						else if ( vat_Split.AppliedTo == DataType.VAT_Split.TargetVAT.Customer ) 
						{
							// The split is about the customer's VAT
							// The insurance company pays his own VAT + a % of the VAT of the customer (renter)
							this.I_SplittedVATAmt = this.I_VATAmt + Math.Round( split * this.C_VATAmt / 100, 2 );
							this.C_SplittedVATAmt = TotalVATAmt - this.I_SplittedVATAmt;
						}
						else //( vat_Split.AppliedTo == DataType.VAT_Split.TargetVAT.Total ) 
						{
							// The split is about the total of all VAT
							this.C_SplittedVATAmt = Math.Round( split * TotalVATAmt / 100, 2 );
							this.I_SplittedVATAmt = TotalVATAmt - this.C_SplittedVATAmt;
						}
					}
					else 
					{
						// if split == -1 : No split
						this.C_SplittedVATAmt = this.C_VATAmt;
						this.I_SplittedVATAmt = this.I_VATAmt;
					}
				}

				this.C_TotalAmt = this.C_TaxableAmt + this.C_SplittedVATAmt + C_NonTaxableAmt;
				this.I_TotalAmt = this.I_TaxableAmt + this.I_SplittedVATAmt + I_NonTaxableAmt;
				
				/*
				//if ( priceSplit.InsurancePayAFixedAmount && priceSplit.FixedAmountIsVATIncluded ) 
				if ( priceSplit.InsurancePayAFixedAmount ) 
				{
					decimal insuranceAmount = bill.AmountCoveredByInsurance;

					if (!priceSplit.FixedAmountIsVATIncluded) 
					{
						this.AmountCoveredByInsurance_VATValue = this.Time_VATValue;
						insuranceAmount += Math.Round( insuranceAmount * this.AmountCoveredByInsurance_VATValue / 100, 2 );
					}

					// The insurance PayAFixeAmount including VAT
					if (  this.C_TotalAmt > insuranceAmount ) 
					{
						this.C_TotalAmt -= insuranceAmount;
						this.I_TotalAmt = insuranceAmount;
						this.C_AmountCoveredByInsurance = insuranceAmount * (-1);
						this.I_AmountCoveredByInsurance = insuranceAmount;
					}
					else 
					{
						this.I_TotalAmt = C_TotalAmt;
						this.C_TotalAmt = 0.0m;

						this.C_AmountCoveredByInsurance = I_TotalAmt * (-1);
						this.I_AmountCoveredByInsurance = I_TotalAmt;
					}
				}
				*/
				this.C_TotalAmt -=  bill.AllreadyPayedByCustomerAmount;

				this.TotalAmt = this.C_TotalAmt + this.I_TotalAmt;

				SetWizardAmt();
			}	
		}

		private TimeElement GetTimeElement( int CurrMonth, int CurrYear, DataType.RatePrice rate, int NbDaysTotal )
		{
		
			if ( CurrMonth < 1 || CurrMonth > 12 ) throw new Exception( "GetTimeElement: Invalid parameter CurrMonth" );
			if ( CurrYear < 1900 || CurrMonth > 3000 ) throw new Exception( "GetTimeElement: Invalid parameter CurrYear" );
			
			int DaysPerMonth = rate.NbDaysMonth;
			int DaysPerWeek = rate.NbDaysWeek;

			if ( DaysPerMonth < 0 || DaysPerMonth > 31 ) throw new Exception( "GetTimeElement: Invalid parameter DaysPerMonth" );
			if ( DaysPerWeek < 1 || DaysPerWeek > 7 ) throw new Exception( "GetTimeElement: Invalid parameter DaysPerWeek" );
		
			// if DayPerMonth >= 30 the price is just calculated per days.
			// No free of charge day.
			if ( DaysPerMonth >= 30 ) return new TimeElement(  NbDaysTotal,  0, 0, 0 );
		
			int Days=NbDaysTotal, Weeks=0, Months=0;
			int CDaysPerMonth=0;			// Number of days per month compensated by the number of free days of the week price
			int FreeDays=0;                 // Number of days free of charge because of the price per week/month
		
			int FreeDaysPerWeek = 7 - DaysPerWeek;
		
			CDaysPerMonth = DaysPerMonth;
		
			// Compensation for free days the 3 first weeks in the month
			CDaysPerMonth += ( 3 * FreeDaysPerWeek );
		
			System.Globalization.GregorianCalendar calendar = new System.Globalization.GregorianCalendar();
      			
			// Calculate number of months
			while ( Days > CDaysPerMonth )
			{
				Months++; // Charge one month
			
				if ( DaysPerMonth >= 30 )
				{
					// Price by month is not the best price
					Days -= calendar.GetDaysInMonth( CurrYear, CurrMonth );
				}
				else
				{
					// Price by month is the best price
					Days -= 31;         // Allways 31 days for the month
					FreeDays += ( 31 - DaysPerMonth );
				}
				if ( Days < 0 ) FreeDays += Days;
			
				// Move to next month
				CurrMonth = ( CurrMonth < 12 ) ? CurrMonth + 1 : 1;
			}
			if ( Days < 0 ) Days = 0;
		
			// Calculate number of weeks
			while ( Days >= 7 )
			{
				Weeks ++;
				Days -= 7;
				FreeDays += ( 7 - DaysPerWeek );
			}

			//PHB 2006.08.09: Get the best price for the rest of days.
			if ( Days * rate.PrDay > rate.PrWeek ) 
			{
				Weeks++;
				Days=0;
			}

			return new TimeElement(  Days,  Weeks, Months, FreeDays );
		}

		public object Clone()
		{
			try
			{
				object sourceObject = this;
				Type sourceType = sourceObject.GetType();
				PropertyInfo[] sourceProperties = sourceType.GetProperties (BindingFlags.Instance | BindingFlags.Public);
				
				Business.CalculationDetails ClonedCalc = new CalculationDetails();

				for(int i = 0; i < sourceProperties.Length; i++)
				{
					Type propertyType = sourceProperties[i].PropertyType;
					String sourcePropertyName = sourceProperties[i].Name;
					String isNullPropertyName = String.Format("Is{0}Null", sourcePropertyName);
#if SHOW_DEBUG_MSG
					System.Diagnostics.Debug.WriteLine(String.Format("Name            : {0}", sourceProperties[i].Name));
					System.Diagnostics.Debug.WriteLine(String.Format("Declaring Type  : {0}", sourceProperties[i].DeclaringType));
					System.Diagnostics.Debug.WriteLine(String.Format("MemberType      : {0}", sourceProperties[i].MemberType));
#endif					
					object Value = null;
					try 
					{
						if ( sourcePropertyName == "AdditionalInsurances" ) 
						{
							ClonedCalc._AdditionalInsurances = new DataType.Insurances[this._AdditionalInsurances.Length];
							this._AdditionalInsurances.CopyTo(ClonedCalc._AdditionalInsurances, 0);
						}
						else 
						{
							Value = sourceProperties[i].GetValue( sourceObject, null );
#if SHOW_DEBUG_MSG
							System.Diagnostics.Debug.WriteLine(String.Format("Value           : {0}", Value));
#endif
							sourceProperties[i].SetValue( ClonedCalc, Value, null );
						}
					}
					catch
					{
						// One exception is generated when we try to get a value from System.DBNull
						sourceProperties[i].SetValue( ClonedCalc, null, null );
					}
				}
				return ClonedCalc;
					
			}
			catch ( Exception Ex )
			{
				throw new ApplicationException("Exception in MapSplitBillingObjects: "+ Ex.Message, Ex);
			}
		}

		private void SetWizardAmt() 
		{
			
			decimal invoiceFeeVATAmount=0.0m;
			decimal calculatedInvoiceFeeVATIncluded=0.0m;
			
			if ( this.C_InvoiceFeeAmt > 0.0m ) 
			{
				if ( this.InvoiceFee_VATValue > 0.0m ) 
				{
					invoiceFeeVATAmount = Math.Round( this.C_InvoiceFeeAmt * this.InvoiceFee_VATValue / 100, 2 );
				}
				calculatedInvoiceFeeVATIncluded = this.C_InvoiceFeeAmt + invoiceFeeVATAmount;
			}
			this._WizardAmt = this.TotalAmt - calculatedInvoiceFeeVATIncluded;
		}
	}

	public class TimeElement 
	{
		private int _days=0;
		private int _weeks=0;
		private int _months=0;
		private int _freeDays=0;
	
		public TimeElement( int Days, int Weeks, int Months, int FreeDays )
		{
			_days=Days;
			_weeks=Weeks;
			_months=Months;
			_freeDays=FreeDays;
		}
	
		public int Days { set { _days=value; } get { return _days; } }
		public int Weeks { set { _weeks=value; } get { return _weeks; } }
		public int Months { set { _months=value; } get { return _months; } }
		public int FreeDays { set { _freeDays=value; } get { return _freeDays; } }
	}

	public class MaxPriceCoveredByInsuranceException : ApplicationException 
	{
		public MaxPriceCoveredByInsuranceException() : base() {;}
		public MaxPriceCoveredByInsuranceException( string message) : base(message) {;}
		public MaxPriceCoveredByInsuranceException( string message, Exception InnerException) : base(message, InnerException) {;}
	}
}
