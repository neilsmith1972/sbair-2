using System;
using System.Data;
using System.Linq;
using System.Reflection;
using avis.sbair.Entities;

namespace avis.sbair.Classes
{
	/// <summary>
	/// Summary description for CaculationDetails.
	/// </summary>
	public class Calculations : CalculationDetail //, System.ICloneable
	{
       
		private decimal _WizardAmt;
        private int SplitBillingID;
        public string errMsg;
        private SBAIR3Context db = new SBAIR3Context();


        public static int CalculateNbDays (DateTime dtFrom, DateTime dtTo)
        {
            //If renting time is n days and 59 (grace period) or less minutes, billed days is round down to n days.
            //If more it is rounded up to n+1 days
            //This grace period is changed by 1/1-2010 by mrj to 29 minuts!
            TimeSpan t = dtTo - dtFrom;
            //return t.Days + (( (t.TotalHours%24) >= 1.0d ) ? 1 : 0);
            return t.Days + (((t.TotalHours % 24) >= 0.5d) ? 1 : 0);

        }

		public decimal WizardAmt{
			get {
				if ( this._WizardAmt==0.0m && this.TotalAmt>0.0m ) 
				{
					SetWizardAmt(); // Calculate the WizardAmt
				}
				return _WizardAmt;
			}
		}
		
		public Calculations( )
		{
			this.NbDaysTotal = 0;
			this.I_NbDays = 0;
			this.I_DayPrice = 0.0m;
			this.I_DayAmt = 0.0m;
			this.C_NbDays = 0;
			this.C_DayPrice = 0.0m;
			this.C_DayAmt = 0.0m;
			this.I_NbWeeks = 0;
			this.I_WeekPrice = 0.0m;
			this.I_WeekAmt = 0.0m;
			this.C_NbWeeks = 0;
			this.C_WeekPrice = 0.0m;
			this.C_WeekAmt = 0.0m;
			this.I_NbMonth = 0;
			this.I_MonthPrice = 0.0m;
			this.I_MonthAmt = 0.0m;
			this.C_NbMonth = 0;
			this.C_MonthPrice = 0.0m;
			this.C_MonthAmt = 0.0m;
			this.Time_VATValue = 0.0m;
			this.I_NbKm = 0;
			this.I_KmPrice = 0.0m;
			this.I_KmAmt = 0.0m;
			this.C_NbKm = 0;
			this.C_KmPrice = 0.0m;
			this.C_KmAmt = 0.0m;
			this.Mileage_VATValue = 0.0m;
			this.I_RefuelingAmt = 0.0m;
			this.C_RefuelingAmt = 0.0m;
			this.Refueling_VATValue = 0.0m;
			this.I_DeliveryFeeAmt = 0.0m;
			this.C_DeliveryFeeAmt = 0.0m;
			this.DeliveryFee_VATValue = 0.0m;
			this.I_ReturnFeeAmt = 0.0m;
			this.C_ReturnFeeAmt = 0.0m;
			this.ReturnFee_VATValue = 0.0m;
			this.I_OneWayFeeAmt = 0.0m;
			this.C_OneWayFeeAmt = 0.0m;
			this.OneWayFee_VATValue = 0.0m;
			this.I_AirportFeeAmt = 0.0m;
			this.C_AirportFeeAmt = 0.0m;
			this.AirportFee_VATValue = 0.0m;
			this.I_VRFAmt = 0.0m;
			this.C_VRFAmt = 0.0m;
			this.VRF_VATValue = 0.0m;

            this.I_OthersAmt = 0.0m;
            this.C_OthersAmt = 0.0m;
            this.Others_VATValue = 0.0m;

			this.C_InvoiceFeeAmt = 0.0m;
			this.InvoiceFee_VATValue = 0.0m;
			this.OtherCost_VATValue = 0.0m;
			this.C_AmountCoveredByInsurance = 0.0m;
			this.I_AmountCoveredByInsurance = 0.0m;
			this.AmountCoveredByInsurance_VATValue = 0.0m;
			this.I_VATAmt = 0.0m;
			this.C_VATAmt = 0.0m;
			this.I_TotalAmt = 0.0m;
			this.C_TotalAmt = 0.0m;
			this.TotalAmt = 0.0m;
            this.C_NonTaxableAmt = 0.0m;
            this.I_NonTaxableAmt = 0.0m;

			this._WizardAmt = 0.0m;
			
		}

		/// <summary>
		/// Calculates all amounts for the specified SplitBilling.
		/// All the exceptions are returned as they are to be processed by the calling method.
		/// </summary>
		/// <param name="bill">The split billing.</param>
		/// <param name="prices">References data: Prices.</param>
		/// <param name="splits">References data: Splits.</param>
		public void Calculate(SplitBilling bill) 
		{
			decimal split=0.0m;
			decimal amount = 0.0m;
			//int iRest = 0;
			DateTime DtFrom = (DateTime) bill.RentalCheckoutDateTime;
			DateTime DtTo = (DateTime) bill.RentalCheckinDateTime;
			//int NbDaysMonth = 31;
			//int NbDaysWeek = 7;

			decimal MaxAmountCoveredByInsuranceIncludingVAT=-1.0m;
			decimal MaxAmountCoveredByInsuranceExcludingVAT=-1.0m;

			//this.SplitBillingID = bill.ID;

            decimal OverrideDaysAmount = bill.OverrideDaysAmount != null ? (decimal)bill.OverrideDaysAmount : 0;
		    decimal OverrideKMAmount = bill.OverrideKMAmount != null ? (decimal) bill.OverrideKMAmount : 0;

            this.NbDaysTotal = CalculateNbDays((DateTime) bill.RentalCheckoutDateTime, (DateTime) bill.RentalCheckinDateTime);

            //RateVersion rVersion;
            //RatePrice rPrice;
            //RateCode rCode;

            //try
            //{
            //    rVersion = db.RateVersions.Where(r => r.RateCode.Code == bill.InsuranceCompany.RateCode).First();
            //    rPrice = db.RatePrices.Where(r => r.RateVersion.RateVersionID == rVersion.RateVersionID && r.CarGroup.ID == bill.CarGroups.ID).First(); //prices.GetRatePrice( bill.InsuranceRateCode, bill.CarGroups.ID, DtFrom );
            //    rCode = db.RateCodes.Where(r => r.ID == rVersion.RateCode.ID).First();
            //}
            //catch (Exception ex)
            //{
            //    this.errMsg = "There is no price data for this insurance company and cargroup. Please select a different criteria.";
            //    return;
            //}
  

			PriceSplit priceSplit = db.PriceSplits.Where(p=>p.ID == bill.PriceSplit).First();

			if ( NbDaysTotal > 0 ) 
			{
				if ( priceSplit.TimeByUnit ) 
				{
					if ( priceSplit.PCentCustomerPartTime > 0 ) 
					{
						// insurance pays a % of a determine amount of days
                        decimal temp = ((int)bill.DaysCoveredByInsurance) * ((100 - priceSplit.PCentCustomerPartTime)/ 100);
						bill.DaysCoveredByInsurance = (int) Math.Round(temp, 0);
					}
					this.I_NbDays = ((int) bill.DaysCoveredByInsurance < (int)NbDaysTotal ) ? (int)bill.DaysCoveredByInsurance : NbDaysTotal ;
					this.C_NbDays = ((int) bill.DaysCoveredByInsurance < (int)NbDaysTotal ) ?(int) NbDaysTotal - (int)bill.DaysCoveredByInsurance : 0 ;
				}
				else 
				{
					TimeElement elt = GetTimeElement( DtFrom.Month, DtFrom.Year, NbDaysTotal, bill );

					this.I_NbDays = elt.Days;
					this.C_NbDays = elt.Days;

					this.I_NbWeeks = elt.Weeks;
					this.C_NbWeeks = elt.Weeks;

					this.I_NbMonth = elt.Months;
					this.C_NbMonth = elt.Months;	
				}
				
				if ( this.C_NbDays > 0 || this.I_NbDays > 0 ) 
				{

                    if (OverrideDaysAmount > 0)
                    {
                        bill.RentalDailyRateAmount = OverrideDaysAmount/NbDaysTotal;
                    }

                    //if (bill.TMDiscount > 0)
                    //{
                    //    bill.RentalDailyRateAmount = bill.RentalDailyRateAmount - (bill.TMDiscount / NbDaysTotal);
                    //}

					amount = (decimal) (bill.RentalDailyRateAmount ?? 0);
					if ( priceSplit.TimeByUnit )
					{
						this.C_DayPrice = amount;
						this.I_DayPrice = amount;
					}
					else if ( priceSplit.InsurancePayAFixedAmount ) 
					{
						// The all amount for the renter's part
						this.C_DayPrice = amount;
						this.I_DayPrice = 0.0m;
					}
					else 
					{
						this.C_DayPrice = priceSplit.PCentCustomerPartTime * amount /100;
						this.I_DayPrice = amount - this.C_DayPrice;
					}
					this.C_DayAmt = C_DayPrice * C_NbDays;
					this.I_DayAmt = I_DayPrice * I_NbDays;
				}

				if ( this.C_NbWeeks > 0 || this.I_NbWeeks > 0 ) 
				{
					amount =(decimal) bill.RentalWeeklyRateAmount;
					if ( priceSplit.TimeByUnit )
					{
						this.C_WeekPrice = amount;
						this.I_WeekPrice = amount;
					}
					else if ( priceSplit.InsurancePayAFixedAmount ) 
					{
						// The all amount for the renter's part
						this.C_WeekPrice = amount;
						this.I_WeekPrice = 0.0m;
					}
					else 
					{	this.C_WeekPrice = priceSplit.PCentCustomerPartTime * amount /100;
						this.I_WeekPrice = amount - this.C_WeekPrice;
					}
					this.C_WeekAmt = C_WeekPrice * C_NbWeeks;
					this.I_WeekAmt = I_WeekPrice * I_NbWeeks;

				}
				if ( this.C_NbMonth > 0 || this.I_NbMonth>0 ) 
				{
					amount =(decimal) bill.RentalMonthlyRateAmount;
					
					if ( priceSplit.TimeByUnit )
					{
						this.C_MonthPrice = amount;
						this.I_MonthPrice = amount;
					}
					else if ( priceSplit.InsurancePayAFixedAmount ) 
					{
						// The all amount for the renter's part
						this.C_MonthPrice = amount;
						this.I_MonthPrice = 0.0m;
					}
					else 
					{	this.C_MonthPrice = priceSplit.PCentCustomerPartTime * amount /100;
						this.I_MonthPrice = amount - this.C_MonthPrice;
						
					}
					this.C_MonthAmt = C_MonthPrice * C_NbMonth;
					this.I_MonthAmt = I_MonthPrice * I_NbMonth;
				}

				if ( NbDaysTotal > 0 ) 
				{
					this.C_TimeAmt = (this.C_DayAmt + this.C_WeekAmt + this.C_MonthAmt);
					this.I_TimeAmt = (this.I_DayAmt + this.I_WeekAmt + this.I_MonthAmt);
                    this.Time_VATValue = db.VATValues.Where(vat => vat.FeeType == "Time" && vat.FromDate <= DtFrom).First().pcent;
					if ( this.Time_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_TimeAmt;
						this.I_TaxableAmt += this.I_TimeAmt;

						this.C_VATAmt += this.C_TimeAmt * this.Time_VATValue / 100;
						this.I_VATAmt += this.I_TimeAmt * this.Time_VATValue / 100;
					}
					else 
					{
						C_NonTaxableAmt += this.C_TimeAmt;
						I_NonTaxableAmt += this.I_TimeAmt;
					}
				}

                if (bill.KMDriven > 0) 
				{
                    if(OverrideKMAmount > 0)
                    {
                        bill.RentalMileageRateAmount = (decimal)(OverrideKMAmount / bill.KMDriven);
                    }

					amount =(decimal) (bill.RentalMileageRateAmount ?? 0);

					if ( priceSplit.MileageByUnit ) 
					{
						// The insurance company pay a fixed amount of km
						if ( priceSplit.PCentCustomerPartMileage > 0 ) 
						{
							// insurance pays a % of a determine amount of km
                            decimal temp = ((int)bill.KmCoveredByInsurance * ( 100 - priceSplit.PCentCustomerPartMileage )/ 100 );
							bill.KmCoveredByInsurance = (int)Math.Round(temp, 0 );
						}
						this.C_KmPrice = amount;
						this.I_KmPrice = amount;
                        this.C_NbKm = ((int)bill.KMDriven > (int)bill.KmCoveredByInsurance) ? (int)bill.KMDriven - (int)bill.KmCoveredByInsurance : 0;
                        this.I_NbKm = ((int)bill.KMDriven > (int)bill.KmCoveredByInsurance) ? (int)bill.KmCoveredByInsurance : (int)bill.KMDriven;
					}
					else if ( priceSplit.InsurancePayAFixedAmount ) 
					{
						// The insurance company pay a fixed amount of the total bel�p
						// The all amount for the renter's part
                        this.C_NbKm = (int)bill.KMDriven;
                        this.I_NbKm = (int)bill.KMDriven;
						this.C_KmPrice = amount;
						this.I_KmPrice = 0.0m;
					}
					else 
					{
						// The usual way: The customer and the insurance company pay a percentage of the total sum.
                        this.C_NbKm = (int)bill.KMDriven;
                        this.I_NbKm = (int)bill.KMDriven;
                        this.C_KmPrice = priceSplit.PCentCustomerPartMileage * amount / 100;
                        this.I_KmPrice = amount - this.C_KmPrice;                        
					}

					this.C_KmAmt = C_KmPrice * C_NbKm;
					this.I_KmAmt = I_KmPrice * I_NbKm;

                    this.Mileage_VATValue = db.VATValues.Where(vat => vat.FeeType == "Mileage" && vat.FromDate <= DtFrom).First().pcent;

					if ( this.Mileage_VATValue > 0.0m ) 
					{
						this.C_TimeAmt += this.C_KmAmt;
						this.I_TimeAmt += this.I_KmAmt;

						this.C_VATAmt += this.C_KmAmt * this.Mileage_VATValue / 100;
						this.I_VATAmt += this.I_KmAmt * this.Mileage_VATValue / 100;
					}
					else 
					{
                        C_TimeAmt += this.C_KmAmt;
                        I_TimeAmt += this.I_KmAmt;
					}
				}


                if (priceSplit.InsurancePayAFixedAmount)
                {
                    this.AmountCoveredByInsurance_VATValue = this.Time_VATValue;

                    if (!priceSplit.FixedAmountIsVATIncluded)
                    {
                        // Calculate max amount covered with VAT
                        MaxAmountCoveredByInsuranceExcludingVAT = (decimal)bill.AmountCoveredByInsurance;
                        //MaxAmountCoveredByInsuranceIncludingVAT = Math.Round(MaxAmountCoveredByInsuranceExcludingVAT + (MaxAmountCoveredByInsuranceExcludingVAT * this.AmountCoveredByInsurance_VATValue / 100), 2);
                        var t = I_TimeAmt + C_TimeAmt;
                        I_TimeAmt = MaxAmountCoveredByInsuranceExcludingVAT > t ? t : MaxAmountCoveredByInsuranceExcludingVAT;
                        C_TimeAmt = t - I_TimeAmt < 0 ? 0 : t - I_TimeAmt;
                        C_TaxableAmt = C_TimeAmt;
                        I_TaxableAmt = I_TimeAmt;
                    }
                    else
                    {
                        // Calculate max amount covered without VAT
                        MaxAmountCoveredByInsuranceIncludingVAT = (decimal)bill.AmountCoveredByInsurance;
                        MaxAmountCoveredByInsuranceExcludingVAT = (MaxAmountCoveredByInsuranceIncludingVAT * 100) / (100 + this.AmountCoveredByInsurance_VATValue);
                    }

                    // The insurance PayAFixeAmount excluding VAT
                    //if (this.C_TaxableAmt > MaxAmountCoveredByInsuranceExcludingVAT)
                    //{
                    //    this.C_TaxableAmt -= MaxAmountCoveredByInsuranceExcludingVAT;
                    //    this.I_TaxableAmt = MaxAmountCoveredByInsuranceExcludingVAT;
                    //    this.C_AmountCoveredByInsurance = MaxAmountCoveredByInsuranceExcludingVAT * (-1);
                    //    this.I_AmountCoveredByInsurance = MaxAmountCoveredByInsuranceExcludingVAT;
                    //}
                    //else
                    //{
                    //    this.I_TaxableAmt = C_TaxableAmt;
                    //    this.C_TaxableAmt = 0.0m;

                    //    this.C_AmountCoveredByInsurance = I_TaxableAmt * (-1);
                    //    this.I_AmountCoveredByInsurance = I_TaxableAmt;
                    //}
                    // TODO: Add the functionality for the VAT value

                    // Re - calculate the VAT Amount base on the VAT for Time and Mileage
                    this.C_VATAmt = this.C_TaxableAmt * this.AmountCoveredByInsurance_VATValue / 100;
                    this.I_VATAmt = this.I_TaxableAmt * this.AmountCoveredByInsurance_VATValue / 100;

                }


				if ( bill.RefuelingSplit > 0 )
				{
					decimal RefuelingAmount = (decimal) bill.RefuellingTotal;
                    split = db.OtherCostSplits.Where(o => o.ID == bill.RefuelingSplit).First().PercentCustomerPays; //splits.GetOtherCostSplitCustomerPart( bill.RefuelingSplitID );

                    //if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

					this.C_RefuelingAmt = split * RefuelingAmount / 100;
					this.I_RefuelingAmt = RefuelingAmount - this.C_RefuelingAmt;

                    this.Refueling_VATValue = db.VATValues.Where(vat => vat.FeeType == "Refueling" && vat.FromDate <= DtFrom).First().pcent;

					if ( this.Refueling_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_RefuelingAmt;
						this.I_TaxableAmt += this.I_RefuelingAmt;

						this.C_VATAmt += this.C_RefuelingAmt * this.Refueling_VATValue / 100;
						this.I_VATAmt += this.I_RefuelingAmt * this.Refueling_VATValue / 100;
					}
					else 
					{
						C_NonTaxableAmt += this.C_RefuelingAmt;
						I_NonTaxableAmt += this.I_RefuelingAmt;
					}
				}

				if ( bill.DeliveryFeeSplit > 0 )
				{
					decimal DeliveryFeeAmount = (decimal) bill.DeliveryTotal;
                    split = db.OtherCostSplits.Where(o => o.ID == bill.DeliveryFeeSplit).First().PercentCustomerPays; //splits.GetOtherCostSplitCustomerPart( bill.DeliveryFeeSplitID );

                    //if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) Salman
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

					this.C_DeliveryFeeAmt = split * DeliveryFeeAmount / 100;
					this.I_DeliveryFeeAmt = DeliveryFeeAmount - this.C_DeliveryFeeAmt;

                    this.DeliveryFee_VATValue = db.VATValues.Where(vat => vat.FeeType == "DeliveryFee" && vat.FromDate <= DtFrom).First().pcent;

					if ( this.DeliveryFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_DeliveryFeeAmt;
						this.I_TaxableAmt += this.I_DeliveryFeeAmt;

						this.C_VATAmt += this.C_DeliveryFeeAmt * this.DeliveryFee_VATValue / 100;
						this.I_VATAmt += this.I_DeliveryFeeAmt * this.DeliveryFee_VATValue / 100;
					}
					else 
					{
						C_NonTaxableAmt += this.C_DeliveryFeeAmt;
						I_NonTaxableAmt += this.I_DeliveryFeeAmt;
					}
				}

				if ( bill.ReturnFeeSplit > 0 )
				{
					decimal ReturnFeeAmount = (decimal) bill.ReturnTotal;
                    split = db.OtherCostSplits.Where(o => o.ID == bill.ReturnFeeSplit).First().PercentCustomerPays; //splits.GetOtherCostSplitCustomerPart( bill.ReturnFeeSplitID );

                    //if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

					this.C_ReturnFeeAmt = split * ReturnFeeAmount / 100;
					this.I_ReturnFeeAmt = ReturnFeeAmount - this.C_ReturnFeeAmt;

                    this.ReturnFee_VATValue = db.VATValues.Where(vat => vat.FeeType == "ReturnFee" && vat.FromDate <= DtFrom).First().pcent;

					if ( this.ReturnFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_ReturnFeeAmt;
						this.I_TaxableAmt += this.I_ReturnFeeAmt;

						this.C_VATAmt += this.C_ReturnFeeAmt * this.ReturnFee_VATValue / 100;
						this.I_VATAmt += this.I_ReturnFeeAmt * this.ReturnFee_VATValue / 100;
					}
					else
					{
						C_NonTaxableAmt += this.C_ReturnFeeAmt;
						I_NonTaxableAmt += this.I_ReturnFeeAmt;
					}
				}

				if ( bill.AirportFeeSplit > 0 ) 
				{
                    decimal AirportFeeAmount = 0; // = db.FeePrices.Where(f => f.FeeVersion.FeeCode.FeeCode == "AIRPORT" && f.CarGroup == bill.CarGroup && f.FeeVersion.FromDate <= DtFrom && (f.FeeVersion.ToDate >= DtTo || f.FeeVersion.ToDate == null)).First().Price; //prices.GetFeePrice( "AIRPORT", bill.CarGroups.ID, DtFrom );
                    if (bill.AirportFeeTotal > 0)
                    {
                        AirportFeeAmount = (decimal)bill.AirportFeeTotal;
                    }
                    
                    split = db.OtherCostSplits.Where(o => o.ID == bill.AirportFeeSplit).First().PercentCustomerPays; //splits.GetFeeSplitCustomerPart( bill.AirportFeeSplitID );

                    //if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

					this.C_AirportFeeAmt = split * AirportFeeAmount / 100;
					this.I_AirportFeeAmt = AirportFeeAmount - this.C_AirportFeeAmt;

                    this.AirportFee_VATValue = db.VATValues.Where(vat => vat.FeeType == "AirportFee" && vat.FromDate <= DtFrom).First().pcent;

					if ( this.AirportFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_AirportFeeAmt;
						this.I_TaxableAmt += this.I_AirportFeeAmt;

						this.C_VATAmt += this.C_AirportFeeAmt * this.AirportFee_VATValue / 100;
						this.I_VATAmt += this.I_AirportFeeAmt * this.AirportFee_VATValue / 100;
					}
					else
					{
						C_NonTaxableAmt += this.C_AirportFeeAmt;
						I_NonTaxableAmt += this.I_AirportFeeAmt;
					}
				}
			
				if (bill.OneWayFeeSplit > 0 ) 
				{
					decimal OneWayFeeAmount=0.0m;
					if ( bill.OneWayTotal > 0 ) 
					{
						OneWayFeeAmount = (decimal) bill.OneWayTotal;
					}
                    //else 
                    //{
                    //    OneWayFeeAmount = db.FeePrices.Where(f => f.FeeVersion.FeeCode.FeeCode == "ONEWAY" && f.CarGroup == bill.CarGroup && f.FeeVersion.FromDate <= DtFrom && (f.FeeVersion.ToDate >= DtTo || f.FeeVersion.ToDate == null)).First().Price; //prices.GetFeePrice( "ONEWAY", bill.CarGroups.ID, DtFrom );
                    //}
                    split = db.OtherCostSplits.Where(o => o.ID == bill.OneWayFeeSplit).First().PercentCustomerPays;//splits.GetFeeSplitCustomerPart( bill.OneWayFeeSplitID );

                    //if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

                    this.C_OneWayFeeAmt = split * OneWayFeeAmount / 100;
					this.I_OneWayFeeAmt = OneWayFeeAmount - this.C_OneWayFeeAmt;

                    this.OneWayFee_VATValue = db.VATValues.Where(vat => vat.FeeType == "OneWayFee" && vat.FromDate <= DtFrom).First().pcent;

					if ( this.OneWayFee_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_OneWayFeeAmt;
						this.I_TaxableAmt += this.I_OneWayFeeAmt;

						this.C_VATAmt += this.C_OneWayFeeAmt * this.OneWayFee_VATValue / 100;
						this.I_VATAmt += this.I_OneWayFeeAmt * this.OneWayFee_VATValue / 100;
					}
					else
					{
						C_NonTaxableAmt += this.C_OneWayFeeAmt;
						I_NonTaxableAmt += this.I_OneWayFeeAmt;
					}
				}

				if ( bill.VRFSplit > 0 )
				{
					decimal VRFAmount = (decimal) bill.VRFTotal;
                    split = db.OtherCostSplits.Where(o => o.ID == bill.VRFSplit).First().PercentCustomerPays;//splits.GetOtherCostSplitCustomerPart( bill.CongestionFeeSplitID );

                    //if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) Salman
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

                    this.C_VRFAmt = Convert.ToDecimal(split * VRFAmount / 100);
					this.I_VRFAmt = VRFAmount - this.C_VRFAmt;

					this.VRF_VATValue = db.VATValues.Where(vat=>vat.FeeType == "VRF" && vat.FromDate <= DtFrom).First().pcent;

					if ( this.VRF_VATValue > 0.0m ) 
					{
						this.C_TaxableAmt += this.C_VRFAmt;
						this.I_TaxableAmt += this.I_VRFAmt;

						this.C_VATAmt += this.C_VRFAmt * this.VRF_VATValue / 100;
						this.I_VATAmt += this.I_VRFAmt * this.VRF_VATValue / 100;
					}
					else
					{
						C_NonTaxableAmt += this.C_VRFAmt;
						I_NonTaxableAmt += this.I_VRFAmt;
					}
				}

                //if ( bill.ContractFeeSplit.ID > 0 )
                //{
                //    decimal ContractFeeAmount = (decimal) bill.ContractFeeTotal;
                //    split = db.OtherCostSplits.Where(o => o.ID == bill.ContractFeeSplit.ID).First().PercentCustomerPays;//splits.GetOtherCostSplitCustomerPart( bill.ContractFeeSplitID );

                //    if ( priceSplit.InsurancePayAFixedAmount && split < 100 ) 
                //    {
                //        throw new MaxPriceCoveredByInsuranceException();
                //    }

                //    this.C_ContractFeeAmt = Math.Round( split * ContractFeeAmount / 100 );
                //    this.I_ContractFeeAmt = ContractFeeAmount - this.C_ContractFeeAmt;

                //    this.ContractFee_VATValue = db.VATValues.Where(vat => vat.FeeType == "ContractFee" && vat.FromDate <= DtFrom).First().pcent;

                //    if ( this.ContractFee_VATValue > 0.0m ) 
                //    {
                //        this.C_TaxableAmt += this.C_ContractFeeAmt;
                //        this.I_TaxableAmt += this.I_ContractFeeAmt;

                //        this.C_VATAmt += Math.Round( this.C_ContractFeeAmt * this.ContractFee_VATValue / 100, 2 );
                //        this.I_VATAmt += Math.Round( this.I_ContractFeeAmt * this.ContractFee_VATValue / 100, 2 );
                //    }
                //    else
                //    {
                //        C_NonTaxableAmt += this.C_ContractFeeAmt;
                //        I_NonTaxableAmt += this.I_ContractFeeAmt;
                //    }
                //}

				if ( bill.MiscCostsTotal > 0 ) 
				{
                    decimal MiscFeeAmount = (decimal)bill.MiscCostsTotal;
                    //split = db.OtherCostSplits.Where(o => o.ID == bill.MiscCostsSplit).First().PercentCustomerPays;//splits.GetOtherCostSplitCustomerPart( bill.ContractFeeSplitID );

                    //if (priceSplit.InsurancePayAFixedAmount && split < 100)
                    //{
                    //    if(MaxAmountCoveredByInsuranceExcludingVAT < MiscFeeAmount) throw new MaxPriceCoveredByInsuranceException();
                    //}

                    this.C_MiscAmt = bill.MiscCostsCustomerPays; // split * MiscFeeAmount / 100;
                    this.I_MiscAmt = (decimal)bill.MiscCostsInsurancePays; // MiscFeeAmount - this.C_MiscAmt;

                    this.OtherCost_VATValue = db.VATValues.Where(vat => vat.FeeType == "MiscCost" && vat.FromDate <= DtFrom).First().pcent;

                    if (this.OtherCost_VATValue > 0.0m)
                    {
                        this.C_TaxableAmt += this.C_MiscAmt;
                        this.I_TaxableAmt += this.I_MiscAmt;

                        this.C_VATAmt += this.C_MiscAmt * this.OtherCost_VATValue / 100;
                        this.I_VATAmt += this.I_MiscAmt * this.OtherCost_VATValue / 100;
                    }
                    else
                    {
                        C_NonTaxableAmt += this.C_MiscAmt;
                        I_NonTaxableAmt += this.I_MiscAmt;
                    }
                    //this.OtherCost_VATValue = db.VATValues.Where(vat => vat.FeeType == "MiscCost" && vat.FromDate <= DtFrom).First().pcent;

                    //if ( this.OtherCost_VATValue > 0.0m ) 
                    //{
                    //    this.C_TaxableAmt += (decimal) bill.MiscCostsTotal;
                    //    this.C_VATAmt += Math.Round( (decimal) bill.MiscCostsTotal * this.OtherCost_VATValue / 100, 2 );
                    //}
                    //else
                    //{
                    //    C_NonTaxableAmt += (decimal) bill.MiscCostsTotal;
                    //}
				}


                if (bill.OtherFeeSplit > 0)
                {
                    decimal OtherAmount = (decimal)bill.OtherFeeTotal;
                    split = db.OtherCostSplits.Where(o => o.ID == bill.OtherFeeSplit).First().PercentCustomerPays;//splits.GetOtherCostSplitCustomerPart( bill.ContractFeeSplitID );

                    //if (priceSplit.InsurancePayAFixedAmount && split < 100)
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

                    this.C_OthersAmt = split * OtherAmount / 100;
                    this.I_OthersAmt = OtherAmount - C_OthersAmt;

                    this.Others_VATValue = db.VATValues.Where(vat => vat.FeeType == "Other" && vat.FromDate <= DtFrom).First().pcent;

                    if (this.Others_VATValue > 0.0m)
                    {
                        this.C_TaxableAmt += this.C_OthersAmt;
                        this.I_TaxableAmt += this.I_OthersAmt;

                        this.C_VATAmt += this.C_OthersAmt * this.Others_VATValue / 100;
                        this.I_VATAmt += this.I_OthersAmt * this.Others_VATValue / 100;
                    }
                    else
                    {
                        C_NonTaxableAmt += this.C_OthersAmt;
                        I_NonTaxableAmt += this.I_OthersAmt;
                    }
                }


                if (bill.CDWSplit > 0)
                {
                    decimal SuperCDWAmount; 
                    decimal CalculatedSuperCDWTotal = (decimal)(bill.RentalCDWPerDayAmount ?? 0) * NbDaysTotal;
                    if (CalculatedSuperCDWTotal != (decimal)bill.SuperCDWTotal && (decimal)bill.SuperCDWTotal > 0)
                    {
                        SuperCDWAmount = (decimal)bill.SuperCDWTotal;
                    }
                    else
                    {
                        SuperCDWAmount = CalculatedSuperCDWTotal;

                    }
                    split = db.OtherCostSplits.Where(o => o.ID == bill.CDWSplit).First().PercentCustomerPays;//splits.GetOtherCostSplitCustomerPart( bill.ContractFeeSplitID );

                    //if (priceSplit.InsurancePayAFixedAmount && split < 100)
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

                    this.C_SuperCDWAmt = split * SuperCDWAmount / 100;
                    this.I_SuperCDWAmt = SuperCDWAmount - this.C_SuperCDWAmt;

                    this.SuperCDW_VATValue = db.OtherInsurancePriceVersions.Where(vat => vat.OtherInsuranceCode_ID == 4 && vat.FromDate <= DtFrom && vat.ToDate >= DtTo).First().VAT_Pcent;

                    if (this.SuperCDW_VATValue > 0.0m)
                    {

                        this.C_TaxableAmt += this.C_SuperCDWAmt;
                        this.I_TaxableAmt += this.I_SuperCDWAmt;

                        this.C_VATAmt += this.C_SuperCDWAmt * this.SuperCDW_VATValue / 100;
                        this.I_VATAmt += this.I_SuperCDWAmt * this.SuperCDW_VATValue / 100;

                        //this.C_TaxableAmt += (decimal)bill.SuperCDWTotal;
                        //this.C_VATAmt += Math.Round((decimal)bill.SuperCDWTotal * this.SuperCDW_VATValue / 100, 2);
                    }
                    else
                    {
                        C_NonTaxableAmt += C_SuperCDWAmt;
                        I_NonTaxableAmt += I_SuperCDWAmt;
                    }
                }



                if (bill.SuperTPSplit > 0)
                {
                    decimal SuperTPAmount;
                    decimal CalculatedSuperTPTotal = (decimal)(bill.RentalALIPerDayAmount ?? 0) * NbDaysTotal;
                    if (CalculatedSuperTPTotal != (decimal)(bill.SuperTPTotal ?? 0) && (decimal)(bill.SuperTPTotal ?? 0) > 0)
                    {
                        SuperTPAmount = (decimal)bill.SuperTPTotal;
                    }
                    else
                    {
                        SuperTPAmount = CalculatedSuperTPTotal;

                    }
                    split = db.OtherCostSplits.Where(o => o.ID == bill.SuperTPSplit).First().PercentCustomerPays;//splits.GetOtherCostSplitCustomerPart( bill.ContractFeeSplitID );

                    //if (priceSplit.InsurancePayAFixedAmount && split < 100)
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

                    this.C_SuperTPAmt = split * SuperTPAmount / 100;
                    this.I_SuperTPAmt = SuperTPAmount - this.C_SuperTPAmt;

                    this.SuperTP_VATValue = db.OtherInsurancePriceVersions.Where(vat => vat.OtherInsuranceCode_ID == 4 && vat.FromDate <= DtFrom && vat.ToDate >= DtTo).First().VAT_Pcent;

                    if (this.SuperTP_VATValue > 0.0m)
                    {

                        this.C_TaxableAmt += this.C_SuperTPAmt;
                        this.I_TaxableAmt += this.I_SuperTPAmt;

                        this.C_VATAmt += this.C_SuperTPAmt * this.SuperTP_VATValue / 100;
                        this.I_VATAmt += this.I_SuperTPAmt * this.SuperTP_VATValue / 100;

                        //this.C_TaxableAmt += (decimal)bill.SuperCDWTotal;
                        //this.C_VATAmt += Math.Round((decimal)bill.SuperCDWTotal * this.SuperCDW_VATValue / 100, 2);
                    }
                    else
                    {
                        C_NonTaxableAmt += C_SuperTPAmt;
                        I_NonTaxableAmt += I_SuperTPAmt;
                    }
                }



                if (bill.RAISplit > 0)
                {
                    decimal RAIAmount; // = (decimal)bill.RAITotal;

                    decimal CalculatedRAITotal = (decimal)(bill.RentalPAIPerDayAmount ?? 0) * NbDaysTotal;
                    if (CalculatedRAITotal != (decimal)bill.RAITotal && (decimal)bill.RAITotal > 0)
                    {
                        RAIAmount = (decimal)bill.RAITotal;
                    }
                    else
                    {
                        RAIAmount = CalculatedRAITotal;

                    }


                    split = db.OtherCostSplits.Where(o => o.ID == bill.RAISplit).First().PercentCustomerPays;//splits.GetOtherCostSplitCustomerPart( bill.ContractFeeSplitID );

                    //if (priceSplit.InsurancePayAFixedAmount && split < 100)
                    //{
                    //    throw new MaxPriceCoveredByInsuranceException();
                    //}

                    this.C_RAIAmt = split * RAIAmount / 100;
                    this.I_RAIAmt = RAIAmount - this.C_RAIAmt;

                    this.RAI_VATValue = db.OtherInsurancePriceVersions.Where(vat => vat.OtherInsuranceCode_ID == 3 && vat.FromDate <= DtFrom && vat.ToDate >= DtTo).First().VAT_Pcent;

                    if (this.RAI_VATValue > 0.0m)
                    {

                        this.C_TaxableAmt += this.C_RAIAmt;
                        this.I_TaxableAmt += this.I_RAIAmt;

                        this.C_VATAmt += this.C_RAIAmt * this.RAI_VATValue / 100;
                        this.I_VATAmt += this.I_RAIAmt * this.RAI_VATValue / 100;

                        //this.C_TaxableAmt += (decimal)bill.RAITotal;
                        //this.C_VATAmt += Math.Round((decimal)bill.RAITotal * this.RAI_VATValue / 100, 2);
                    }
                    else
                    {
                        C_NonTaxableAmt += C_RAIAmt;
                        I_NonTaxableAmt += I_RAIAmt;
                    }
                }

                //if ( bill.GetAdditionalInsurancesCount() > 0 ) 
                //{
                //    this._AdditionalInsurances = new DataType.Insurances[bill.GetAdditionalInsurancesCount()];
                //    int i=0;
                //    foreach ( DataType.Insurance insurance in bill.GetAdditionalInsurances() ) 
                //    {
                //        if ( priceSplit.InsurancePayAFixedAmount && insurance.ChargedTo == DataType.Insurance.ChargedToEnum.Insurance ) 
                //        {
                //            throw new MaxPriceCoveredByInsuranceException();
                //        }
                //        this._AdditionalInsurances[i] = new Avis.sd.SBAIR.DataType.Insurances( this.SplitBillingID, insurance.InsuranceCode );
                //        this._AdditionalInsurances[i].VATValue = prices.GetVAT_Insurance( insurance.InsuranceCode, DtFrom );
                //        this._AdditionalInsurances[i].Price = prices.GetInsurancePrice( insurance.InsuranceCode, bill.CarGroup, DtFrom ); 
                //        this._AdditionalInsurances[i].Amount = this._AdditionalInsurances[i].Price * this.NbDaysTotal;
						
                //        if (insurance.ChargedTo == DataType.Insurance.ChargedToEnum.Customer ) 
                //        {
                //            this._AdditionalInsurances[i].ChargedTo = "C";
                //            if ( this._AdditionalInsurances[i].VATValue > 0.0m ) 
                //            {
                //                this.C_TaxableAmt += this._AdditionalInsurances[i].Amount;
                //                this.C_VATAmt += ( this._AdditionalInsurances[i].Amount * this._AdditionalInsurances[i].VATValue / 100 );
                //            }
                //            else
                //            {
                //                C_NonTaxableAmt += this._AdditionalInsurances[i].Amount;
                //            }
                //        }
                //        else 
                //        {
                //            this._AdditionalInsurances[i].ChargedTo = "I";
                //            if ( this._AdditionalInsurances[i].VATValue > 0.0m ) 
                //            {
                //                this.I_TaxableAmt += this._AdditionalInsurances[i].Amount;
                //                this.I_VATAmt += ( this._AdditionalInsurances[i].Amount * this._AdditionalInsurances[i].VATValue / 100 );
                //            }
                //            else
                //            {
                //                I_NonTaxableAmt += this._AdditionalInsurances[i].Amount;
                //            }
                //        }
                //        i++;
                //    }
                //}
                decimal invoiceFeeVATAmount = 0.0m;

				if ( bill.InvoiceFeeTotal != 0 )
				{
                    this.C_InvoiceFeeAmt = (decimal) bill.InvoiceFeeTotal;
					if ( this.C_InvoiceFeeAmt > 0.0m ) 
					{
                        this.InvoiceFee_VATValue = db.VATValues.Where(vat => vat.FeeType == "InvoiceFee" && vat.FromDate <= DtFrom).First().pcent;
					
						if ( this.InvoiceFee_VATValue > 0.0m ) 
						{
							this.C_TaxableAmt += this.C_InvoiceFeeAmt;
							invoiceFeeVATAmount = this.C_InvoiceFeeAmt * this.InvoiceFee_VATValue / 100;
							this.C_VATAmt += invoiceFeeVATAmount;
						}
						else
						{
							C_NonTaxableAmt += this.C_InvoiceFeeAmt;
						}
					}
				}

				
				
				/*
				VAT Split
				*/
				if ( ( this.C_VATAmt + this.I_VATAmt ) > 0 ) 
				{
                    VATSplit vat_Split = db.VATSplits.Where(v => v.ID == bill.VATSplit).First();

					split = vat_Split.PercentageCustomerPays;

					if ( split >= 0 ) 
					{
						decimal TotalVATAmt = this.C_VATAmt + this.I_VATAmt;

                        if ( vat_Split.AppliedTo == "I" ) 
                        {
                            // The split is about the insurance company's VAT
                            // The customer (renter) pays his own VAT + a % of the VAT of the insurance company
                            this.C_SplittedVATAmt = this.C_VATAmt + split * this.I_VATAmt / 100;
                            this.I_SplittedVATAmt = TotalVATAmt - this.C_SplittedVATAmt;
                        }
                        else if (vat_Split.AppliedTo == "T")
                        {
                            // The split is about the customer's VAT
                            // The insurance company pays his own VAT + a % of the VAT of the customer (renter)
                            this.I_SplittedVATAmt = this.I_VATAmt + split * this.C_VATAmt / 100;
                            this.C_SplittedVATAmt = TotalVATAmt - this.I_SplittedVATAmt;
                        }
                        else //( vat_Split.AppliedTo == DataType.VAT_Split.TargetVAT.Total ) 
                        {
							// The split is about the total of all VAT
							this.C_SplittedVATAmt = (split * TotalVATAmt / 100) + invoiceFeeVATAmount;
							this.I_SplittedVATAmt = TotalVATAmt - this.C_SplittedVATAmt;
						}
					}
					else 
					{
						// if split == -1 : No split
						this.C_SplittedVATAmt = this.C_VATAmt;
						this.I_SplittedVATAmt = this.I_VATAmt;
					}
				}


                //if (priceSplit.InsurancePayAFixedAmount && (this.I_TaxableAmt + I_NonTaxableAmt) > MaxAmountCoveredByInsuranceExcludingVAT)
                //{
                //    var difference = (this.I_TaxableAmt + I_NonTaxableAmt) - MaxAmountCoveredByInsuranceExcludingVAT;
                //    C_SplittedVATAmt += difference;
                //    I_SplittedVATAmt -= difference;
                //}

                this.I_TotalAmt = this.I_TaxableAmt + this.I_SplittedVATAmt + I_NonTaxableAmt;
                this.C_TotalAmt = this.C_TaxableAmt + this.C_SplittedVATAmt + C_NonTaxableAmt;

				
				
                //if ( priceSplit.InsurancePayAFixedAmount ) 
                //{
                //    decimal insuranceAmount = (decimal) bill.AmountCoveredByInsurance;

                //    if (!priceSplit.FixedAmountIsVATIncluded) 
                //    {
                //        this.AmountCoveredByInsurance_VATValue = this.Time_VATValue;
                //        insuranceAmount += Math.Round( insuranceAmount * this.AmountCoveredByInsurance_VATValue / 100, 2 );
                //    }

                //    // The insurance PayAFixeAmount including VAT
                //    if (  this.C_TotalAmt > insuranceAmount ) 
                //    {
                //        this.C_TotalAmt -= insuranceAmount;
                //        this.I_TotalAmt = insuranceAmount;
                //        this.C_AmountCoveredByInsurance = insuranceAmount * (-1);
                //        this.I_AmountCoveredByInsurance = insuranceAmount;
                //    }
                //    else 
                //    {
                //        this.I_TotalAmt = C_TotalAmt;
                //        this.C_TotalAmt = 0.0m;

                //        this.C_AmountCoveredByInsurance = I_TotalAmt * (-1);
                //        this.I_AmountCoveredByInsurance = I_TotalAmt;
                //    }
                //}
				

				//this.C_TotalAmt -=  (decimal) bill.AlreadyPaidByCustomerAmount;
                Console.WriteLine(Math.Round(this.C_TotalAmt, 2));
                Console.WriteLine(Math.Round(this.I_TotalAmt, 2));
                Console.WriteLine(Math.Round(this.C_TotalAmt + this.I_TotalAmt, 2));

                this.TotalAmt = this.C_TotalAmt + this.I_TotalAmt;

				SetWizardAmt();
			}	
		}


        //public bool SaveCalc()
        //{
            
        //    return true;
        //}

        private int DaysInMonth = 30;
        private int DaysInWeek = 7;

		private TimeElement GetTimeElement( int CurrMonth, int CurrYear, int NbDaysTotal, SplitBilling bill )
		{

            //RatePrice rPrice = db.RatePrices.Where(r => r.RateVersion.RateVersionID == rVersion.RateVersionID).First();

			if ( CurrMonth < 1 || CurrMonth > 12 ) throw new Exception( "GetTimeElement: Invalid parameter CurrMonth" );
			if ( CurrYear < 1900 || CurrMonth > 3000 ) throw new Exception( "GetTimeElement: Invalid parameter CurrYear" );

            int DaysPerMonth = DaysInMonth; //bill.DaysInMonth;
            int DaysPerWeek = DaysInWeek;

			if ( DaysPerMonth < 0 || DaysPerMonth > 31 ) throw new Exception( "GetTimeElement: Invalid parameter DaysPerMonth" );
			if ( DaysPerWeek < 1 || DaysPerWeek > 7 ) throw new Exception( "GetTimeElement: Invalid parameter DaysPerWeek" );
		
			// if DayPerMonth >= 30 the price is just calculated per days.
			// No free of charge day.
			if ( DaysPerMonth >= 30 ) return new TimeElement(  NbDaysTotal,  0, 0, 0 );
		
			int Days=NbDaysTotal, Weeks=0, Months=0;
			int CDaysPerMonth=0;			// Number of days per month compensated by the number of free days of the week price
			int FreeDays=0;                 // Number of days free of charge because of the price per week/month
		
			int FreeDaysPerWeek = 7 - DaysPerWeek;
		
			CDaysPerMonth = DaysPerMonth;
		
			// Compensation for free days the 3 first weeks in the month
			CDaysPerMonth += ( 3 * FreeDaysPerWeek );
		
			System.Globalization.GregorianCalendar calendar = new System.Globalization.GregorianCalendar();
      			
			// Calculate number of months
			while ( Days > CDaysPerMonth )
			{
				Months++; // Charge one month
			
				if ( DaysPerMonth >= 30 )
				{
					// Price by month is not the best price
					Days -= calendar.GetDaysInMonth( CurrYear, CurrMonth );
				}
				else
				{
					// Price by month is the best price
					Days -= 31;         // Allways 31 days for the month
					FreeDays += ( 31 - DaysPerMonth );
				}
				if ( Days < 0 ) FreeDays += Days;
			
				// Move to next month
				CurrMonth = ( CurrMonth < 12 ) ? CurrMonth + 1 : 1;
			}
			if ( Days < 0 ) Days = 0;
		
			// Calculate number of weeks
			while ( Days >= 7 )
			{
				Weeks ++;
				Days -= 7;
				FreeDays += ( 7 - DaysPerWeek );
			}

			//PHB 2006.08.09: Get the best price for the rest of days.
			if ( Days * bill.RentalDailyRateAmount > bill.RentalWeeklyRateAmount ) 
			{
				Weeks++;
				Days=0;
			}

			return new TimeElement(  Days,  Weeks, Months, FreeDays );
		}

//        public object Clone()
//        {
//            try
//            {
//                object sourceObject = this;
//                Type sourceType = sourceObject.GetType();
//                PropertyInfo[] sourceProperties = sourceType.GetProperties (BindingFlags.Instance | BindingFlags.Public);
				
//                Business.CalculationDetails ClonedCalc = new CalculationDetails();

//                for(int i = 0; i < sourceProperties.Length; i++)
//                {
//                    Type propertyType = sourceProperties[i].PropertyType;
//                    String sourcePropertyName = sourceProperties[i].Name;
//                    String isNullPropertyName = String.Format("Is{0}Null", sourcePropertyName);
//#if SHOW_DEBUG_MSG
//                    System.Diagnostics.Debug.WriteLine(String.Format("Name            : {0}", sourceProperties[i].Name));
//                    System.Diagnostics.Debug.WriteLine(String.Format("Declaring Type  : {0}", sourceProperties[i].DeclaringType));
//                    System.Diagnostics.Debug.WriteLine(String.Format("MemberType      : {0}", sourceProperties[i].MemberType));
//#endif					
//                    object Value = null;
//                    try 
//                    {
//                        if ( sourcePropertyName == "AdditionalInsurances" ) 
//                        {
//                            ClonedCalc._AdditionalInsurances = new DataType.Insurances[this._AdditionalInsurances.Length];
//                            this._AdditionalInsurances.CopyTo(ClonedCalc._AdditionalInsurances, 0);
//                        }
//                        else 
//                        {
//                            Value = sourceProperties[i].GetValue( sourceObject, null );
//#if SHOW_DEBUG_MSG
//                            System.Diagnostics.Debug.WriteLine(String.Format("Value           : {0}", Value));
//#endif
//                            sourceProperties[i].SetValue( ClonedCalc, Value, null );
//                        }
//                    }
//                    catch
//                    {
//                        // One exception is generated when we try to get a value from System.DBNull
//                        sourceProperties[i].SetValue( ClonedCalc, null, null );
//                    }
//                }
//                return ClonedCalc;
					
//            }
//            catch ( Exception Ex )
//            {
//                throw new ApplicationException("Exception in MapSplitBillingObjects: "+ Ex.Message, Ex);
//            }
//        }

		private void SetWizardAmt() 
		{
			
			decimal invoiceFeeVATAmount=0.0m;
			decimal calculatedInvoiceFeeVATIncluded=0.0m;
			
			if ( this.C_InvoiceFeeAmt > 0.0m ) 
			{
				if ( this.InvoiceFee_VATValue > 0.0m ) 
				{
					invoiceFeeVATAmount = this.C_InvoiceFeeAmt * this.InvoiceFee_VATValue / 100;
				}
				calculatedInvoiceFeeVATIncluded = this.C_InvoiceFeeAmt + invoiceFeeVATAmount;
			}
			this._WizardAmt = this.TotalAmt - calculatedInvoiceFeeVATIncluded;
		}

        public decimal C_SplittedVATAmt { get; set; }

        public decimal I_SplittedVATAmt { get; set; }
    }

	public class TimeElement 
	{
		private int _days=0;
		private int _weeks=0;
		private int _months=0;
		private int _freeDays=0;
	
		public TimeElement( int Days, int Weeks, int Months, int FreeDays )
		{
			_days=Days;
			_weeks=Weeks;
			_months=Months;
			_freeDays=FreeDays;
		}
	
		public int Days { set { _days=value; } get { return _days; } }
		public int Weeks { set { _weeks=value; } get { return _weeks; } }
		public int Months { set { _months=value; } get { return _months; } }
		public int FreeDays { set { _freeDays=value; } get { return _freeDays; } }
	}

	public class MaxPriceCoveredByInsuranceException : ApplicationException 
	{
		public MaxPriceCoveredByInsuranceException() : base() {;}
		public MaxPriceCoveredByInsuranceException( string message) : base(message) {;}
		public MaxPriceCoveredByInsuranceException( string message, Exception InnerException) : base(message, InnerException) {;}
	}
}
