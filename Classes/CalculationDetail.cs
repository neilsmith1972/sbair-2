using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Classes
{
	public partial class CalculationDetail
	{
		    [Key]
            public int ID {get; set;}
            public int NbDaysTotal  {get; set;}
            public int I_NbDays { get; set; }
            public decimal I_DayPrice { get; set; }
            public decimal I_DayAmt { get; set; }
            public int C_NbDays { get; set; }
            public decimal C_DayPrice { get; set; }
            public decimal C_DayAmt { get; set; }
            public int I_NbWeeks { get; set; }
            public decimal I_WeekPrice { get; set; }
            public decimal I_WeekAmt { get; set; }
            public int C_NbWeeks { get; set; }
            public decimal C_WeekPrice { get; set; }
            public decimal C_WeekAmt { get; set; }
            public int I_NbMonth { get; set; }
            public decimal I_MonthPrice { get; set; }
            public decimal I_MonthAmt { get; set; }
            public int C_NbMonth { get; set; }
            public decimal C_MonthPrice { get; set; }
            public decimal C_MonthAmt { get; set; }
            public decimal Time_VATValue { get; set; }
            public int I_NbKm { get; set; }
            public decimal I_KmPrice { get; set; }
            public decimal I_KmAmt { get; set; }
            public int C_NbKm { get; set; }
            public decimal C_KmPrice { get; set; }
            public decimal C_KmAmt { get; set; }
            public decimal Mileage_VATValue { get; set; }
            public decimal I_RefuelingAmt { get; set; }
            public decimal C_RefuelingAmt { get; set; }
            public decimal Refueling_VATValue { get; set; }
            public decimal I_DeliveryFeeAmt { get; set; }
            public decimal C_DeliveryFeeAmt { get; set; }
            public decimal DeliveryFee_VATValue { get; set; }
            public decimal I_ReturnFeeAmt { get; set; }
            public decimal C_ReturnFeeAmt { get; set; }
            public decimal ReturnFee_VATValue { get; set; }
            public decimal I_OneWayFeeAmt { get; set; }
            public decimal C_OneWayFeeAmt { get; set; }
            public decimal OneWayFee_VATValue { get; set; }
            public decimal I_AirportFeeAmt { get; set; }
            public decimal C_AirportFeeAmt { get; set; }
            public decimal AirportFee_VATValue { get; set; }

            public decimal I_SuperCDWAmt { get; set; }
            public decimal C_SuperCDWAmt { get; set; }
            public decimal SuperCDW_VATValue { get; set; }

            public decimal I_SuperTPAmt { get; set; }
            public decimal C_SuperTPAmt { get; set; }
            public decimal SuperTP_VATValue { get; set; }

            public decimal I_RAIAmt { get; set; }
            public decimal C_RAIAmt { get; set; }
            public decimal RAI_VATValue { get; set; }

            public decimal I_VRFAmt { get; set; }
            public decimal C_VRFAmt { get; set; }
            public decimal VRF_VATValue { get; set; }

            public decimal I_OthersAmt { get; set; }
            public decimal C_OthersAmt { get; set; }
            public decimal Others_VATValue { get; set; }

            public decimal C_InvoiceFeeAmt { get; set; }
            public decimal InvoiceFee_VATValue { get; set; }
            public decimal OtherCost_VATValue { get; set; }
            public decimal C_AmountCoveredByInsurance { get; set; }
            public decimal I_AmountCoveredByInsurance { get; set; }
            public decimal AmountCoveredByInsurance_VATValue { get; set; }
            public decimal I_VATAmt { get; set; }
            public decimal C_VATAmt { get; set; }

            public decimal I_SplitVATAmt { get; set; }
            public decimal C_SplitVATAmt { get; set; }

            public decimal I_TimeAmt { get; set; }
            public decimal C_TimeAmt { get; set; }

            public decimal I_MiscAmt { get; set; }
            public decimal C_MiscAmt { get; set; }

            public decimal I_TaxableAmt { get; set; }
            public decimal C_TaxableAmt { get; set; }

            public decimal I_TotalAmt { get; set; }
            public decimal C_TotalAmt { get; set; }
            public decimal TotalAmt { get; set; }

            public decimal C_NonTaxableAmt { get; set; }
            public decimal I_NonTaxableAmt { get; set; }

            //public decimal WizardAmt { get; set; }
		
	}
}
