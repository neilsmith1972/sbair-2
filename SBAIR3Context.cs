using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using avis.sbair.Entities;
using avis.sbair.Mapping;

namespace avis.sbair
{
	public class SBAIR3Context : DbContext
	{
		static SBAIR3Context()
		{ 
			Database.SetInitializer<SBAIR3Context>(null);
		}

		public DbSet<audit> audits { get; set; }
		//public DbSet<ClosedRental> ClosedRentals { get; set; }
		public DbSet<Country> Countries { get; set; }
		public DbSet<ExceptionRental> ExceptionRentals { get; set; }
		public DbSet<FeePrice> FeePrices { get; set; }
		public DbSet<FeeSplit> FeeSplits { get; set; }
		public DbSet<FeeType> FeeTypes { get; set; }
		public DbSet<FeeVersion> FeeVersions { get; set; }
		public DbSet<InvoicedRental> InvoicedRentals { get; set; }
		public DbSet<InvoiceFee> InvoiceFees { get; set; }
		public DbSet<OpenRental> OpenRentals { get; set; }
		public DbSet<OtherCost> OtherCosts { get; set; }
		public DbSet<OtherCostSplit> OtherCostSplits { get; set; }
		public DbSet<OtherInsuranceCode> OtherInsuranceCodes { get; set; }
		public DbSet<OtherInsurancePrice> OtherInsurancePrices { get; set; }
		public DbSet<OtherInsurancePriceVersion> OtherInsurancePriceVersions { get; set; }
		public DbSet<PaymentMethod> PaymentMethods { get; set; }
		public DbSet<PriceSplit> PriceSplits { get; set; }
		public DbSet<SplitBilling> SplitBillings { get; set; }
		public DbSet<Status> Status { get; set; }
		public DbSet<VATSplit> VATSplits { get; set; }
		public DbSet<VATValue> VATValues { get; set; }
        public DbSet<InsuranceCompany> InsuranceCompanies { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<AdjustmentProcessed> AdjustmentProcessed { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<MiscCode> MiscCodes { get; set; }
        public DbSet<ExpenseCode> ExpenseCodes { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
            modelBuilder.Conventions.Remove<IncludeMetadataConvention>();
			modelBuilder.Configurations.Add(new auditMap());
			//modelBuilder.Configurations.Add(new ClosedRentalMap());
			modelBuilder.Configurations.Add(new CountryMap());
			modelBuilder.Configurations.Add(new ExceptionRentalMap());
			modelBuilder.Configurations.Add(new FeePriceMap());
			modelBuilder.Configurations.Add(new FeeSplitMap());
			modelBuilder.Configurations.Add(new FeeTypeMap());
			modelBuilder.Configurations.Add(new FeeVersionMap());
			modelBuilder.Configurations.Add(new InvoicedRentalMap());
			modelBuilder.Configurations.Add(new InvoiceFeeMap());
			modelBuilder.Configurations.Add(new OpenRentalMap());
			modelBuilder.Configurations.Add(new OtherCostMap());
			modelBuilder.Configurations.Add(new OtherCostSplitMap());
			modelBuilder.Configurations.Add(new OtherInsuranceCodeMap());
			modelBuilder.Configurations.Add(new OtherInsurancePriceMap());
			modelBuilder.Configurations.Add(new OtherInsurancePriceVersionMap());
			modelBuilder.Configurations.Add(new PaymentMethodMap());
			modelBuilder.Configurations.Add(new PriceSplitMap());
			modelBuilder.Configurations.Add(new SplitBillingMap());
			modelBuilder.Configurations.Add(new StatusMap());
			modelBuilder.Configurations.Add(new VATSplitMap());
			modelBuilder.Configurations.Add(new VATValueMap());
            modelBuilder.Configurations.Add(new InsuranceCompanyMap());
            modelBuilder.Configurations.Add(new RateMap());
            modelBuilder.Configurations.Add(new AdjustmentProcessedMap());
            modelBuilder.Configurations.Add(new InvoiceMap());

            modelBuilder.Configurations.Add(new MiscCodeMap());
            modelBuilder.Configurations.Add(new ExpenseCodeMap());


        }
	}
}

