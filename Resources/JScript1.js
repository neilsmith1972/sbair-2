﻿var decimalSeperator = ',';
var zeroDecimalString = '0,00';
var zeroDecimalValue = '0.00';
var decimalPlacesToDisplay = 2;
$(function () {
    $('.lineItemAmountSplitter').each(function () {
        var container = $(this);
        var lineItemAmountSlider = $(this).find('.lineItemAmountSlider');
        var lineItemAmountInput = $(this).find('.lineItemAmountInput');
        var customerAmountLabel = $(this).find('.customerLineItemAmount');
        var customerAmountInput = $(this).find('.customerLineItemAmountInput');
        var insurerAmountLabel = $(this).find('.insurerLineItemAmount');
        var insurerAmountInput = $(this).find('.insurerLineItemAmountInput');
        $(this).find('.customerRadioOption').button().click(function () {
            lineItemAmountSlider.slider('value', 0);
        });
        $(this).find('.insurerRadioOption').button().click(function () {
            lineItemAmountSlider.slider('value', 100);
        });
        $(this).find('.lineItemSplitterButtonGroup').buttonset({
            create: function (event, ui) {
                $(this).css('margin-right', '0');
                $(this).children('label').css('width', ($(this).width() / 2) - 2);
                $(this).children('label:first').css('text-align', 'left');
                $(this).children('label:last').css('text-align', 'right');
            }
        });
        lineItemAmountSlider.slider({
            minvalue: 0,
            maxvalue: 100,
            value: 50,
            create: function (event, ui) {
                $(this).children('.ui-slider-handle').css('width', '6px').css('height', '11px').css('top', '-3px').css('margin-left', '-3px').css('background', '#ccc');
            },
            slide: function (event, ui) {
                splitAssignedValue(container, getCurrentValue(lineItemAmountInput), ui.value);
            },
            change: function (event, ui) {
                splitAssignedValue(container, getCurrentValue(lineItemAmountInput), ui.value);
            }
        });
        $(this).find('.lineItemAmountInput').keyup(function (event) {
            var val = getCurrentValue(lineItemAmountInput);
            if (val.toString() == zeroDecimalValue) {
                disableControl(container);
                customerAmountLabel.html(displayMonetaryValue(zeroDecimalString));
                insurerAmountLabel.html(displayMonetaryValue(zeroDecimalString));
                insurerAmountInput.val(displayMonetaryValue(zeroDecimalString));
                customerAmountInput.val(displayMonetaryValue(zeroDecimalString));
                lineItemAmountSlider.slider('value', 50);
            } else {
                enableControl(container);
                splitAssignedValue(container, val, lineItemAmountSlider.slider('value'));
            }
            onLineItemAllocationChanged();
        }).focusin(function () {
            $(this).select();
        });
        if (getCurrentValue(lineItemAmountInput) == null || getCurrentValue(lineItemAmountInput).toString() == zeroDecimalValue) {
            disableControl(container);
        } else {
            enableControl(container);
            if (getCurrentValue(insurerAmountInput) + getCurrentValue(customerAmountInput) != getCurrentValue(lineItemAmountInput)) {
                splitAssignedValue(container, getCurrentValue(lineItemAmountInput), lineItemAmountSlider.slider('value'));
            }
        }
        customerAmountLabel.click(function () {
            if (getCurrentValue(lineItemAmountInput) != null && getCurrentValue(lineItemAmountInput).toString() != zeroDecimalValue) {
                customerAmountInput.show();
                customerAmountInput.select();
                customerAmountLabel.hide();
            }
        });
        insurerAmountLabel.click(function () {
            if (getCurrentValue(lineItemAmountInput) != null && getCurrentValue(lineItemAmountInput).toString() != zeroDecimalValue) {
                insurerAmountInput.show();
                insurerAmountInput.select();
                insurerAmountLabel.hide();
            }
        });
        lineItemAmountInput.blur(function () {
            var val = getCurrentValue($(this));
            $(this).val(displayMonetaryValue(val));
        });
        customerAmountInput.blur(function () {
            var val = getCurrentValue($(this));
            if (val >= 0 && val <= getCurrentValue(lineItemAmountInput)) {
                customerAmountLabel.html(displayMonetaryValue(customerAmountInput.val()));
            } else if (val < 0) {
                $(this).val(displayMonetaryValue(zeroDecimalValue)) insurerAmountInput.val(displayMonetaryValue(getCurrentValue(lineItemAmountInput)));
            } else if (val > getCurrentValue(lineItemAmountInput)) {
                $(this).val(displayMonetaryValue(getCurrentValue(lineItemAmountInput))) insurerAmountInput.val(displayMonetaryValue(zeroDecimalValue));
            }
            customerAmountLabel.show();
            customerAmountInput.hide();
            var percentage = (1 - (val / getCurrentValue(lineItemAmountInput))) * 100;
            lineItemAmountSlider.slider('value', percentage);
            onLineItemAllocationChanged();
        });
        insurerAmountInput.blur(function () {
            var val = getCurrentValue($(this));
            if (val >= 0 && val <= getCurrentValue(lineItemAmountInput)) {
                insurerAmountLabel.html(displayMonetaryValue(insurerAmountInput.val()));
            } else if (val < 0) {
                $(this).val(zeroDecimalValue) customerAmountInput.val(displayMonetaryValue(getCurrentValue(lineItemAmountInput)));
            } else if (val > getCurrentValue(lineItemAmountInput)) {
                $(this).val(displayMonetaryValue(getCurrentValue(lineItemAmountInput))) customerAmountInput.val(displayMonetaryValue(zeroDecimalValue));
            }
            insurerAmountLabel.show();
            insurerAmountInput.hide();
            var percentage = (val / getCurrentValue(lineItemAmountInput)) * 100;
            lineItemAmountSlider.slider('value', percentage);
            onLineItemAllocationChanged();
        });
    });
});

function getCurrentValue(input) {
    if (input == null) {
        return zeroDecimalValue;
    }
    return parseFloat(input.val().replace(',', '.')).toFixed(decimalPlacesToDisplay);
}
function displayMonetaryValue(decimalValue) {
    if (decimalValue == null) {
        return parseFloat(zeroDecimalValue).toFixed(decimalPlacesToDisplay).replace('.', decimalSeperator).replace(',', decimalSeperator);
    }
    return parseFloat(decimalValue).toFixed(decimalPlacesToDisplay).replace('.', decimalSeperator).replace(',', decimalSeperator);
}
function splitAssignedValue(container, value, sliderValue) {
    if (value == null || value.toString() == zeroDecimalValue) {
        return;
    }
    var customerAmountLabel = container.find('.customerLineItemAmount');
    var customerAmountInput = container.find('.customerLineItemAmountInput');
    var insurerAmountLabel = container.find('.insurerLineItemAmount');
    var insurerAmountInput = container.find('.insurerLineItemAmountInput');
    var customerRadioButton = container.find('.customerRadioOption');
    var insurerRadioButton = container.find('.insurerRadioOption');
    if (sliderValue < 50) {
        customerRadioButton.next().addClass('ui-state-active');
        insurerRadioButton.next().removeClass('ui-state-active');
    } else {
        insurerRadioButton.next().addClass('ui-state-active');
        customerRadioButton.next().removeClass('ui-state-active');
    }
    var customerValue = parseFloat(value * (1 - (sliderValue / 100))).toFixed(decimalPlacesToDisplay);
    var insurerValue = parseFloat(value * (sliderValue / 100)).toFixed(decimalPlacesToDisplay);
    if (sliderValue != 0 && sliderValue != 100) {
        var cent = value.substring(value.length - 1);
        if (cent.search('1|3|5|7|9') > -1) {
            value = parseFloat(value.substring(0, value.length - 1));
        }
        customerValue = parseFloat(value * (1 - (sliderValue / 100))).toFixed(decimalPlacesToDisplay);
        insurerValue = parseFloat(value * (sliderValue / 100)).toFixed(decimalPlacesToDisplay);
        if (cent.search('1|3|5|7|9') > -1) {
            var split = parseFloat((Math.floor(cent / 2) / 100));
            customerValue = (parseFloat(customerValue) + parseFloat(split)).toFixed(decimalPlacesToDisplay);
            insurerValue = (parseFloat(insurerValue) + parseFloat(split) + parseFloat(0.01)).toFixed(decimalPlacesToDisplay);
        }
    }
    customerAmountLabel.html(displayMonetaryValue(customerValue));
    insurerAmountLabel.html(displayMonetaryValue(insurerValue));
    customerAmountInput.val(displayMonetaryValue(customerValue));
    insurerAmountInput.val(displayMonetaryValue(insurerValue));
    onLineItemAllocationChanged();
}
function enableControl(container) {
    var lineItemAmountSlider = container.find('.lineItemAmountSlider');
    var customerAmountLabel = container.find('.customerLineItemAmount');
    var insurerAmountLabel = container.find('.insurerLineItemAmount');
    container.find('.lineItemSplitterButtonGroup').find('input:radio').button('enable');
    lineItemAmountSlider.slider('enable');
    customerAmountLabel.removeClass('disabledAmountLabel');
    insurerAmountLabel.removeClass('disabledAmountLabel');
}
function disableControl(container) {
    var lineItemAmountSlider = container.find('.lineItemAmountSlider');
    var customerAmountLabel = container.find('.customerLineItemAmount');
    var insurerAmountLabel = container.find('.insurerLineItemAmount');
    container.find('.lineItemSplitterButtonGroup').find('input:radio').button('disable');
    container.find('.lineItemSplitterButtonGroup').find('input:radio').button('refresh');
    lineItemAmountSlider.slider('disable');
    customerAmountLabel.addClass('disabledAmountLabel');
    insurerAmountLabel.addClass('disabledAmountLabel');
}
function onLineItemAllocationChanged() {
    $('body').trigger('lineItemAllocationChanged', ['Custom', 'Event']);
}