﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class CarGroup
    {
        [Key]
        public int ID { get; set; }
        public string CarGroupCode { get; set; }
        public string Description { get; set; }
    }
}