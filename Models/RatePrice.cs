﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class RatePrice
    {
        [Key]
        public int RatePriceID {get; set;}
	    public RateVersion RateVersion {get; set;}
	    public CarGroup CarGroup {get; set;}
	    public decimal prKm  {get; set;}
	    public decimal prDay  {get; set;}
	    public decimal prWeek {get; set;}
        public decimal prMonth { get; set; }
    }
}