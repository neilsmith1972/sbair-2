﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class OtherInsurancePriceVersion
    {
        [Key]
        public int ID { get; set; }
        public int MappedID { get; set; }
        public OtherInsuranceCode OtherInsuranceCode { get; set; }
        public Country CountryCode { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Informaton { get; set; }
        public decimal VAT_Pcent { get; set; }
    }
}