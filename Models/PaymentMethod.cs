﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class PaymentMethod
    {
        [Key]
        public int ID { get; set; }
        public string PaymentMethodName { get; set; }
        public decimal CustomerPays { get; set; }
        public decimal InsurancePays { get; set; }
    }
}