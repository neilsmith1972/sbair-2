﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class OtherInsuranceCode
    {
        [Key]
        public int ID { get; set; }
        public string Insurance_Code { get; set; }
        public string Name { get; set; }
    }
}