﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class RateVersion
    {
        [Key]
        public int RateVersionID {get; set;}
	    public virtual Country CountryCode {get; set;}
	    public virtual RateCode RateCode {get; set;}
	    public DateTime FromDate {get; set;}
	    public DateTime ToDate {get; set;}
	    public int NbDaysWeek {get; set;}
	    public int NbDaysMonth {get; set;}
        public string Information { get; set; }
    }
}