﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class audit
    {
        [Key]
        public int ID { get; set; }
        public virtual Status Status { get; set; }
        public DateTime TimeChanged { get; set; }
        public string Comment { get; set; }
        public string RentalNumber { get; set; }
    }
}