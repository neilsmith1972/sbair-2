﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class FeeType
    {
        [Key]
        public int ID { get; set; }
        public string FeeCode { get; set;}
        public string Name { get; set; }
    }
}