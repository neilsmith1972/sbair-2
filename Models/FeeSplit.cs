﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class FeeSplit
    {
        [Key]
        public int ID { get; set; }
        public virtual Country CountryCode { get; set; }
        public virtual FeeType FeeType { get; set; }
        public string Description { get; set; }
        public decimal PCentCustomerPart { get; set; }
    }
}