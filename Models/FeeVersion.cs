﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class FeeVersion
    {
        [Key]
        public int ID { get; set; }
        public Country CountryCode { get; set; }
        public FeeType FeeCode { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}