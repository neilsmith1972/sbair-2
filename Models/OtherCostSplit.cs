﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class OtherCostSplit
    {
        [Key]
        public int ID { get; set; }
        public Country CountryCode { get; set; }
        public OtherCost Code { get; set; }
        public string Description { get; set; }
        public decimal PercentCustomerPays { get; set; }
    }
}