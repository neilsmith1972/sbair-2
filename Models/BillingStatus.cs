﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class BillingStatus
    {
        [Key]
        public int BillingStatusID { get; set; }
        public string BillingStatusText { get; set; }
    }
}