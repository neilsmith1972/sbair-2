﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class VATSplit
    {
        [Key]
        public int ID { get; set; }
        //public virtual Country CountryCode { get; set; }
        public string Description { get; set; }
        public string PrintingDescription { get; set; }
        public decimal PercentageCustomerPays { get; set; }
        public string AppliedTo { get; set; }
    }
}