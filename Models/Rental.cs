﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class Rental
    {
        [Key]
        public string RentalAgreementNumber { get; set; }
        public string ReservationNumber { get; set; }
        public string CheckoutLocationCode { get; set; }
        public string CheckoutLocationName { get; set; }
        public DateTime CheckoutDateTime { get; set; }
        public string CheckinLocationCode { get; set; }
        public string CheckinLocationName { get; set; }
        public DateTime CheckinDateTime { get; set; }
        public string RateCode { get; set; }
        public string DamageNumber { get; set; }
        public string ReservationCarGroup { get; set; }
        public string ChargedCarGroup { get; set; }
        public string RentedCarGroup { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public string RenterName { get; set; }
        public string RenterCompanyName { get; set; }
        public string RenterBillingName { get; set; }
        public string RenterBillingAddress1 { get; set; }
        public string RenterBillingAddress2 { get; set; }
        public string RenterBillingAddress3 { get; set; }
        public string RenterBillingCountryCode { get; set; }
        public string InsuranceBillingCompany { get; set; }
        public string InsuranceBillingName { get; set; }
        public string InsuranceBillingAddress1 { get; set; }
        public string InsuranceBillingAddress2 { get; set; }
        public string InsuranceBillingAddress3 { get; set; }
        public string InsuranceBillingCountryCode { get; set; }
        public decimal TotalRentalChargedAmount { get; set; }
        public decimal NetTimeDistanceAmount { get; set; }
        public decimal TPIAmount { get; set; }
        public decimal PAIAmount { get; set; }
        public decimal CDWAmount { get; set; }
        public decimal OneWayFeeAmount { get; set; }
        public decimal SurchargeAmount { get; set; }
        public decimal FuelAmount { get; set; }
        public decimal CollectionAmount { get; set; }
        public decimal MiscAmount { get; set; }
        public string Status { get; set; }
        public decimal prKM { get; set; }
        public decimal prDay { get; set; }
        public decimal prWeek { get; set; }
        public decimal prMonth { get; set; }
    }
}