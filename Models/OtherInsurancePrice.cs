﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class OtherInsurancePrice
    {
        [Key]
        public int ID { get; set; }
        public OtherInsurancePriceVersion OtherInsurancePriceVersion { get; set; }
        //public CarGroup CarGroup { get; set; }
        public string CarGroup { get; set; }
        public decimal Price { get; set; }
    }
}