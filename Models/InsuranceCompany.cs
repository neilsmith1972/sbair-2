﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Models
{
    public class InsuranceCompany
    {
        [Key]
        public int InsuranceCompanyID { get; set; }
        public virtual Country Country {get; set;}
        public string Name {get;set;}
        public string OrgNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonTitle { get; set; }
        public string Phone { get; set; }
        public string AccountNb { get; set; }
        public string WizzardNb { get; set; }
        public string AWD { get; set; }
        public string RateCode { get; set; }
        public string VATSplitInformation { get; set; }
        public string MaxDaysInformation { get; set; }
    }
}