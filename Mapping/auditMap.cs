using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class auditMap : EntityTypeConfiguration<audit>
	{
		public auditMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.TimeChanged });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("audits");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.TimeChanged).HasColumnName("TimeChanged");
			this.Property(t => t.Comment).HasColumnName("Comment");
			this.Property(t => t.RentalNumber).HasColumnName("RentalNumber");
			this.Property(t => t.Status_ID).HasColumnName("Status_ID");
		}
	}
}

