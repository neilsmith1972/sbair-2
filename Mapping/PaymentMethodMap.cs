using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class PaymentMethodMap : EntityTypeConfiguration<PaymentMethod>
	{
		public PaymentMethodMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.CustomerPays, t.InsurancePays });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.CustomerPays)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.InsurancePays)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("PaymentMethods");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.PaymentMethodName).HasColumnName("PaymentMethodName");
			this.Property(t => t.CustomerPays).HasColumnName("CustomerPays");
			this.Property(t => t.InsurancePays).HasColumnName("InsurancePays");
		}
	}
}

