using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class OtherInsurancePriceMap : EntityTypeConfiguration<OtherInsurancePrice>
	{
		public OtherInsurancePriceMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.Price });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.Price)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("OtherInsurancePrices");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.CarGroup).HasColumnName("CarGroup");
			this.Property(t => t.Price).HasColumnName("Price");
			this.Property(t => t.OtherInsurancePriceVersion_ID).HasColumnName("OtherInsurancePriceVersion_ID");
		}
	}
}

