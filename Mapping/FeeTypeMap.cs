using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class FeeTypeMap : EntityTypeConfiguration<FeeType>
	{
		public FeeTypeMap()
		{
			// Primary Key
			this.HasKey(t => t.ID);

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("FeeTypes");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.FeeCode).HasColumnName("FeeCode");
			this.Property(t => t.Name).HasColumnName("Name");
		}
	}
}

