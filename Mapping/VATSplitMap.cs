using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class VATSplitMap : EntityTypeConfiguration<VATSplit>
	{
		public VATSplitMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.PercentageCustomerPays });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.PercentageCustomerPays)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("VATSplits");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.Description).HasColumnName("Description");
			this.Property(t => t.PrintingDescription).HasColumnName("PrintingDescription");
			this.Property(t => t.PercentageCustomerPays).HasColumnName("PercentageCustomerPays");
			this.Property(t => t.AppliedTo).HasColumnName("AppliedTo");
		}
	}
}

