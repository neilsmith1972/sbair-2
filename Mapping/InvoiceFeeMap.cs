using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class InvoiceFeeMap : EntityTypeConfiguration<InvoiceFee>
	{
		public InvoiceFeeMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.FromDate, t.ToDate, t.Price });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.Price)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("InvoiceFees");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.FromDate).HasColumnName("FromDate");
			this.Property(t => t.ToDate).HasColumnName("ToDate");
			this.Property(t => t.Price).HasColumnName("Price");
			this.Property(t => t.CountryCode_ID).HasColumnName("CountryCode_ID");
		}
	}
}

