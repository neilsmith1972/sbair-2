using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class InvoicedRentalMap : EntityTypeConfiguration<InvoicedRental>
	{
		public InvoicedRentalMap()
		{
			// Primary Key
			this.HasKey(t => t.InvoicedRentalId);

			// Properties
			this.Property(t => t.RateCode)
				.IsFixedLength()
				.HasMaxLength(2);
				
			this.Property(t => t.AvisRANumber)
				.HasMaxLength(13);
				
			this.Property(t => t.RANumber)
				.IsFixedLength()
				.HasMaxLength(9);
				
			this.Property(t => t.RANumberPrefix)
				.HasMaxLength(1);
				
			this.Property(t => t.SplitBillingInd)
				.IsFixedLength()
				.HasMaxLength(1);
				
			// Table & Column Mappings
			this.ToTable("InvoicedRental");
			this.Property(t => t.InvoicedRentalId).HasColumnName("InvoicedRentalId");
			this.Property(t => t.Source).HasColumnName("Source");
			this.Property(t => t.RateCode).HasColumnName("RateCode");
			this.Property(t => t.AvisRANumber).HasColumnName("AvisRANumber");
			this.Property(t => t.RANumber).HasColumnName("RANumber");
			this.Property(t => t.RANumberPrefix).HasColumnName("RANumberPrefix");
			this.Property(t => t.CheckInYear).HasColumnName("CheckInYear");
			this.Property(t => t.SplitBillingInd).HasColumnName("SplitBillingInd");
			this.Property(t => t.AgressoInvoiceNo).HasColumnName("AgressoInvoiceNo");
			this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
			this.Property(t => t.InsertedTimestamp).HasColumnName("InsertedTimestamp");
			this.Property(t => t.Processed).HasColumnName("Processed");
			this.Property(t => t.OK).HasColumnName("OK");
            this.Property(t => t.MVANumber).HasColumnName("MVANumber");

		}
	}
}

