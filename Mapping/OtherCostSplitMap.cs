using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class OtherCostSplitMap : EntityTypeConfiguration<OtherCostSplit>
	{
		public OtherCostSplitMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.PercentCustomerPays });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.PercentCustomerPays)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("OtherCostSplits");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.Description).HasColumnName("Description");
			this.Property(t => t.PercentCustomerPays).HasColumnName("PercentCustomerPays");
			this.Property(t => t.CountryCode_ID).HasColumnName("CountryCode_ID");
			this.Property(t => t.Code_ID).HasColumnName("Code_ID");
		}
	}
}

