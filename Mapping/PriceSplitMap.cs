using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class PriceSplitMap : EntityTypeConfiguration<PriceSplit>
	{
		public PriceSplitMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.PCentCustomerPartTime, t.PCentCustomerPartMileage, t.PercentageCustomerPays, t.MileageByUnit, t.TimeByUnit, t.InsurancePayAFixedAmount, t.FixedAmountIsVATIncluded });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.PCentCustomerPartTime)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.PCentCustomerPartMileage)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.PercentageCustomerPays)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("PriceSplits");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.Description).HasColumnName("Description");
			this.Property(t => t.PrintingDescription).HasColumnName("PrintingDescription");
			this.Property(t => t.PCentCustomerPartTime).HasColumnName("PCentCustomerPartTime");
			this.Property(t => t.PCentCustomerPartMileage).HasColumnName("PCentCustomerPartMileage");
			this.Property(t => t.PercentageCustomerPays).HasColumnName("PercentageCustomerPays");
			this.Property(t => t.MileageByUnit).HasColumnName("MileageByUnit");
			this.Property(t => t.TimeByUnit).HasColumnName("TimeByUnit");
			this.Property(t => t.InsurancePayAFixedAmount).HasColumnName("InsurancePayAFixedAmount");
			this.Property(t => t.FixedAmountIsVATIncluded).HasColumnName("FixedAmountIsVATIncluded");
		}
	}
}

