using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class StatusMap : EntityTypeConfiguration<Status>
	{
		public StatusMap()
		{
			// Primary Key
			this.HasKey(t => t.ID);

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("Status");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.StatusText).HasColumnName("StatusText");
		}
	}
}

