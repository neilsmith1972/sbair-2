using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class SplitBillingMap : EntityTypeConfiguration<SplitBilling>
	{
		public SplitBillingMap()
		{
			// Primary Key
			this.HasKey(t => t.RentalId);

			// Properties
			this.Property(t => t.RentalAgreementNumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalTransactionType)
				.IsRequired()
				.HasMaxLength(50);
				
			this.Property(t => t.RentalReservationNumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalCheckoutLocationCode)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalCheckoutLocationText)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalCheckoutCorporateLicenseeIndicator)
				.IsFixedLength()
				.HasMaxLength(1);
				
			this.Property(t => t.RentalCheckinLocationCode)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalCheckinLocationText)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalCheckinCorporateLicenseeIndicator)
				.IsFixedLength()
				.HasMaxLength(1);
				
			this.Property(t => t.RentalRateCode)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalIATANumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalAWDNumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalACTONumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalWizardOfAvisNumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalDamageNumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalRemarks)
				.HasMaxLength(70);
				
			this.Property(t => t.RentalLocalContactText)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalCustomerName)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalCompanyName)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalRenterBillingName)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalRenterAddress1Text)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalRenterAddress2Text)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalRenterAddressCityText)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalRenterBillingCountryCode)
				.IsFixedLength()
				.HasMaxLength(2);
				
			this.Property(t => t.RentalRenterCreditClubCode)
				.IsFixedLength()
				.HasMaxLength(2);
				
			this.Property(t => t.RentalRenterCreditCardNumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalInsuranceBillingCompany)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalInsuranceBillingName)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalInsuranceBillingAddress1)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalInsuranceBillingAddress2)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalInsuranceBillingAddress3)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalInsuranceBillingCountryCode)
				.IsFixedLength()
				.HasMaxLength(2);
				
			this.Property(t => t.RentalInsuranceCreditClubCode)
				.IsFixedLength()
				.HasMaxLength(2);
				
			this.Property(t => t.RentalInsuranceCreditCardNumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalVehicleLicensePlateNumber)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalVehicleDescription)
				.HasMaxLength(50);
				
			this.Property(t => t.RentalOwningCorpLicIndicator)
				.IsFixedLength()
				.HasMaxLength(1);
				
			this.Property(t => t.RentalVehicleClassChargedCode)
				.IsFixedLength()
				.HasMaxLength(1);
				
			this.Property(t => t.RentalConfirmedVehicleClassCode)
				.IsFixedLength()
				.HasMaxLength(1);
				
			this.Property(t => t.RentalPreferredVehicleClassCode)
				.IsFixedLength()
				.HasMaxLength(1);
				
			this.Property(t => t.RentalMiscChargeCode)
				.IsFixedLength()
				.HasMaxLength(1);
				
			this.Property(t => t.InsuranceCompanyContact)
				.HasMaxLength(50);
				
            //this.Property(t => t.InsuranceInvoiceNumber)
            //    .HasMaxLength(50);
				
            //this.Property(t => t.RenterInvoiceNumber)
            //    .HasMaxLength(50);
				
			// Table & Column Mappings
			this.ToTable("SplitBilling");
			this.Property(t => t.RentalId).HasColumnName("RentalId");
			this.Property(t => t.Timestamp).HasColumnName("Timestamp");
			this.Property(t => t.Source).HasColumnName("Source");
			this.Property(t => t.RentalAgreementNumber).HasColumnName("RentalAgreementNumber");
			this.Property(t => t.RentalCheckInYear).HasColumnName("RentalCheckInYear");
			this.Property(t => t.RentalTransactionType).HasColumnName("RentalTransactionType");
			this.Property(t => t.RentalReservationNumber).HasColumnName("RentalReservationNumber");
			this.Property(t => t.RentalCheckoutDateTime).HasColumnName("RentalCheckoutDateTime");
			this.Property(t => t.RentalCheckoutLocationCode).HasColumnName("RentalCheckoutLocationCode");
			this.Property(t => t.RentalCheckoutLocationText).HasColumnName("RentalCheckoutLocationText");
			this.Property(t => t.RentalCheckoutCorporateLicenseeIndicator).HasColumnName("RentalCheckoutCorporateLicenseeIndicator");
			this.Property(t => t.RentalCheckinDateTime).HasColumnName("RentalCheckinDateTime");
			this.Property(t => t.RentalCheckinLocationCode).HasColumnName("RentalCheckinLocationCode");
			this.Property(t => t.RentalCheckinLocationText).HasColumnName("RentalCheckinLocationText");
			this.Property(t => t.RentalCheckinCorporateLicenseeIndicator).HasColumnName("RentalCheckinCorporateLicenseeIndicator");
			this.Property(t => t.RentalRateCode).HasColumnName("RentalRateCode");
			this.Property(t => t.RentalIATANumber).HasColumnName("RentalIATANumber");
			this.Property(t => t.RentalAWDNumber).HasColumnName("RentalAWDNumber");
			this.Property(t => t.RentalACTONumber).HasColumnName("RentalACTONumber");
			this.Property(t => t.RentalWizardOfAvisNumber).HasColumnName("RentalWizardOfAvisNumber");
			this.Property(t => t.RentalDamageNumber).HasColumnName("RentalDamageNumber");
			this.Property(t => t.RentalRemarks).HasColumnName("RentalRemarks");
			this.Property(t => t.RentalLocalContactText).HasColumnName("RentalLocalContactText");
			this.Property(t => t.RentalCustomerName).HasColumnName("RentalCustomerName");
			this.Property(t => t.RentalCompanyName).HasColumnName("RentalCompanyName");
			this.Property(t => t.RentalRenterBillingName).HasColumnName("RentalRenterBillingName");
			this.Property(t => t.RentalRenterAddress1Text).HasColumnName("RentalRenterAddress1Text");
			this.Property(t => t.RentalRenterAddress2Text).HasColumnName("RentalRenterAddress2Text");
			this.Property(t => t.RentalRenterAddressCityText).HasColumnName("RentalRenterAddressCityText");
			this.Property(t => t.RentalRenterBillingCountryCode).HasColumnName("RentalRenterBillingCountryCode");
			this.Property(t => t.RentalRenterCreditClubCode).HasColumnName("RentalRenterCreditClubCode");
			this.Property(t => t.RentalRenterCreditCardNumber).HasColumnName("RentalRenterCreditCardNumber");
			this.Property(t => t.RentalRenterInvoiceNumber).HasColumnName("RentalRenterInvoiceNumber");
			this.Property(t => t.RentalRenterTotalAmount).HasColumnName("RentalRenterTotalAmount");
			this.Property(t => t.RentalInsuranceBillingCompany).HasColumnName("RentalInsuranceBillingCompany");
			this.Property(t => t.RentalInsuranceBillingName).HasColumnName("RentalInsuranceBillingName");
			this.Property(t => t.RentalInsuranceBillingAddress1).HasColumnName("RentalInsuranceBillingAddress1");
			this.Property(t => t.RentalInsuranceBillingAddress2).HasColumnName("RentalInsuranceBillingAddress2");
			this.Property(t => t.RentalInsuranceBillingAddress3).HasColumnName("RentalInsuranceBillingAddress3");
			this.Property(t => t.RentalInsuranceBillingCountryCode).HasColumnName("RentalInsuranceBillingCountryCode");
			this.Property(t => t.RentalInsuranceCreditClubCode).HasColumnName("RentalInsuranceCreditClubCode");
			this.Property(t => t.RentalInsuranceCreditCardNumber).HasColumnName("RentalInsuranceCreditCardNumber");
			this.Property(t => t.RentalInsuranceInvoiceNumber).HasColumnName("RentalInsuranceInvoiceNumber");
			this.Property(t => t.RentalInsuranceTotalAmount).HasColumnName("RentalInsuranceTotalAmount");
			this.Property(t => t.RentalVehicleLicensePlateNumber).HasColumnName("RentalVehicleLicensePlateNumber");
			this.Property(t => t.RentalVehicleDescription).HasColumnName("RentalVehicleDescription");
			this.Property(t => t.RentalOwningCorpLicIndicator).HasColumnName("RentalOwningCorpLicIndicator");
			this.Property(t => t.RentalVehicleClassChargedCode).HasColumnName("RentalVehicleClassChargedCode");
			this.Property(t => t.RentalConfirmedVehicleClassCode).HasColumnName("RentalConfirmedVehicleClassCode");
			this.Property(t => t.RentalPreferredVehicleClassCode).HasColumnName("RentalPreferredVehicleClassCode");
			this.Property(t => t.KMDriven).HasColumnName("KMDriven");
            this.Property(t => t.KMCheckin).HasColumnName("KmCheckin");
			this.Property(t => t.RentalTotalRentalChargesAmount).HasColumnName("RentalTotalRentalChargesAmount");
			this.Property(t => t.NetTimeDistanceAmount).HasColumnName("NetTimeDistanceAmount");
			this.Property(t => t.RentalOneWayFeeAmount).HasColumnName("RentalOneWayFeeAmount");
			this.Property(t => t.RentalSurchargeAmount).HasColumnName("RentalSurchargeAmount");
			this.Property(t => t.RentalDeliveryChargeAmount).HasColumnName("RentalDeliveryChargeAmount");
			this.Property(t => t.RentalCollectionChargeAmount).HasColumnName("RentalCollectionChargeAmount");
			this.Property(t => t.RentalGasChargesAmount).HasColumnName("RentalGasChargesAmount");
			this.Property(t => t.RentalMiscChargeAmount).HasColumnName("RentalMiscChargeAmount");
			this.Property(t => t.RentalCDWAmount).HasColumnName("RentalCDWAmount");

            this.Property(t => t.RentalTPAmount).HasColumnName("RentalTPAmount");

			this.Property(t => t.RentalPAIAmount).HasColumnName("RentalPAIAmount");
			this.Property(t => t.RentalTPIAmount).HasColumnName("RentalTPIAmount");
			this.Property(t => t.RentalContractFeeAmount).HasColumnName("RentalContractFeeAmount");
			this.Property(t => t.RentalMiscChargeCode).HasColumnName("RentalMiscChargeCode");
			this.Property(t => t.RentalTaxNumber).HasColumnName("RentalTaxNumber");
			this.Property(t => t.RentalMilesChargedCount).HasColumnName("RentalMilesChargedCount");
			this.Property(t => t.RentalFreeMileageCount).HasColumnName("RentalFreeMileageCount");
			this.Property(t => t.RentalMileageRateAmount).HasColumnName("RentalMileageRateAmount");
			this.Property(t => t.RentalHourlyRateAmount).HasColumnName("RentalHourlyRateAmount");
			this.Property(t => t.RentalDailyRateAmount).HasColumnName("RentalDailyRateAmount");
			this.Property(t => t.RentalWeeklyRateAmount).HasColumnName("RentalWeeklyRateAmount");
			this.Property(t => t.RentalMonthlyRateAmount).HasColumnName("RentalMonthlyRateAmount");
			this.Property(t => t.RentalAdditionalDaysAmount).HasColumnName("RentalAdditionalDaysAmount");
			this.Property(t => t.RentalAdditionalHoursAmount).HasColumnName("RentalAdditionalHoursAmount");
			this.Property(t => t.RentalCDWLiabilityAmount).HasColumnName("RentalCDWLiabilityAmount");
			this.Property(t => t.RentalCDWPerDayAmount).HasColumnName("RentalCDWPerDayAmount");
			this.Property(t => t.RentalPAIPerDayAmount).HasColumnName("RentalPAIPerDayAmount");
			this.Property(t => t.RentalALIPerDayAmount).HasColumnName("RentalALIPerDayAmount");
			this.Property(t => t.RentalPEPPerDayAmount).HasColumnName("RentalPEPPerDayAmount");
			this.Property(t => t.RentalFreeDaysNumber).HasColumnName("RentalFreeDaysNumber");
			this.Property(t => t.RentalNetRateCDWAmount).HasColumnName("RentalNetRateCDWAmount");
			this.Property(t => t.RentalRNetRateCDWAmount).HasColumnName("RentalRNetRateCDWAmount");
			this.Property(t => t.RentalNetRatePAIAmount).HasColumnName("RentalNetRatePAIAmount");
			this.Property(t => t.RentalRNetRatePAIAmount).HasColumnName("RentalRNetRatePAIAmount");
			this.Property(t => t.RentalNetRatePEPAmount).HasColumnName("RentalNetRatePEPAmount");
			this.Property(t => t.RentalRNetRatePEPAmount).HasColumnName("RentalRNetRatePEPAmount");
			this.Property(t => t.RentalNetRateALIAmount).HasColumnName("RentalNetRateALIAmount");
			this.Property(t => t.RentalRNetRateALIAmount).HasColumnName("RentalRNetRateALIAmount");
			this.Property(t => t.RentALInclusiveNetRateInd).HasColumnName("RentALInclusiveNetRateInd");
			this.Property(t => t.RentalGasCreditAmount).HasColumnName("RentalGasCreditAmount");
			this.Property(t => t.Country).HasColumnName("Country");
			this.Property(t => t.PaymentMethod).HasColumnName("PaymentMethod");
			this.Property(t => t.Status).HasColumnName("StatusID");
			this.Property(t => t.PriceSplit).HasColumnName("PriceSplit");
			this.Property(t => t.VATSplit).HasColumnName("VATSplit");
			this.Property(t => t.CDWSplit).HasColumnName("CDWSplit");
            this.Property(t => t.SuperTPSplit).HasColumnName("SuperTPSplit");
			this.Property(t => t.RAISplit).HasColumnName("RAISplit");
			this.Property(t => t.AirportFeeSplit).HasColumnName("AirportFeeSplit");
			this.Property(t => t.ReturnFeeSplit).HasColumnName("ReturnFeeSplit");
			this.Property(t => t.DeliveryFeeSplit).HasColumnName("DeliveryFeeSplit");
			this.Property(t => t.RefuelingSplit).HasColumnName("RefuelingSplit");
			this.Property(t => t.OtherFeeSplit).HasColumnName("OtherFeeSplit");
			this.Property(t => t.VRFSplit).HasColumnName("VRFSplit");
			this.Property(t => t.OneWayFeeSplit).HasColumnName("OneWayFeeSplit");
			this.Property(t => t.MiscCostsSplit).HasColumnName("MiscCostsSplit");
            this.Property(t => t.MiscCostRemarks).HasColumnName("MiscCostRemarks");
			this.Property(t => t.InvoiceFeeSplit).HasColumnName("InvoiceFeeSplit");
			this.Property(t => t.PriceSplitTotal).HasColumnName("PriceSplitTotal");
			this.Property(t => t.PriceSplitCustomerPays).HasColumnName("PriceSplitCustomerPays");
			this.Property(t => t.VATTotal).HasColumnName("VATTotal");
			this.Property(t => t.VATCustomerPays).HasColumnName("VATCustomerPays");
			this.Property(t => t.SuperCDWTotal).HasColumnName("SuperCDWTotal");
			this.Property(t => t.SuperCDWCustomerPays).HasColumnName("SuperCDWCustomerPays");

            this.Property(t => t.SuperTPTotal).HasColumnName("SuperTPTotal");
            this.Property(t => t.SuperTPCustomerPays).HasColumnName("SuperTPCustomerPays");

			this.Property(t => t.RAITotal).HasColumnName("RAITotal");
			this.Property(t => t.RAICustomerPays).HasColumnName("RAICustomerPays");
			this.Property(t => t.AirportFeeTotal).HasColumnName("AirportFeeTotal");
			this.Property(t => t.AirportFeeCustomerPays).HasColumnName("AirportFeeCustomerPays");
			this.Property(t => t.OneWayTotal).HasColumnName("OneWayTotal");
			this.Property(t => t.OneWayCustomerPays).HasColumnName("OneWayCustomerPays");
			this.Property(t => t.RefuellingTotal).HasColumnName("RefuellingTotal");
			this.Property(t => t.RefuellingCustomerPays).HasColumnName("RefuellingCustomerPays");
			this.Property(t => t.DeliveryTotal).HasColumnName("DeliveryTotal");
			this.Property(t => t.DeliveryCustomerPays).HasColumnName("DeliveryCustomerPays");
			this.Property(t => t.ReturnTotal).HasColumnName("ReturnTotal");
			this.Property(t => t.ReturnCustomerPays).HasColumnName("ReturnCustomerPays");
			this.Property(t => t.VRFTotal).HasColumnName("VRFTotal");
			this.Property(t => t.VRFCustomerPays).HasColumnName("VRFCustomerPays");
			this.Property(t => t.InvoiceFeeTotal).HasColumnName("InvoiceFeeTotal");
			this.Property(t => t.InvoiceFeeCustomerPays).HasColumnName("InvoiceFeeCustomerPays");
			this.Property(t => t.MiscCostsTotal).HasColumnName("MiscCostsTotal");
			this.Property(t => t.MiscCostsCustomerPays).HasColumnName("MiscCostsCustomerPays");
			this.Property(t => t.OtherFeeTotal).HasColumnName("OtherFeeTotal");
			this.Property(t => t.OtherFeeCustomerPays).HasColumnName("OtherFeeCustomerPays");
			this.Property(t => t.KmCoveredByInsurance).HasColumnName("KmCoveredByInsurance");
			this.Property(t => t.DaysCoveredByInsurance).HasColumnName("DaysCoveredByInsurance");
			this.Property(t => t.AlreadyPaidByCustomerAmount).HasColumnName("AlreadyPaidByCustomerAmount");
			this.Property(t => t.AlreadyPaidByInsuranceAmount).HasColumnName("AlreadyPaidByInsuranceAmount");
			this.Property(t => t.InvoiceAmountInsurance).HasColumnName("InvoiceAmountInsurance");
			this.Property(t => t.InvoiceAmountCustomer).HasColumnName("InvoiceAmountCustomer");
			this.Property(t => t.Remarks).HasColumnName("Remarks");
			this.Property(t => t.InsuranceCompanyContact).HasColumnName("InsuranceCompanyContact");
            //this.Property(t => t.InsuranceInvoiceNumber).HasColumnName("InsuranceInvoiceNumber");
            //this.Property(t => t.RenterInvoiceNumber).HasColumnName("RenterInvoiceNumber");
            this.Property(t => t.RentalRenterInvoiceFeeAmount).HasColumnName("RentalRenterInvoiceFeeAmount");
		    this.Property(t => t.AmountCoveredByInsurance).HasColumnName("AmountCoveredByInsurance").HasPrecision(18,4);
            this.Property(t => t.MVANumber).HasColumnName("MVANumber");
            this.Property(t => t.AmendedDate).HasColumnName("AmendedDate");
            this.Property(t => t.NewInvoiceRequired).HasColumnName("NewInvoiceRequired");
            this.Property(t => t.DateArchived).HasColumnName("DateArchived");
            this.Property(t => t.IsArchive).HasColumnName("IsArchive");

            this.Property(t => t.OverrideDaysAmount).HasColumnName("OverrideDaysAmount");
            this.Property(t => t.OverrideKMAmount).HasColumnName("OverrideKMAmount");
            this.Property(t => t.KMCheckout).HasColumnName("KMCheckout");

            this.Property(t => t.SentToWizard).HasColumnName("SentToWizard");
            this.Property(t => t.WizardSuccess).HasColumnName("WizardSuccess");
            this.Property(t => t.VoucherValueUpdate).HasColumnName("VoucherValueUpdate");
            this.Property(t => t.TotalUpdate).HasColumnName("TotalUpdate");
            this.Property(t => t.MiscCode).HasColumnName("MiscCode");
            this.Property(t => t.OnTheRoadCode).HasColumnName("OnTheRoadCode");
            this.Property(t => t.ParentRentalId).HasColumnName("ParentRentalId");
            this.Property(t => t.AVISComments).HasColumnName("AVISComments");
            this.Property(t => t.CCIUpdated).HasColumnName("CCIUpdated");
            this.Property(t => t.CustomerAddressUpdated).HasColumnName("CustomerAddressUpdated");
            this.Property(t => t.ManualInvoice).HasColumnName("ManualInvoice");

            this.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy");
            this.Property(t => t.Void).HasColumnName("Void");
            this.Property(t => t.OverrideWizardLock).HasColumnName("OverrideWizardLock");
            //this.Property(t => t.TMDiscount).HasColumnName("TMDiscount");
            this.Property(t => t.MiscCostsInsurancePays).HasColumnName("MiscCostsInsurancePays");

            this.Property(t => t.Output_Type_C).HasColumnName("Output_Type_C");
            this.Property(t => t.Output_Type_I).HasColumnName("Output_Type_I");
        }
	}
}

