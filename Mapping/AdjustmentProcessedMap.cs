using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class AdjustmentProcessedMap : EntityTypeConfiguration<AdjustmentProcessed>
	{
        public AdjustmentProcessedMap()
		{
			// Primary Key
			this.HasKey(t => t.AdjustmentId);

			// Properties
            this.Property(t => t.AdjustmentId)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				
			// Table & Column Mappings
            this.ToTable("AdjustmentProcessed");
            this.Property(t => t.AdjustmentId).HasColumnName("AdjustmentId");
            this.Property(t => t.OK).HasColumnName("OK");
            this.Property(t => t.Processed).HasColumnName("Processed");
            this.Property(t => t.RentalAgreementNumber).HasColumnName("RentalAgreementNumber");
            this.Property(t => t.RentalCheckinYear).HasColumnName("RentalCheckinYear");
            this.Property(t => t.SplitBillingId).HasColumnName("SplitBillingId");
            this.Property(t => t.TimeStamp).HasColumnName("TimeStamp");

		}
	}
}

