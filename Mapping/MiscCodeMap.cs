using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class MiscCodeMap : EntityTypeConfiguration<MiscCode>
	{
		public MiscCodeMap()
		{
			// Primary Key
			this.HasKey(t => new { t.MiscCodeLetter });

						
			// Table & Column Mappings
			this.ToTable("MiscCodes");
            this.Property(t => t.MiscCodeLetter).HasColumnName("MiscCodeLetter");
            this.Property(t => t.MiscCodeDescription).HasColumnName("MiscCodeDescription");
		}
	}
}

