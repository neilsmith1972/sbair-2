using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class FeeSplitMap : EntityTypeConfiguration<FeeSplit>
	{
		public FeeSplitMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.PCentCustomerPart });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.PCentCustomerPart)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("FeeSplits");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.Description).HasColumnName("Description");
			this.Property(t => t.PCentCustomerPart).HasColumnName("PCentCustomerPart");
			this.Property(t => t.CountryCode_ID).HasColumnName("CountryCode_ID");
			this.Property(t => t.FeeType_ID).HasColumnName("FeeType_ID");
		}
	}
}

