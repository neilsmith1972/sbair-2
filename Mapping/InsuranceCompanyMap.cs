﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
    public class InsuranceCompanyMap : EntityTypeConfiguration<InsuranceCompany>
    {
        public InsuranceCompanyMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Account });

            // Table & Column Mappings
            this.ToTable("InsuranceCompany");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.Address3).HasColumnName("Address3");
            this.Property(t => t.Country).HasColumnName("Country");

            this.Property(t => t.AwdNumber).HasColumnName("AwdNumber");
            this.Property(t => t.RateCode).HasColumnName("RateCode");
            this.Property(t => t.Account).HasColumnName("Account");
            this.Property(t => t.WizardNumber).HasColumnName("WizardNumber");
            this.Property(t => t.PhoneNumber).HasColumnName("PhoneNumber");

            this.Property(t => t.e_billingindicator).HasColumnName("e_billingindicator");
            this.Property(t => t.emailaddress).HasColumnName("emailaddress");

        }        
        
    }
}