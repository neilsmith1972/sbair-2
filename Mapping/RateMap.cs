﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
    public class RateMap : EntityTypeConfiguration<Rate>
    {
        public RateMap()
		{
			// Primary Key
			this.HasKey(t => t.RateCode);

			// Table & Column Mappings
			this.ToTable("Rate");
            this.Property(t => t.RateCode).HasColumnName("RateCode");
            this.Property(t => t.CountryCode).HasColumnName("CountryCode");
            this.Property(t => t.Description).HasColumnName("Description");

		}
    }
}