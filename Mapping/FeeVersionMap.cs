using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class FeeVersionMap : EntityTypeConfiguration<FeeVersion>
	{
		public FeeVersionMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.FromDate });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("FeeVersions");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.FromDate).HasColumnName("FromDate");
			this.Property(t => t.ToDate).HasColumnName("ToDate");
			this.Property(t => t.CountryCode_ID).HasColumnName("CountryCode_ID");
			this.Property(t => t.FeeCode_ID).HasColumnName("FeeCode_ID");
		}
	}
}

