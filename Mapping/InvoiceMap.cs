using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class InvoiceMap : EntityTypeConfiguration<Invoice>
	{
		public InvoiceMap()
		{
			// Primary Key
			this.HasKey(t => t.InvoiceId);

			// Table & Column Mappings
			this.ToTable("Invoice");
            this.Property(t => t.InvoiceId).HasColumnName("InvoiceId");
			this.Property(t => t.RentalId).HasColumnName("RentalId");
            this.Property(t => t.AvisCompanyAddress1).HasColumnName("AvisCompanyAddress1");
            this.Property(t => t.AvisCompanyAddress2).HasColumnName("AvisCompanyAddress2");
            this.Property(t => t.AvisCompanyAddress3).HasColumnName("AvisCompanyAddress3");
            this.Property(t => t.AvisCompanyName).HasColumnName("AvisCompanyName");
            this.Property(t => t.BillingAddress1).HasColumnName("BillingAddress1");
            this.Property(t => t.BillingAddress2).HasColumnName("BillingAddress2");
            this.Property(t => t.BillingAddress3).HasColumnName("BillingAddress3");
            this.Property(t => t.BillingCompany).HasColumnName("BillingCompany");
            this.Property(t => t.BillingCountryCode).HasColumnName("BillingCountryCode");
            this.Property(t => t.BillingName).HasColumnName("BillingName");
            this.Property(t => t.CustomerType).HasColumnName("CustomerType");
            this.Property(t => t.DueDate).HasColumnName("DueDate");
            this.Property(t => t.InsertedTimestamp).HasColumnName("InsertedTimestamp");
            this.Property(t => t.InvoiceDate).HasColumnName("InvoiceDate");
            this.Property(t => t.InvoiceNumber).HasColumnName("InvoiceNumber");
            this.Property(t => t.InvoiceType).HasColumnName("InvoiceType");
            this.Property(t => t.PaymentType).HasColumnName("PaymentType");
            this.Property(t => t.Printed).HasColumnName("Printed");
            this.Property(t => t.PrintedTimestamp).HasColumnName("PrintedTimestamp");
            this.Property(t => t.VatRegNumber).HasColumnName("VatRegNumber");
            this.Property(t => t.InvoiceFileName).HasColumnName("InvoiceFileName");

		}
	}
}

