using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class OtherCostMap : EntityTypeConfiguration<OtherCost>
	{
		public OtherCostMap()
		{
			// Primary Key
			this.HasKey(t => t.ID);

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("OtherCosts");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.Code).HasColumnName("Code");
			this.Property(t => t.Name).HasColumnName("Name");
		}
	}
}

