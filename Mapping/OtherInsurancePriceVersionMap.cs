using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class OtherInsurancePriceVersionMap : EntityTypeConfiguration<OtherInsurancePriceVersion>
	{
		public OtherInsurancePriceVersionMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ID, t.MappedID, t.FromDate, t.VAT_Pcent });

			// Properties
			this.Property(t => t.ID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.MappedID)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			this.Property(t => t.VAT_Pcent)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				
			// Table & Column Mappings
			this.ToTable("OtherInsurancePriceVersions");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.MappedID).HasColumnName("MappedID");
			this.Property(t => t.FromDate).HasColumnName("FromDate");
			this.Property(t => t.ToDate).HasColumnName("ToDate");
			this.Property(t => t.Informaton).HasColumnName("Informaton");
			this.Property(t => t.VAT_Pcent).HasColumnName("VAT_Pcent");
			this.Property(t => t.OtherInsuranceCode_ID).HasColumnName("OtherInsuranceCode_ID");
			this.Property(t => t.CountryCode_ID).HasColumnName("CountryCode_ID");
		}
	}
}

