using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using avis.sbair.Entities;

namespace avis.sbair.Mapping
{
	public class ExpenseCodeMap : EntityTypeConfiguration<ExpenseCode>
	{
        public ExpenseCodeMap()
		{
			// Primary Key
			this.HasKey(t => new { t.ExpenseCodeLetter });

						
			// Table & Column Mappings
            this.ToTable("ExpenseCodes");
            this.Property(t => t.ExpenseCodeLetter).HasColumnName("ExpenseCodeLetter");
            this.Property(t => t.ExpenseCodeDescription).HasColumnName("ExpenseCodeDescription");
		}
	}
}

