﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18052
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace avis.sbair.InvoiceService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DocumentStoreType", Namespace="http://schemas.datacontract.org/2004/07/Avis.InvoiceManager.DocumentDiggerService" +
        "")]
    public enum DocumentStoreType : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        All = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Aurora = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Onbase = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        AgressoFines = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        EpccAjCreditNote = 4,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        InvoiceTool = 5,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Screen30Invoice = 6,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DtoDocumentSearchResult", Namespace="http://schemas.datacontract.org/2004/07/Avis.InvoiceManager.DocumentDiggerService" +
        "")]
    [System.SerializableAttribute()]
    public partial class DtoDocumentSearchResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private avis.sbair.InvoiceService.DtoDocumentStoreSearchResult[] DocumentStoreSearchResultsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public avis.sbair.InvoiceService.DtoDocumentStoreSearchResult[] DocumentStoreSearchResults {
            get {
                return this.DocumentStoreSearchResultsField;
            }
            set {
                if ((object.ReferenceEquals(this.DocumentStoreSearchResultsField, value) != true)) {
                    this.DocumentStoreSearchResultsField = value;
                    this.RaisePropertyChanged("DocumentStoreSearchResults");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DtoDocumentStoreSearchResult", Namespace="http://schemas.datacontract.org/2004/07/Avis.InvoiceManager.DocumentDiggerService" +
        "")]
    [System.SerializableAttribute()]
    public partial class DtoDocumentStoreSearchResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private avis.sbair.InvoiceService.DtoDocument[] DocumentsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private avis.sbair.InvoiceService.DocumentStoreType TypeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public avis.sbair.InvoiceService.DtoDocument[] Documents {
            get {
                return this.DocumentsField;
            }
            set {
                if ((object.ReferenceEquals(this.DocumentsField, value) != true)) {
                    this.DocumentsField = value;
                    this.RaisePropertyChanged("Documents");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public avis.sbair.InvoiceService.DocumentStoreType Type {
            get {
                return this.TypeField;
            }
            set {
                if ((this.TypeField.Equals(value) != true)) {
                    this.TypeField = value;
                    this.RaisePropertyChanged("Type");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DtoDocument", Namespace="http://schemas.datacontract.org/2004/07/Avis.InvoiceManager.DocumentDiggerService" +
        "")]
    [System.SerializableAttribute()]
    public partial class DtoDocument : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DescriptionUrlField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Tuple<string, string>[] DocumentMetaDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private avis.sbair.InvoiceService.DtoDocumentElement[] ElementsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description {
            get {
                return this.DescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.DescriptionField, value) != true)) {
                    this.DescriptionField = value;
                    this.RaisePropertyChanged("Description");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string DescriptionUrl {
            get {
                return this.DescriptionUrlField;
            }
            set {
                if ((object.ReferenceEquals(this.DescriptionUrlField, value) != true)) {
                    this.DescriptionUrlField = value;
                    this.RaisePropertyChanged("DescriptionUrl");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Tuple<string, string>[] DocumentMetaData {
            get {
                return this.DocumentMetaDataField;
            }
            set {
                if ((object.ReferenceEquals(this.DocumentMetaDataField, value) != true)) {
                    this.DocumentMetaDataField = value;
                    this.RaisePropertyChanged("DocumentMetaData");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public avis.sbair.InvoiceService.DtoDocumentElement[] Elements {
            get {
                return this.ElementsField;
            }
            set {
                if ((object.ReferenceEquals(this.ElementsField, value) != true)) {
                    this.ElementsField = value;
                    this.RaisePropertyChanged("Elements");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DtoDocumentElement", Namespace="http://schemas.datacontract.org/2004/07/Avis.InvoiceManager.DocumentDiggerService" +
        "")]
    [System.SerializableAttribute()]
    public partial class DtoDocumentElement : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DocumentThumbnailUrlField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DocumentUrlField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description {
            get {
                return this.DescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.DescriptionField, value) != true)) {
                    this.DescriptionField = value;
                    this.RaisePropertyChanged("Description");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string DocumentThumbnailUrl {
            get {
                return this.DocumentThumbnailUrlField;
            }
            set {
                if ((object.ReferenceEquals(this.DocumentThumbnailUrlField, value) != true)) {
                    this.DocumentThumbnailUrlField = value;
                    this.RaisePropertyChanged("DocumentThumbnailUrl");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string DocumentUrl {
            get {
                return this.DocumentUrlField;
            }
            set {
                if ((object.ReferenceEquals(this.DocumentUrlField, value) != true)) {
                    this.DocumentUrlField = value;
                    this.RaisePropertyChanged("DocumentUrl");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DtoDocumentViewResult", Namespace="http://schemas.datacontract.org/2004/07/Avis.InvoiceManager.DocumentDiggerService" +
        "")]
    [System.SerializableAttribute()]
    public partial class DtoDocumentViewResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private byte[] BufferField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FileNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MimeTypeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public byte[] Buffer {
            get {
                return this.BufferField;
            }
            set {
                if ((object.ReferenceEquals(this.BufferField, value) != true)) {
                    this.BufferField = value;
                    this.RaisePropertyChanged("Buffer");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FileName {
            get {
                return this.FileNameField;
            }
            set {
                if ((object.ReferenceEquals(this.FileNameField, value) != true)) {
                    this.FileNameField = value;
                    this.RaisePropertyChanged("FileName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MimeType {
            get {
                return this.MimeTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.MimeTypeField, value) != true)) {
                    this.MimeTypeField = value;
                    this.RaisePropertyChanged("MimeType");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="InvoiceService.IDocumentSearch")]
    public interface IDocumentSearch {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentSearch/SearchDocuments", ReplyAction="http://tempuri.org/IDocumentSearch/SearchDocumentsResponse")]
        avis.sbair.InvoiceService.DtoDocumentSearchResult SearchDocuments(avis.sbair.InvoiceService.DocumentStoreType storeTypeToSearch, string raNumber);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentSearch/ViewDocument", ReplyAction="http://tempuri.org/IDocumentSearch/ViewDocumentResponse")]
        avis.sbair.InvoiceService.DtoDocumentViewResult ViewDocument(string encrypted, bool forceOriginal);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentSearch/SearchInvoiceCopyDocuments", ReplyAction="http://tempuri.org/IDocumentSearch/SearchInvoiceCopyDocumentsResponse")]
        avis.sbair.InvoiceService.DtoDocumentSearchResult SearchInvoiceCopyDocuments(string lang, string raNumber, string reservationNumber, string lastName);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IDocumentSearchChannel : avis.sbair.InvoiceService.IDocumentSearch, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DocumentSearchClient : System.ServiceModel.ClientBase<avis.sbair.InvoiceService.IDocumentSearch>, avis.sbair.InvoiceService.IDocumentSearch {
        
        public DocumentSearchClient() {
        }
        
        public DocumentSearchClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DocumentSearchClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DocumentSearchClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DocumentSearchClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public avis.sbair.InvoiceService.DtoDocumentSearchResult SearchDocuments(avis.sbair.InvoiceService.DocumentStoreType storeTypeToSearch, string raNumber) {
            return base.Channel.SearchDocuments(storeTypeToSearch, raNumber);
        }
        
        public avis.sbair.InvoiceService.DtoDocumentViewResult ViewDocument(string encrypted, bool forceOriginal) {
            return base.Channel.ViewDocument(encrypted, forceOriginal);
        }
        
        public avis.sbair.InvoiceService.DtoDocumentSearchResult SearchInvoiceCopyDocuments(string lang, string raNumber, string reservationNumber, string lastName) {
            return base.Channel.SearchInvoiceCopyDocuments(lang, raNumber, reservationNumber, lastName);
        }
    }
}
