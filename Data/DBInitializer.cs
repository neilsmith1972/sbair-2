﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using avis.sbair.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace avis.sbair.Data
{
    public class DBInitializer : DropCreateDatabaseIfModelChanges<DB_Context>

    {
        protected override void Seed(DB_Context context)
        {
            List<Country> countries = new List<Country> { new Country {Name="Sweden", CountryCode="SE"}};
            foreach (Country r in countries)
            {
                context.Countries.Add(r);
            }
            context.SaveChanges();

            
            List<PaymentMethod> p_methods = new List<PaymentMethod> { new PaymentMethod {  PaymentMethodName="Cash / Card", CustomerPays=5, InsurancePays=0 } };
            foreach (PaymentMethod r in p_methods)
            {
                context.PaymentMethods.Add(r);
            }
            context.SaveChanges();

            List<Status> status = new List<Status> { new Status { StatusText="Open" }, 
                new Status { StatusText="Pending"}, 
                new Status { StatusText="Closed - Has Invoice Number"} 
            };
            foreach (Status r in status)
            {
                context.StatusCodes.Add(r);
            }
            context.SaveChanges();

            //List<InsuranceCompany> i_comp = new List<InsuranceCompany> { new InsuranceCompany { Country = context.Countries.Where(c => c.ID == 1).First() , Name = "ABC Insurance" } };
            //foreach (InsuranceCompany r in i_comp)
            //{
            //    context.InsuranceCompanies.Add(r);
            //}
            //context.SaveChanges();

            //List<SplitBilling> sb = new List<SplitBilling> { new SplitBilling { AirportFeeCustomerPays=0, AirportFeeTotal=50, AlreadyPaidByCustomerAmount=100, AlreadyPaidByInsuranceAmount=20, 
            //    CheckInDate=DateTime.Parse("07/12/2012"), CheckOutDate=DateTime.Parse("01/02/2012"), SentToPrinting = false, CongestionCustomerPays=0, 
            //    CongestionTotal=50, ContractFeeCustomerPays=0, ContractFeeTotal=50, DeliveryCustomerPays=0, DeliveryTotal=50, InvoiceFeeCustomerPays=0,
            //    InvoiceFeeTotal=50, MiscCostsTotal=25, NbDaysCoveredByInsurance=50, NbKmCoveredByInsurance=1000, OneWayCustomerPays=0, OneWayTotal=50,
            //    RAICustomerPays=0, RAITotal=50, RefuellingCustomerPays=0, RefuellingTotal=50, ReturnCustomerPays=0, ReturnTotal=50, SuperCDWCustomerPays=0,
            //    SuperCDWTotal=50, TotalRentalChargedAmount=1000, VATTotal=250, InsuranceCompany = context.InsuranceCompanies.First(), CarGroups = context.CarGroups.First(),
            //    Countries = context.Countries.First(), PriceSplit = context.PriceSplits.First(), VATSplit = context.VATSplits.First(), RA_number = "76878787"} };
            //foreach (SplitBilling r in sb)
            //{
            //    context.SplitBillings.Add(r);
            //}
            //context.SaveChanges();

            List<VATValues> vat = new List<VATValues> { new VATValues { FeeType = "AirportFee", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()}, 
                new VATValues { FeeType = "VRF", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()},
                new VATValues { FeeType = "DeliveryFee", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()},
                new VATValues { FeeType = "InvoiceFee", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()},
                new VATValues { FeeType = "Mileage", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()},
                new VATValues { FeeType = "MiscCost", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()},
                new VATValues { FeeType = "OneWayFee", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()},
                new VATValues { FeeType = "Refueling", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()},
                new VATValues { FeeType = "ReturnFee", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()},
                new VATValues { FeeType = "Time", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()},
                new VATValues { FeeType = "Other", pcent = 25, FromDate = DateTime.Parse("2004/01/01"), ToDate = DateTime.Parse("2015/01/01"), CountryCode = context.Countries.First()}
            };
            foreach (VATValues r in vat)
            {
                context.VATValues.Add(r);
            }
            context.SaveChanges();

            List<VATSplit> sp = new List<VATSplit> { new VATSplit { Description="No split. Private person", PercentageCustomerPays = -1, AppliedTo="T" },
                 new VATSplit { Description="The customer pays 50%", PercentageCustomerPays = 50, AppliedTo="T"  },
                 new VATSplit { Description="The customer pays 100%", PercentageCustomerPays = 100, AppliedTo="T"  },
                 new VATSplit { Description="The customer pays 100% of his own VAT and 50% of the insurance's VAT", PercentageCustomerPays = 50, AppliedTo="I" }
            };
            foreach (VATSplit r in sp)
            {
                context.VATSplits.Add(r);
            }
            context.SaveChanges();



            SqlCommand objComm = new SqlCommand("select * from InsuranceCompany", new SqlConnection(ConfigurationManager.AppSettings["DevSourceDataConnectionString"].ToString()));
            SqlDataAdapter objAdapter = new SqlDataAdapter(objComm);
            DataSet objData = new DataSet();
            //if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            //objAdapter.Fill(objData, "rec");
            //objComm.Connection.Close();
            //foreach (DataRow row in objData.Tables[0].Rows)
            //{
            //    InsuranceCompany ins = new InsuranceCompany { Name = row["Name"].ToString(), RateCode = row["RateCode"].ToString(), Country = context.Countries.First() };
            //    context.InsuranceCompanies.Add(ins);
            //}
            //context.SaveChanges();

            //objComm.CommandText = "Select * from cargroup";
            //objData.Clear();
            //if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            //objAdapter.Fill(objData, "rec");
            //objComm.Connection.Close();
            //foreach (DataRow row in objData.Tables[0].Rows)
            //{
            //    CarGroup ins = new CarGroup { CarGroupCode = row["CarGroup"].ToString(), Description = row["Description"].ToString() };
            //    context.CarGroups.Add(ins);
            //}
            //context.SaveChanges();


            objComm.CommandText = "Select * from insurancecodes";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                OtherInsuranceCode ins = new OtherInsuranceCode { Insurance_Code=row["insurancecode"].ToString(), Name = row["name"].ToString() };
                context.OtherInsuranceCodes.Add(ins);
            }
            context.SaveChanges();


            objComm.CommandText = "Select * from insurancepriceversion";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();            
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                string code = row["insurancecode"].ToString();
                string t = row["todate"].ToString();
                if (t == string.Empty) { t = "01/01/2099"; }
                
                OtherInsurancePriceVersion ins = new OtherInsurancePriceVersion
                {
                    MappedID = (int) row["InsurancePriceVersionID"],
                    OtherInsuranceCode = context.OtherInsuranceCodes.Where(o => o.Insurance_Code == code).First(),
                    CountryCode = context.Countries.First(),
                    FromDate = (DateTime)row["fromdate"],
                    ToDate = DateTime.Parse(t),
                    VAT_Pcent = (decimal)row["vat_pcent"]
                };
                context.OtherInsurancePriceVersions.Add(ins);
            }
            context.SaveChanges();

            objComm.CommandText = "Select * from insuranceprice";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                string cg = row["cargroup"].ToString();
                int ip = (int)row["InsurancePriceVersionID"];
                OtherInsurancePrice ins = new OtherInsurancePrice
                {
                    OtherInsurancePriceVersion = context.OtherInsurancePriceVersions.Where(o => o.MappedID == ip).First(),
                    //CarGroup = context.CarGroups.Where(c => c.CarGroupCode == cg).First(),
                    Price = (decimal) row["price"]
                  
                };
                context.OtherInsurancePrices.Add(ins);
            }
            context.SaveChanges();


            //objComm.CommandText = "Select * from rate";
            //objData.Clear();
            //if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            //objAdapter.Fill(objData, "rec");
            //objComm.Connection.Close();
            //foreach (DataRow row in objData.Tables[0].Rows)
            //{
            //    RateCode ins = new RateCode
            //    {
            //        Code = row["ratecode"].ToString(),
            //        Description = row["description"].ToString(),
            //        CountryCode = context.Countries.First()
            //    };
            //    context.RateCodes.Add(ins);
            //}
            //context.SaveChanges();

            //objComm.CommandText = "Select * from rateversion";
            //objData.Clear();
            //if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            //objAdapter.Fill(objData, "rec");
            //objComm.Connection.Close();
            //foreach (DataRow row in objData.Tables[0].Rows)
            //{
            //    string rCode = row["ratecode"].ToString();
            //    RateVersion ins = new RateVersion {
            //        RateCode = context.RateCodes.Where(r => r.Code == rCode).First(),
            //        FromDate = DateTime.Parse("2004/01/01"),
            //        ToDate = DateTime.Parse("2015/01/01"),
            //        NbDaysWeek = (int)row["NbDaysWeek"],
            //        NbDaysMonth = (int)row["NbDaysMonth"],
            //        CountryCode = context.Countries.First()
            //    };
            //    context.RateVersions.Add(ins);
            //}
            //context.SaveChanges();

            objComm.CommandText = "Select * from pricesplit";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                PriceSplit ins = new PriceSplit
                {
                     Description=row["description"].ToString(), FixedAmountIsVATIncluded= (bool) row["FixedAmountIsVATIncluded"], InsurancePayAFixedAmount=(bool)row["InsurancePayAFixedAmount"],
                      MileageByUnit=(bool) row["MileageByUnit"], PCentCustomerPartMileage=(decimal) row["PCentCustomerPartMileage"], PCentCustomerPartTime=(decimal) row["PCentCustomerPartTime"],
                      PrintingDescription=row["PrintingDescription"].ToString(), TimeByUnit=(bool) row["TimeByUnit"]
                };
                context.PriceSplits.Add(ins);
            }
            context.SaveChanges();


            objComm.CommandText = "Select * from othercost";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                OtherCost ins = new OtherCost
                {
                     Code=row["othercostcode"].ToString(), Name=row["name"].ToString()
                };
                context.OtherCosts.Add(ins);
            }
            context.SaveChanges();


            objComm.CommandText = "Select * from othercostsplit";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                string code = row["othercostcode"].ToString();
                OtherCostSplit ins = new OtherCostSplit
                {
                     Code=context.OtherCosts.Where(c=>c.Code==code).First(), CountryCode=context.Countries.First(), Description=row["description"].ToString(), PercentCustomerPays=(decimal) row["pcentcustomerpart"]
                };
                context.OtherCostSplits.Add(ins);
            }
            context.SaveChanges();


            objComm.CommandText = "Select * from fee";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                //string code = row["feecode"].ToString();
                FeeType ins = new FeeType
                {
                    FeeCode = row["feecode"].ToString(), Name = row["name"].ToString()
                };
                context.FeeTypes.Add(ins);
            }
            context.SaveChanges();


            objComm.CommandText = "Select * from feesplit";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                string code = row["feecode"].ToString();
                FeeSplit ins = new FeeSplit
                {
                    FeeType = context.FeeTypes.Where(c=>c.FeeCode==code).First(),
                    CountryCode = context.Countries.First(),
                    Description = row["description"].ToString(), 
                    PCentCustomerPart = (decimal)row["pcentcustomerpart"]
                };
                context.FeeSplits.Add(ins);
            }
            context.SaveChanges();


            objComm.CommandText = "Select * from feepriceversion";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                string code = row["feecode"].ToString();
                FeeVersion ins = new FeeVersion
                {
                     CountryCode = context.Countries.First(),
                     FeeCode = context.FeeTypes.Where(f=>f.FeeCode == code).First(),
                     FromDate = (DateTime) row["fromdate"]
                     //ToDate = (DateTime) row["todate"]
                };
                context.FeeVersions.Add(ins);
            }
            context.SaveChanges();


            objComm.CommandText = "Select * from feeprice";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                int vers = (int) row["feepriceversionid"];
                FeePrice ins = new FeePrice
                {
                     FeeVersion = context.FeeVersions.Where(f=>f.ID == vers).First(),
                     CarGroup = row["cargroup"].ToString(),
                      Price = (decimal) row["price"]
                };
                context.FeePrices.Add(ins);
            }
            context.SaveChanges();

            objComm.CommandText = "Select * from InvoiceFee";
            objData.Clear();
            if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            objAdapter.Fill(objData, "rec");
            objComm.Connection.Close();
            foreach (DataRow row in objData.Tables[0].Rows)
            {
                string t = row["todate"].ToString();
                if (t == string.Empty) { t = "01/01/2099"; }
                InvoiceFee ins = new InvoiceFee
                {
                    CountryCode = context.Countries.First(),
                    FromDate = (DateTime)row["FromDate"],
                    ToDate = DateTime.Parse(t),
                    Price = (decimal)row["price"]
                };
                context.InvoiceFees.Add(ins);
            }
            context.SaveChanges();



            //objComm.CommandText = "Select * from rateprice";
            //objData.Clear();
            //if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            //objAdapter.Fill(objData, "rec");
            //objComm.Connection.Close();
            //foreach (DataRow row in objData.Tables[0].Rows)
            //{
            //    //string cargroup = row["cargroup"].ToString();
            //    int vers = (int)row["rateversionid"];
            //    RatePrice ins = new RatePrice
            //    {
            //         CarGroup= context.CarGroups.First(), RateVersion = context.RateVersions.Where(r=>r.RateVersionID == vers).First(),
            //          prDay = (decimal) row["prDay"], prKm = (decimal) row["prKm"], prMonth = (decimal) row["prMonth"],
            //           prWeek = (decimal) row["prWeek"]
            //    };
            //    context.RatePrices.Add(ins);
            //}
            //context.SaveChanges();





            //objComm = new SqlCommand("select * from OpenRental", new SqlConnection(ConfigurationManager.AppSettings["SourceDataConnectionString"].ToString()));
            //objAdapter = new SqlDataAdapter(objComm);
            //objData = new DataSet();
            //if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            //objAdapter.Fill(objData, "rec");
            //objComm.Connection.Close();
            ////List<ClosedRental> ClosedRentals = new List<ClosedRental>();
            //foreach (DataRow row in objData.Tables[0].Rows)
            //{
            //    //string cg = row["ChargedCarGroup"].ToString();
            //    //string ins = row["InsuranceBillingName"].ToString();
            //    OpenRental rec = new OpenRental();
            //    rec.RentalAgreementNumber = row["RentalAgreementNumber"].ToString();
            //    rec.RenterName = row["RentalCustomerName"].ToString();
            //    rec.CheckoutDateTime = DateTime.Parse(row["RentalCheckoutDateTime"].ToString());
            //    rec.CheckoutLocationName = row["RentalCheckoutLocationText"].ToString();
            //    rec.CheckinDateTime = DateTime.Parse(row["RentalCheckinDateTime"].ToString());
            //    rec.CheckinLocationName = row["RentalCheckinLocationText"].ToString();
            //    rec.ChargedCarGroup = row["RentalVehicleClassChargedCode"].ToString();
            //    rec.VehicleRegistrationNumber = row["RentalVehicleLicensePlateNumber"].ToString();
            //    rec.RenterBillingAddress2 = row["RentalRenterAddressCityText"].ToString();
            //    rec.RenterBillingCountryCode = row["RentalRenterAddressCountryCode"].ToString();
                
            //    context.OpenRentals.Add(rec);
            //}
            //context.SaveChanges();




            //objComm = new SqlCommand("select * from ClosedRental order by ChargedCarGroup asc", new SqlConnection(ConfigurationManager.AppSettings["SourceDataConnectionString"].ToString()));
            //    objAdapter = new SqlDataAdapter(objComm);
            //    objData = new DataSet();
            //    if (objComm.Connection.State != ConnectionState.Open) { objComm.Connection.Open(); }
            //    objAdapter.Fill(objData, "rec");
            //    objComm.Connection.Close();
            //    //List<ClosedRental> ClosedRentals = new List<ClosedRental>();
            //    int i = 0;
            //    foreach (DataRow row in objData.Tables[0].Rows)
            //    {
            //        string cg = row["ChargedCarGroup"].ToString();
            //        string ins = row["InsuranceBillingName"].ToString();
            //        ClosedRental rec = new ClosedRental();
            //        rec.RentalAgreementNumber = row["RentalAgreementNumber"].ToString();
            //        rec.CheckoutDateTime = DateTime.Parse(row["CheckoutDateTime"].ToString());
            //        rec.CheckoutLocationName = row["CheckoutLocationName"].ToString();
            //        rec.CheckinDateTime = DateTime.Parse(row["CheckinDateTime"].ToString());
            //        rec.CheckinLocationName = row["CheckinLocationName"].ToString();
            //        rec.ChargedCarGroup = row["ChargedCarGroup"].ToString();
            //        rec.VehicleRegistrationNumber = row["VehicleRegistrationNumber"].ToString();
            //        context.ClosedRentals.Add(rec);
            //        i++;
            //        if (i >= 500) { break; }
            //    }
            //    context.SaveChanges();

               
        }
    }
}