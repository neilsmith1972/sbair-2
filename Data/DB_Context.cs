﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace avis.sbair.Data
{
    public class DB_Context : DbContext
    {
        public DB_Context() : base("name=avis_sbair") { }
        public DbSet<Models.PriceSplit> PriceSplits { get; set; }
        public DbSet<Models.VATSplit> VATSplits { get; set; }
        public DbSet<Models.OtherInsuranceCode> OtherInsuranceCodes { get; set; }
        public DbSet<Models.OtherInsurancePriceVersion> OtherInsurancePriceVersions { get; set; }
        public DbSet<Models.OtherInsurancePrice> OtherInsurancePrices { get; set; }
        public DbSet<Models.Country> Countries { get; set; }
        public DbSet<Models.PaymentMethod> PaymentMethods { get; set; }
        public DbSet<Models.VATValues> VATValues { get; set; }
        public DbSet<Models.FeeType> FeeTypes { get; set; }
        public DbSet<Models.FeePrice> FeePrices { get; set; }
        public DbSet<Models.FeeSplit> FeeSplits { get; set; }
        public DbSet<Models.FeeVersion> FeeVersions { get; set; }
        public DbSet<Models.OtherCost> OtherCosts { get; set; }
        public DbSet<Models.OtherCostSplit> OtherCostSplits { get; set; }
        public DbSet<Models.Status> StatusCodes { get; set; }
        public DbSet<Models.audit> AuditRecords { get; set; }
        public DbSet<Models.InvoiceFee> InvoiceFees { get; set; }
    }
}