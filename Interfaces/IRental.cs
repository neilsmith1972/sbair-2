﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace avis.sbair.Interfaces
{
    public interface IRental
    {
        string RentalAgreementNumber { get; set; }
        Nullable<System.DateTime> RentalCheckoutDateTime { get; set; }
        string RentalCheckoutLocationText { get; set; }
        Nullable<System.DateTime> RentalCheckinDateTime { get; set; }
        string RentalDamageNumber { get; set; }
        string RentalVehicleClassChargedCode { get; set; }
        string RentalVehicleLicensePlateNumber { get; set; }
        string RentalCustomerName { get; set; }
        string RentalCompanyName { get; set; }
        string RentalRenterAddress1Text { get; set; }
        string RentalRenterAddress2Text { get; set; }
        string RentalRenterAddressCityText { get; set; }
        Nullable<decimal> RentalMileageRateAmount { get; set; }
        Nullable<decimal> RentalHourlyRateAmount { get; set; }
        Nullable<decimal> RentalDailyRateAmount { get; set; }
        Nullable<decimal> RentalWeeklyRateAmount { get; set; }
        Nullable<decimal> RentalMonthlyRateAmount { get; set; }

    }
}
