﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc.Html;

namespace avis.sbair
{
    public static class HtmlExtensions
    {
        /// <summary>
        /// Renders a control that allows users to easily assign a split without a cost.
        /// </summary>
        /// <param name="htmlHelper">
        /// The current html helper.
        /// </param>
        /// <param name="splitPropertyName">
        /// The property that stores the split id
        /// </param>
        /// <param name="customerValue">
        /// The value that will be assigned if the customer is assigned the split cost/fee.
        /// </param>
        /// <param name="insurerValue">
        /// The value that will be assigned if the insurance agency is assigned the split cost/fee.
        /// </param>
        /// <param name="countryCode">
        /// The 2 letter country code.
        /// </param>
        /// <returns>
        /// Returns a HTML control.
        /// </returns>
        public static IHtmlString SplitChoice(this HtmlHelper htmlHelper, string propertyName, string customerValue, string insurerValue, string countryCode)
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Value = customerValue, Text = Avis.SBair.Web.Resources.Common.Customer }, new SelectListItem() { Value = insurerValue, Text = Avis.SBair.Web.Resources.Common.Insurer } };
            return htmlHelper.SplitCost(propertyName, null, items, countryCode, true);
        }

        /// <summary>
        /// Renders a control that allows users to easily assign a split without a cost.
        /// </summary>
        /// <param name="htmlHelper">
        /// The current html helper.
        /// </param>
        /// <param name="splitPropertyName">
        /// The property that stores the split id
        /// </param>
        /// <param name="customerValue">
        /// The value that will be assigned if the customer is assigned the split cost/fee.
        /// </param>
        /// <param name="insurerValue">
        /// The value that will be assigned if the insurance agency is assigned the split cost/fee.
        /// </param>
        /// <param name="countryCode">
        /// The 2 letter country code.
        /// </param>
        /// <returns>
        /// Returns a HTML control.
        /// </returns>
        public static IHtmlString SplitChoice(this HtmlHelper htmlHelper, string propertyName, IEnumerable<SelectListItem> items, string countryCode, bool renderJavaScript)
        {            
            return htmlHelper.SplitCost(propertyName, null, items, countryCode, renderJavaScript);
        }

        /// <summary>
        /// Renders a control that allows users to easily assign a split without a cost.
        /// </summary>
        /// <param name="htmlHelper">
        /// The current html helper.
        /// </param>
        /// <param name="splitPropertyName">
        /// The property that stores the split id
        /// </param>
        /// <param name="customerValue">
        /// The value that will be assigned if the customer is assigned the split cost/fee.
        /// </param>
        /// <param name="insurerValue">
        /// The value that will be assigned if the insurance agency is assigned the split cost/fee.
        /// </param>
        /// <param name="countryCode">
        /// The 2 letter country code.
        /// </param>
        /// <returns>
        /// Returns a HTML control.
        /// </returns>
        public static IHtmlString SplitCost<T>(this HtmlHelper<T> htmlHelper, Expression<Func<T, byte>> splitIdProperty, int customerSplitId, int insuranceSplitId, string countryCode)
        {
            return htmlHelper.SplitCost<T>(splitIdProperty, null, customerSplitId.ToString(), insuranceSplitId.ToString(), countryCode);
        }

        /// <summary>
        /// Renders a control that allows users to easily assign a split cost with an amount.
        /// </summary>
        /// <param name="htmlHelper">
        /// The current html helper.
        /// </param>
        /// <param name="splitPropertyName">
        /// The property that stores the split id
        /// </param>
        /// <param name="splitAmountPropertyName">
        /// The property that stores the split amount
        /// </param>
        /// <param name="customerValue">
        /// The value that will be assigned if the customer is assigned the split cost/fee.
        /// </param>
        /// <param name="insurerValue">
        /// The value that will be assigned if the insurance agency is assigned the split cost/fee.
        /// </param>
        /// <param name="countryCode">
        /// The 2 letter country code.
        /// </param>
        /// <returns>
        /// Returns a HTML control.
        /// </returns>
        public static IHtmlString SplitCost<T>(this HtmlHelper<T> htmlHelper, Expression<Func<T, byte>> splitIdProperty, Expression<Func<T, decimal>> splitAmountProperty, int customerSplitId, int insuranceSplitId, string countryCode)
        {
            return htmlHelper.SplitCost<T>(splitIdProperty, splitAmountProperty, customerSplitId.ToString(), insuranceSplitId.ToString(), countryCode);
        }

        /// <summary>
        /// Renders a control that allows users to easily assign a split without a cost.
        /// </summary>
        /// <param name="htmlHelper">
        /// The current html helper.
        /// </param>
        /// <param name="splitPropertyName">
        /// The property that stores the split id
        /// </param>
        /// <param name="customerValue">
        /// The value that will be assigned if the customer is assigned the split cost/fee.
        /// </param>
        /// <param name="insurerValue">
        /// The value that will be assigned if the insurance agency is assigned the split cost/fee.
        /// </param>
        /// <param name="countryCode">
        /// The 2 letter country code.
        /// </param>
        /// <returns>
        /// Returns a HTML control.
        /// </returns>
        public static IHtmlString SplitCost<T>(this HtmlHelper<T> htmlHelper, Expression<Func<T, byte>> splitIdProperty, string customerSplitId, string insuranceSplitId, string countryCode)
        {
            return htmlHelper.SplitCost<T>(splitIdProperty, null, customerSplitId, insuranceSplitId, countryCode);
        }

        /// <summary>
        /// Renders a control that allows users to easily assign a split without a cost.
        /// </summary>
        /// <param name="htmlHelper">
        /// The current html helper.
        /// </param>
        /// <param name="splitPropertyName">
        /// The property that stores the split id
        /// </param>
        /// <param name="customerValue">
        /// The value that will be assigned if the customer is assigned the split cost/fee.
        /// </param>
        /// <param name="insurerValue">
        /// The value that will be assigned if the insurance agency is assigned the split cost/fee.
        /// </param>
        /// <param name="countryCode">
        /// The 2 letter country code.
        /// </param>
        /// <returns>
        /// Returns a HTML control.
        /// </returns>
        public static IHtmlString SplitCost<T>(this HtmlHelper<T> htmlHelper, Expression<Func<T, byte>> splitIdProperty, IEnumerable<SelectListItem> items, string countryCode)
        {
            return htmlHelper.SplitCost<T>(splitIdProperty, null, items, countryCode);
        }

                /// <summary>
        /// Renders a control that allows users to easily assign a split cost with an amount.
        /// </summary>
        /// <param name="htmlHelper">
        /// The current html helper.
        /// </param>
        /// <param name="splitPropertyName">
        /// The property that stores the split id
        /// </param>
        /// <param name="splitAmountPropertyName">
        /// The property that stores the split amount
        /// </param>
        /// <param name="customerValue">
        /// The value that will be assigned if the customer is assigned the split cost/fee.
        /// </param>
        /// <param name="insurerValue">
        /// The value that will be assigned if the insurance agency is assigned the split cost/fee.
        /// </param>
        /// <param name="countryCode">
        /// The 2 letter country code.
        /// </param>
        /// <returns>
        /// Returns a HTML control.
        /// </returns>
        public static IHtmlString SplitCost<T>(this HtmlHelper<T> htmlHelper, Expression<Func<T, byte>> splitIdProperty, Expression<Func<T, decimal>> splitAmountProperty, string customerSplitId, string insuranceSplitId, string countryCode)
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Value = customerSplitId, Text = Avis.SBair.Web.Resources.Common.Customer }, new SelectListItem() { Value = insuranceSplitId, Text = Avis.SBair.Web.Resources.Common.Insurer } };
            return htmlHelper.SplitCost(splitIdProperty, splitAmountProperty, items, countryCode);
        }

        /// <summary>
        /// Renders a control that allows users to easily assign a split cost with an amount.
        /// </summary>
        /// <param name="htmlHelper">
        /// The current html helper.
        /// </param>
        /// <param name="splitPropertyName">
        /// The property that stores the split id
        /// </param>
        /// <param name="splitAmountPropertyName">
        /// The property that stores the split amount
        /// </param>
        /// <param name="customerValue">
        /// The value that will be assigned if the customer is assigned the split cost/fee.
        /// </param>
        /// <param name="insurerValue">
        /// The value that will be assigned if the insurance agency is assigned the split cost/fee.
        /// </param>
        /// <param name="countryCode">
        /// The 2 letter country code.
        /// </param>
        /// <returns>
        /// Returns a HTML control.
        /// </returns>
        public static IHtmlString SplitCost<T>(this HtmlHelper<T> htmlHelper, Expression<Func<T, byte>> splitIdProperty, Expression<Func<T, decimal>> splitAmountProperty, IEnumerable<SelectListItem> items, string countryCode)
        {
            var splitIdMemberExpression = splitIdProperty.Body as MemberExpression;
            var splitIdPropertyName = splitIdMemberExpression.Member.Name;

            string splitAmountPropertyName = null;
            if (splitAmountProperty != null)
            {
                //var splitAmountMemberExpression = splitIdProperty.Body as MemberExpression;
                //splitAmountPropertyName = splitAmountMemberExpression.Member.Name;
                splitAmountPropertyName = GetFullPropertyName(splitAmountProperty);
            }
            
            return htmlHelper.SplitCost(splitIdPropertyName, splitAmountPropertyName, items, countryCode, true);
        }
        
        /// <summary>
        /// Renders a control that allows users to easily assign a split cost with an amount.
        /// </summary>
        /// <param name="htmlHelper">
        /// The current html helper.
        /// </param>
        /// <param name="splitPropertyName">
        /// The property that stores the split id
        /// </param>
        /// <param name="splitAmountPropertyName">
        /// The property that stores the split amount
        /// </param>
        /// <param name="customerValue">
        /// The value that will be assigned if the customer is assigned the split cost/fee.
        /// </param>
        /// <param name="insurerValue">
        /// The value that will be assigned if the insurance agency is assigned the split cost/fee.
        /// </param>
        /// <param name="countryCode">
        /// The 2 letter country code.
        /// </param>
        /// <returns>
        /// Returns a HTML control.
        /// </returns>
        private static IHtmlString SplitCost(this HtmlHelper htmlHelper, string splitPropertyName, string splitAmountPropertyName, IEnumerable<SelectListItem> items, string countryCode, bool renderJavaScript)
        {
            if (countryCode == null || countryCode.Length != 2)
            {
                throw new ArgumentException("The countryCode parameter must be exactly 2 characters in length.", "countryCode");
            }

            StringBuilder controlBuilder = new StringBuilder();
            string SplitCostKey = "SplitCostKey!";
            string SplitCostControlClassName = "splitCostControl";
            if (!htmlHelper.ViewContext.RequestContext.HttpContext.Items.Contains(SplitCostKey) && renderJavaScript)
            {
                /* ============ Renders the following JavaScript ===============
                <script type="text/javascript">
                    $(function () {
                        $(".splitCostControl span")
                            .buttonset()
                            .click(function () {
                                $(this).next().attr('disabled', false);
                            });
                    });
                </script>
                */

                controlBuilder.Append(string.Format("<script type=\"text/javascript\">$(function(){{$(\".{0} span\").buttonset().click(function(){{$(this).next().attr('disabled', false);$(this).next().focus().select();}});}});</script>", SplitCostControlClassName));
                htmlHelper.ViewContext.RequestContext.HttpContext.Items.Add(SplitCostKey, string.Empty);
            }

            /* ================= Renders the following HTML ====================
            
            <span id="splitCostGroup___" class="splitCostControl">
                <span id="radio___">
		            <input type="radio" id="radio1" name="radio___" /><label for="radio1">Customer</label>
		            <input type="radio" id="radio2" name="radio___" checked="checked" /><label for="radio2">Insurer</label>
	            </span>
            
                @Html.TextBoxFor(model => model.RefuelingAmount, new { disabled = "true" })
                @Html.ValidationMessageFor(model => model.RefuelingAmount)
                @Html.ValidationMessageFor(model => model.InsuranceCompanyContact)
            </span>
            
            */

            string groupName = splitPropertyName + "buttonGroup";
            string previousValue = null;
            int counter = 1;
            controlBuilder.Append(string.Format("<span class=\"{0}\"><span id=\"{1}\">", SplitCostControlClassName, groupName));
            foreach (var item in items)
            {
                controlBuilder.Append(string.Format("<input type=\"radio\" id=\"{2}{4}\" name=\"{1}\" value=\"{3}\"", SplitCostControlClassName, groupName, splitPropertyName, item.Value, counter));
                if (item.Selected)
                {
                    controlBuilder.Append("checked=\"checked\"");
                    previousValue = item.Value;
                }

                controlBuilder.Append(string.Format("/><label for=\"{2}{4}\">{3}</label>", SplitCostControlClassName, groupName, splitPropertyName, item.Text, counter));
                counter++;
            }

            controlBuilder.Append(string.Format("</span>"));

            /*
            controlBuilder.Append(string.Format("<span class=\"{0}\"><span id=\"{1}\"><input type=\"radio\" id=\"{2}1\" name=\"{1}\" value=\"{3}\"", SplitCostControlClassName, groupName, splitPropertyName, customerValue));
            if (customerIsChecked)
            {
                controlBuilder.Append("checked=\"checked\"");
            }

            controlBuilder.Append(string.Format("/><label for=\"{2}1\">{3}</label><input type=\"radio\" id=\"{2}2\" name=\"{1}\" value=\"{4}\"", SplitCostControlClassName, groupName, splitPropertyName, Avis.SBair.Web.Resources.Common.Customer, insurerValue));
            if (insurerIsChecked)
            {
                controlBuilder.Append("checked=\"checked\"");
            }

            controlBuilder.Append(string.Format("/><label for=\"{2}2\">{3}</label></span>", SplitCostControlClassName, groupName, splitPropertyName, Avis.SBair.Web.Resources.Common.Insurer));
            */

            IDictionary<string, object> amountInputhtmlAttributes = new Dictionary<string, object>() { { "style", "width:80px;" } };
            if (string.IsNullOrWhiteSpace(previousValue))
            {
                amountInputhtmlAttributes.Add("disabled", "true");
            }

            if (!string.IsNullOrWhiteSpace(splitAmountPropertyName))
            {
                decimal previousAmount = 0m;
                decimal.TryParse(previousValue, out previousAmount);
                controlBuilder.Append(htmlHelper.TextBox(splitAmountPropertyName, previousAmount.ToString("F", System.Globalization.CultureInfo.GetCultureInfo(countryCode)), amountInputhtmlAttributes).ToString());
                controlBuilder.Append(htmlHelper.ValidationMessage(splitAmountPropertyName).ToString());
            }

            controlBuilder.Append(htmlHelper.ValidationMessage(splitPropertyName).ToString());
            controlBuilder.Append(string.Format("</span>"));
            return new MvcHtmlString(controlBuilder.ToString());
        }

        public static IHtmlString LineItemAssignment<T>(this HtmlHelper<T> htmlHelper, string propertyDescriptor, Expression<Func<T, decimal>> customerFeeAmountExpression, Expression<Func<T, decimal>> insurerFeeAmountExpression, Expression<Func<T, decimal>> totalAmountExpression, Expression<Func<T, int>> splitIdExpression, IList<SelectListItem> splitItems, string countryCode, bool isAmountReadOnly)
        {
            var cultureInfo = System.Globalization.CultureInfo.GetCultureInfo(countryCode);
            var customerFeeAmountPropertyName = GetFullPropertyName(customerFeeAmountExpression); // GetPropertyNameFromExpression(customerFeeAmountExpression);
            var customerFeeAmount = customerFeeAmountExpression.Compile().Invoke(htmlHelper.ViewData.Model);
            var insurerFeeAmountPropertyName = GetFullPropertyName(insurerFeeAmountExpression); //GetPropertyNameFromExpression(insurerFeeAmountExpression);
            var insurerFeeAmount = insurerFeeAmountExpression.Compile().Invoke(htmlHelper.ViewData.Model);
            var totalAmountPropertyName = totalAmountExpression == null ? propertyDescriptor + "totalAmount" : GetFullPropertyName(totalAmountExpression); //GetPropertyNameFromExpression(totalAmountExpression);
            var totalAmount = totalAmountExpression == null ? insurerFeeAmount + customerFeeAmount : totalAmountExpression.Compile().Invoke(htmlHelper.ViewData.Model);
            var splitIdPropertyName = splitIdExpression == null ? propertyDescriptor + "splitId" : GetFullPropertyName(splitIdExpression); //GetPropertyNameFromExpression(splitIdExpression);
            var splitId = splitIdExpression == null ? 0 : splitIdExpression.Compile().Invoke(htmlHelper.ViewData.Model);

            return htmlHelper.LineItemAssignment(propertyDescriptor, customerFeeAmountPropertyName, customerFeeAmount, insurerFeeAmountPropertyName, insurerFeeAmount, totalAmountPropertyName, totalAmount, splitIdPropertyName, splitId, splitItems, countryCode, isAmountReadOnly);
        }

        public static IHtmlString LineItemAssignment<T>(this HtmlHelper<T> htmlHelper, string propertyDescriptor, string customerFeeAmountPropertyName, decimal customerFeeAmount,
            string insurerFeeAmountPropertyName, decimal insurerFeeAmount, string totalAmountPropertyName, decimal totalAmount,
            string splitIdPropertyName, int splitId, IList<SelectListItem> splitItems, string countryCode, bool isAmountReadOnly)
        {
            if (countryCode == null || countryCode.Length != 2)
            {
                throw new ArgumentException("The countryCode parameter must be exactly 2 characters in length.", "countryCode");
            }

            if (splitItems.Count != 2)
            {
                throw new ArgumentException("splitItems must contain exactly 2 items.");
            }

            string customerFeeAmountId = customerFeeAmountPropertyName.Replace('.', '_');
            string insurerFeeAmountId =  insurerFeeAmountPropertyName.Replace('.', '_');
            string totalAmountPropertyId = totalAmountPropertyName.Replace('.', '_');
            string splitIdPropertyId = splitIdPropertyName.Replace('.', '_');

            StringBuilder controlBuilder = new StringBuilder();
            string LineItemAssignmentKey = "LineItemAssignmentKey!";
            string LineItemAssignmentKeyControlClassName = "lineItemAmountSplitter";
            var cultureInfo = System.Globalization.CultureInfo.GetCultureInfo(countryCode);
            var displayDecimalSeperator = cultureInfo.NumberFormat.CurrencyDecimalSeparator;
            var displayDecimalPlaces = cultureInfo.NumberFormat.CurrencyDecimalDigits;
            var zeroDecimalString = string.Format("0{0}00", displayDecimalSeperator);
            if (!htmlHelper.ViewContext.RequestContext.HttpContext.Items.Contains(LineItemAssignmentKey))
            {
                #region "===== JavaScript that is rendered ====="
                /* ============ Renders the following JavaScript ===============
                <script type="text/javascript">

                var decimalSeperator = ',';
                var zeroDecimalString = '0,00';
                var zeroDecimalValue = '0.00';
                var decimalPlacesToDisplay = 2;
                
                $(function () {

                    $('.lineItemAmountSplitter').each(function () {
                        var container = $(this);
                        var lineItemAmountSlider = $(this).find('.lineItemAmountSlider');
                        var lineItemAmountInput = $(this).find('.lineItemAmountInput');
                        var customerAmountLabel = $(this).find('.customerLineItemAmount');
                        var customerAmountInput = $(this).find('.customerLineItemAmountInput');
                        var insurerAmountLabel = $(this).find('.insurerLineItemAmount');
                        var insurerAmountInput = $(this).find('.insurerLineItemAmountInput');
                 
                        var noneButtonLabel = $(this).find('.noneButtonLabel');
                        var noneButtonLabelText = $(this).find('.noneButton');
                        var noneButtonRadioOption = $(this).find('.noneRadioOption');
                        noneButtonLabel.click(function() { noneButtonRadioOption.click(); noneButtonLabel.removeClass('noneButtonWrapper').addClass('noneButtonWrapper-active'); });
                 
                        $(this).find('.customerRadioOption')
                        .button()
			            .click(function () {
			                lineItemAmountSlider.slider('value', 0);
                            noneButtonLabel.removeClass('noneButtonWrapper-active').addClass('noneButtonWrapper');
			            });

                        $(this).find('.insurerRadioOption')
                        .button()
			            .click(function () {
			                lineItemAmountSlider.slider('value', 100);
                            noneButtonLabel.removeClass('noneButtonWrapper-active').addClass('noneButtonWrapper');
			            });

                        $(this).find('.lineItemSplitterButtonGroup')
                        .buttonset({
                            create: function (event, ui) {
                                $(this).css('margin-right', '0');
                                $(this).children('label').css('width', ($(this).width() / 2) - 2);
                                $(this).children('label:first').css('text-align', 'left');
                                $(this).children('label:last').css('text-align', 'right');
                            }
                        });

                        lineItemAmountSlider.slider({
                            minvalue: 0,
                            maxvalue: 100,
                            value: 50,
                            create: function (event, ui) {
                                $(this).children('.ui-slider-handle')
                                .css('width', '6px')
                                .css('height', '11px')
                                .css('top', '-3px')
                                .css('margin-left', '-3px')
                                .css('background', '#ccc');
                            },
                            slide: function (event, ui) { splitAssignedValue(container, getCurrentValue(lineItemAmountInput), ui.value); },
                            change: function (event, ui) { splitAssignedValue(container, getCurrentValue(lineItemAmountInput), ui.value); }
                        });

                        $(this).find('.lineItemAmountInput').keyup(function (event) {
                            var val = getCurrentValue(lineItemAmountInput);
                            if (val.toString() == zeroDecimalValue) {
                                disableControl(container);
                                customerAmountLabel.html(displayMonetaryValue(zeroDecimalString));
                                insurerAmountLabel.html(displayMonetaryValue(zeroDecimalString));
                                insurerAmountInput.val(displayMonetaryValue(zeroDecimalString));
                                customerAmountInput.val(displayMonetaryValue(zeroDecimalString));
                                lineItemAmountSlider.slider('value', 50);
                            }
                            else {
                                enableControl(container);
                                splitAssignedValue(container, val, lineItemAmountSlider.slider('value'));
                            }
                            onLineItemAllocationChanged();
                        }).focusin(function () { $(this).select(); });

                        if (getCurrentValue(lineItemAmountInput) == null || getCurrentValue(lineItemAmountInput).toString() == zeroDecimalValue) {
                            disableControl(container);
                        }
                        else {
                            enableControl(container);
                            if (getCurrentValue(insurerAmountInput) + getCurrentValue(customerAmountInput) != getCurrentValue(lineItemAmountInput)) {
                                splitAssignedValue(container, getCurrentValue(lineItemAmountInput), lineItemAmountSlider.slider('value'));
                            }
                        }

                        customerAmountLabel.click(function () {
                            if (getCurrentValue(lineItemAmountInput) != null && getCurrentValue(lineItemAmountInput).toString() != zeroDecimalValue) {
                                customerAmountInput.show();
                                customerAmountInput.select();
                                customerAmountLabel.hide();
                            }
                        });

                        insurerAmountLabel.click(function () {
                            if (getCurrentValue(lineItemAmountInput) != null && getCurrentValue(lineItemAmountInput).toString() != zeroDecimalValue) {
                                insurerAmountInput.show();
                                insurerAmountInput.select();
                                insurerAmountLabel.hide();
                            }
                        });

                        lineItemAmountInput.blur(function () {
                            var val = getCurrentValue($(this));
                            $(this).val(displayMonetaryValue(val));
                        });

                        customerAmountInput.blur(function () {
                            var val = getCurrentValue($(this));
                            customerAmountLabel.show();
                            customerAmountInput.hide();
                            assignSpecificValue(container, val, getCurrentValue(lineItemAmountInput), true);                 
                        });

                        insurerAmountInput.blur(function () {
                            var val = getCurrentValue($(this));
                            insurerAmountLabel.show();
                            insurerAmountInput.hide();
                            assignSpecificValue(container, val, getCurrentValue(lineItemAmountInput), false);
                        });
                    });
                });
                  
                function assignSpecificValue(container, value, totalValue, isCustomerValue) {  
                    if (value == null) {
                        return;
                    }

                    var customerAmountLabel = container.find('.customerLineItemAmount');
                    var customerAmountInput = container.find('.customerLineItemAmountInput');
                    var insurerAmountLabel = container.find('.insurerLineItemAmount');
                    var insurerAmountInput = container.find('.insurerLineItemAmountInput');
                    var customerRadioButton = container.find('.customerRadioOption');
                    var insurerRadioButton = container.find('.insurerRadioOption');
                    var lineItemAmountSlider = container.find('.lineItemAmountSlider');
                    
                    var customerValue;
                    var insurerValue;                    
                    if (parseFloat(value) < 0) {
                        value = parseFloat(zeroDecimalValue);
                    } else if (parseFloat(value) > parseFloat(totalValue)) {
                        value = parseFloat(totalValue);                        
                    }
                 
                    if (isCustomerValue) {
                        customerValue = parseFloat(value).toFixed(decimalPlacesToDisplay);
                        insurerValue = parseFloat(parseFloat(totalValue) - parseFloat(value)).toFixed(decimalPlacesToDisplay);
                    }
                    else {
                        insurerValue = parseFloat(value).toFixed(decimalPlacesToDisplay);
                        customerValue = parseFloat(parseFloat(totalValue) - parseFloat(value)).toFixed(decimalPlacesToDisplay);
                    }
                    
                    var percentage = (customerValue / parseFloat(totalValue)) * 100;                            
                    lineItemAmountSlider.find('.ui-slider-handle').css('left', percentage + '%');
                    if (percentage < 50) {
                        customerRadioButton.next().addClass('ui-state-active');
                        insurerRadioButton.next().removeClass('ui-state-active');
                    } else {
                        insurerRadioButton.next().addClass('ui-state-active');
                        customerRadioButton.next().removeClass('ui-state-active');
                    }
                 
                    var noneAmountLabel = container.find('.noneButtonLabel'); 
                    lineItemAmountSlider.slider('enable');
                    noneAmountLabel.removeClass('ui-state-active');
                    noneAmountLabel.removeClass('noneButtonWrapper-active').addClass('noneButtonWrapper');
                 
                    customerAmountLabel.html(displayMonetaryValue(customerValue));
                    insurerAmountLabel.html(displayMonetaryValue(insurerValue));
                    customerAmountInput.val(displayMonetaryValue(customerValue));
                    insurerAmountInput.val(displayMonetaryValue(insurerValue));
                    onLineItemAllocationChanged();  
                }
                
                function getCurrentValue(input) {
                    if (input == null) {
                        return zeroDecimalValue;
                    }

                    return parseFloat(input.val().replace(',', '.')).toFixed(decimalPlacesToDisplay);
                }

                function displayMonetaryValue(decimalValue) {                    
                    if (decimalValue == null) {
                        return parseFloat(zeroDecimalValue).toFixed(decimalPlacesToDisplay).replace('.', decimalSeperator).replace(',', decimalSeperator);
                    }

                    //return decimalValue;
                    return parseFloat(decimalValue).toFixed(decimalPlacesToDisplay).replace('.', decimalSeperator).replace(',', decimalSeperator);                    
                }

                function splitAssignedValue(container, value, sliderValue) {
                    
                    var customerAmountLabel = container.find('.customerLineItemAmount');
                    var customerAmountInput = container.find('.customerLineItemAmountInput');
                    var insurerAmountLabel = container.find('.insurerLineItemAmount');
                    var insurerAmountInput = container.find('.insurerLineItemAmountInput');
                    var customerRadioButton = container.find('.customerRadioOption');
                    var insurerRadioButton = container.find('.insurerRadioOption');                    
                    var noneAmountLabel = container.find('.noneButtonLabel');                  
                    if (value == null || value.toString() == zeroDecimalValue) {
                        noneAmountLabel.addClass('ui-state-active');
                        insurerRadioButton.next().removeClass('ui-state-active');
                        customerRadioButton.next().removeClass('ui-state-active');
                        return;
                    } 
                    noneAmountLabel.removeClass('ui-state-active');
                    if (sliderValue < 50) {
                        customerRadioButton.next().addClass('ui-state-active');
                        insurerRadioButton.next().removeClass('ui-state-active');
                    } else {
                        insurerRadioButton.next().addClass('ui-state-active');
                        customerRadioButton.next().removeClass('ui-state-active');
                    }

                    var customerValue = parseFloat(value * (1 - (sliderValue / 100))).toFixed(decimalPlacesToDisplay);
                    var insurerValue = parseFloat(value * (sliderValue / 100)).toFixed(decimalPlacesToDisplay);

                    if (sliderValue != 0 && sliderValue != 100) {
                        var cent = value.substring(value.length - 1);
                        if (cent.search('1|3|5|7|9') > -1) {
                            value = parseFloat(value.substring(0, value.length - 1));
                        }

                        customerValue = parseFloat(value * (1 - (sliderValue / 100))).toFixed(decimalPlacesToDisplay);
                        insurerValue = parseFloat(value * (sliderValue / 100)).toFixed(decimalPlacesToDisplay);

                        if (cent.search('1|3|5|7|9') > -1) {
                            var split = parseFloat((Math.floor(cent / 2) / 100));
                            customerValue = (parseFloat(customerValue) + parseFloat(split)).toFixed(decimalPlacesToDisplay);
                            insurerValue = (parseFloat(insurerValue) + parseFloat(split) + parseFloat(0.01)).toFixed(decimalPlacesToDisplay);
                        }
                    }

                    customerAmountLabel.html(displayMonetaryValue(customerValue));
                    insurerAmountLabel.html(displayMonetaryValue(insurerValue));
                    customerAmountInput.val(displayMonetaryValue(customerValue));
                    insurerAmountInput.val(displayMonetaryValue(insurerValue));
                    onLineItemAllocationChanged();
                }

                function enableControl(container) {
                    var lineItemAmountSlider = container.find('.lineItemAmountSlider');
                    var customerAmountLabel = container.find('.customerLineItemAmount');
                    var insurerAmountLabel = container.find('.insurerLineItemAmount');

                    container.find('.lineItemSplitterButtonGroup').find('input:radio').button('enable');
                    lineItemAmountSlider.slider('enable');
                    customerAmountLabel.removeClass('disabledAmountLabel');
                    insurerAmountLabel.removeClass('disabledAmountLabel');
                }

                function disableControl(container) {
                    var lineItemAmountSlider = container.find('.lineItemAmountSlider');
                    var customerAmountLabel = container.find('.customerLineItemAmount');
                    var insurerAmountLabel = container.find('.insurerLineItemAmount');
                    var lineItemAmount = getCurrentValue(container.find('.lineItemAmountInput'));
                 
                    if (lineItemAmount == 0) {
                    container.find('.lineItemSplitterButtonGroup').find('input:radio').button('disable');
                    container.find('.lineItemSplitterButtonGroup').find('input:radio').button('refresh');                    
                    customerAmountLabel.addClass('disabledAmountLabel');
                    insurerAmountLabel.addClass('disabledAmountLabel');
                    } else {
                        container.find('.lineItemSplitterButtonGroup').find('input:radio').button('enable');
                        customerAmountLabel.removeClass('disabledAmountLabel');
                        insurerAmountLabel.removeClass('disabledAmountLabel');
                    }
                    lineItemAmountSlider.slider('disable');
                }

                function onLineItemAllocationChanged() {
                    $('body').trigger('lineItemAllocationChanged', ['Custom', 'Event']);
                }
              
            </script>
                */

                #endregion
                var middleButtonWidth = 70; // Pixels
                controlBuilder.Append(string.Format("<script type=\"text/javascript\">  var decimalSeperator = '{0}'; var zeroDecimalString = '{3}'; var zeroDecimalValue = '0.00'; var decimalPlacesToDisplay = {1};  $(function () {{  $('.{2}').each(function () {{ var container = $(this); var lineItemAmountSlider = $(this).find('.lineItemAmountSlider'); var lineItemAmountInput = $(this).find('.lineItemAmountInput'); var customerAmountLabel = $(this).find('.customerLineItemAmount'); var customerAmountInput = $(this).find('.customerLineItemAmountInput'); var insurerAmountLabel = $(this).find('.insurerLineItemAmount'); var insurerAmountInput = $(this).find('.insurerLineItemAmountInput'); var noneButtonLabel = $(this).find('.noneButtonLabel');var noneButtonLabelText = $(this).find('.noneButton');var noneButtonRadioOption = $(this).find('.noneRadioOption');noneButtonLabel.click(function() {{ lineItemAmountSlider.slider('value', 50); customerAmountLabel.html(displayMonetaryValue(zeroDecimalValue));insurerAmountLabel.html(displayMonetaryValue(zeroDecimalValue));customerAmountInput.val(displayMonetaryValue(zeroDecimalValue));insurerAmountInput.val(displayMonetaryValue(zeroDecimalValue));onLineItemAllocationChanged(); disableControl(noneButtonRadioOption.parent().parent().parent()); noneButtonRadioOption.parent().find('.ui-state-active').removeClass('ui-state-active'); noneButtonLabel.removeClass('noneButtonWrapper').addClass('noneButtonWrapper-active').addClass('ui-state-active'); }});  $(this).find('.customerRadioOption') .button() .click(function () {{ lineItemAmountSlider.slider('enable'); lineItemAmountSlider.slider('value', 0); noneButtonLabel.removeClass('noneButtonWrapper-active').addClass('noneButtonWrapper').removeClass('ui-state-active'); }}); $(this).find('.noneRadioOption').button().click(function () {{ lineItemAmountSlider.slider('value', 50); }});  $(this).find('.insurerRadioOption') .button() .click(function () {{ lineItemAmountSlider.slider('enable'); lineItemAmountSlider.slider('value', 100); noneButtonLabel.removeClass('noneButtonWrapper-active').addClass('noneButtonWrapper').removeClass('ui-state-active'); }});  $(this).find('.lineItemSplitterButtonGroup') .buttonset({{ create: function (event, ui) {{ $(this).css('margin-right', '0'); $(this).children('label').css('width', (($(this).width() - {4}) / 2) - 2 + 'px'); $(this).children('label:first').css('text-align', 'left');  $(this).children('label:first').next().next().css('width', '{4}px'); $(this).children('label:last').css('text-align', 'right'); }} }});  lineItemAmountSlider.slider({{ minvalue: 0, maxvalue: 100, value: 50, create: function (event, ui) {{ $(this).children('.ui-slider-handle') .css('width', '6px') .css('height', '11px') .css('top', '-3px') .css('margin-left', '-3px') .css('background', '#ccc'); }}, slide: function (event, ui) {{ splitAssignedValue(container, getCurrentValue(lineItemAmountInput), ui.value); }}, change: function (event, ui) {{ splitAssignedValue(container, getCurrentValue(lineItemAmountInput), ui.value); }} }});  $(this).find('.lineItemAmountInput').keyup(function (event) {{ var val = getCurrentValue(lineItemAmountInput); if (val.toString() == zeroDecimalValue) {{ disableControl(container); customerAmountLabel.html(displayMonetaryValue(zeroDecimalString)); insurerAmountLabel.html(displayMonetaryValue(zeroDecimalString)); insurerAmountInput.val(displayMonetaryValue(zeroDecimalString)); customerAmountInput.val(displayMonetaryValue(zeroDecimalString)); lineItemAmountSlider.slider('value', 50); }} else {{ enableControl(container); splitAssignedValue(container, val, lineItemAmountSlider.slider('value')); }} onLineItemAllocationChanged(); }}).focusin(function () {{ $(this).select(); }});  /*if (getCurrentValue(lineItemAmountInput) == null || getCurrentValue(lineItemAmountInput).toString() == zeroDecimalValue) {{ disableControl(container); }} else {{ enableControl(container); if (getCurrentValue(insurerAmountInput) + getCurrentValue(customerAmountInput) != getCurrentValue(lineItemAmountInput)) {{ splitAssignedValue(container, getCurrentValue(lineItemAmountInput), lineItemAmountSlider.slider('value')); }} }} */ customerAmountLabel.click(function () {{ if (getCurrentValue(lineItemAmountInput) != null && getCurrentValue(lineItemAmountInput).toString() != zeroDecimalValue) {{ customerAmountInput.show(); customerAmountInput.select(); customerAmountLabel.hide(); }} }});  insurerAmountLabel.click(function () {{ if (getCurrentValue(lineItemAmountInput) != null && getCurrentValue(lineItemAmountInput).toString() != zeroDecimalValue) {{ insurerAmountInput.show(); insurerAmountInput.select(); insurerAmountLabel.hide(); }} }});  lineItemAmountInput.blur(function () {{ var val = getCurrentValue($(this)); $(this).val(displayMonetaryValue(val)); }});  customerAmountInput.blur(function () {{ var val = getCurrentValue($(this)); customerAmountLabel.show(); customerAmountInput.hide(); assignSpecificValue(container, val, getCurrentValue(lineItemAmountInput), true); }});  insurerAmountInput.blur(function () {{ var val = getCurrentValue($(this)); insurerAmountLabel.show(); insurerAmountInput.hide(); assignSpecificValue(container, val, getCurrentValue(lineItemAmountInput), false); }}); }}); }});  function getCurrentValue(input) {{ if (input == null) {{ return zeroDecimalValue; }}  return parseFloat(input.val().replace(',', '.')).toFixed(decimalPlacesToDisplay); }}  function displayMonetaryValue(decimalValue) {{ if (decimalValue == null) {{ return parseFloat(zeroDecimalValue).toFixed(decimalPlacesToDisplay).replace('.', decimalSeperator).replace(',', decimalSeperator); }}  return parseFloat(decimalValue.toString().replace(',', '.')).toFixed(decimalPlacesToDisplay).replace('.', decimalSeperator).replace(',', decimalSeperator); }}  function splitAssignedValue(container, value, sliderValue) {{  var customerAmountLabel = container.find('.customerLineItemAmount'); var customerAmountInput = container.find('.customerLineItemAmountInput'); var insurerAmountLabel = container.find('.insurerLineItemAmount'); var insurerAmountInput = container.find('.insurerLineItemAmountInput'); var customerRadioButton = container.find('.customerRadioOption'); var insurerRadioButton = container.find('.insurerRadioOption'); var noneAmountLabel = container.find('.noneButtonLabel'); if (value == null || value.toString() == zeroDecimalValue) {{ noneAmountLabel.addClass('ui-state-active');insurerRadioButton.next().removeClass('ui-state-active');customerRadioButton.next().removeClass('ui-state-active'); return; }} noneAmountLabel.removeClass('ui-state-active'); noneAmountLabel.removeClass('noneButtonWrapper-active').addClass('noneButtonWrapper'); if (sliderValue < 50) {{ customerRadioButton.next().addClass('ui-state-active'); insurerRadioButton.next().removeClass('ui-state-active'); }} else {{ insurerRadioButton.next().addClass('ui-state-active'); customerRadioButton.next().removeClass('ui-state-active'); }}  var customerValue = parseFloat(value * (1 - (sliderValue / 100))).toFixed(decimalPlacesToDisplay); var insurerValue = parseFloat(value * (sliderValue / 100)).toFixed(decimalPlacesToDisplay);  if (sliderValue != 0 && sliderValue != 100) {{ var cent = value.substring(value.length - 1); if (cent.search('1|3|5|7|9') > -1) {{ value = parseFloat(value.substring(0, value.length - 1)); }}  customerValue = parseFloat(value * (1 - (sliderValue / 100))).toFixed(decimalPlacesToDisplay); insurerValue = parseFloat(value * (sliderValue / 100)).toFixed(decimalPlacesToDisplay);  if (cent.search('1|3|5|7|9') > -1) {{ var split = parseFloat((Math.floor(cent / 2) / 100)); customerValue = (parseFloat(customerValue) + parseFloat(split)).toFixed(decimalPlacesToDisplay); insurerValue = (parseFloat(insurerValue) + parseFloat(split) + parseFloat(0.01)).toFixed(decimalPlacesToDisplay); }} }}  customerAmountLabel.html(displayMonetaryValue(customerValue)); insurerAmountLabel.html(displayMonetaryValue(insurerValue)); customerAmountInput.val(displayMonetaryValue(customerValue)); insurerAmountInput.val(displayMonetaryValue(insurerValue)); onLineItemAllocationChanged(); }}  function enableControl(container) {{ var lineItemAmountSlider = container.find('.lineItemAmountSlider'); var customerAmountLabel = container.find('.customerLineItemAmount'); var insurerAmountLabel = container.find('.insurerLineItemAmount');  container.find('.lineItemSplitterButtonGroup').find('input:radio').button('enable'); lineItemAmountSlider.slider('enable'); customerAmountLabel.removeClass('disabledAmountLabel'); insurerAmountLabel.removeClass('disabledAmountLabel'); }}  function disableControl(container) {{ var lineItemAmountSlider = container.find('.lineItemAmountSlider'); var customerAmountLabel = container.find('.customerLineItemAmount'); var insurerAmountLabel = container.find('.insurerLineItemAmount'); var lineItemAmount = getCurrentValue(container.find('.lineItemAmountInput')); if (lineItemAmount == 0) {{ var noneButtonLabel = container.find('.noneButtonLabel');noneButtonLabel.removeClass('noneButtonWrapper').addClass('noneButtonWrapper-active');container.find('.lineItemSplitterButtonGroup').find('input:radio').button('disable'); container.find('.lineItemSplitterButtonGroup').find('input:radio').button('refresh'); customerAmountLabel.addClass('disabledAmountLabel'); insurerAmountLabel.addClass('disabledAmountLabel'); lineItemAmountSlider.slider('disable'); }} else {{ var noneRadioButton = container.find('.noneRadioOption');noneRadioButton.next().removeClass('ui-state-active');var noneAmountLabel = container.find('.noneButtonLabel');noneAmountLabel.removeClass('ui-state-active'); noneAmountLabel.removeClass('noneButtonWrapper-active').addClass('noneButtonWrapper');container.find('.lineItemSplitterButtonGroup').find('input:radio').button('enable'); customerAmountLabel.removeClass('disabledAmountLabel'); insurerAmountLabel.removeClass('disabledAmountLabel'); lineItemAmountSlider.slider('enable'); }} }} function assignSpecificValue(container, value, totalValue, isCustomerValue) {{ if (value == null) {{ return; }}  var customerAmountLabel = container.find('.customerLineItemAmount'); var customerAmountInput = container.find('.customerLineItemAmountInput'); var insurerAmountLabel = container.find('.insurerLineItemAmount'); var insurerAmountInput = container.find('.insurerLineItemAmountInput'); var customerRadioButton = container.find('.customerRadioOption'); var insurerRadioButton = container.find('.insurerRadioOption'); var lineItemAmountSlider = container.find('.lineItemAmountSlider');  var customerValue; var insurerValue; if (parseFloat(value) < 0) {{ value = parseFloat(zeroDecimalValue); }} else if (parseFloat(value) > parseFloat(totalValue)) {{ value = parseFloat(totalValue); }}  if (isCustomerValue) {{ customerValue = parseFloat(value).toFixed(decimalPlacesToDisplay); insurerValue = parseFloat(parseFloat(totalValue) - parseFloat(value)).toFixed(decimalPlacesToDisplay); }} else {{ insurerValue = parseFloat(value).toFixed(decimalPlacesToDisplay); customerValue = parseFloat(parseFloat(totalValue) - parseFloat(value)).toFixed(decimalPlacesToDisplay); }}  var percentage = (insurerValue / parseFloat(totalValue)) * 100; lineItemAmountSlider.find('.ui-slider-handle').css('left', percentage + '%'); if (percentage < 50) {{ customerRadioButton.next().addClass('ui-state-active'); insurerRadioButton.next().removeClass('ui-state-active'); }} else {{ insurerRadioButton.next().addClass('ui-state-active'); customerRadioButton.next().removeClass('ui-state-active'); }} var noneAmountLabel = container.find('.noneButtonLabel'); lineItemAmountSlider.slider('enable');noneAmountLabel.removeClass('ui-state-active');noneAmountLabel.removeClass('noneButtonWrapper-active').addClass('noneButtonWrapper'); customerAmountLabel.html(displayMonetaryValue(customerValue)); insurerAmountLabel.html(displayMonetaryValue(insurerValue)); customerAmountInput.val(displayMonetaryValue(customerValue)); insurerAmountInput.val(displayMonetaryValue(insurerValue)); onLineItemAllocationChanged(); }} function onLineItemAllocationChanged() {{ $('body').trigger('lineItemAllocationChanged', ['Custom', 'Event']); }}  </script>", displayDecimalSeperator, displayDecimalPlaces, LineItemAssignmentKeyControlClassName, zeroDecimalString, middleButtonWidth));
                htmlHelper.ViewContext.RequestContext.HttpContext.Items.Add(LineItemAssignmentKey, string.Empty);
            }

            #region " ===== HTML that is rendered ====="

            /*
            
            <div class="lineItemAmountSplitter ui-corner-all" id="container5" style="clear:both;position:absolute;width:368px;height:48px;background-color:#fff;">                    

                <!--------- ID REQUIRED ------------>
                <input type="text" id="customerAmountInput" class="customerLineItemAmountInput" value="0,00" style="display:none;width:60px;left:0px;top:5px;position:absolute;text-align:right;" />
                <label id="customerAmountLabel" class="lineItemAmountLabel customerLineItemAmount" style="position:absolute;left:0px;top:9px;width:60px;text-align:right;">0,00</label>

                <div id="topRow" style="position:relative;margin-right:0.3em;left:63px;width:240px;">
                    <div id="btnGroup" class="lineItemSplitterButtonGroup" style="position:relative;">
                        <input type="radio" id="repeat0" name="repeat" class="customerRadioOption" /><label for="repeat0">Customer</label>
		                <input type="radio" id="repeat1" name="repeat" class="insurerRadioOption" /><label for="repeat1">Insurer</label>
                    </div>
                    <div id="costInput" class="invoiceLineItemAmount" style="position:absolute;z-index:2;top:7px;left:0;right:0;margin:0 auto;width:70px;border:1px solid #CCC">
                        
                    <!--------- READ-ONLY IMPLEMENTATION ------------>
                    <div class="" style="width:70px;background-color:#ccc;height:auto;position:relative;text-align:right; cursor: default;">
                        10.00&nbsp;
                        <!--------- ID REQUIRED ------------>
                        <input type="hidden" id="amountInput" class="lineItemAmountInput" style="width:60px;text-align:right;border-width:0px;padding:0px;" value="10,00" />
                        </div>
                    </div>
                </div>
                <div id="bottomRow" style="position:relative;top:4px;left:63px;width:240px;">
                    <div id="slider" class="lineItemAmountSlider" style="height:5px;width:160px;left:0;right:0;margin:0 auto;"></div>
                </div>                    

                <!--------- ID REQUIRED ------------>
                <input type="text" id="insurerAmountInput" class="insurerLineItemAmountInput" value="0,00" style="display:none;width:60px;left:305px;top:5px;position:absolute;" />
                <label id="insurerAmountLabel" class="lineItemAmountLabel insurerLineItemAmount" style="position:absolute;left:305px;text-align:right;top:9px;width:60px;">0,00</label>

            </div>
            
            */

            #endregion


            // <!-- Customer Fee Amount (calc details), Insurer Fee Amount(calc details), Customer Split Id (model items), Insurer Split Id (model items), Total Amount (Bill)  -->            
            var customerFeeAmountString = customerFeeAmount.ToString("F", cultureInfo);            
            var insurerFeeAmountString = insurerFeeAmount.ToString("F", cultureInfo);            
            var totalAmountString = totalAmount.ToString("F", cultureInfo);            
            var customerSplitId = splitItems[0].Value;
            var insurerSplitId = splitItems[1].Value;
            string customerSplitDescription = splitItems[0].Text;
            string insurerSplitDescription = splitItems[1].Text;

            controlBuilder.AppendFormat("<div class=\"{0} ui-corner-all\" id=\"{1}_container\" style=\"clear:both;position:absolute;width:368px;height:48px;background-color:#fff;\">", LineItemAssignmentKeyControlClassName, propertyDescriptor);
            controlBuilder.AppendFormat("<input type=\"text\" id=\"{0}\" name=\"{1}\" class=\"customerLineItemAmountInput\" value=\"{2}\" style=\"display:none;width:60px;left:0px;top:5px;position:absolute;text-align:right;\" />", customerFeeAmountId, customerFeeAmountPropertyName, customerFeeAmountString);
            controlBuilder.AppendFormat("<label id=\"{0}Label\" class=\"lineItemAmountLabel customerLineItemAmount\" style=\"position:absolute;left:0px;top:9px;width:60px;text-align:right;\">{1}</label>", customerFeeAmountPropertyName, customerFeeAmountString);
            
            // Top Row
            controlBuilder.AppendFormat("<div id=\"{0}topRow\" style=\"position:relative;margin-right:0.3em;left:63px;width:240px;\">", propertyDescriptor);
            controlBuilder.AppendFormat("<div id=\"{0}btnGroup\" class=\"lineItemSplitterButtonGroup\" style=\"position:relative;\">", propertyDescriptor);
            
            // Radio Buttons
            var customerRadioOptionChecked = customerSplitId.ToUpperInvariant() == splitId.ToString().ToUpperInvariant() ? " checked=\"checked\"" : string.Empty;
            var insurerRadioOptionChecked = insurerSplitId.ToUpperInvariant() == splitId.ToString().ToUpperInvariant() ? " checked=\"checked\"" : string.Empty;
            controlBuilder.AppendFormat("<input type=\"radio\" id=\"{0}customerOption\" name=\"{0}OptionGroup\" value=\"{1}\" class=\"customerRadioOption\"{2} /><label style=\"width:35%;\" for=\"{0}customerOption\">{3}</label>", propertyDescriptor, customerSplitId, customerRadioOptionChecked, customerSplitDescription);

            var noneRadioOptionChecked = string.Empty; // "0" == splitId.ToString().ToUpperInvariant() ? " checked=\"checked\"" : string.Empty;
            var nonRadioOptionClass = "noneButtonWrapper";
            if (string.IsNullOrWhiteSpace(customerRadioOptionChecked) && string.IsNullOrWhiteSpace(insurerRadioOptionChecked))
            {
                noneRadioOptionChecked = " checked=\"checked\"";
                nonRadioOptionClass = "noneButtonWrapper-active";
            }

            controlBuilder.AppendFormat("<input type=\"radio\" id=\"{0}NoneOption\" name=\"{0}OptionGroup\" value=\"0\" class=\"noneRadioOption\"{2} /><label style=\"width:30%;\" for=\"{0}NoneOption\">&nbsp;</label>", propertyDescriptor, customerSplitId, noneRadioOptionChecked, customerSplitDescription);

            controlBuilder.AppendFormat("<input type=\"radio\" id=\"{0}insurerOption\" name=\"{0}OptionGroup\" value=\"{1}\" class=\"insurerRadioOption\"{2} /><label style=\"width:35%;\" for=\"{0}insurerOption\">{3}</label>", propertyDescriptor, insurerSplitId, insurerRadioOptionChecked, insurerSplitDescription);
            controlBuilder.Append("</div>");
            controlBuilder.AppendFormat("<div id=\"{0}CostInput\" class=\"invoiceLineItemAmount\" style=\"position:absolute;z-index:2;top:0;left:81px;width:67px;border:0px solid #CCC;\">", propertyDescriptor);
            
            // Read/Write Section
            if (isAmountReadOnly) 
            {
                // The amount is read only and can not be modified by the user.
                controlBuilder.AppendFormat("<div style=\"width:100%;background-color:#ccc;height:auto;position:relative;text-align:right; cursor: default;border:1px solid #CCC;\"><span class=\"lineItemAmountText\">{0}</span>", totalAmountString);
                controlBuilder.AppendFormat("<input type=\"hidden\" id=\"{0}\" name=\"{1}\" class=\"lineItemAmountInput\" style=\"width:60px;text-align:right;border-width:0px;padding:0px;\" value=\"{2}\" />", totalAmountPropertyId, totalAmountPropertyName, totalAmountString);
                controlBuilder.Append("</div>");
            }
            else
            {
                controlBuilder.Append("<div class=\"\" style=\"width:100%;background-color:#fff;position:relative;border:1px solid #CCC;\">");
                controlBuilder.AppendFormat("<input type=\"text\" id=\"{0}\" name=\"{1}\" class=\"lineItemAmountInput\" style=\"width:60px;text-align:right;border-width:0px;padding:0px;\" value=\"{2}\" />", totalAmountPropertyId, totalAmountPropertyName, totalAmountString);
                controlBuilder.Append("</div>");
            }

            controlBuilder.AppendFormat("<div style=\"text-align:center;\" class=\"noneButtonLabel {1}\"><span class=\"noneButton\">{0}</span></div>", "None", nonRadioOptionClass);
            controlBuilder.Append("</div>");
            controlBuilder.Append("</div>");
            // End Top Row / Bottom Row
            controlBuilder.AppendFormat("<div id=\"{0}BottomRow\" style=\"position:relative;top:4px;left:63px;width:240px;\">", propertyDescriptor);
            controlBuilder.AppendFormat("<div id=\"{0}Slider\" class=\"lineItemAmountSlider\" style=\"height:5px;width:160px;left:0;right:0;margin:0 auto;\"></div>", propertyDescriptor);
            controlBuilder.Append("</div>");

            // Insurer Amount Label / Input
            controlBuilder.AppendFormat("<input type=\"text\" id=\"{0}\" name=\"{1}\"  class=\"insurerLineItemAmountInput\" value=\"{2}\" style=\"display:none;width:60px;left:305px;top:5px;position:absolute;\" />", insurerFeeAmountId, insurerFeeAmountPropertyName, insurerFeeAmountString);
            controlBuilder.AppendFormat("<label id=\"{0}InsurerAmountLabel\" class=\"lineItemAmountLabel insurerLineItemAmount\" style=\"position:absolute;left:305px;text-align:right;top:9px;width:60px;\">{1}</label>", propertyDescriptor, insurerFeeAmountString);
            controlBuilder.Append("</div>");            

            return new MvcHtmlString(controlBuilder.ToString());            
        }       

        private static string GetFullPropertyName<T, TProperty>(Expression<Func<T, TProperty>> exp)
        {
            MemberExpression memberExp;
            if (!TryFindMemberExpression(exp.Body, out memberExp))
                return string.Empty;

            var memberNames = new Stack<string>();
            do
            {
                memberNames.Push(memberExp.Member.Name);
            }
            while (TryFindMemberExpression(memberExp.Expression, out memberExp));

            return string.Join(".", memberNames.ToArray());
        }

        // code adjusted to prevent horizontal overflow
        private static bool TryFindMemberExpression
        (Expression exp, out MemberExpression memberExp)
        {
            memberExp = exp as MemberExpression;
            if (memberExp != null)
            {
                // heyo! that was easy enough
                return true;
            }

            // if the compiler created an automatic conversion,
            // it'll look something like...
            // obj => Convert(obj.Property) [e.g., int -> object]
            // OR:
            // obj => ConvertChecked(obj.Property) [e.g., int -> long]
            // ...which are the cases checked in IsConversion
            if (IsConversion(exp) && exp is UnaryExpression)
            {
                memberExp = ((UnaryExpression)exp).Operand as MemberExpression;
                if (memberExp != null)
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsConversion(Expression exp)
        {
            return (
                exp.NodeType == ExpressionType.Convert ||
                exp.NodeType == ExpressionType.ConvertChecked
            );
        }

    }
}