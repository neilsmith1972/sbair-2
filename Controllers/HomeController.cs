﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace avis.sbair.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //ViewBag.Message = "SBAIR Login";

            return RedirectToAction("index","listing");
        }

        public ViewResult Faq()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(FormCollection frm)
        {
            if (FormsAuthentication.Authenticate(frm["txtUser"], frm["txtPW"]))
            {
                FormsAuthentication.RedirectFromLoginPage(frm["txtUser"], false);
                return RedirectToAction("index", "listing");
            }
            return View();
        }
    }
}
