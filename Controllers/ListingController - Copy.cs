﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using avis.sbair.Entities;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Objects;
using System.IO;

namespace avis.sbair.Controllers
{
    public class ListingController : Controller
    {
        private SBAIR3Context db = new SBAIR3Context();

        public ViewResult Index()
        {
            string sSearch = Request["searchTerm"];
            return Search_Index(sSearch);
        }


        ViewResult Search_Index(string searchTerm)
        {
            string search = searchTerm;
            if (search == null) { search = string.Empty; }

            string tab = Request["tab"];
            if (tab == null || tab == string.Empty) { tab = "0"; }
            ViewBag.tab = tab;

            if (tab == "4")
            {
                ViewBag.InsuranceCompanies = db.InsuranceCompanies.Where(a => a.Name.Contains(search) || a.WizardNumber.Contains(search) || a.AwdNumber.Contains(search) || a.Account.Contains(search) || a.RateCode.Contains(search)).OrderBy(i => i.Name).ToList();
                return View("index");
            }

            if (tab == "5")
            {
                ViewBag.RateCodes = db.Rates.OrderBy(i => i.RateCode).ToList();
                return View("index");
            }

            string status = string.Empty;
            string query = string.Empty;

            IEnumerable<Interfaces.IRental> Rentals = new List<Interfaces.IRental>();

            switch (tab)
            {
                case "0":
                status = "Open";
                //get records in openrentals table that don't have a matching records in splitbilling
                    List<OpenRental> open = db.OpenRentals.Where(s => !db.SplitBillings.Any(y => y.RentalAgreementNumber == s.RentalAgreementNumber) && (s.RentalAgreementNumber.Contains(search) || s.RentalVehicleLicensePlateNumber.Contains(search) || (s.RentalACTONumber != null ? s.RentalACTONumber.Contains(search) : false) || s.RentalCheckinLocationText.Contains(search))).ToList();
                    Rentals = open; //.Where(o=>o.RentalAgreementNumber.Contains(search) || o.RentalVehicleLicensePlateNumber.Contains(search) || o.RentalCheckinLocationText.Contains(search)).ToList();
                break;

                case "1":
                    //get records in splitbilling table that don't have a matching records in exceptions or closed rentals
                List<SplitBilling> sb = db.SplitBillings.Where(s => !db.ExceptionRentals.Any(y => y.RentalAgreementNumber == s.RentalAgreementNumber) && !db.ClosedRentals.Any(z => z.RentalAgreementNumber == s.RentalAgreementNumber)).ToList();
                Rentals = sb.Where(s => s.Status == 2 && (s.RentalAgreementNumber.Contains(search) || s.RentalVehicleLicensePlateNumber.Contains(search) || (s.RentalACTONumber != null ? s.RentalACTONumber.Contains(search) : false) || s.RentalCheckinLocationText.Contains(search))).ToList();
                break;

                case "2":
                Rentals = db.ExceptionRentals.Where(o => o.RentalAgreementNumber.Contains(search) || o.RentalVehicleLicensePlateNumber.Contains(search) || (o.RentalACTONumber != null ? o.RentalACTONumber.Contains(search) : false) || o.RentalCheckinLocationText.Contains(search)).ToList();
                break;

                case "3":
                var r = db.ClosedRentals.ToList().AsEnumerable();
                    List<ClosedRental> cr = new List<ClosedRental>();
                if (search.Trim() != string.Empty)
                {
                    foreach (var item in r)
                    {

                        if (item.RentalAgreementNumber.ToLower().Contains(search.ToLower()) ||
                            item.RentalVehicleLicensePlateNumber.ToLower().Contains(search.ToLower()) ||
                            item.RentalCheckinLocationText.ToLower().Contains(search.ToLower())
                            || item.RentalInsuranceInvoiceNumber.ToString().Contains(search)
                            || item.RentalRenterInvoiceNumber.ToString().Contains(search)
                            || (item.RentalACTONumber != null ? item.RentalACTONumber.Contains(search) : false))
                        {
                            cr.Add(item);
                        }
                       
                    }
                    Rentals = cr;
                }
                else
                {
                    Rentals = r.ToList();
                }
                    //Rentals = r.Where(o => o.RentalAgreementNumber.Contains(search) || o.RentalVehicleLicensePlateNumber.Contains(search) || o.RentalCheckinLocationText.Contains(search)).ToList();
                    //Rentals = r.Where(x => x.RentalInsuranceInvoiceNumber.ToString().Contains(search)).ToList();
                break;
            }

            List<Interfaces.IRental> rentals = Rentals.ToList();
            return View("index",rentals);
        }

        public ViewResult Search(FormCollection frm)
        {
            string sSearch = Request["searchTerm"];
            if (sSearch != null)
            {
                ViewBag.Search = sSearch;
            }
            else
            {
                if (ViewBag.Search != null)
                {
                    sSearch = ViewBag.Search;
                    if (sSearch != string.Empty)
                    {
                        ViewBag.Search = sSearch;
                    }
                }
            }
            return Search_Index(sSearch);
        }

        public ActionResult Edit(string id)
        {
            ViewBag.Region = ConfigurationManager.AppSettings["Region"].ToString();

            string type = Request["type"];

            if (Request["return"] != null)
            { ViewBag.Return = Request["return"]; }
            else
            {
                ViewBag.Return = "listing/index";
            }

            ViewBag.WZD = "none";

            SplitBilling rec = new SplitBilling();
            switch (type)
            {
                case "open":

                    //does this record already exist in a pending state? if so, get the pending record, not the open one
                    if (db.SplitBillings.Where(o => o.RentalAgreementNumber == id).FirstOrDefault() != null)
                    {
                        rec = db.SplitBillings.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();
                        ViewBag.Status = db.Status.Where(s => s.ID == rec.Status).First().StatusText;
                        if (rec.KMCheckin == null || rec.KMCheckin < rec.KMDriven) rec.KMCheckin = rec.KMDriven;
                        break;
                    }


                    ViewBag.Status = "Open";
                    Entities.OpenRental openrental = db.OpenRentals.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();
                    rec.RentalAgreementNumber = openrental.RentalAgreementNumber;
                    DateTime eTemp = new DateTime(openrental.RentalCheckinDateTime.Value.Year, openrental.RentalCheckinDateTime.Value.Month, openrental.RentalCheckinDateTime.Value.Day, openrental.RentalCheckinDateTime.Value.Hour, openrental.RentalCheckinDateTime.Value.Minute, openrental.RentalCheckinDateTime.Value.Second);
                    rec.RentalCheckinDateTime = openrental.RentalCheckinDateTime;
                    rec.RentalCheckinLocationText = openrental.RentalCheckinLocationText;
                    DateTime sTemp = new DateTime(openrental.RentalCheckoutDateTime.Value.Year, openrental.RentalCheckoutDateTime.Value.Month, openrental.RentalCheckoutDateTime.Value.Day, openrental.RentalCheckoutDateTime.Value.Hour, openrental.RentalCheckoutDateTime.Value.Minute, openrental.RentalCheckoutDateTime.Value.Second);
                    rec.RentalCheckoutDateTime = openrental.RentalCheckoutDateTime;
                    rec.RentalCheckoutLocationText = openrental.RentalCheckoutLocationText;
                    rec.Status = 1; //db.Status.First().ID;
                    rec.RentalVehicleLicensePlateNumber = openrental.RentalVehicleLicensePlateNumber;
                    rec.Country = db.Countries.First().ID;
                    rec.RentalDamageNumber = openrental.RentalDamageNumber;
                    rec.RentalCustomerName = openrental.RentalCustomerName;
                    rec.RentalRenterAddress1Text = openrental.RentalRenterAddress1Text;
                    rec.RentalRenterAddress2Text = openrental.RentalRenterAddress2Text;
                    rec.RentalRenterAddressCityText = openrental.RentalRenterAddressCityText;
                    rec.KMDriven = (int) openrental.KMDriven;
                    rec.RentalCompanyName = openrental.RentalCompanyName;
                    rec.RentalVehicleClassChargedCode = openrental.RentalVehicleClassChargedCode;
                    rec.RentalCDWPerDayAmount = openrental.RentalCDWPerDayAmount;
                    rec.RentalPAIPerDayAmount = openrental.RentalPAIPerDayAmount;
                    rec.RentalACTONumber = openrental.RentalACTONumber;
                    rec.RentalRenterCreditClubCode = openrental.RentalRenterCreditClubCode;
                    rec.RentalRenterCreditCardNumber = openrental.RentalRenterCreditCardNumber;
                    rec.RentalInsuranceBillingCompany = openrental.RentalInsuranceBillingCompany;
                    rec.RentalInsuranceBillingName = openrental.RentalInsuranceBillingName;
                    rec.RentalInsuranceBillingAddress1 = openrental.RentalInsuranceBillingAddress1;
                    rec.RentalInsuranceBillingAddress2 = openrental.RentalInsuranceBillingAddress2;
                    rec.RentalInsuranceBillingAddress3 = openrental.RentalInsuranceBillingAddress3;
                    
                    if (openrental.RentalContractFeeAmount == null) { openrental.RentalContractFeeAmount = 0; }

                    rec.AlreadyPaidByCustomerAmount = 0;
                    rec.AlreadyPaidByInsuranceAmount = 1;
                    rec.VATTotal = 0;
                    rec.RAITotal = 0;
                    rec.RAICustomerPays = 0;
                    rec.SuperCDWTotal = 0;
                    rec.SuperCDWCustomerPays = 0;
                    rec.OneWayTotal = 0;
                    rec.OneWayCustomerPays = 0;
                    rec.RefuellingTotal = 0;
                    rec.RefuellingCustomerPays = 0;
                    rec.DeliveryTotal = 0;
                    rec.DeliveryCustomerPays = 0;
                    rec.ReturnTotal = 0;
                    rec.ReturnCustomerPays = 0;
                    rec.MiscCostsTotal = 0;
                    rec.MiscCostsCustomerPays = 0;
                    rec.VRFTotal = 52;
                    rec.VRFCustomerPays = 0;
                    rec.VRFSplit = 9;
                    rec.MiscCostsTotal = 0;
                    rec.MiscCostsCustomerPays = 0;
                    rec.InvoiceFeeTotal = (decimal) openrental.RentalContractFeeAmount;
                    rec.InvoiceFeeCustomerPays = (decimal)openrental.RentalContractFeeAmount;
                    rec.AirportFeeTotal = 0;
                    rec.AirportFeeCustomerPays = 0;
                    rec.OtherFeeTotal = 0;
                    rec.OtherFeeCustomerPays = 0;
                    rec.RentalContractFeeAmount = (decimal)openrental.RentalContractFeeAmount;

                    rec.PriceSplitTotal = 0;
                    //rec.CDWSplit = 11;
                    //rec.RAISplit = 13;
                    rec.RentalRenterInvoiceFeeAmount = (decimal)openrental.RentalRenterInvoiceFeeAmount;
                    rec.RentalMileageRateAmount = (decimal) openrental.RentalMileageRateAmount; 
                    rec.RentalDailyRateAmount = (decimal) openrental.RentalDailyRateAmount;
                    rec.RentalWeeklyRateAmount = (decimal) openrental.RentalWeeklyRateAmount; 
                    rec.RentalMonthlyRateAmount = (decimal) openrental.RentalMonthlyRateAmount;
                    rec.KmCoveredByInsurance = openrental.RentalFreeMileageCount;
                    rec.DaysCoveredByInsurance = 0;

                    if (rec.KMCheckin == null || rec.KMCheckin < rec.KMDriven) rec.KMCheckin = rec.KMDriven;
                    if (rec.RentalCheckinDateTime < rec.RentalCheckoutDateTime) rec.RentalCheckinDateTime = rec.RentalCheckoutDateTime.Value.AddHours(1);

                    break;

                case "exception":
                    rec = db.SplitBillings.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();
                    Entities.ExceptionRental exceptionRental = db.ExceptionRentals.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();
                    ViewBag.WZD = "block";
                    if (rec == null)
                    {
                        rec = new SplitBilling();
                        rec.RentalAgreementNumber = exceptionRental.RentalAgreementNumber;
                        //DateTime eTemp = new DateTime(exceptionRental.RentalCheckinDateTime.Value.Year, exceptionRental.RentalCheckinDateTime.Value.Month, exceptionRental.RentalCheckinDateTime.Value.Day, exceptionRental.RentalCheckinDateTime.Value.Hour, exceptionRental.RentalCheckinDateTime.Value.Minute, exceptionRental.RentalCheckinDateTime.Value.Second);
                        rec.RentalCheckinDateTime = exceptionRental.RentalCheckinDateTime;
                        rec.RentalCheckinLocationText = exceptionRental.RentalCheckinLocationText;
                        //DateTime sTemp = new DateTime(exceptionRental.RentalCheckoutDateTime.Value.Year, exceptionRental.RentalCheckoutDateTime.Value.Month, exceptionRental.RentalCheckoutDateTime.Value.Day, exceptionRental.RentalCheckoutDateTime.Value.Hour, exceptionRental.RentalCheckoutDateTime.Value.Minute, exceptionRental.RentalCheckoutDateTime.Value.Second);
                        rec.RentalCheckoutDateTime = exceptionRental.RentalCheckoutDateTime;
                        rec.RentalCheckoutLocationText = exceptionRental.RentalCheckoutLocationText;
                        rec.Status = 3;
                        rec.RentalVehicleLicensePlateNumber = exceptionRental.RentalVehicleLicensePlateNumber;
                        rec.Country = db.Countries.First().ID;
                        rec.RentalDamageNumber = exceptionRental.RentalDamageNumber;
                        rec.RentalCustomerName = exceptionRental.RentalCustomerName;
                        rec.RentalRenterAddress1Text = exceptionRental.RentalRenterAddress1Text;
                        rec.RentalRenterAddress2Text = exceptionRental.RentalRenterAddress2Text;
                        rec.RentalRenterAddressCityText = exceptionRental.RentalRenterAddressCityText;
                        rec.KMDriven = (int)exceptionRental.KMDriven;
                        rec.RentalCompanyName = exceptionRental.RentalCompanyName;
                        rec.RentalVehicleClassChargedCode = exceptionRental.RentalVehicleClassChargedCode;
                        rec.RentalCDWPerDayAmount = exceptionRental.RentalCDWPerDayAmount;
                        rec.RentalPAIPerDayAmount = exceptionRental.RentalPAIPerDayAmount;
                        rec.RentalACTONumber = exceptionRental.RentalACTONumber;
                        rec.RentalRenterCreditClubCode = exceptionRental.RentalRenterCreditClubCode;
                        rec.RentalRenterCreditCardNumber = exceptionRental.RentalRenterCreditCardNumber;

                        rec.RentalVATAmount = exceptionRental.RentalVATAmount;
                        rec.RentalInvoiceFeeAmount = exceptionRental.RentalInvoiceFeeAmount;
                        rec.RentalOtherCosts = exceptionRental.RentalOtherCosts;

                        rec.RentalInsuranceBillingCompany = exceptionRental.RentalInsuranceBillingCompany;
                        rec.RentalInsuranceBillingName = exceptionRental.RentalInsuranceBillingName;
                        rec.RentalInsuranceBillingAddress1 = exceptionRental.RentalInsuranceBillingAddress1;
                        rec.RentalInsuranceBillingAddress2 = exceptionRental.RentalInsuranceBillingAddress2;
                        rec.RentalInsuranceBillingAddress3 = exceptionRental.RentalInsuranceBillingAddress3;

                        if (exceptionRental.RentalContractFeeAmount == null) { exceptionRental.RentalContractFeeAmount = 0; }
                        if (rec.KMCheckin == null || rec.KMCheckin < rec.KMDriven) rec.KMCheckin = rec.KMDriven;

                        rec.RentalRenterInvoiceFeeAmount = (decimal)exceptionRental.RentalRenterInvoiceFeeAmount;


                        rec.AlreadyPaidByCustomerAmount = 0;
                        rec.AlreadyPaidByInsuranceAmount = 1;
                        rec.VATTotal = 0;
                        rec.RAITotal = 0;
                        rec.RAICustomerPays = 0;
                        rec.SuperCDWTotal = 0;
                        rec.SuperCDWCustomerPays = 0;
                        rec.OneWayTotal = 0;
                        rec.OneWayCustomerPays = 0;
                        rec.RefuellingTotal = 0;
                        rec.RefuellingCustomerPays = 0;
                        rec.DeliveryTotal = 0;
                        rec.DeliveryCustomerPays = 0;
                        rec.ReturnTotal = 0;
                        rec.ReturnCustomerPays = 0;
                        rec.MiscCostsTotal = 0;
                        rec.MiscCostsCustomerPays = 0;
                        rec.VRFTotal = 52;
                        rec.VRFCustomerPays = 0;
                        rec.VRFSplit = 9;
                        rec.MiscCostsTotal = 0;
                        rec.MiscCostsCustomerPays = 0;
                        rec.InvoiceFeeTotal = (decimal)exceptionRental.RentalContractFeeAmount;
                        rec.InvoiceFeeCustomerPays = (decimal)exceptionRental.RentalContractFeeAmount;
                        rec.AirportFeeTotal = 0;
                        rec.AirportFeeCustomerPays = 0;
                        rec.OtherFeeTotal = 0;
                        rec.OtherFeeCustomerPays = 0;
                        rec.RentalContractFeeAmount = (decimal)exceptionRental.RentalContractFeeAmount;

                        rec.PriceSplitTotal = 0;
                        rec.RentalMileageRateAmount = (decimal)exceptionRental.RentalMileageRateAmount;
                        rec.RentalDailyRateAmount = (decimal)exceptionRental.RentalDailyRateAmount;
                        rec.RentalWeeklyRateAmount = (decimal)exceptionRental.RentalWeeklyRateAmount;
                        rec.RentalMonthlyRateAmount = (decimal)exceptionRental.RentalMonthlyRateAmount;
                        rec.KmCoveredByInsurance = exceptionRental.RentalFreeMileageCount;
                        rec.DaysCoveredByInsurance = 0;
                        rec.KmCoveredByInsurance = 0;
                    }
                    else
                    {
                        rec.Status = 3;
                    }
                    ViewBag.Status = "Exception";
                    ViewBag.RentalRenterTotalAmount = exceptionRental.RentalRenterTotalAmount;
                    ViewBag.RentalInsuranceTotalAmount = exceptionRental.RentalInsuranceTotalAmount;

                    rec.RentalVATAmount = exceptionRental.RentalVATAmount;
                    rec.RentalInvoiceFeeAmount = exceptionRental.RentalInvoiceFeeAmount;
                    rec.RentalOtherCosts = exceptionRental.RentalOtherCosts;

                    rec.NetTimeDistanceAmount = exceptionRental.NetTimeDistanceAmount;
                    rec.RentalCDWAmount = exceptionRental.RentalCDWAmount;
                    rec.RentalPAIAmount = exceptionRental.RentalPAIAmount;
                    rec.RentalSurchargeAmount = exceptionRental.RentalSurchargeAmount;
                    rec.RentalOneWayFeeAmount = exceptionRental.RentalOneWayFeeAmount;
                    rec.RentalGasChargesAmount = exceptionRental.RentalGasChargesAmount;
                    rec.RentalDeliveryChargeAmount = exceptionRental.RentalDeliveryChargeAmount;
                    rec.RentalCollectionChargeAmount = exceptionRental.RentalCollectionChargeAmount;
                    rec.RentalContractFeeAmount = exceptionRental.RentalContractFeeAmount;
                    rec.RentalMiscChargeAmount = exceptionRental.RentalMiscChargeAmount;

                    break;

                case "closed":

                    Entities.ClosedRental closedrental = db.ClosedRentals.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();

                    FileInfo c_i = new FileInfo(ConfigurationManager.AppSettings["InvoiceStore"].ToString() + "customer_Invoice_" + closedrental.RentalAgreementNumber + ".pdf");
                    if (c_i.Exists)
                    {
                        ViewBag.CustomerInvoice = ConfigurationManager.AppSettings["SiteUrl"].ToString() + "InvoiceStore/customer_Invoice_" + closedrental.RentalAgreementNumber + ".pdf";
                        ViewBag.InsuranceInvoice = ConfigurationManager.AppSettings["SiteUrl"].ToString() + "InvoiceStore/insurance_Invoice_" + closedrental.RentalAgreementNumber + ".pdf";
                    }


                    //does this record already exist in a pending state? if so, get the pending record, not the closed one
                    if (db.SplitBillings.Where(o => o.RentalAgreementNumber == id).FirstOrDefault() != null)
                    {
                        rec = db.SplitBillings.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();
                        //rec.InsuranceInvoiceNumber = closedrental.InsuranceInvoiceNumber;
                        rec.RentalRenterInvoiceNumber = closedrental.RentalRenterInvoiceNumber;
                        rec.RentalInsuranceInvoiceNumber = closedrental.RentalInsuranceInvoiceNumber;
                        //rec.RenterInvoiceNumber = closedrental.RenterInvoiceNumber;
                        ViewBag.Status = "Closed";
                        break;
                    }

                    rec.RentalRenterTotalAmount = closedrental.RentalRenterTotalAmount;
                    rec.RentalInsuranceTotalAmount = closedrental.RentalInsuranceTotalAmount;
                    rec.RentalInsuranceInvoiceNumber = closedrental.RentalInsuranceInvoiceNumber;
                    rec.RentalRenterInvoiceNumber = closedrental.RentalRenterInvoiceNumber;
                    rec.RentalAgreementNumber = closedrental.RentalAgreementNumber;
                    rec.RentalCheckinDateTime = closedrental.RentalCheckinDateTime;
                    rec.RentalCheckinLocationText = closedrental.RentalCheckinLocationText;
                    rec.RentalCheckoutDateTime = closedrental.RentalCheckoutDateTime;
                    rec.RentalCheckoutLocationText = closedrental.RentalCheckoutLocationText;
                    rec.Status = 5;
                    rec.RentalVehicleLicensePlateNumber = closedrental.RentalVehicleLicensePlateNumber;
                    rec.Country = db.Countries.First().ID;
                    rec.RentalDamageNumber = closedrental.RentalDamageNumber;
                    rec.RentalCustomerName = closedrental.RentalCustomerName;
                    rec.RentalRenterAddress1Text = closedrental.RentalRenterAddress1Text;
                    rec.RentalRenterAddress2Text = closedrental.RentalRenterAddress2Text;
                    rec.RentalRenterAddressCityText = closedrental.RentalRenterAddressCityText;
                    rec.KMDriven = (int)closedrental.KMDriven;
                    rec.RentalCompanyName = closedrental.RentalCompanyName;
                    rec.RentalVehicleClassChargedCode = closedrental.RentalVehicleClassChargedCode;
                    rec.RentalACTONumber = closedrental.RentalACTONumber;
                    rec.RentalRenterCreditClubCode = closedrental.RentalRenterCreditClubCode;
                    rec.RentalRenterCreditCardNumber = closedrental.RentalRenterCreditCardNumber;

                    rec.RentalInsuranceBillingCompany = closedrental.RentalInsuranceBillingCompany;
                    rec.RentalInsuranceBillingName = closedrental.RentalInsuranceBillingName;
                    rec.RentalInsuranceBillingAddress1 = closedrental.RentalInsuranceBillingAddress1;
                    rec.RentalInsuranceBillingAddress2 = closedrental.RentalInsuranceBillingAddress2;
                    rec.RentalInsuranceBillingAddress3 = closedrental.RentalInsuranceBillingAddress3;

                    if (closedrental.RentalContractFeeAmount == null) { closedrental.RentalContractFeeAmount = 0; }

                    rec.AlreadyPaidByCustomerAmount = 0;
                    rec.AlreadyPaidByInsuranceAmount = 0;
                    rec.VATTotal = 0;
                    rec.RAITotal = 0;
                    rec.RAICustomerPays = 0;
                    rec.SuperCDWTotal = 0;
                    rec.SuperCDWCustomerPays = 0;
                    rec.OneWayTotal = 0;
                    rec.OneWayCustomerPays = 0;
                    rec.RefuellingTotal = 0;
                    rec.RefuellingCustomerPays = 0;
                    rec.DeliveryTotal = 0;
                    rec.DeliveryCustomerPays = 0;
                    rec.ReturnTotal = 0;
                    rec.ReturnCustomerPays = 0;
                    rec.MiscCostsTotal = 0;
                    rec.MiscCostsCustomerPays = 0;
                    rec.VRFTotal = 0;
                    rec.VRFCustomerPays = 0;
                    rec.MiscCostsTotal = 0;
                    rec.MiscCostsCustomerPays = 0;
                    rec.InvoiceFeeTotal = (decimal)closedrental.RentalContractFeeAmount;
                    rec.InvoiceFeeCustomerPays = (decimal)closedrental.RentalContractFeeAmount;
                    rec.AirportFeeTotal = 0;
                    rec.AirportFeeCustomerPays = 0;
                    rec.OtherFeeTotal = 0;
                    rec.OtherFeeCustomerPays = 0;
                    rec.RentalContractFeeAmount = (decimal)closedrental.RentalContractFeeAmount;

                    rec.PriceSplitTotal = 0;
                    rec.RentalMileageRateAmount = (decimal) closedrental.RentalMileageRateAmount; 
                    rec.RentalDailyRateAmount = (decimal) closedrental.RentalDailyRateAmount;
                    rec.RentalWeeklyRateAmount = (decimal) closedrental.RentalWeeklyRateAmount;
                    rec.RentalMonthlyRateAmount = (decimal) closedrental.RentalMonthlyRateAmount;
                    rec.KmCoveredByInsurance = closedrental.RentalFreeMileageCount;
                    rec.DaysCoveredByInsurance = 0;
                    ViewBag.Status = "Closed";

                    break;
                case "pending":
                    rec = db.SplitBillings.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();
                    ViewBag.Status = "Pending";
                    bool editable = false;
                    audit auditRec =
                        db.audits.Where(au => au.RentalNumber == id && au.Status_ID == 2 && !au.Comment.StartsWith("Record viewed")).OrderByDescending(
                            au => au.TimeChanged).FirstOrDefault();
                    if (auditRec != null)
                    {
                        DateTime threshold = new DateTime(auditRec.TimeChanged.Year, auditRec.TimeChanged.Month, auditRec.TimeChanged.Day, 23, 00, 00);
                        editable = DateTime.Now <= threshold;
                    }

                    ViewBag.Editable = editable;
                    break;
            }

            BuildLists(rec);

            //ViewBag.InvoiceFee = db.InvoiceFees.Where(i => i.FromDate <= DateTime.Now && i.ToDate >= DateTime.Now).First().Price;

            audit a = new audit();
            a.Status_ID = rec.Status;
            a.RentalNumber = rec.RentalAgreementNumber;
            a.TimeChanged = DateTime.Now;
            a.Comment = "Record viewed in SBAIR.";
            db.audits.Add(a);
            db.SaveChanges();

            if (rec.RentalContractFeeAmount == null) { rec.RentalContractFeeAmount = 0; }

            return View(rec);
        }


        void BuildLists(SplitBilling rec)
        {
            List<SelectListItem> statuscodes = new List<SelectListItem>();
            foreach (Entities.Status status in db.Status.OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = status.ID.ToString();
                s.Text = status.StatusText;
                statuscodes.Add(s);
            }
            ViewBag.StatusCodes = statuscodes;

            //List<SelectListItem> priceSplits = new List<SelectListItem>();
            //foreach (Entities.PriceSplit priceSplit in db.PriceSplits.OrderBy(c => c.ID).ToList())
            //{
            //    SelectListItem s = new SelectListItem();
            //    s.Value = priceSplit.ID.ToString();
            //    s.Text = priceSplit.Description;
            //    string x = s.ToString();
            //    //if (rec.PriceSplit.ID == priceSplit.ID) { s.Selected = true; }
            //    priceSplits.Add(s);
            //}
            //ViewBag.PriceSplits = priceSplits;

            string ps = string.Empty;
            if (rec.PriceSplit == null) rec.PriceSplit = 1;
            ps = "<select id='PriceSplit' name='PriceSplit' style = 'margin-top:5px;' >";
            foreach (Entities.PriceSplit priceSplit in db.PriceSplits.OrderBy(c => c.ID).ToList())
            {
                ps = ps + "<option value='" + priceSplit.ID + "' title='" + priceSplit.Description + "'";
                if (rec.PriceSplit.Value == priceSplit.ID) ps = ps + " selected = 'selected' ";
                ps = ps + ">" + priceSplit.Description + "</option>";
                
            }
            ps = ps + "</select>";
            ViewBag.PriceSplits = ps;

            string vs = string.Empty;
            if (rec.VATSplit == null) rec.VATSplit = 1;
            vs = "<select id='VATSplit' name='VATSplit' style = 'margin-top:5px;' >";
            foreach (Entities.VATSplit vatSplit in db.VATSplits.OrderBy(c => c.ID).ToList())
            {
                vs = vs + "<option value='" + vatSplit.ID + "' title=\"" + vatSplit.Description + "\"";
                if (rec.VATSplit.Value == vatSplit.ID) vs = vs + " selected = 'selected' ";
                vs = vs + ">" + vatSplit.Description + "</option>";
            }
            vs = vs + "</select>";
            ViewBag.VATSplits = vs;

            //List<SelectListItem> vatSplits = new List<SelectListItem>();
            //foreach (Entities.VATSplit vatSplit in db.VATSplits.OrderBy(c => c.ID).ToList())
            //{
            //    SelectListItem s = new SelectListItem();
            //    s.Value = vatSplit.ID.ToString();
            //    s.Text = vatSplit.Description;
            //    //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
            //    vatSplits.Add(s);
            //}
            //ViewBag.VATSplits = vatSplits;

            List<SelectListItem> CDWSplits = new List<SelectListItem>();
            CDWSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit CDWSplit in db.OtherCostSplits.Where(o => o.Code_ID == 1).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = CDWSplit.ID.ToString();
                s.Text = CDWSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                CDWSplits.Add(s);
            }
            ViewBag.CDWSplits = CDWSplits;

            List<SelectListItem> RAISplits = new List<SelectListItem>();
            RAISplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit RAISplit in db.OtherCostSplits.Where(o => o.Code_ID == 5).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = RAISplit.ID.ToString();
                s.Text = RAISplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                RAISplits.Add(s);
            }
            ViewBag.RAISplits = RAISplits;

            List<SelectListItem> AirportSplits = new List<SelectListItem>();
            AirportSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.FeeSplit AirportSplit in db.FeeSplits.Where(o => o.FeeType_ID == 1).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = AirportSplit.ID.ToString();
                s.Text = AirportSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                AirportSplits.Add(s);
            }
            ViewBag.AirportSplits = AirportSplits;

            List<SelectListItem> OneWaySplits = new List<SelectListItem>();
            OneWaySplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.FeeSplit OneWaySplit in db.FeeSplits.Where(o => o.FeeType_ID==3).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = OneWaySplit.ID.ToString();
                s.Text = OneWaySplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                OneWaySplits.Add(s);
            }
            ViewBag.OneWaySplits = OneWaySplits;

            List<SelectListItem> OtherSplits = new List<SelectListItem>();
            OtherSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit OtherSplit in db.OtherCostSplits.Where(o => o.Code_ID == 4).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = OtherSplit.ID.ToString();
                s.Text = OtherSplit.Description;
                OtherSplits.Add(s);
            }
            ViewBag.OtherSplits = OtherSplits;

            List<SelectListItem> RefuellingSplits = new List<SelectListItem>();
            RefuellingSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit RefuellingSplit in db.OtherCostSplits.Where(o => o.Code_ID == 6).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = RefuellingSplit.ID.ToString();
                s.Text = RefuellingSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                RefuellingSplits.Add(s);
            }
            ViewBag.RefuellingSplits = RefuellingSplits;

            List<SelectListItem> DeliverySplits = new List<SelectListItem>();
            DeliverySplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit DeliverySplit in db.OtherCostSplits.Where(o => o.Code_ID == 2).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = DeliverySplit.ID.ToString();
                s.Text = DeliverySplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                DeliverySplits.Add(s);
            }
            ViewBag.DeliverySplits = DeliverySplits;

            List<SelectListItem> ReturnSplits = new List<SelectListItem>();
            ReturnSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit ReturnSplit in db.OtherCostSplits.Where(o => o.Code_ID == 7).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = ReturnSplit.ID.ToString();
                s.Text = ReturnSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                ReturnSplits.Add(s);
            }
            ViewBag.ReturnSplits = ReturnSplits;

            List<SelectListItem> VRFSplits = new List<SelectListItem>();
            VRFSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit VRFSplit in db.OtherCostSplits.Where(o => o.Code_ID == 8).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = VRFSplit.ID.ToString();
                s.Text = VRFSplit.Description;
                //if (rec.VRFSplit > 0 && (rec.CongestionFeeSplit == null || rec.CongestionFeeSplit.ID < 0)) { s.Selected = true; }
                VRFSplits.Add(s);
            }
            ViewBag.VRFSplits = VRFSplits;

            List<SelectListItem> MiscSplits = new List<SelectListItem>();
            MiscSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit MiscSplit in db.OtherCostSplits.Where(o => o.Code_ID == 3).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = MiscSplit.ID.ToString();
                s.Text = MiscSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                MiscSplits.Add(s);
            }
            ViewBag.MiscSplits = MiscSplits;

        }


        ////
        //// POST: /Listing/Edit/5

        [HttpPost]
        public ActionResult Edit(SplitBilling SB, FormCollection frm)
        {

            SplitBilling rec = db.SplitBillings.Where(s => s.RentalAgreementNumber == SB.RentalAgreementNumber).FirstOrDefault();
            
            SB.RentalTransactionType = "generic";
            SB.Timestamp = DateTime.Now;
            SB.Status = 2;
            SB.RentalCheckInYear = SB.RentalCheckinDateTime.Value.Year;
            //switch(Request["type"])
            //{
            //    case "open":
            //        SB.Status = 2;
            //        break;
            //    case "exception":
            //        SB.Status = 2;
            //        break;
            //}

            //decimal dInvoice = db.InvoiceFees.Where(i => i.FromDate <= DateTime.Now && i.ToDate >= DateTime.Now).First().Price;
            //if (Request["PM"] == "2") { SB.InvoiceFeeTotal = dInvoice; SB.InvoiceFeeCustomerPays = dInvoice; } else { SB.InvoiceFeeTotal = 0; SB.InvoiceFeeCustomerPays = 0; } 
            if (rec != null)
            {
                bool wasException = false;
                if (rec.Status == 3) { wasException = true; }
                rec.PriceSplit = SB.PriceSplit;
                rec.PriceSplitTotal = SB.PriceSplitTotal;
                rec.PriceSplitCustomerPays = SB.PriceSplitCustomerPays;
                rec.VATSplit = SB.VATSplit;
                rec.VATTotal = SB.VATTotal;
                rec.VATCustomerPays = SB.VATCustomerPays;
                rec.CDWSplit = SB.CDWSplit;
                rec.SuperCDWTotal = SB.SuperCDWTotal;
                rec.SuperCDWCustomerPays = SB.SuperCDWCustomerPays;
                rec.RAISplit = SB.RAISplit;
                rec.RAITotal = SB.RAITotal;
                rec.RAICustomerPays = SB.RAICustomerPays;
                rec.AirportFeeSplit = SB.AirportFeeSplit;
                rec.AirportFeeTotal = SB.AirportFeeTotal;
                rec.AirportFeeCustomerPays = SB.AirportFeeCustomerPays;
                rec.VRFSplit = SB.VRFSplit;
                rec.VRFTotal = SB.VRFTotal;
                rec.VRFCustomerPays = SB.VRFCustomerPays;
                rec.OneWayFeeSplit = SB.OneWayFeeSplit;
                rec.OneWayTotal = SB.OneWayTotal;
                rec.OneWayCustomerPays = SB.OneWayCustomerPays;
                rec.RefuelingSplit = SB.RefuelingSplit;
                rec.RefuellingTotal = SB.RefuellingTotal;
                rec.RefuellingCustomerPays = SB.RefuellingCustomerPays;
                rec.DeliveryFeeSplit = SB.DeliveryFeeSplit;
                rec.DeliveryTotal = SB.DeliveryTotal;
                rec.DeliveryCustomerPays = SB.DeliveryCustomerPays;
                rec.ReturnFeeSplit = SB.ReturnFeeSplit;
                rec.ReturnTotal = SB.ReturnTotal;
                rec.ReturnCustomerPays = SB.ReturnCustomerPays;
                rec.MiscCostsSplit = SB.MiscCostsSplit;
                rec.MiscCostsTotal = SB.MiscCostsTotal;
                rec.MiscCostsCustomerPays = SB.MiscCostsCustomerPays;
                rec.OtherFeeSplit = SB.OtherFeeSplit;
                rec.OtherFeeTotal = SB.OtherFeeTotal;
                rec.OtherFeeCustomerPays = SB.OtherFeeCustomerPays;
                rec.InvoiceFeeTotal = SB.InvoiceFeeTotal;
                rec.InvoiceFeeCustomerPays = SB.InvoiceFeeCustomerPays;
                rec.InvoiceAmountCustomer = SB.InvoiceAmountCustomer;
                rec.InvoiceAmountInsurance = SB.InvoiceAmountInsurance;
                rec.AlreadyPaidByCustomerAmount = SB.AlreadyPaidByCustomerAmount;
                rec.AlreadyPaidByInsuranceAmount = SB.AlreadyPaidByInsuranceAmount;
                rec.RentalCheckInYear = rec.RentalCheckinDateTime.Value.Year;
                rec.RentalCheckinDateTime = DateTime.Parse(frm["checkInDateDate"] + " " + frm["checkInDateTime"]);
                rec.Remarks = SB.Remarks;
                rec.RentalLocalContactText = SB.RentalLocalContactText;
                //if (wasException) 
                //{
                //    rec.Status = 3; 
                //}
                //else
                //{
                    rec.Status = 2;
                //} 
                
                db.Entry(rec).State = EntityState.Modified;
                db.SaveChanges();

                audit a = new audit();
                a.Status_ID = rec.Status;
                a.RentalNumber = rec.RentalAgreementNumber;
                a.TimeChanged = DateTime.Now;
                if (wasException)
                {
                    a.Comment = "Exception record updated in SBAIR. Awaiting back end process comparison with Wizard.";

                    List<InvoicedRental> i = db.InvoicedRentals.Where(x => x.AvisRANumber == rec.RentalAgreementNumber).ToList();
                    foreach (InvoicedRental ir in i)
                    {
                        ir.Processed = 0;
                        //db.SaveChanges();
                    }
                }
                else
                {
                    a.Comment = "Split billing record saved in SBAIR. Status set to Pending.";
                }
                db.audits.Add(a);
                db.SaveChanges();
            }
            else
            {
                //if (ModelState.IsValid)
                //{
                    try
                    {
                        
                        if (Request["type"] == "exception")
                        {
                            Entities.ExceptionRental o_r = db.ExceptionRentals.Where(o => o.RentalAgreementNumber == SB.RentalAgreementNumber).First();
                            SB.RentalInsuranceBillingCompany = o_r.RentalInsuranceBillingCompany;
                            SB.RentalInsuranceBillingAddress1 = o_r.RentalInsuranceBillingAddress1;
                            SB.RentalInsuranceBillingAddress2 = o_r.RentalInsuranceBillingAddress2;
                            SB.RentalInsuranceBillingAddress3 = o_r.RentalInsuranceBillingAddress3;
                            SB.RentalInsuranceBillingName = o_r.RentalInsuranceBillingName;
                            SB.RentalACTONumber = o_r.RentalACTONumber;
                            SB.RentalAWDNumber = o_r.RentalAWDNumber;
                            SB.RentalDamageNumber = o_r.RentalDamageNumber;
                            SB.RentalIATANumber = o_r.RentalIATANumber;
                            SB.RentalInsuranceInvoiceNumber = o_r.RentalInsuranceInvoiceNumber;
                            SB.RentalReservationNumber = o_r.RentalReservationNumber;
                            SB.RentalTaxNumber = o_r.RentalTaxNumber;
                            SB.RentalWizardOfAvisNumber = o_r.RentalWizardOfAvisNumber;
                            SB.RentalContractFeeAmount = o_r.RentalContractFeeAmount;
                            SB.RentalRenterInvoiceFeeAmount = o_r.RentalRenterInvoiceFeeAmount;
                        }
                        else
                        {


                            Entities.OpenRental o_r = db.OpenRentals.Where(o => o.RentalAgreementNumber == SB.RentalAgreementNumber).First();
                            SB.RentalInsuranceBillingCompany = o_r.RentalInsuranceBillingCompany;
                            SB.RentalInsuranceBillingAddress1 = o_r.RentalInsuranceBillingAddress1;
                            SB.RentalInsuranceBillingAddress2 = o_r.RentalInsuranceBillingAddress2;
                            SB.RentalInsuranceBillingAddress3 = o_r.RentalInsuranceBillingAddress3;
                            SB.RentalInsuranceBillingName = o_r.RentalInsuranceBillingName;
                            SB.RentalACTONumber = o_r.RentalACTONumber;
                            SB.RentalAWDNumber = o_r.RentalAWDNumber;
                            SB.RentalDamageNumber = o_r.RentalDamageNumber;
                            SB.RentalIATANumber = o_r.RentalIATANumber;
                            SB.RentalInsuranceInvoiceNumber = o_r.RentalInsuranceInvoiceNumber;
                            SB.RentalReservationNumber = o_r.RentalReservationNumber;
                            SB.RentalTaxNumber = o_r.RentalTaxNumber;
                            SB.RentalWizardOfAvisNumber = o_r.RentalWizardOfAvisNumber;
                            SB.RentalContractFeeAmount = o_r.RentalContractFeeAmount;
                            SB.RentalRenterInvoiceFeeAmount = o_r.RentalRenterInvoiceFeeAmount;
                        }
                        

                        SB.RentalCheckinDateTime = DateTime.Parse(frm["checkInDateDate"] + " " + frm["checkInDateTime"]);


                        SB.Status = 2;
                        db.SplitBillings.Add(SB);

                        

                        db.SaveChanges();
                        audit a = new audit();
                        a.Status_ID = SB.Status;
                        a.RentalNumber = SB.RentalAgreementNumber;
                        a.TimeChanged = DateTime.Now;
                        a.Comment = "Split billing record updated in SBAIR. Status set to pending.";
                        db.audits.Add(a);
                        db.SaveChanges();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                    }
                   
                //}

            }

            Entities.ExceptionRental exp = db.ExceptionRentals.Where(e => e.RentalAgreementNumber == SB.RentalAgreementNumber).FirstOrDefault();
            if (exp != null) { db.ExceptionRentals.Remove(exp); db.SaveChanges(); }

            return RedirectToAction("index", "listing", new { tab = "1" });
        }

        [HttpPost]
        public ActionResult GetPriceSplitData(int ID)
        {
            return Content(db.PriceSplits.Where(p => p.ID == ID).First().PercentageCustomerPays.ToString());
        }

        [HttpPost]
        public ActionResult GetVATSplitData(int ID)
        {
            return Content(db.VATSplits.Where(p => p.ID == ID).First().PercentageCustomerPays.ToString());
        }

        [HttpPost]
        public ActionResult Calculate(string ranumber, int pricesplit, int vatsplit, string TotalRentalChargedAmount, string VATTotal,
            int AirportFeeSplit, int CDWSplit, int RAISplit, int VRFSplit, int OneWayFeeSplit, int RefuelingSplit, int DeliverySplit, int ReturnFeeSplit,
            int MiscCostsSplit, int OtherCostsSplit, string SuperCDWTotal, string RAITotal, string AirportFeeTotal,
            string OneWayFeeTotal, string RefuelingTotal, string DeliveryTotal, string ReturnFeeTotal, string VRFTotal, string MiscCostsTotal, string OtherCostTotal,
            DateTime RentalCheckoutDateTime, DateTime RentalCheckinDateTime, string InvoiceFee, string KMRate, string DayRate, string WeekRate,
            string MonthRate, int KMDriven, string RentalCDWPerDayAmount, string RentalPAIPerDayAmount, int NbKmCoveredByInsurance, int NbDaysCoveredByInsurance,
            int KMCheckin
            )
            
        {
            if (RentalPAIPerDayAmount == string.Empty) { RentalPAIPerDayAmount = "0"; }
            if (RentalCDWPerDayAmount == string.Empty) { RentalCDWPerDayAmount = "0"; }

            if (KMCheckin == null || (KMCheckin < KMDriven))
            {
                KMCheckin = KMDriven;
            }

            int KM = KMCheckin - KMDriven;

            double number = 0;

            Classes.Calculations calc = new Classes.Calculations();
            SplitBilling bill = db.SplitBillings.Where(s=>s.RentalAgreementNumber == ranumber).FirstOrDefault();
            if (bill == null) { bill = new SplitBilling(); }
            bill.RentalAgreementNumber = ranumber;
            bill.RentalCheckoutDateTime = RentalCheckoutDateTime;
            bill.RentalCheckinDateTime = RentalCheckinDateTime;
            bill.RentalMileageRateAmount = decimal.Parse(KMRate);
            bill.RentalDailyRateAmount = decimal.Parse(DayRate);
            bill.RentalWeeklyRateAmount = decimal.Parse(WeekRate);
            bill.RentalMonthlyRateAmount = decimal.Parse(MonthRate);
            //bill.KMDriven = KMDriven;
            bill.KMDriven = KM;
            bill.KmCoveredByInsurance = NbKmCoveredByInsurance;
            bill.DaysCoveredByInsurance = NbDaysCoveredByInsurance;
            bill.RentalPAIPerDayAmount = decimal.Parse(RentalPAIPerDayAmount);
            bill.RentalCDWPerDayAmount = decimal.Parse(RentalCDWPerDayAmount);
            bill.PriceSplit = db.PriceSplits.Where(s=>s.ID == pricesplit).First().ID;
            bill.VATSplit = db.VATSplits.Where(s=>s.ID == vatsplit).First().ID;
            bill.RentalContractFeeAmount = decimal.Parse(InvoiceFee);
            bill.OtherFeeSplit = SplitOrNullOther(OtherCostsSplit);
            bill.OtherFeeTotal = decimal.Parse(double.TryParse(OtherCostTotal, out number) == true ? OtherCostTotal : number.ToString());
            bill.RefuelingSplit = SplitOrNullOther(RefuelingSplit);
            bill.RefuellingTotal = decimal.Parse(double.TryParse(RefuelingTotal, out number) == true ? RefuelingTotal : number.ToString());
            if (bill.RefuelingSplit > 0 && bill.RefuellingTotal > 0) bill.RefuellingTotal += 0;
            bill.DeliveryFeeSplit = SplitOrNullOther(DeliverySplit);
            bill.DeliveryTotal = decimal.Parse(double.TryParse(DeliveryTotal, out number) == true ? DeliveryTotal : number.ToString());
            bill.ReturnFeeSplit = SplitOrNullOther(ReturnFeeSplit);
            bill.ReturnTotal = decimal.Parse(double.TryParse(ReturnFeeTotal, out number) == true ? ReturnFeeTotal : number.ToString());
            bill.VRFSplit = SplitOrNullOther(VRFSplit);
            bill.VRFTotal = decimal.Parse(double.TryParse(VRFTotal, out number) == true ? VRFTotal : number.ToString());
            bill.MiscCostsSplit = SplitOrNullOther(MiscCostsSplit);
            bill.MiscCostsTotal = decimal.Parse(double.TryParse(MiscCostsTotal, out number) == true ? MiscCostsTotal : number.ToString());
            bill.AirportFeeSplit = SplitOrNull(AirportFeeSplit);
            bill.AirportFeeTotal = decimal.Parse(double.TryParse(AirportFeeTotal, out number) == true ? AirportFeeTotal : number.ToString());
            bill.OneWayFeeSplit = SplitOrNull(OneWayFeeSplit);
            bill.OneWayTotal = decimal.Parse(double.TryParse(OneWayFeeTotal, out number) == true ? OneWayFeeTotal : number.ToString());
            bill.CDWSplit = SplitOrNullOther(CDWSplit);
            bill.SuperCDWTotal = decimal.Parse(double.TryParse(SuperCDWTotal, out number) == true ? SuperCDWTotal : number.ToString());
            bill.RAISplit = SplitOrNullOther(RAISplit);
            bill.RAITotal = decimal.Parse(double.TryParse(RAITotal, out number) == true ? RAITotal : number.ToString());

            calc.Calculate(bill);

            bill.NumDaysRental = calc.C_NbDays;

            return Json(new { tot_C = calc.C_TotalAmt, tot_I = calc.I_TotalAmt, vrf_C = calc.C_VRFAmt, vrf_I = calc.I_VRFAmt,
                refuel_C = calc.C_RefuelingAmt, refuel_I = calc.I_RefuelingAmt, vat_C = calc.C_SplittedVATAmt, vat_I = calc.I_SplittedVATAmt,
                del_C = calc.C_DeliveryFeeAmt, del_I = calc.I_DeliveryFeeAmt, ret_C = calc.C_ReturnFeeAmt, ret_I = calc.I_ReturnFeeAmt,
                air_C = calc.C_AirportFeeAmt, air_I = calc.I_AirportFeeAmt,
                              ow_C = calc.C_OneWayFeeAmt,
                              ow_I = calc.I_OneWayFeeAmt,
                              price_C = calc.C_TimeAmt,
                              price_I = calc.I_TimeAmt,
                              superCDW_C = calc.C_SuperCDWAmt,
                              superCDW_I = calc.I_SuperCDWAmt,
                              rai_C = calc.C_RAIAmt,
                              rai_I = calc.I_RAIAmt, errMsg = calc.errMsg,
                              misc_C = calc.C_MiscAmt, misc_I = calc.I_MiscAmt,
                              other_C = calc.C_OthersAmt, other_I = calc.I_OthersAmt, Kilometers = KM, Days = calc.C_NbDays
            }); 
        }

        private int SplitOrNull(int iType)
        {

            try
            {
                return  db.FeeSplits.Where(o => o.ID == iType).First().ID;
            }
            catch
            {
                return -1;
            }

            
        }

        private int SplitOrNullOther(int iType)
        {

            try
            {
                return db.OtherCostSplits.Where(o => o.ID == iType).First().ID;
            }
            catch
            {
                return -1;
            }


        }
    }

}