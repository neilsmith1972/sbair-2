﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using avis.sbair.Entities;

namespace avis.sbair.Controllers
{ 
    public class AuditController : Controller
    {
        private SBAIR3Context db = new SBAIR3Context();

        //
        // GET: /Audit/

        public ViewResult Index(string id)
        {
            ViewBag.RA = id;
            ViewBag.Title = "Audit Records:" + ViewBag.RA;
            List<audit> recs = new List<audit>();
            
            recs = db.audits.Where(r => r.RentalNumber == id).OrderBy(o => o.TimeChanged).ToList();
            return View(recs);
        }

        //
        // GET: /Audit/Details/5

        public ViewResult Details(int id)
        {
            audit audit = db.audits.Find(id);
            return View(audit);
        }

        //
        // GET: /Audit/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Audit/Create

        [HttpPost]
        public ActionResult Create(audit audit)
        {
            if (ModelState.IsValid)
            {
                db.audits.Add(audit);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(audit);
        }
        
        //
        // GET: /Audit/Edit/5
 
        public ActionResult Edit(int id)
        {
            audit audit = db.audits.Find(id);
            return View(audit);
        }

        //
        // POST: /Audit/Edit/5

        [HttpPost]
        public ActionResult Edit(audit audit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(audit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(audit);
        }

        //
        // GET: /Audit/Delete/5
 
        public ActionResult Delete(int id)
        {
            audit audit = db.audits.Find(id);
            return View(audit);
        }

        //
        // POST: /Audit/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            audit audit = db.audits.Find(id);
            db.audits.Remove(audit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}