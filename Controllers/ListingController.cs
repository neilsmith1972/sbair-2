﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using avis.sbair.Entities;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Objects;
using System.IO;
using System.Data.Entity.Validation;
using avis.sbair.WizardWebService;
using avis.sbair.InvoiceService;
using System.Reflection;

namespace avis.sbair.Controllers
{
    public class ListingController : Controller
    {
        private SBAIR3Context db = new SBAIR3Context();

        public ViewResult Index()
        {
            string sSearch = Request["searchTerm"];
            return Search_Index(sSearch);
        }


        ViewResult Search_Index(string searchTerm)
        {
            string search = searchTerm;
            if (search == null) { search = string.Empty; }

            search = search.ToUpper();

            string tab = Request["tab"];
            if (tab == null || tab == string.Empty) { tab = "0"; }
            ViewBag.tab = tab;

            if (tab == "4")
            {
                ViewBag.InsuranceCompanies = db.InsuranceCompanies.Where(a => a.Name.Contains(search) || a.WizardNumber.Contains(search) || a.AwdNumber.Contains(search) || a.Account.Contains(search) || a.RateCode.Contains(search)).OrderBy(i => i.Name).ToList();
                return View("index");
            }

            if (tab == "5")
            {
                ViewBag.RateCodes = db.Rates.OrderBy(i => i.RateCode).ToList();
                return View("index");
            }

            string status = string.Empty;
            string query = string.Empty;

            IEnumerable<Interfaces.IRental> Rentals = new List<Interfaces.IRental>();

            switch (tab)
            {
                case "0":
                status = "Open";
                //get records in openrentals table that don't have a matching records in splitbilling
                List<OpenRental> open = db.OpenRentals.Where(s => !db.SplitBillings.Any(y => y.RentalAgreementNumber == s.RentalAgreementNumber) && (s.RentalAgreementNumber.Contains(search) || s.RentalVehicleLicensePlateNumber.Contains(search) || (s.RentalACTONumber != null ? s.RentalACTONumber.Contains(search) : false) || s.RentalCheckinLocationText.Contains(search))).ToList();
                    Rentals = open; //.Where(o=>o.RentalAgreementNumber.Contains(search) || o.RentalVehicleLicensePlateNumber.Contains(search) || o.RentalCheckinLocationText.Contains(search)).ToList();
                break;

                case "1":
                    //get records in splitbilling table that don't have a matching records in exceptions
                List<SplitBilling> sb = db.SplitBillings.Where(s => !db.ExceptionRentals.Any(y => y.RentalAgreementNumber == s.RentalAgreementNumber) && s.Status == 2).ToList();
                Rentals = sb.Where(s => (s.RentalAgreementNumber.Contains(search) || s.RentalVehicleLicensePlateNumber.Contains(search) || (s.RentalACTONumber != null ? s.RentalACTONumber.Contains(search) : false) || s.RentalCheckinLocationText.Contains(search))).ToList();
                break;

                case "2":
                Rentals = db.ExceptionRentals.Where(o => o.RentalAgreementNumber.Contains(search) || o.RentalVehicleLicensePlateNumber.Contains(search) || (o.RentalACTONumber != null ? o.RentalACTONumber.Contains(search) : false) || o.RentalCheckinLocationText.Contains(search)).ToList();
                break;

                case "3":
                    List<SplitBilling> ir = db.SplitBillings.Where(s => !db.SplitBillings.Any(y => y.RentalAgreementNumber == s.RentalAgreementNumber && y.Status == 2) && !db.ExceptionRentals.Any(y => y.RentalAgreementNumber == s.RentalAgreementNumber) && db.Invoices.Any(a=>a.RentalId == s.RentalId) && s.Status == 3).ToList();
                    var tempRentals = ir.Where(s => s.RentalAgreementNumber.Contains(search) || s.RentalVehicleLicensePlateNumber.Contains(search) || (s.RentalACTONumber != null ? s.RentalACTONumber.Contains(search) : false) || s.RentalCheckinLocationText.Contains(search) || s.RentalInsuranceInvoiceNumber.ToString().Contains(search) || s.RentalRenterInvoiceNumber.ToString().Contains(search)).ToList();
                    //var iCtr = 1;
                    //var q = new List<SplitBilling>();
                    //foreach (var tempRental in tempRentals)
                    //{
                    //    if(!q.Any(z=>z.RentalAgreementNumber == tempRental.RentalAgreementNumber))
                    //    {
                    //        //var n = tempRentals.Where(x => x.RentalAgreementNumber == tempRental.RentalAgreementNumber).OrderByDescending(o => o.Timestamp);
                    //        q.Add(tempRentals.Where(x => x.RentalAgreementNumber == tempRental.RentalAgreementNumber).OrderByDescending(o => o.Timestamp).First());
                    //    }
                    //    iCtr += 1;
                    //}
                    //Rentals = q;
                    Rentals = tempRentals.OrderByDescending(o=>o.RentalCheckinDateTime);
                    break;

                //var r = db.ClosedRentals.OrderByDescending(o=>o.RentalCheckinDateTime).ToList().AsEnumerable();
                //    List<ClosedRental> cr = new List<ClosedRental>();
                //if (search.Trim() != string.Empty)
                //{
                //    foreach (var item in r)
                //    {

                //        if ((item.RentalAgreementNumber.ToLower().Contains(search.ToLower()) ||
                //            item.RentalVehicleLicensePlateNumber.ToLower().Contains(search.ToLower()) ||
                //            item.RentalCheckinLocationText.ToLower().Contains(search.ToLower())
                //            || item.RentalInsuranceInvoiceNumber.ToString().Contains(search)
                //            || item.RentalRenterInvoiceNumber.ToString().Contains(search)
                //            || (item.RentalACTONumber != null ? item.RentalACTONumber.Contains(search) : false)) && item.NewInvoiceRequired != true)
                //        {
                //            cr.Add(item);
                //        }
                       
                //    }
                //    Rentals = cr;
                //}
                //else
                //{
                //    Rentals = r.Where(x=>x.NewInvoiceRequired != true).ToList();
                //    //Rentals = r.ToList();
                //}
                //    //Rentals = r.Where(o => o.RentalAgreementNumber.Contains(search) || o.RentalVehicleLicensePlateNumber.Contains(search) || o.RentalCheckinLocationText.Contains(search)).ToList();
                //    //Rentals = r.Where(x => x.RentalInsuranceInvoiceNumber.ToString().Contains(search)).ToList();
                //break;

                case "6":
                    if (!string.IsNullOrEmpty(search))
                    {
                        var exception =
                            db.ExceptionRentals.Where(
                                o =>
                                o.RentalAgreementNumber.Contains(search) ||
                                o.RentalVehicleLicensePlateNumber.Contains(search) ||
                                (o.RentalACTONumber != null ? o.RentalACTONumber.Contains(search) : false) ||
                                o.RentalCheckinLocationText.Contains(search));
                        var open2 =
                            db.OpenRentals.Where(
                                s =>
                                !db.SplitBillings.Any(y => y.RentalAgreementNumber == s.RentalAgreementNumber) &&
                                (s.RentalAgreementNumber.Contains(search) ||
                                 s.RentalVehicleLicensePlateNumber.Contains(search) ||
                                 (s.RentalACTONumber != null ? s.RentalACTONumber.Contains(search) : false) ||
                                 s.RentalCheckinLocationText.Contains(search) ||
                                 s.RentalCustomerName.Contains(search) ||
                                 s.MVANumber.Contains(search)));

                        //!db.SplitBillings.Any(y => y.RentalAgreementNumber == c.RentalAgreementNumber && y.Status == 2) &&
                        var closed =
                            db.SplitBillings.Where(
                                c => c.Status == 3 && 
                                (c.RentalAgreementNumber.Contains(search) ||
                                c.RentalVehicleLicensePlateNumber.Contains(search) ||
                                (c.RentalACTONumber != null ? c.RentalACTONumber.Contains(search) : false) ||
                                c.RentalCheckinLocationText.Contains(search)));
                        List<SplitBilling> sb2 =
                            db.SplitBillings.Where(
                                s =>
                                !db.ExceptionRentals.Any(y => y.RentalAgreementNumber == s.RentalAgreementNumber) &&
                                s.Status == 2).ToList();
                        sb2 = sb2.Where(s => (s.IsArchive == null || s.IsArchive == false)).ToList();
                        var pending =
                            sb2.Where(
                                s =>
                                s.Status == 2 &&
                                (s.RentalAgreementNumber.Contains(search) ||
                                 s.RentalVehicleLicensePlateNumber.Contains(search) ||
                                 (s.RentalACTONumber != null ? s.RentalACTONumber.Contains(search) : false) ||
                                 s.RentalCheckinLocationText.Contains(search))).ToList();
                        


                        var searchresults = new List<BaseRental>();

                        foreach (var item in open2)
                        {
                            var br = new BaseRental
                                         {
                                             RentalAgreementNumber = item.RentalAgreementNumber,
                                             RentalCheckoutDateTime = item.RentalCheckoutDateTime,
                                             RentalCheckoutLocationText = item.RentalCheckoutLocationText,
                                             RentalCheckinDateTime = item.RentalCheckinDateTime,
                                             RentalCheckinLocationText = item.RentalCheckinLocationText,
                                             RentalVehicleClassChargedCode = item.RentalVehicleClassChargedCode,
                                             RentalVehicleLicensePlateNumber = item.RentalVehicleLicensePlateNumber,
                                             RentalTransactionType = item.RentalTransactionType,
                                             RentalCustomerName =  item.RentalCustomerName,
                                             MVANumber=item.MVANumber,
                                             type = "Open",
                                             TimeStamp = item.Timestamp
                                         };
                            searchresults.Add(br);
                        }

                        foreach (var item in exception)
                        {
                            var br = new BaseRental
                                         {
                                             RentalAgreementNumber = item.RentalAgreementNumber,
                                             RentalCheckoutDateTime = item.RentalCheckoutDateTime,
                                             RentalCheckoutLocationText = item.RentalCheckoutLocationText,
                                             RentalCheckinDateTime = item.RentalCheckinDateTime,
                                             RentalCheckinLocationText = item.RentalCheckinLocationText,
                                             RentalVehicleClassChargedCode = item.RentalVehicleClassChargedCode,
                                             RentalVehicleLicensePlateNumber = item.RentalVehicleLicensePlateNumber,
                                             RentalTransactionType = item.RentalTransactionType,
                                             type = "Error",
                                             TimeStamp = item.Timestamp
                                         };
                            searchresults.Add(br);
                        }

                        foreach (var item in closed)
                        {
                            var br = new BaseRental
                                         {
                                             RentalAgreementNumber = item.RentalAgreementNumber,
                                             RentalCheckoutDateTime = item.RentalCheckoutDateTime,
                                             RentalCheckoutLocationText = item.RentalCheckoutLocationText,
                                             RentalCheckinDateTime = item.RentalCheckinDateTime,
                                             RentalCheckinLocationText = item.RentalCheckinLocationText,
                                             RentalVehicleClassChargedCode = item.RentalVehicleClassChargedCode,
                                             RentalVehicleLicensePlateNumber = item.RentalVehicleLicensePlateNumber,
                                             RentalTransactionType = item.RentalTransactionType,
                                             RentalId = item.RentalId.ToString(),
                                             type = "Invoiced",
                                             TimeStamp = item.Timestamp
                                         };
                            searchresults.Add(br);
                        }

                        foreach (var item in pending)
                        {
                            var br = new BaseRental
                                         {
                                             RentalAgreementNumber = item.RentalAgreementNumber,
                                             RentalCheckoutDateTime = item.RentalCheckoutDateTime,
                                             RentalCheckoutLocationText = item.RentalCheckoutLocationText,
                                             RentalCheckinDateTime = item.RentalCheckinDateTime,
                                             RentalCheckinLocationText = item.RentalCheckinLocationText,
                                             RentalVehicleClassChargedCode = item.RentalVehicleClassChargedCode,
                                             RentalVehicleLicensePlateNumber = item.RentalVehicleLicensePlateNumber,
                                             RentalTransactionType = item.RentalTransactionType,
                                             RentalId = item.RentalId.ToString(),
                                             type = "Waiting",
                                             TimeStamp = item.Timestamp
                                         };
                            searchresults.Add(br);
                        }

                        Rentals = searchresults;
                    }
                        break;
                    
            }

            List<Interfaces.IRental> rentals = Rentals.ToList();
            return View("index",rentals);
        }

        public ViewResult Search(FormCollection frm)
        {
            string sSearch = Request["searchTerm"];
            if (sSearch != null)
            {
                ViewBag.Search = sSearch;
            }
            else
            {
                if (ViewBag.Search != null)
                {
                    sSearch = ViewBag.Search;
                    if (sSearch != string.Empty)
                    {
                        ViewBag.Search = sSearch;
                    }
                }
            }
            return Search_Index(sSearch);
        }

        public ActionResult Edit(string id)
        {
            ViewBag.Region = ConfigurationManager.AppSettings["Region"].ToString();

            var invoiceListCustomer = new List<string>();
            var invoiceListInsurance = new List<string>();

            if(!string.IsNullOrEmpty(Request["fromDeleted"]))
            {
                ViewBag.Deleted = "true";
            }

            if (Session["NoChanges"] != null)
            {
                ViewBag.NoChanges = "true";
                Session["NoChanges"] = null;
            }

            string type = Request["type"].ToLower();
            switch (type)
            {
                case "waiting":
                    type = "pending";
                    break;
                case "error":
                    type = "exception";
                    break;
                case "invoiced":
                    type = "closed";
                    break;
            }

            if (Request["return"] != null)
            { ViewBag.Return = Request["return"]; }
            else
            {
                ViewBag.Return = "listing/index";
            }

            ViewBag.WZD = "none";

            SplitBilling rec = new SplitBilling();
            
            switch (type)
            {
                case "open":

                    //does this record already exist in a pending state? if so, get the pending record, not the open one
                    if (db.SplitBillings.Where(o => o.RentalAgreementNumber == id && o.IsArchive != true).FirstOrDefault() != null)
                    {
                        rec = db.SplitBillings.Where(o => o.RentalAgreementNumber == id && o.IsArchive != true).FirstOrDefault();
                        ViewBag.Status = db.Status.Where(s => s.ID == rec.Status).First().StatusText;
                        if (rec.KMCheckin == null || rec.KMCheckin < rec.KMDriven) rec.KMCheckin = rec.KMDriven;
                        break;
                    }


                    ViewBag.Status = "Open";
                    Entities.OpenRental openrental = db.OpenRentals.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();
                    rec.RentalAgreementNumber = openrental.RentalAgreementNumber;
                    DateTime eTemp = new DateTime(openrental.RentalCheckinDateTime.Value.Year, openrental.RentalCheckinDateTime.Value.Month, openrental.RentalCheckinDateTime.Value.Day, openrental.RentalCheckinDateTime.Value.Hour, openrental.RentalCheckinDateTime.Value.Minute, openrental.RentalCheckinDateTime.Value.Second);
                    rec.RentalCheckinDateTime = openrental.RentalCheckinDateTime;
                    rec.RentalCheckinLocationText = openrental.RentalCheckinLocationText;
                    DateTime sTemp = new DateTime(openrental.RentalCheckoutDateTime.Value.Year, openrental.RentalCheckoutDateTime.Value.Month, openrental.RentalCheckoutDateTime.Value.Day, openrental.RentalCheckoutDateTime.Value.Hour, openrental.RentalCheckoutDateTime.Value.Minute, openrental.RentalCheckoutDateTime.Value.Second);
                    rec.RentalCheckoutDateTime = openrental.RentalCheckoutDateTime;
                    rec.RentalCheckoutLocationText = openrental.RentalCheckoutLocationText;
                    rec.Status = Convert.ToInt32(Classes.enums.enums.Status.Open); //db.Status.First().ID;
                    rec.RentalVehicleLicensePlateNumber = openrental.RentalVehicleLicensePlateNumber;
                    rec.Country = db.Countries.First().ID;
                    rec.RentalDamageNumber = openrental.RentalDamageNumber;
                    rec.RentalCustomerName = openrental.RentalCustomerName;
                    rec.RentalRenterAddress1Text = openrental.RentalRenterAddress1Text;
                    rec.RentalRenterAddress2Text = openrental.RentalRenterAddress2Text;
                    rec.RentalRenterAddressCityText = openrental.RentalRenterAddressCityText;
                    rec.KMDriven = (int) openrental.KMDriven;
                    rec.RentalCompanyName = openrental.RentalCompanyName;
                    rec.RentalRenterBillingName = openrental.RentalRenterBillingName;
                    rec.RentalVehicleClassChargedCode = openrental.RentalVehicleClassChargedCode;
                    rec.RentalCDWPerDayAmount = openrental.RentalCDWPerDayAmount;
                    rec.RentalPAIPerDayAmount = openrental.RentalPAIPerDayAmount;
                    rec.RentalACTONumber = openrental.RentalACTONumber;
                    rec.RentalRenterCreditClubCode = openrental.RentalRenterCreditClubCode;
                    rec.RentalRenterCreditCardNumber = openrental.RentalRenterCreditCardNumber;
                    rec.RentalInsuranceBillingCompany = openrental.RentalInsuranceBillingCompany;
                    rec.RentalInsuranceBillingName = openrental.RentalInsuranceBillingName;
                    rec.RentalInsuranceBillingAddress1 = openrental.RentalInsuranceBillingAddress1;
                    rec.RentalInsuranceBillingAddress2 = openrental.RentalInsuranceBillingAddress2;
                    rec.RentalInsuranceBillingAddress3 = openrental.RentalInsuranceBillingAddress3;
                    rec.RentalALIPerDayAmount = openrental.RentalALIPerDayAmount;
                    rec.RentalInsuranceCreditCardNumber = openrental.RentalInsuranceCreditCardNumber;
                    rec.RentalInsuranceCreditClubCode = openrental.RentalInsuranceCreditClubCode;
                    rec.KMCheckout = openrental.KMDriven;
                    if (openrental.RentalContractFeeAmount == null) { openrental.RentalContractFeeAmount = 0; }

                    rec.AlreadyPaidByCustomerAmount = 0;
                    rec.AlreadyPaidByInsuranceAmount = 0;
                    rec.VATTotal = 0;
                    rec.RAITotal = 0;
                    rec.RAICustomerPays = 0;
                    rec.SuperCDWTotal = 0;
                    rec.SuperCDWCustomerPays = 0;
                    rec.SuperTPTotal = 0;
                    rec.SuperTPCustomerPays = 0;
                    rec.OneWayTotal = 0;
                    rec.OneWayCustomerPays = 0;
                    rec.RefuellingTotal = 0;
                    rec.RefuellingCustomerPays = 0;
                    rec.DeliveryTotal = 0;
                    rec.DeliveryCustomerPays = 0;
                    rec.ReturnTotal = 0;
                    rec.ReturnCustomerPays = 0;
                    rec.MiscCostsTotal = 0;
                    rec.MiscCostsCustomerPays = 0;
                    rec.MiscCostsInsurancePays = 0;
                    rec.VRFTotal = 52;
                    rec.VRFCustomerPays = 0;
                    rec.VRFSplit = 9;
                    rec.MiscCostsTotal = 0;
                    rec.MiscCostsCustomerPays = 0;
                    rec.InvoiceFeeTotal = (decimal) openrental.RentalContractFeeAmount;
                    rec.InvoiceFeeCustomerPays = (decimal)openrental.RentalRenterInvoiceFeeAmount;
                    rec.AirportFeeTotal = 0;
                    rec.AirportFeeCustomerPays = 0;
                    rec.OtherFeeTotal = 0;
                    rec.OtherFeeCustomerPays = 0;
                    rec.RentalContractFeeAmount = (decimal)openrental.RentalContractFeeAmount;

                    rec.PriceSplitTotal = 0;
                    //rec.CDWSplit = 11;
                    //rec.RAISplit = 13;
                    rec.RentalRenterInvoiceFeeAmount = (decimal)openrental.RentalRenterInvoiceFeeAmount;
                    rec.RentalMileageRateAmount = (decimal) openrental.RentalMileageRateAmount; 
                    rec.RentalDailyRateAmount = (decimal) openrental.RentalDailyRateAmount;
                    rec.RentalWeeklyRateAmount = (decimal) openrental.RentalWeeklyRateAmount; 
                    rec.RentalMonthlyRateAmount = (decimal) openrental.RentalMonthlyRateAmount;
                    rec.KmCoveredByInsurance = openrental.RentalFreeMileageCount;
                    rec.DaysCoveredByInsurance = 0;
                    rec.AmountCoveredByInsurance = 0;

                    if (rec.KMCheckin == null || rec.KMCheckin < rec.KMDriven) rec.KMCheckin = rec.KMDriven;
                    if (rec.RentalCheckinDateTime < rec.RentalCheckoutDateTime) rec.RentalCheckinDateTime = rec.RentalCheckoutDateTime.Value.AddHours(1);

                    break;

                case "exception":
                    rec = db.SplitBillings.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();
                    Entities.ExceptionRental exceptionRental = db.ExceptionRentals.Where(o => o.RentalAgreementNumber == id).FirstOrDefault();
                    ViewBag.WZD = "block";
                    if (rec == null)
                    {
                        rec = new SplitBilling();
                        rec.RentalAgreementNumber = exceptionRental.RentalAgreementNumber;
                        //DateTime eTemp = new DateTime(exceptionRental.RentalCheckinDateTime.Value.Year, exceptionRental.RentalCheckinDateTime.Value.Month, exceptionRental.RentalCheckinDateTime.Value.Day, exceptionRental.RentalCheckinDateTime.Value.Hour, exceptionRental.RentalCheckinDateTime.Value.Minute, exceptionRental.RentalCheckinDateTime.Value.Second);
                        rec.RentalCheckinDateTime = exceptionRental.RentalCheckinDateTime;
                        rec.RentalCheckinLocationText = exceptionRental.RentalCheckinLocationText;
                        //DateTime sTemp = new DateTime(exceptionRental.RentalCheckoutDateTime.Value.Year, exceptionRental.RentalCheckoutDateTime.Value.Month, exceptionRental.RentalCheckoutDateTime.Value.Day, exceptionRental.RentalCheckoutDateTime.Value.Hour, exceptionRental.RentalCheckoutDateTime.Value.Minute, exceptionRental.RentalCheckoutDateTime.Value.Second);
                        rec.RentalCheckoutDateTime = exceptionRental.RentalCheckoutDateTime;
                        rec.RentalCheckoutLocationText = exceptionRental.RentalCheckoutLocationText;
                        rec.Status = Convert.ToInt32(Classes.enums.enums.Status.Pending);
                        rec.RentalVehicleLicensePlateNumber = exceptionRental.RentalVehicleLicensePlateNumber;
                        rec.Country = db.Countries.First().ID;
                        rec.RentalDamageNumber = exceptionRental.RentalDamageNumber;
                        rec.RentalCustomerName = exceptionRental.RentalCustomerName;
                        rec.RentalRenterAddress1Text = exceptionRental.RentalRenterAddress1Text;
                        rec.RentalRenterAddress2Text = exceptionRental.RentalRenterAddress2Text;
                        rec.RentalRenterAddressCityText = exceptionRental.RentalRenterAddressCityText;
                        rec.KMDriven = (int)exceptionRental.CheckOutMileage;
                        rec.RentalCompanyName = exceptionRental.RentalCompanyName;
                        rec.RentalRenterBillingName = exceptionRental.RentalRenterBillingName;
                        rec.RentalVehicleClassChargedCode = exceptionRental.RentalVehicleClassChargedCode;
                        rec.RentalCDWPerDayAmount = exceptionRental.RentalCDWPerDayAmount;
                        rec.RentalPAIPerDayAmount = exceptionRental.RentalPAIPerDayAmount;
                        rec.RentalACTONumber = exceptionRental.RentalACTONumber;
                        rec.RentalRenterCreditClubCode = exceptionRental.RentalRenterCreditClubCode;
                        rec.RentalRenterCreditCardNumber = exceptionRental.RentalRenterCreditCardNumber;

                        rec.RentalVATAmount = exceptionRental.RentalVATAmount;
                        rec.RentalInvoiceFeeAmount = exceptionRental.RentalInvoiceFeeAmount;
                        rec.RentalOtherCosts = exceptionRental.RentalOtherCosts;

                        rec.RentalInsuranceBillingCompany = exceptionRental.RentalInsuranceBillingCompany;
                        rec.RentalInsuranceBillingName = exceptionRental.RentalInsuranceBillingName;
                        rec.RentalInsuranceBillingAddress1 = exceptionRental.RentalInsuranceBillingAddress1;
                        rec.RentalInsuranceBillingAddress2 = exceptionRental.RentalInsuranceBillingAddress2;
                        rec.RentalInsuranceBillingAddress3 = exceptionRental.RentalInsuranceBillingAddress3;

                        if (exceptionRental.RentalContractFeeAmount == null) { exceptionRental.RentalContractFeeAmount = 0; }

                        if (rec.KMCheckout == null || rec.KMCheckout == 0)
                        {
                            //if (rec.KMCheckin > rec.KMDriven) { 
                               rec.KMCheckout = rec.KMDriven; rec.KMDriven = rec.KMCheckin - rec.KMCheckout;
                            //}
                        }
                        if (rec.KMCheckin == null || rec.KMCheckin < rec.KMDriven) rec.KMCheckin = rec.KMDriven;

                        rec.RentalRenterInvoiceFeeAmount = (decimal)exceptionRental.RentalRenterInvoiceFeeAmount;


                        rec.AlreadyPaidByCustomerAmount = 0;
                        rec.AlreadyPaidByInsuranceAmount = 0;
                        rec.VATTotal = 0;
                        rec.RAITotal = 0;
                        rec.RAICustomerPays = 0;
                        rec.SuperCDWTotal = 0;
                        rec.SuperCDWCustomerPays = 0;
                        rec.SuperTPTotal = 0;
                        rec.SuperTPCustomerPays = 0;

                        rec.OneWayTotal = 0;
                        rec.OneWayCustomerPays = 0;
                        rec.RefuellingTotal = 0;
                        rec.RefuellingCustomerPays = 0;
                        rec.DeliveryTotal = 0;
                        rec.DeliveryCustomerPays = 0;
                        rec.ReturnTotal = 0;
                        rec.ReturnCustomerPays = 0;
                        rec.MiscCostsTotal = 0;
                        rec.MiscCostsCustomerPays = 0;
                        rec.MiscCostsInsurancePays = 0;
                        rec.VRFTotal = 52;
                        rec.VRFCustomerPays = 0;
                        rec.VRFSplit = 9;
                        rec.MiscCostsTotal = 0;
                        rec.MiscCostsCustomerPays = 0;
                        rec.InvoiceFeeTotal = (decimal)exceptionRental.RentalContractFeeAmount;
                        rec.InvoiceFeeCustomerPays = (decimal)exceptionRental.RentalRenterInvoiceFeeAmount;
                        rec.AirportFeeTotal = 0;
                        rec.AirportFeeCustomerPays = 0;
                        rec.OtherFeeTotal = 0;
                        rec.OtherFeeCustomerPays = 0;
                        rec.RentalContractFeeAmount = (decimal)exceptionRental.RentalContractFeeAmount;

                        rec.PriceSplitTotal = 0;
                        rec.RentalMileageRateAmount = (decimal)exceptionRental.RentalMileageRateAmount;
                        rec.RentalDailyRateAmount = (decimal)exceptionRental.RentalDailyRateAmount;
                        rec.RentalWeeklyRateAmount = (decimal)exceptionRental.RentalWeeklyRateAmount;
                        rec.RentalMonthlyRateAmount = (decimal)exceptionRental.RentalMonthlyRateAmount;
                        rec.KmCoveredByInsurance = exceptionRental.RentalFreeMileageCount;
                        rec.DaysCoveredByInsurance = 0;
                        rec.KmCoveredByInsurance = 0;
                        rec.AmountCoveredByInsurance = 0;
                        
                    }
                    else
                    {
                        rec.Status = Convert.ToInt32(Classes.enums.enums.Status.Error);
                    }
                    ViewBag.Status = "Exception";
                    ViewBag.RentalRenterTotalAmount = exceptionRental.RentalRenterTotalAmount != null ? exceptionRental.RentalRenterTotalAmount : 0M;
                    ViewBag.RentalInsuranceTotalAmount = exceptionRental.RentalInsuranceTotalAmount;

                    rec.RentalVATAmount = exceptionRental.RentalVATAmount;
                    rec.RentalInvoiceFeeAmount = exceptionRental.RentalInvoiceFeeAmount;
                    rec.RentalOtherCosts = exceptionRental.RentalOtherCosts;

                    rec.NetTimeDistanceAmount = exceptionRental.NetTimeDistanceAmount;
                    rec.RentalCDWAmount = exceptionRental.RentalCDWAmount;
                    rec.RentalPAIAmount = exceptionRental.RentalPAIAmount;
                    rec.RentalSurchargeAmount = exceptionRental.RentalSurchargeAmount;
                    rec.RentalOneWayFeeAmount = exceptionRental.RentalOneWayFeeAmount;
                    rec.RentalGasChargesAmount = exceptionRental.RentalGasChargesAmount;
                    rec.RentalDeliveryChargeAmount = exceptionRental.RentalDeliveryChargeAmount;
                    rec.RentalCollectionChargeAmount = exceptionRental.RentalCollectionChargeAmount;
                    rec.RentalContractFeeAmount = exceptionRental.RentalContractFeeAmount;
                    rec.RentalMiscChargeAmount = exceptionRental.RentalMiscChargeAmount;
                    rec.RentalRenterInvoiceFeeAmount = exceptionRental.RentalRenterInvoiceFeeAmount;
                    rec.RentalTPAmount = exceptionRental.RentalTPIAmount;
                    break;

               
                default:
                    var lId1 = long.Parse(id);
                    ViewBag.closedRentalId = lId1;
                    rec = db.SplitBillings.FirstOrDefault(sp => sp.RentalId == lId1);
                    if (rec == null) return RedirectToAction("index", "listing");
                    ViewBag.Status = "Pending";
                    bool editable = false;
                    //if(TempData["message"] != null) ViewBag.Updated = true; //"<script>alert('" + TempData["message"] as string + "');</script>";
                    var threshold1 = new DateTime(rec.Timestamp.Year, rec.Timestamp.Month, rec.Timestamp.Day, 22, 00, 00);

                    var count_RA = db.SplitBillings.Where(z => z.RentalAgreementNumber == rec.RentalAgreementNumber).Count();

                    if(DateTime.Now < threshold1)
                    {
                        editable = true;
                        if (count_RA > 1) ViewBag.allowRevert = true;
                    }

                    
                   // SELECT Sbair.[dbo].[fn_isCSARental]('E986294923') – not csa
 
                    //SELECT Sbair.[dbo].[fn_isCSARental]('E718580365') – is csa
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SBAIR3Context"].ConnectionString);
                    SqlCommand comm = new SqlCommand("SELECT Sbair.[dbo].[fn_isCSARental]('" + rec.RentalAgreementNumber + "')", conn);
                    comm.CommandType = CommandType.Text;
                    conn.Open();
                    var csa = comm.ExecuteScalar();
                    conn.Close();

                    if (int.Parse(csa.ToString()) == 1)
                    {
                        ViewBag.CSA = true;
                    }
                    else
                    {
                        if (rec.RentalCheckinDateTime < DateTime.Parse(ConfigurationManager.AppSettings["WizardCheckCutOffDate"]))
                        {
                            // check wizard to see if this record has already been updated
                            var client = new WizardClient();

                            var result = client.GetAdjustmentMonetary(ConfigurationManager.AppSettings["WizardUser"], ConfigurationManager.AppSettings["WizardPW"], rec.RentalAgreementNumber);
                            Console.WriteLine(result.WizardResult.Message);
                            try
                            {
                                if (result.WizardResult.Message.Contains("RA HAS NO PRIOR ADJUSTMENT") || Request["user"] == "jenny" || (rec.OverrideWizardLock != null && rec.OverrideWizardLock == true))
                                {
                                    ViewBag.WizardLocked = false;
                                }
                                else
                                {
                                    ViewBag.HideFirstLockText = result.WizardResult.Message.ToLower().Contains("time(s)");
                                    ViewBag.WizardMessage = result.WizardResult.Message.Replace("<br/>", "").Trim();
                                    ViewBag.WizardLocked = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                ViewBag.WizardMessage = "<script>alert('This page has been locked for editing. This could be because the rental has not been found in Wizard. Please ensure the rental is checked in and reloaded if it is older than 6 months.</label>";
                                ViewBag.WizardLocked = true;
                            }
                        }
                    }

                   
                    var invoices = db.Invoices.Where(i => i.RentalId == lId1).ToList();
                    var path = ConfigurationManager.AppSettings["InvoiceStore"];
                    if (db.Invoices.Where(i => i.RentalId == lId1).Any())
                    {
                        ViewBag.Status = "Closed";
                        ViewBag.tp = "closed";

                    }

                    if(rec.Status != 3 && DateTime.Now > threshold1)
                    {
                        editable = false;
                        ViewBag.allowRevert = false;
                    }


                    var recs = db.SplitBillings.Where(o => o.RentalAgreementNumber == rec.RentalAgreementNumber).OrderByDescending(o => o.Timestamp).ToList();
                    if (rec.ParentRentalId != null && rec.ParentRentalId > 0)
                    {
                        var differences = PublicInstancePropertiesEqual(rec, db.SplitBillings.First(p=>p.RentalId == rec.ParentRentalId));
                        foreach (var s in differences)
                        {
                            ViewBag.ChangedList += s + ",";
                        }
                    }

                    if (rec.RentalId != recs.First().RentalId) { editable = false; ViewBag.NoSave = true; }

                    var historic1 =
                        recs.Select(
                            cRental =>
                            new SelectListItem
                                {
                                    Value = cRental.RentalId.ToString(),
                                    Text = cRental.Timestamp.ToString(),
                                    Selected = cRental.RentalId == rec.RentalId
                                }).ToList();
                    ViewBag.CustomerInvoiceHistoric = historic1;
                    ViewData["cboHistoric"] = historic1;
                    ViewBag.Editable = editable;
                    if (rec.SuperTPTotal == null) rec.SuperTPTotal = 0;
                    if (rec.DaysCoveredByInsurance == null) rec.DaysCoveredByInsurance = 0;
                    if (rec.KmCoveredByInsurance == null) rec.KmCoveredByInsurance = 0;
                    if (rec.AmountCoveredByInsurance == null) rec.AmountCoveredByInsurance = 0;
                    if (rec.MiscCostsInsurancePays == null) rec.MiscCostsInsurancePays = 0;

                    if (rec.MiscCostsTotal != null && rec.MiscCostsTotal > 0 && rec.MiscCostsCustomerPays != null) rec.MiscCostsInsurancePays = rec.MiscCostsTotal - rec.MiscCostsCustomerPays;


                    if (rec.KMCheckout == null || rec.KMCheckout == 0)
                    {
                        if (rec.KMCheckin > rec.KMDriven)
                        {
                            rec.KMCheckout = rec.KMDriven; rec.KMDriven = rec.KMCheckin - rec.KMCheckout;
                        }
                        //else
                        //{
                        //    rec.KMCheckout = rec.KMDriven; rec.KMCheckin = rec.KMCheckout + rec.KMDriven;

                        //}
                    }

                    //did wizard update?
                    try
                    {
                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AppLogConnection"].ConnectionString);
                        comm = new SqlCommand("select top 1 Message from Appllog.dbo.Log4Net where AppID = 46 and Message like '" + rec.RentalId + ": Exception%' order by [Date] desc", conn);
                        comm.CommandType = CommandType.Text;
                        conn.Open();
                        ViewBag.WizardUpdateError = comm.ExecuteScalar();
                        conn.Close();
                    }
                    catch
                    {
                        ViewBag.WizardUpdateError = "NOTE: There was an error retrieving the wizard error information. This does not mean that there was actually a Wizard update error.";
                    }



                    break;
            }

            BuildLists(rec);

            //ViewBag.InvoiceFee = db.InvoiceFees.Where(i => i.FromDate <= DateTime.Now && i.ToDate >= DateTime.Now).First().Price;

            audit a = new audit();
            a.Status_ID = rec.Status;
            a.RentalNumber = rec.RentalAgreementNumber;
            a.TimeChanged = DateTime.Now;
            a.Comment = "Record viewed in SBAIR.";
            db.audits.Add(a);
            db.SaveChanges();

            if (rec.RentalContractFeeAmount == null) { rec.RentalContractFeeAmount = 0; }

            ViewBag.CustomerInvoiceHistoric = invoiceListCustomer;
            ViewBag.InsuranceInvoiceHistoric = invoiceListInsurance;

            //only show void button is this RA number has previously had an invoice against one of its splitbilling records
            var alreadyVoided = db.SplitBillings.Where(ra => ra.RentalAgreementNumber == rec.RentalAgreementNumber && ra.Void != null && ra.Void == true).Any();
            var ras = db.SplitBillings.Where(ra => ra.RentalAgreementNumber == rec.RentalAgreementNumber).ToList();
            var hasInv = false;
            ViewBag.Voidable = "false";

            if (!alreadyVoided)
            {
                foreach (var z in ras)
                {
                    hasInv = db.Invoices.Where(i => i.RentalId == z.RentalId).Any();
                    ViewBag.Voidable = hasInv.ToString();
                    if (hasInv) break;
                }
            }
            //if (rec.TMDiscount == null) rec.TMDiscount = 0;
            //rec.AmountCoveredByInsurance = decimal.Round((decimal) rec.AmountCoveredByInsurance, 2);

            if (rec.MiscCostsInsurancePays == null) rec.MiscCostsInsurancePays = 0;

            ViewBag.InvoiceList = GetInvoiceList(rec.RentalAgreementNumber);

            return View(rec);
        }


        void BuildLists(SplitBilling rec)
        {
            List<SelectListItem> statuscodes = new List<SelectListItem>();
            foreach (Entities.Status status in db.Status.OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = status.ID.ToString();
                s.Text = status.StatusText;
                statuscodes.Add(s);
            }
            ViewBag.StatusCodes = statuscodes;

            List<SelectListItem> outputCodes_I = new List<SelectListItem>();
            outputCodes_I.Add(new SelectListItem { Text = "Select...", Value = "-1", Selected = string.IsNullOrEmpty(rec.Output_Type_I) ? true : false });
            outputCodes_I.Add(new SelectListItem { Text = "New Invoice", Value = "NEW", Selected = rec.Output_Type_I == "NEW" ? true : false });
            outputCodes_I.Add(new SelectListItem { Text = "Debit and credit", Value = "DAC", Selected = rec.Output_Type_I == "DAC" ? true : false });
            outputCodes_I.Add(new SelectListItem { Text = "Debit or credit", Value = "DOC", Selected = rec.Output_Type_I == "DOC" ? true : false });
            outputCodes_I.Add(new SelectListItem { Text = "No Print", Value = "NPR", Selected = rec.Output_Type_I == "NPR" ? true : false });
            ViewBag.outputCodes_I = outputCodes_I;

            List<SelectListItem> outputCodes_C = new List<SelectListItem>();
            outputCodes_C.Add(new SelectListItem { Text = "Select...", Value = "-1", Selected = string.IsNullOrEmpty(rec.Output_Type_C) ? true : false });
            outputCodes_C.Add(new SelectListItem { Text = "New Invoice", Value = "NEW", Selected = rec.Output_Type_C == "NEW" ? true : false });
            outputCodes_C.Add(new SelectListItem { Text = "Debit and credit", Value = "DAC", Selected = rec.Output_Type_C == "DAC" ? true : false });
            outputCodes_C.Add(new SelectListItem { Text = "Debit or credit", Value = "DOC", Selected = rec.Output_Type_C == "DOC" ? true : false });
            outputCodes_C.Add(new SelectListItem { Text = "No Print", Value = "NPR", Selected = rec.Output_Type_C == "NPR" ? true : false });
            ViewBag.outputCodes_C = outputCodes_C;

            //List<SelectListItem> priceSplits = new List<SelectListItem>();
            //foreach (Entities.PriceSplit priceSplit in db.PriceSplits.OrderBy(c => c.ID).ToList())
            //{
            //    SelectListItem s = new SelectListItem();
            //    s.Value = priceSplit.ID.ToString();
            //    s.Text = priceSplit.Description;
            //    string x = s.ToString();
            //    //if (rec.PriceSplit.ID == priceSplit.ID) { s.Selected = true; }
            //    priceSplits.Add(s);
            //}
            //ViewBag.PriceSplits = priceSplits;

            string ps = string.Empty;
            if (rec.PriceSplit == null) rec.PriceSplit = -1;
            ps = "<select id='PriceSplit' rhs='true' compare='true' name='PriceSplit' style = 'margin-top:5px;' >";
            foreach (Entities.PriceSplit priceSplit in db.PriceSplits.OrderBy(c => c.ID).ToList())
            {
                ps = ps + "<option value='" + priceSplit.ID + "' title='" + priceSplit.Description + "'";
                if (rec.PriceSplit.Value == priceSplit.ID) ps = ps + " selected = 'selected' ";
                ps = ps + ">" + priceSplit.Description + "</option>";
                
            }
            ps = ps + "</select>";
            ViewBag.PriceSplits = ps;

            string vs = string.Empty;
            if (rec.VATSplit == null) rec.VATSplit = -1;
            vs = "<select id='VATSplit' rhs='true' compare='true' name='VATSplit' style = 'margin-top:5px;' >";
            foreach (Entities.VATSplit vatSplit in db.VATSplits.OrderBy(c => c.ID).ToList())
            {
                vs = vs + "<option value='" + vatSplit.ID + "' title=\"" + vatSplit.Description + "\"";
                if (rec.VATSplit.Value == vatSplit.ID) vs = vs + " selected = 'selected' ";
                vs = vs + ">" + vatSplit.Description + "</option>";
            }
            vs = vs + "</select>";
            ViewBag.VATSplits = vs;

            //List<SelectListItem> vatSplits = new List<SelectListItem>();
            //foreach (Entities.VATSplit vatSplit in db.VATSplits.OrderBy(c => c.ID).ToList())
            //{
            //    SelectListItem s = new SelectListItem();
            //    s.Value = vatSplit.ID.ToString();
            //    s.Text = vatSplit.Description;
            //    //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
            //    vatSplits.Add(s);
            //}
            //ViewBag.VATSplits = vatSplits;

            List<SelectListItem> CDWSplits = new List<SelectListItem>();
            CDWSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit CDWSplit in db.OtherCostSplits.Where(o => o.Code_ID == 1).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = CDWSplit.ID.ToString();
                s.Text = CDWSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                CDWSplits.Add(s);
            }
            ViewBag.CDWSplits = CDWSplits;

            List<SelectListItem> RAISplits = new List<SelectListItem>();
            RAISplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit RAISplit in db.OtherCostSplits.Where(o => o.Code_ID == 5).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = RAISplit.ID.ToString();
                s.Text = RAISplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                RAISplits.Add(s);
            }
            ViewBag.RAISplits = RAISplits;

            List<SelectListItem> AirportSplits = new List<SelectListItem>();
            AirportSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.FeeSplit AirportSplit in db.FeeSplits.Where(o => o.FeeType_ID == 1).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = AirportSplit.ID.ToString();
                s.Text = AirportSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                AirportSplits.Add(s);
            }
            ViewBag.AirportSplits = AirportSplits;

            List<SelectListItem> OneWaySplits = new List<SelectListItem>();
            OneWaySplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.FeeSplit OneWaySplit in db.FeeSplits.Where(o => o.FeeType_ID==3).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = OneWaySplit.ID.ToString();
                s.Text = OneWaySplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                OneWaySplits.Add(s);
            }
            ViewBag.OneWaySplits = OneWaySplits;

            List<SelectListItem> OtherSplits = new List<SelectListItem>();
            OtherSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit OtherSplit in db.OtherCostSplits.Where(o => o.Code_ID == 4).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = OtherSplit.ID.ToString();
                s.Text = OtherSplit.Description;
                OtherSplits.Add(s);
            }
            ViewBag.OtherSplits = OtherSplits;

            List<SelectListItem> RefuellingSplits = new List<SelectListItem>();
            RefuellingSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit RefuellingSplit in db.OtherCostSplits.Where(o => o.Code_ID == 6).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = RefuellingSplit.ID.ToString();
                s.Text = RefuellingSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                RefuellingSplits.Add(s);
            }
            ViewBag.RefuellingSplits = RefuellingSplits;

            List<SelectListItem> DeliverySplits = new List<SelectListItem>();
            DeliverySplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit DeliverySplit in db.OtherCostSplits.Where(o => o.Code_ID == 2).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = DeliverySplit.ID.ToString();
                s.Text = DeliverySplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                DeliverySplits.Add(s);
            }
            ViewBag.DeliverySplits = DeliverySplits;

            List<SelectListItem> ReturnSplits = new List<SelectListItem>();
            ReturnSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit ReturnSplit in db.OtherCostSplits.Where(o => o.Code_ID == 7).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = ReturnSplit.ID.ToString();
                s.Text = ReturnSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                ReturnSplits.Add(s);
            }
            ViewBag.ReturnSplits = ReturnSplits;

            List<SelectListItem> VRFSplits = new List<SelectListItem>();
            VRFSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit VRFSplit in db.OtherCostSplits.Where(o => o.Code_ID == 8).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = VRFSplit.ID.ToString();
                s.Text = VRFSplit.Description;
                //if (rec.VRFSplit > 0 && (rec.CongestionFeeSplit == null || rec.CongestionFeeSplit.ID < 0)) { s.Selected = true; }
                VRFSplits.Add(s);
            }
            ViewBag.VRFSplits = VRFSplits;

            List<SelectListItem> MiscSplits = new List<SelectListItem>();
            MiscSplits.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            foreach (Entities.OtherCostSplit MiscSplit in db.OtherCostSplits.Where(o => o.Code_ID == 3).OrderBy(c => c.ID).ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = MiscSplit.ID.ToString();
                s.Text = MiscSplit.Description;
                //if (rec.VATSplit.ID == vatSplit.ID) { s.Selected = true; }
                MiscSplits.Add(s);
            }
            ViewBag.MiscSplits = MiscSplits;

            //List<SelectListItem> MiscCodes = new List<SelectListItem>();
            ////MiscCodes.Add(new SelectListItem { Value = "-1", Text = "Please Select..." });
            //SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ACMEE"]);
            //SqlCommand comm = new SqlCommand("select * from misc_code where country_code = 'SE' order by misc_code", conn);
            //comm.CommandType = CommandType.Text;
            //DataSet dta = new DataSet();
            //SqlDataAdapter adapter = new SqlDataAdapter(comm);
            //conn.Open();
            //adapter.Fill(dta);
            //conn.Close();

            //foreach (DataRow row in dta.Tables[0].Rows)
            //{
            //    SelectListItem s = new SelectListItem();
            //    s.Value = row["Misc_Code"].ToString();
            //    s.Text = row["Misc_Desc"].ToString();
            //    if (rec.MiscCode == row["Misc_Code"].ToString()) s.Selected = true;
            //    MiscCodes.Add(s);
            //}
            //ViewBag.MiscCodes = MiscCodes;

            List<SelectListItem> MiscCodes = new List<SelectListItem>();
            if(rec.MiscCode == null || string.IsNullOrEmpty(rec.MiscCode.Trim())) MiscCodes.Add(new SelectListItem { Value = "", Text = "" });
            foreach (Entities.MiscCode MiscCode in db.MiscCodes.ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = MiscCode.MiscCodeLetter;
                s.Text = MiscCode.MiscCodeDescription;
                if (rec.MiscCode == MiscCode.MiscCodeLetter) { s.Selected = true; }
                MiscCodes.Add(s);
            }
            ViewBag.MiscCodes = MiscCodes;

            List<SelectListItem> ExpenseCodes = new List<SelectListItem>();
            if (rec.OnTheRoadCode == null || string.IsNullOrEmpty(rec.OnTheRoadCode.Trim())) { rec.OnTheRoadCode = "O"; } //ExpenseCodes.Add(new SelectListItem { Value = "", Text = "" });
            foreach (Entities.ExpenseCode ExpenseCode in db.ExpenseCodes.ToList())
            {
                SelectListItem s = new SelectListItem();
                s.Value = ExpenseCode.ExpenseCodeLetter;
                s.Text = ExpenseCode.ExpenseCodeDescription;
                if (rec.OnTheRoadCode == ExpenseCode.ExpenseCodeLetter) { s.Selected = true; }
                ExpenseCodes.Add(s);
            }
            ViewBag.ExpenseCodes = ExpenseCodes;

        }

            //bool VoucherValueUpdated = false;
            //bool TotalUpdated = false;
            //private void ValuesChanged(SplitBilling previous, SplitBilling current)
            //{
            //    bool v = false;
            //    bool c = false;

            //    if(current.SuperCDWTotal > 0)
            //    {
            //        v = (previous.SuperCDWTotal - previous.SuperCDWCustomerPays != current.SuperCDWTotal - current.SuperCDWCustomerPays);
            //        c = previous.SuperCDWTotal != current.SuperCDWTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.SuperTPTotal > 0)
            //    {
            //        v = (previous.SuperTPTotal - previous.SuperTPCustomerPays != current.SuperTPTotal - current.SuperTPCustomerPays);
            //        c = previous.SuperTPTotal != current.SuperTPTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.RAITotal > 0)
            //    {
            //        v = (previous.RAITotal - previous.RAICustomerPays != current.RAITotal - current.RAICustomerPays);
            //        c = previous.RAITotal != current.RAITotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.AirportFeeTotal > 0)
            //    {
            //        v = (previous.AirportFeeTotal - previous.AirportFeeCustomerPays != current.AirportFeeTotal - current.AirportFeeCustomerPays);
            //        c = previous.AirportFeeTotal != current.AirportFeeTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.OneWayTotal > 0)
            //    {
            //        v = (previous.OneWayTotal - previous.OneWayCustomerPays != current.OneWayTotal - current.OneWayCustomerPays);
            //        c = previous.OneWayTotal != current.OneWayTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.RefuellingTotal > 0)
            //    {
            //        v = (previous.RefuellingTotal - previous.RefuellingCustomerPays != current.RefuellingTotal - current.RefuellingCustomerPays);
            //        c = previous.RefuellingTotal != current.RefuellingTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.DeliveryTotal > 0)
            //    {
            //        v = (previous.DeliveryTotal - previous.DeliveryCustomerPays != current.DeliveryTotal - current.DeliveryCustomerPays);
            //        c = previous.DeliveryTotal != current.DeliveryTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.ReturnTotal > 0)
            //    {
            //        v = (previous.ReturnTotal - previous.ReturnCustomerPays != current.ReturnTotal - current.ReturnCustomerPays);
            //        c = previous.ReturnTotal != current.ReturnTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.VRFTotal > 0)
            //    {
            //        if(current.VRFSplit == -1) current.VRFTotal = 0; 
            //        v = (previous.VRFTotal - previous.VRFCustomerPays != current.VRFTotal - current.VRFCustomerPays);
            //        c = previous.VRFTotal != current.VRFTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.OtherFeeTotal > 0)
            //    {
            //        v = (previous.OtherFeeTotal - previous.OtherFeeCustomerPays != current.OtherFeeTotal - current.OtherFeeCustomerPays);
            //        c = previous.OtherFeeTotal != current.OtherFeeTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }
            //    if(current.MiscCostsTotal > 0)
            //    {
            //        v = (previous.MiscCostsTotal - previous.MiscCostsCustomerPays != current.MiscCostsTotal - current.MiscCostsCustomerPays);
            //        c = previous.MiscCostsTotal != current.MiscCostsTotal;
            //        if(v) VoucherValueUpdated = v;
            //        if(c) TotalUpdated = c;
            //    }

            //}

        [HttpPost]
        public ActionResult Edit(SplitBilling SB, FormCollection frm, string btVoid)
        {

            SplitBilling rec = db.SplitBillings.Where(s => s.RentalId == SB.RentalId).FirstOrDefault();
            var voucherCompareRec = rec;
            if (voucherCompareRec != null && voucherCompareRec.Timestamp.Date >= DateTime.Now.Date)
            {
                var nowThreshold = DateTime.Now.Date;
                voucherCompareRec = db.SplitBillings.Where(z => z.RentalAgreementNumber == rec.RentalAgreementNumber && z.Timestamp < nowThreshold).OrderByDescending(o => o.Timestamp).FirstOrDefault();
                if (voucherCompareRec == null) voucherCompareRec = rec;
            }

            if (frm["chkManualInvoice"] != null && frm["chkManualInvoice"] == "on")
            {
                SB.ManualInvoice = true; 
                SB.Timestamp = DateTime.Now;
                SB.SentToWizard = true;
                SB.WizardSuccess = true;
                
            }
            else
            {
                SB.ManualInvoice = false; 
            }

            if (frm["chkOverrideLock"] != null && frm["chkOverrideLock"] == "on") { SB.OverrideWizardLock = true; } else { SB.OverrideWizardLock = false; }
            SB.OnTheRoadCode = frm["cboExpenseCode"];

            if (btVoid == "btVoid")
            {
                return RedirectToAction("voidcontract", new { Id=SB.RentalId, @ManualInvoice=SB.ManualInvoice, @comment=SB.AVISComments, @otrcode=SB.OnTheRoadCode, @otr_c = SB.AlreadyPaidByCustomerAmount, @otr_i = SB.AlreadyPaidByInsuranceAmount });
            }


            bool wasException = false;
            if (frm["status_text"].ToLower() == "exception") wasException = true;
            SB.RentalTransactionType = "generic";
            SB.Timestamp = DateTime.Now;
            SB.Status = 2;
            SB.RentalCheckInYear = SB.RentalCheckinDateTime.Value.Year;
            SB.MiscCode = frm["cboMiscCode"];
            SB.UpdatedBy = System.Environment.UserName;

            //SB.AVISComments = frm["AvisCom"];
            if(SB.SuperTPTotal == null) SB.SuperTPTotal = 0;
            if (SB.DaysCoveredByInsurance == null) SB.DaysCoveredByInsurance = 0;
            if (SB.KmCoveredByInsurance == null) SB.KmCoveredByInsurance = 0;
            if (SB.AmountCoveredByInsurance == null) SB.AmountCoveredByInsurance = 0;

            if (SB.CDWSplit == -1) { SB.SuperCDWTotal = 0; SB.SuperCDWCustomerPays = 0; }
            if (SB.SuperTPSplit == -1) { SB.SuperTPTotal = 0; SB.SuperTPCustomerPays = 0; }
            if (SB.RAISplit == -1) { SB.RAITotal = 0; SB.RAICustomerPays = 0; }
            if (SB.AirportFeeSplit == -1) { SB.AirportFeeTotal = 0; SB.AirportFeeCustomerPays = 0; }
            if (SB.VRFSplit == -1) { SB.VRFTotal = 0; SB.VRFCustomerPays = 0; }
            if (SB.OneWayFeeSplit == -1) { SB.OneWayTotal = 0; SB.OneWayCustomerPays = 0; }
            if (SB.RefuelingSplit == -1) { SB.RefuellingTotal = 0; SB.RefuellingCustomerPays = 0; }
            if (SB.DeliveryFeeSplit == -1) { SB.DeliveryTotal = 0; SB.DeliveryCustomerPays = 0; }
            if (SB.ReturnFeeSplit == -1) { SB.ReturnTotal = 0; SB.ReturnCustomerPays = 0; }
            SB.MiscCostsTotal = SB.MiscCostsCustomerPays + (decimal) SB.MiscCostsInsurancePays;
            if (SB.OtherFeeSplit == -1) { SB.OtherFeeTotal = 0; SB.OtherFeeCustomerPays = 0; }

            if (SB.Output_Type_C == "-1") SB.Output_Type_C = null;
            if (SB.Output_Type_I == "-1") SB.Output_Type_I = null;

            if (rec != null)
            {

                //var allRecs = db.SplitBillings.Where(r => r.RentalAgreementNumber == rec.RentalAgreementNumber).OrderByDescending(o => o.Timestamp).ToList();
                //Entities.SplitBilling previousRec = null;
                //var takeNext = false;
                //foreach(var r in allRecs)
                //{
                //    if(takeNext) { ValuesChanged(r, rec); break; }
                //    if(r.RentalId == rec.RentalId) { takeNext = true; }
                //}

                //ValuesChanged(rec, SB);

                rec.Output_Type_I = SB.Output_Type_I;
                rec.Output_Type_C = SB.Output_Type_C;

                rec.VoucherValueUpdate = voucherCompareRec.InvoiceAmountInsurance != SB.InvoiceAmountInsurance; //VoucherValueUpdated;
                rec.TotalUpdate = (voucherCompareRec.InvoiceAmountInsurance + voucherCompareRec.InvoiceAmountCustomer) != (SB.InvoiceAmountInsurance + SB.InvoiceAmountCustomer); //TotalUpdated;

                rec.CCIUpdated = rec.RentalDamageNumber != SB.RentalDamageNumber;
                if (SB.RentalRenterAddress2Text == null) SB.RentalRenterAddress2Text = string.Empty;
                if (rec.RentalRenterAddress2Text == null) rec.RentalRenterAddress2Text = string.Empty;
                rec.CustomerAddressUpdated = (rec.RentalRenterAddress1Text != SB.RentalRenterAddress1Text || rec.RentalRenterAddress2Text != SB.RentalRenterAddress2Text || rec.RentalRenterAddressCityText != SB.RentalRenterAddressCityText);

                if (rec.VoucherValueUpdate.HasValue && rec.VoucherValueUpdate.Value == false && rec.TotalUpdate.HasValue && rec.TotalUpdate.Value == false && rec.CustomerAddressUpdated.HasValue && rec.CustomerAddressUpdated.Value == false)
                {
                    if (rec.RentalInsuranceBillingAddress2 == null) rec.RentalInsuranceBillingAddress2 = string.Empty;
                    if (SB.RentalInsuranceBillingAddress2 == null) SB.RentalInsuranceBillingAddress2 = string.Empty;

                    var insuranceAddressUpdated = (rec.RentalInsuranceBillingAddress1 != SB.RentalInsuranceBillingAddress1 || rec.RentalInsuranceBillingAddress2 != SB.RentalInsuranceBillingAddress2 || rec.RentalInsuranceBillingAddress3 != SB.RentalInsuranceBillingAddress3);
                    if (!insuranceAddressUpdated)
                    {
                        Session["NoChanges"] = true;
                        return Redirect(Request["thisUrl"]);
                    }
                }

                var customerAVUpdated = rec.RentalRenterCreditCardNumber != SB.RentalRenterCreditCardNumber;
                if (rec.CustomerAddressUpdated.HasValue)
                {
                    if (rec.CustomerAddressUpdated.Value == true && customerAVUpdated) rec.CustomerAddressUpdated = false;
                }

                //if (frm["tp"] == "closed")
                //{
                //    rec.DateArchived = DateTime.Now;
                //    rec.IsArchive = true;
                //    db.Entry(rec).State = EntityState.Modified;
                //    db.SaveChanges();
                //}


                if (rec.Status == 4)
                {
                    wasException = true;
                }
                rec.AVISComments = SB.AVISComments;
                rec.AmendedDate = DateTime.Now;
                rec.NewInvoiceRequired = true;
                rec.PriceSplit = SB.PriceSplit;
                rec.PriceSplitTotal = SB.PriceSplitTotal;
                rec.PriceSplitCustomerPays = SB.PriceSplitCustomerPays;
                rec.VATSplit = SB.VATSplit;
                rec.VATTotal = SB.VATTotal;
                rec.VATCustomerPays = SB.VATCustomerPays;

                rec.CDWSplit = SB.CDWSplit;
                rec.SuperCDWTotal = SB.SuperCDWTotal;
                rec.SuperCDWCustomerPays = SB.SuperCDWCustomerPays;

                rec.SuperTPSplit = SB.SuperTPSplit;
                rec.SuperTPTotal = SB.SuperTPTotal;
                rec.SuperTPCustomerPays = SB.SuperTPCustomerPays;

                rec.RAISplit = SB.RAISplit;
                rec.RAITotal = SB.RAITotal;
                rec.RAICustomerPays = SB.RAICustomerPays;

                rec.AirportFeeSplit = SB.AirportFeeSplit;
                rec.AirportFeeTotal = SB.AirportFeeTotal;
                rec.AirportFeeCustomerPays = SB.AirportFeeCustomerPays;

                rec.VRFSplit = SB.VRFSplit;
                rec.VRFTotal = SB.VRFTotal;
                rec.VRFCustomerPays = SB.VRFCustomerPays;

                rec.OneWayFeeSplit = SB.OneWayFeeSplit;
                rec.OneWayTotal = SB.OneWayTotal;
                rec.OneWayCustomerPays = SB.OneWayCustomerPays;

                rec.RefuelingSplit = SB.RefuelingSplit;
                rec.RefuellingTotal = SB.RefuellingTotal;
                rec.RefuellingCustomerPays = SB.RefuellingCustomerPays;

                rec.DeliveryFeeSplit = SB.DeliveryFeeSplit;
                rec.DeliveryTotal = SB.DeliveryTotal;
                rec.DeliveryCustomerPays = SB.DeliveryCustomerPays;

                rec.ReturnFeeSplit = SB.ReturnFeeSplit;
                rec.ReturnTotal = SB.ReturnTotal;
                rec.ReturnCustomerPays = SB.ReturnCustomerPays;

                //rec.MiscCostsSplit = SB.MiscCostsSplit;
                rec.MiscCostsTotal = SB.MiscCostsTotal;
                rec.MiscCostsCustomerPays = SB.MiscCostsCustomerPays;
                rec.MiscCostsInsurancePays = SB.MiscCostsInsurancePays;

                rec.OtherFeeSplit = SB.OtherFeeSplit;
                rec.OtherFeeTotal = SB.OtherFeeTotal;
                rec.OtherFeeCustomerPays = SB.OtherFeeCustomerPays;

                rec.InvoiceFeeTotal = SB.InvoiceFeeTotal;
                rec.InvoiceFeeCustomerPays = SB.InvoiceFeeCustomerPays;

                rec.InvoiceAmountCustomer = SB.InvoiceAmountCustomer;
                rec.InvoiceAmountInsurance = SB.InvoiceAmountInsurance;
                rec.AlreadyPaidByCustomerAmount = SB.AlreadyPaidByCustomerAmount;
                rec.AlreadyPaidByInsuranceAmount = SB.AlreadyPaidByInsuranceAmount;
                rec.RentalCheckInYear = rec.RentalCheckinDateTime.Value.Year;
                rec.RentalCheckinDateTime = DateTime.Parse(frm["checkInDateDate"] + " " + frm["checkInDateTime"]);
                rec.Remarks = SB.Remarks;
                rec.RentalLocalContactText = SB.RentalLocalContactText;
                rec.KMCheckin = SB.KMCheckin;
                rec.KmCoveredByInsurance = SB.KmCoveredByInsurance;
                rec.AmountCoveredByInsurance = SB.AmountCoveredByInsurance;
                rec.DaysCoveredByInsurance = SB.DaysCoveredByInsurance;
                rec.RentalCustomerName = SB.RentalCustomerName;
                rec.RentalRenterAddress1Text = SB.RentalRenterAddress1Text;
                rec.RentalRenterAddress2Text = SB.RentalRenterAddress2Text == null ? string.Empty : SB.RentalRenterAddress2Text;
                rec.RentalRenterAddressCityText = SB.RentalRenterAddressCityText;
                rec.RentalACTONumber = SB.RentalACTONumber;
                rec.OverrideDaysAmount = SB.OverrideDaysAmount;
                rec.OverrideKMAmount = SB.OverrideKMAmount;
                rec.RentalDamageNumber = SB.RentalDamageNumber;
                rec.MiscCode = SB.MiscCode;
                rec.Status = 2;
                rec.Timestamp = DateTime.Now;
                if (SB.ManualInvoice == false)
                {
                    rec.WizardSuccess = false;
                    rec.SentToWizard = false;
                }

                rec.RentalInsuranceCreditClubCode = SB.RentalInsuranceCreditClubCode;
                rec.RentalInsuranceCreditCardNumber = SB.RentalInsuranceCreditCardNumber;
                rec.RentalRenterCreditClubCode = SB.RentalRenterCreditClubCode;
                rec.RentalRenterCreditCardNumber = SB.RentalRenterCreditCardNumber;
                rec.UpdatedBy = System.Environment.UserName;
                rec.RentalCompanyName = SB.RentalCompanyName;
                rec.RentalRenterBillingName = SB.RentalRenterBillingName;
                rec.OnTheRoadCode = SB.OnTheRoadCode;
                rec.ManualInvoice = SB.ManualInvoice;

                rec.RentalInsuranceBillingCompany = SB.RentalInsuranceBillingCompany;
                rec.RentalInsuranceBillingAddress1 = SB.RentalInsuranceBillingAddress1;
                rec.RentalInsuranceBillingAddress2 = SB.RentalInsuranceBillingAddress2;
                rec.RentalInsuranceBillingAddress3 = SB.RentalInsuranceBillingAddress3;
                rec.RentalInsuranceBillingName = SB.RentalInsuranceBillingName;


                if (frm["tp"] == "closed") // && PublicInstancePropertiesEqual(rec, SB).Count == 0)
                {
                    rec.ParentRentalId = rec.RentalId;
                    
                    rec.OverrideWizardLock = SB.OverrideWizardLock;
                    db.Entry(rec).State = EntityState.Added;

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    var ad = new AdjustmentProcessed();
                    ad.TimeStamp = DateTime.Now;
                    ad.SplitBillingId =(int) rec.RentalId;
                    ad.RentalCheckinYear = rec.RentalCheckinDateTime.Value.Year;
                    ad.RentalAgreementNumber = rec.RentalAgreementNumber;
                    ad.Processed = 0;
                    ad.OK = 0;
                    if (rec.ManualInvoice.HasValue && rec.ManualInvoice.Value == true) ad.TimeStamp = DateTime.Now;
                    db.AdjustmentProcessed.Add(ad);

                }
                else
                {
                    db.Entry(rec).State = EntityState.Modified;
                }
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                    throw;
                }
                //var cr = db.ClosedRentals.FirstOrDefault(c => c.RentalAgreementNumber == SB.RentalAgreementNumber);
                //if (cr != null)
                //{
                //    cr.AmendedDate = DateTime.Now;
                //    cr.NewInvoiceRequired = true;
                //    db.SaveChanges();
                //}

                audit a = new audit();
                a.Status_ID = rec.Status;
                a.RentalNumber = rec.RentalAgreementNumber;
                a.TimeChanged = DateTime.Now;
                if (wasException)
                {
                    a.Comment = "Exception record updated in SBAIR. Awaiting back end process comparison with Wizard.";

                    List<InvoicedRental> i = db.InvoicedRentals.Where(x => x.AvisRANumber == rec.RentalAgreementNumber).ToList();
                    foreach (InvoicedRental ir in i)
                    {
                        ir.Processed = 0;
                        //db.SaveChanges();
                    }
                }
                else
                {
                    a.Comment = "Split billing record saved in SBAIR. Status set to Pending.";
                }
                db.audits.Add(a);
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                    throw;
                }
            }
            else
            {
                //if (ModelState.IsValid)
                //{
                    try
                    {
                        
                        if (Request["type"] == "exception")
                        {
                            Entities.ExceptionRental o_r = db.ExceptionRentals.Where(o => o.RentalAgreementNumber == SB.RentalAgreementNumber).First();
                            SB.RentalInsuranceBillingCompany = o_r.RentalInsuranceBillingCompany;
                            SB.RentalInsuranceBillingAddress1 = o_r.RentalInsuranceBillingAddress1;
                            SB.RentalInsuranceBillingAddress2 = o_r.RentalInsuranceBillingAddress2;
                            SB.RentalInsuranceBillingAddress3 = o_r.RentalInsuranceBillingAddress3;
                            SB.RentalInsuranceBillingName = o_r.RentalInsuranceBillingName;
                            SB.RentalACTONumber = o_r.RentalACTONumber;
                            SB.RentalAWDNumber = o_r.RentalAWDNumber;
                            //SB.RentalDamageNumber = o_r.RentalDamageNumber;
                            SB.RentalIATANumber = o_r.RentalIATANumber;
                            SB.RentalInsuranceInvoiceNumber = o_r.RentalInsuranceInvoiceNumber;
                            SB.RentalReservationNumber = o_r.RentalReservationNumber;
                            SB.RentalTaxNumber = o_r.RentalTaxNumber;
                            SB.RentalWizardOfAvisNumber = o_r.RentalWizardOfAvisNumber;
                            SB.RentalContractFeeAmount = o_r.RentalContractFeeAmount;
                            SB.RentalRenterInvoiceFeeAmount = o_r.RentalRenterInvoiceFeeAmount;
                            SB.InvoiceFeeTotal = (decimal) o_r.RentalRenterInvoiceFeeAmount;
                            SB.InvoiceFeeCustomerPays = (decimal)o_r.RentalRenterInvoiceFeeAmount;
                        }
                        else
                        {


                            Entities.OpenRental o_r = db.OpenRentals.Where(o => o.RentalAgreementNumber == SB.RentalAgreementNumber).First();
                            SB.RentalInsuranceBillingCompany = o_r.RentalInsuranceBillingCompany;
                            SB.RentalInsuranceBillingAddress1 = o_r.RentalInsuranceBillingAddress1;
                            SB.RentalInsuranceBillingAddress2 = o_r.RentalInsuranceBillingAddress2;
                            SB.RentalInsuranceBillingAddress3 = o_r.RentalInsuranceBillingAddress3;
                            SB.RentalInsuranceBillingName = o_r.RentalInsuranceBillingName;
                            SB.RentalACTONumber = o_r.RentalACTONumber;
                            SB.RentalAWDNumber = o_r.RentalAWDNumber;
                            //SB.RentalDamageNumber = o_r.RentalDamageNumber;
                            SB.RentalIATANumber = o_r.RentalIATANumber;
                            SB.RentalInsuranceInvoiceNumber = o_r.RentalInsuranceInvoiceNumber;
                            SB.RentalReservationNumber = o_r.RentalReservationNumber;
                            SB.RentalTaxNumber = o_r.RentalTaxNumber;
                            SB.RentalWizardOfAvisNumber = o_r.RentalWizardOfAvisNumber;
                            SB.RentalContractFeeAmount = o_r.RentalContractFeeAmount;
                            SB.RentalRenterInvoiceFeeAmount = o_r.RentalRenterInvoiceFeeAmount;
                            SB.InvoiceFeeTotal = (decimal)o_r.RentalRenterInvoiceFeeAmount;
                            SB.InvoiceFeeCustomerPays = (decimal)o_r.RentalRenterInvoiceFeeAmount;

                            SB.MVANumber = o_r.MVANumber;
                            SB.RentalCompanyName = o_r.RentalCompanyName;
                            SB.RentalRenterBillingName = o_r.RentalRenterBillingName;
                            //SB.RentalCustomerName = o_r.RentalCustomerName;
                        }

                        SB.RentalCheckinDateTime = DateTime.Parse(frm["checkInDateDate"] + " " + frm["checkInDateTime"]);
                        SB.Timestamp = DateTime.Now;

                        SB.Status = 2;
                        SB.SentToWizard = false;
                        SB.WizardSuccess = false;
                        db.SplitBillings.Add(SB);

                        

                        db.SaveChanges();

                        //var cr = db.ClosedRentals.FirstOrDefault(c => c.RentalAgreementNumber == SB.RentalAgreementNumber);
                        //if (cr != null)
                        //{
                        //    cr.AmendedDate = DateTime.Now;
                        //    cr.NewInvoiceRequired = true;
                        //    db.SaveChanges();
                        //}


                        audit a = new audit();
                        a.Status_ID = SB.Status;
                        a.RentalNumber = SB.RentalAgreementNumber;
                        a.TimeChanged = DateTime.Now;
                        a.Comment = "Split billing record updated in SBAIR. Status set to pending.";
                        db.audits.Add(a);
                        db.SaveChanges();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                    }
                   
                //}

            }

            Entities.ExceptionRental exp = db.ExceptionRentals.Where(e => e.RentalAgreementNumber == SB.RentalAgreementNumber).FirstOrDefault();
            if (exp != null) { db.ExceptionRentals.Remove(exp); db.SaveChanges(); }

            var tmpID = db.SplitBillings.OrderByDescending(o => o.Timestamp).FirstOrDefault(t => t.RentalAgreementNumber == SB.RentalAgreementNumber).RentalId;
            TempData["Updated"] = "yes";
            return RedirectToAction("edit", "listing", new { @id = tmpID, @type = "pending", @return = "/listing", @tab = "3" });
        }

        private Dictionary<string, decimal> getDifferences(SplitBilling currentRec)
        {
            var ret = new Dictionary<string, decimal>();

            var allRecs = db.SplitBillings.Where(r => r.RentalAgreementNumber == currentRec.RentalAgreementNumber).OrderByDescending(o => o.Timestamp).ToList();
            Entities.SplitBilling previousRec = null;
            var takeNext = false;
            foreach(var r in allRecs)
            {
                if(takeNext) { previousRec = r; break; }
                if(r.RentalId == currentRec.RentalId) { currentRec = r; takeNext = true; }
            }

            if(previousRec != null)
            {
                var previousCalc = new Classes.Calculations();
                if(previousRec.SuperCDWCustomerPays > 0 && previousRec.SuperCDWTotal == 0) { previousRec.SuperCDWTotal = previousRec.SuperCDWCustomerPays; }
                if(previousRec.RAICustomerPays > 0 && previousRec.RAITotal == 0) { previousRec.RAITotal = previousRec.RAICustomerPays; }
                if (previousRec.KMCheckout == null || previousRec.KMCheckout == 0)
                {
                    if (previousRec.KMCheckin > previousRec.KMDriven)
                    {
                        previousRec.KMCheckout = previousRec.KMDriven; previousRec.KMDriven = previousRec.KMCheckin - previousRec.KMCheckout;
                    }
                    //else
                    //{
                    //    rec.KMCheckout = rec.KMDriven; rec.KMCheckin = rec.KMCheckout + rec.KMDriven;

                    //}
                }
                previousRec.KMDriven = previousRec.KMCheckin - previousRec.KMCheckout;
                previousRec.InvoiceFeeTotal = 0;
                previousRec.InvoiceFeeCustomerPays = 0;
                previousCalc.Calculate(previousRec);

                ret.Add("CustomerTotal", Decimal.Round(previousCalc.C_TimeAmt + previousCalc.C_SuperCDWAmt + previousCalc.C_SuperTPAmt + previousCalc.C_RAIAmt + previousCalc.C_VRFAmt + previousCalc.C_SplittedVATAmt + previousCalc.C_AirportFeeAmt + previousCalc.C_RefuelingAmt + previousCalc.C_DeliveryFeeAmt + previousCalc.C_ReturnFeeAmt + previousCalc.C_OneWayFeeAmt + previousCalc.C_MiscAmt + previousCalc.C_OthersAmt - previousRec.AlreadyPaidByCustomerAmount, 2));
                ret.Add("InsuranceTotal", Decimal.Round(previousCalc.I_TimeAmt + previousCalc.I_SuperCDWAmt + previousCalc.I_SuperTPAmt + previousCalc.I_RAIAmt + previousCalc.I_VRFAmt + previousCalc.I_SplittedVATAmt + previousCalc.I_AirportFeeAmt + previousCalc.I_RefuelingAmt + previousCalc.I_DeliveryFeeAmt + previousCalc.I_ReturnFeeAmt + previousCalc.I_OneWayFeeAmt + previousCalc.I_MiscAmt + previousCalc.I_OthersAmt - previousRec.AlreadyPaidByInsuranceAmount, 2));
                ret.Add("hasPrevious", 1);
            }
            else
            {
                ret.Add("CustomerTotal", 0);
                ret.Add("InsuranceTotal", 0);
                ret.Add("hasPrevious", 0);
            }

            return ret;
        }

        [HttpPost]
        public ActionResult GetPriceSplitData(int ID)
        {
            return Content(db.PriceSplits.Where(p => p.ID == ID).First().PercentageCustomerPays.ToString());
        }

        [HttpPost]
        public ActionResult GetVATSplitData(int ID)
        {
            return Content(db.VATSplits.Where(p => p.ID == ID).First().PercentageCustomerPays.ToString());
        }

        [HttpPost]
        public ActionResult Calculate(string ranumber, int pricesplit, int vatsplit, string TotalRentalChargedAmount, string VATTotal,
            int AirportFeeSplit, int CDWSplit, int RAISplit, int VRFSplit, int OneWayFeeSplit, int RefuelingSplit, int DeliverySplit, int ReturnFeeSplit,
            string MiscCostsCustomerPays, string SuperCDWTotal, string RAITotal, string AirportFeeTotal,
            string OneWayFeeTotal, string RefuelingTotal, string DeliveryTotal, string ReturnFeeTotal, string VRFTotal, string MiscCostsInsurancePays,
            DateTime RentalCheckoutDateTime, DateTime RentalCheckinDateTime, string InvoiceFee, string KMRate, string DayRate, string WeekRate,
            string MonthRate, int KMDriven, string RentalCDWPerDayAmount, string RentalPAIPerDayAmount, int NbKmCoveredByInsurance, int NbDaysCoveredByInsurance,
            int KMCheckin, string AmountCoveredByInsurance, string RentalTPPerDayAmount, int SuperTPSplit, string SuperTPTotal, string OverrideDaysAmount,
            string OverrideKMAmount, string RentalId
            )
            
        {
            if (RentalPAIPerDayAmount == string.Empty) { RentalPAIPerDayAmount = "0"; }
            if (RentalCDWPerDayAmount == string.Empty) { RentalCDWPerDayAmount = "0"; }
            if (RentalTPPerDayAmount == string.Empty) { RentalTPPerDayAmount = "0"; }
            if (AmountCoveredByInsurance == string.Empty) { AmountCoveredByInsurance = "0"; }
            if (OverrideDaysAmount == string.Empty) { OverrideDaysAmount = "0"; }
            if (OverrideKMAmount == string.Empty) { OverrideKMAmount = "0"; }
            if (MiscCostsCustomerPays == string.Empty) { MiscCostsCustomerPays = "0"; }
            if (MiscCostsInsurancePays == string.Empty) { MiscCostsInsurancePays = "0"; }

            if (KMCheckin == null || (KMCheckin < KMDriven))
            {
                KMCheckin = KMDriven;
            }

            int KM = KMCheckin - KMDriven;

            double number = 0;

            var r = int.Parse(RentalId);
            //var tm = decimal.Parse(TMDiscount);

            Classes.Calculations calc = new Classes.Calculations();
            SplitBilling bill = db.SplitBillings.Where(s => s.RentalId == r).FirstOrDefault();
            if (bill == null) { bill = new SplitBilling(); }
            bill.RentalAgreementNumber = ranumber;
            bill.RentalCheckoutDateTime = RentalCheckoutDateTime;
            bill.RentalCheckinDateTime = RentalCheckinDateTime;
            bill.RentalMileageRateAmount = decimal.Parse(KMRate);
            bill.RentalDailyRateAmount = decimal.Parse(DayRate);
            bill.RentalWeeklyRateAmount = decimal.Parse(WeekRate);
            bill.RentalMonthlyRateAmount = decimal.Parse(MonthRate);
            //bill.KMDriven = KMDriven;
            bill.KMDriven = KM;
            bill.KmCoveredByInsurance = NbKmCoveredByInsurance;
            bill.DaysCoveredByInsurance = NbDaysCoveredByInsurance;
            bill.AmountCoveredByInsurance = decimal.Parse(AmountCoveredByInsurance);
            bill.RentalPAIPerDayAmount = decimal.Parse(RentalPAIPerDayAmount);
            bill.RentalCDWPerDayAmount = decimal.Parse(RentalCDWPerDayAmount);

            bill.RentalALIPerDayAmount = decimal.Parse(RentalTPPerDayAmount);

            bill.PriceSplit = db.PriceSplits.Where(s=>s.ID == pricesplit).First().ID;
            bill.VATSplit = db.VATSplits.Where(s=>s.ID == vatsplit).First().ID;
            bill.RentalContractFeeAmount = 0; // decimal.Parse(InvoiceFee);
            bill.InvoiceFeeTotal = 0;
            //bill.OtherFeeSplit = SplitOrNullOther(OtherCostsSplit);
            //bill.OtherFeeTotal = decimal.Parse(double.TryParse(OtherCostTotal, out number) == true ? OtherCostTotal : number.ToString());
            bill.RefuelingSplit = SplitOrNullOther(RefuelingSplit);
            bill.RefuellingTotal = decimal.Parse(double.TryParse(RefuelingTotal, out number) == true ? RefuelingTotal : number.ToString());
            if (bill.RefuelingSplit > 0 && bill.RefuellingTotal > 0) bill.RefuellingTotal += 0;
            bill.DeliveryFeeSplit = SplitOrNullOther(DeliverySplit);
            bill.DeliveryTotal = decimal.Parse(double.TryParse(DeliveryTotal, out number) == true ? DeliveryTotal : number.ToString());
            bill.ReturnFeeSplit = SplitOrNullOther(ReturnFeeSplit);
            bill.ReturnTotal = decimal.Parse(double.TryParse(ReturnFeeTotal, out number) == true ? ReturnFeeTotal : number.ToString());
            bill.VRFSplit = SplitOrNullOther(VRFSplit);
            bill.VRFTotal = decimal.Parse(double.TryParse(VRFTotal, out number) == true ? VRFTotal : number.ToString());
            //bill.MiscCostsSplit = SplitOrNullOther(MiscCostsSplit);
            //bill.MiscCostsTotal = decimal.Parse(double.TryParse(MiscCostsTotal, out number) == true ? MiscCostsTotal : number.ToString());
            bill.AirportFeeSplit = SplitOrNull(AirportFeeSplit);
            bill.AirportFeeTotal = decimal.Parse(double.TryParse(AirportFeeTotal, out number) == true ? AirportFeeTotal : number.ToString());
            bill.OneWayFeeSplit = SplitOrNull(OneWayFeeSplit);
            bill.OneWayTotal = decimal.Parse(double.TryParse(OneWayFeeTotal, out number) == true ? OneWayFeeTotal : number.ToString());
            bill.CDWSplit = SplitOrNullOther(CDWSplit);
            bill.SuperCDWTotal = decimal.Parse(double.TryParse(SuperCDWTotal, out number) == true ? SuperCDWTotal : number.ToString());

            bill.SuperTPSplit = SplitOrNullOther(SuperTPSplit);
            bill.SuperTPTotal = decimal.Parse(double.TryParse(SuperTPTotal, out number) == true ? SuperTPTotal : number.ToString());

            bill.RAISplit = SplitOrNullOther(RAISplit);
            bill.RAITotal = decimal.Parse(double.TryParse(RAITotal, out number) == true ? RAITotal : number.ToString());

            bill.OverrideDaysAmount = decimal.Parse(double.TryParse(OverrideDaysAmount, out number) == true ? OverrideDaysAmount : number.ToString());
            bill.OverrideKMAmount = decimal.Parse(double.TryParse(OverrideKMAmount, out number) == true ? OverrideKMAmount : number.ToString());

            bill.MiscCostsTotal = decimal.Parse(MiscCostsCustomerPays) + decimal.Parse(MiscCostsInsurancePays);
            bill.MiscCostsCustomerPays = decimal.Parse(MiscCostsCustomerPays);
            bill.MiscCostsInsurancePays = decimal.Parse(MiscCostsInsurancePays);
            //bill.TMDiscount = tm;

            calc.Calculate(bill);

            bill.NumDaysRental = calc.C_NbDays;

            var previousTotals = getDifferences(bill);

            return Json(new { tot_C = calc.C_TotalAmt, tot_I = calc.I_TotalAmt, vrf_C = calc.C_VRFAmt, vrf_I = calc.I_VRFAmt,
                refuel_C = calc.C_RefuelingAmt, refuel_I = calc.I_RefuelingAmt, vat_C = calc.C_SplittedVATAmt, vat_I = calc.I_SplittedVATAmt,
                del_C = calc.C_DeliveryFeeAmt, del_I = calc.I_DeliveryFeeAmt, ret_C = calc.C_ReturnFeeAmt, ret_I = calc.I_ReturnFeeAmt,
                air_C = calc.C_AirportFeeAmt, air_I = calc.I_AirportFeeAmt,
                              ow_C = calc.C_OneWayFeeAmt,
                              ow_I = calc.I_OneWayFeeAmt,
                              price_C = calc.C_TimeAmt,
                              price_I = calc.I_TimeAmt,
                              superCDW_C = calc.C_SuperCDWAmt,
                              superCDW_I = calc.I_SuperCDWAmt,
                              rai_C = calc.C_RAIAmt,
                              rai_I = calc.I_RAIAmt, errMsg = calc.errMsg,
                              misc_C = calc.C_MiscAmt, misc_I = calc.I_MiscAmt,
                              other_C = calc.C_OthersAmt, other_I = calc.I_OthersAmt, Kilometers = KM, Days = calc.NbDaysTotal, 
                              tp_C = calc.C_SuperTPAmt, tp_I = calc.I_SuperTPAmt,
                              c_previousTotal = previousTotals["CustomerTotal"],
                              i_previousTotal = previousTotals["InsuranceTotal"],
                              hasPrevious = previousTotals["hasPrevious"]
                              
            }); 
        }

        private int SplitOrNull(int iType)
        {

            try
            {
                return  db.FeeSplits.Where(o => o.ID == iType).First().ID;
            }
            catch
            {
                return -1;
            }

            
        }

        private int SplitOrNullOther(int iType)
        {

            try
            {
                return db.OtherCostSplits.Where(o => o.ID == iType).First().ID;
            }
            catch
            {
                return -1;
            }


        }

        public ActionResult VoidContract(int Id, bool ManualInvoice, string comment, string otrcode, decimal otr_c, decimal otr_i)
        {
            var originalRec = db.SplitBillings.FirstOrDefault(r => r.RentalId == Id);
            originalRec.ManualInvoice = ManualInvoice;
            originalRec.AVISComments = comment;

            originalRec.RentalCheckinDateTime = originalRec.RentalCheckoutDateTime;
            //originalRec.KMCheckin = originalRec.KMDriven;

            //originalRec.AlreadyPaidByCustomerAmount = originalRec.VRFCustomerPays;
            //originalRec.AlreadyPaidByInsuranceAmount = originalRec.VRFTotal - originalRec.VRFCustomerPays;
            originalRec.OnTheRoadCode = "O";
            originalRec.AlreadyPaidByCustomerAmount = 52; // otr_c;
            originalRec.AlreadyPaidByInsuranceAmount = 0; // otr_i;

            //originalRec.OnTheRoadCode = otrcode;

            originalRec.SuperCDWTotal = 0; originalRec.SuperCDWCustomerPays = 0;
            originalRec.SuperTPTotal = 0; originalRec.SuperTPCustomerPays = 0;
            originalRec.RAITotal = 0; originalRec.RAICustomerPays = 0;
            originalRec.AirportFeeTotal = 0; originalRec.AirportFeeCustomerPays = 0;
            originalRec.OneWayTotal = 0; originalRec.OneWayCustomerPays = 0;
            originalRec.RefuellingTotal = 0; originalRec.RefuellingCustomerPays = 0;
            originalRec.DeliveryTotal = 0; originalRec.DeliveryCustomerPays = 0;
            originalRec.ReturnTotal = 0; originalRec.ReturnCustomerPays = 0;
            originalRec.OtherFeeTotal = 0; originalRec.OtherFeeCustomerPays = 0;
            originalRec.MiscCostsTotal = 0; originalRec.MiscCostsCustomerPays = 0; originalRec.MiscCostsInsurancePays = 0;
            originalRec.SuperCDWTotal = 0; originalRec.SuperCDWCustomerPays = 0;
            originalRec.Timestamp = DateTime.Now;
            originalRec.PriceSplit = -1;
            originalRec.VATSplit = -1;
            originalRec.InvoiceAmountCustomer = 0;
            originalRec.InvoiceAmountInsurance = 0;
            originalRec.PriceSplitTotal = 0; originalRec.PriceSplitCustomerPays = 0;
            originalRec.VATTotal = 0; originalRec.VATCustomerPays = 0;
            originalRec.WizardSuccess = false;
            originalRec.SentToWizard = false;
            originalRec.TotalUpdate = true;
            originalRec.VoucherValueUpdate = true;
            originalRec.AmendedDate = DateTime.Now;
            originalRec.ParentRentalId = Id;
            originalRec.UpdatedBy = System.Environment.UserName;
            originalRec.Void = true;

            db.Entry(originalRec).State = EntityState.Added;
            db.SaveChanges();

            var ad = new AdjustmentProcessed();
            ad.TimeStamp = DateTime.Now;
            ad.SplitBillingId = (int)originalRec.RentalId;
            ad.RentalCheckinYear = originalRec.RentalCheckinDateTime.Value.Year;
            ad.RentalAgreementNumber = originalRec.RentalAgreementNumber;
            ad.Processed = 0;
            ad.OK = 0;
            db.AdjustmentProcessed.Add(ad);
            db.SaveChanges();

            return RedirectToAction("edit", "listing", new { @id = originalRec.RentalId, @type = "closed", @return = "/listing", @tab = "3" });
        }


        public ActionResult Delete(int Id)
        {
            var rec = db.SplitBillings.FirstOrDefault(r => r.RentalId == Id);
            var ra = rec.RentalAgreementNumber;
            var a = db.AdjustmentProcessed.FirstOrDefault(f => f.RentalAgreementNumber == ra && f.SplitBillingId == rec.RentalId);

            var recs = db.SplitBillings.Where(x => x.RentalAgreementNumber == rec.RentalAgreementNumber).OrderByDescending(x => x.Timestamp).ToList();
            var takenext = false;
            long nextId = -1;
            foreach(var z in recs)
            {
                if(takenext)
                {
                    nextId = z.RentalId;
                    break;
                }
                if(z.RentalId == Id)
                {
                    takenext = true;
                }
            }

            db.SplitBillings.Remove(rec);

            if(a != null)
            {
                db.AdjustmentProcessed.Remove(a);
            }

            db.SaveChanges();

            //TempData["deleted"] = "true";

            //?type=closed&return=%2Flisting%3Ftab%3D3
            if (nextId > -1)
            {
                return RedirectToAction("edit", "listing", new { @id = nextId, @type = "closed", @return = "/listing", @tab = "3", @fromDeleted = "true" });
            }
            else
            {
                return RedirectToAction("edit", "listing", new { @id = ra, @type = "open", @return = "/listing", @fromDeleted = "true" });
            }

        }

        public string GetInvoiceList(string RANumber)
        {
            //RANumber = "E834587563";

            var viewModel = new InvoiceViewModel();
            var client = new DocumentSearchClient();

            if (string.IsNullOrWhiteSpace(RANumber))// == string.IsNullOrWhiteSpace(invoiceNumber))
            {
                this.ModelState.AddModelError(string.Empty, "Can only search on a single value.");
                viewModel.SearchResult = null;
                //return PartialView(viewModel);
            }

            if (!string.IsNullOrWhiteSpace(RANumber))
            {
                viewModel.SearchResult = client.SearchDocuments(DocumentStoreType.Onbase, RANumber);
            }
            //else if (!string.IsNullOrWhiteSpace(invoiceNumber))
            //{
            //    viewModel.SearchResult = client.SearchDocumentsByInvoiceNumber(invoiceNumber);
            //}

            var html = string.Empty;
            html = "<table><tr><td width='85%'><strong>Invoice info</strong></td><td width='15%'><strong>View</strong></td></tr>";
            foreach (var documentStoreSearchResult in viewModel.SearchResult.DocumentStoreSearchResults)
                       {

                           foreach (var item in documentStoreSearchResult.Documents)
                           {
                               html += "<tr>";
                               // get metadata
                               //var sbMeta = new System.Text.StringBuilder();
                               //sbMeta.Append("<ul>");
                               //foreach (var metaDataItem in item.DocumentMetaData)
                               //{
                               html += "<td width='70%'><br />" + item.DocumentMetaData[2].Item2 + "<br /></td>";
                               //}
                               //sbMeta.Append("</ul>");
                               //html += sbMeta.ToString();

                               //<table style="float: left;"  class="imageRow">
                               //    <tr class="set">
                                       for (int i = 0; i < item.Elements.Count(); i++)
                                       {
                                           var element = item.Elements[i];

                               //            <td class="single">
                               //                <div>
                               //                    @{
                               //                        var target = "";
                               //                        if (element.DocumentUrl.Substring(element.DocumentUrl.Length - 4, 4) == ".tif")
                               //                        {
                               //                            target = "";
                               //                        }
                               //                        else
                               //                        {
                               //                            target = " target='_blank' ";
                               //                        }
                               //                    }
                                           if (ConfigurationManager.AppSettings["AvisOfficeInternal"] == "true") element.DocumentUrl = element.DocumentUrl.Replace("https://webportal.absrac.net", "http://n10os2swb088");
                                                   html += "<td width='25%'><a target='_blank' href='" + element.DocumentUrl + "' >View</a></td>";
                               //                        <img src="@element.DocumentThumbnailUrl" alt="Image 1 0f @item.Elements.Count() thumb" />
                               //                   </a>
                               //                </div>
                               //            </td>


                                       }
                                       //html += "</td>";
                               //    </tr>
                               //    <tr>
                               //        <td colspan="@item.Elements.Count()">
                               //            @if (!string.IsNullOrWhiteSpace(item.DescriptionUrl))
                               //            {
                               //                <a title="View in app" href="@item.DescriptionUrl" target="_blank">@item.Description</a>
                               //            }
                               //            else
                               //            {
                               //                @item.Description
                               //            }
                               //        </td>

                               //    </tr>
                               //</table>
                           }
                           html += "</tr>";

                       }

            html += "</table>";
            return html;


        }
        
        [HttpPost]
        public string OkToUpdate(long RentalId, string Checked)
        {
            try
            {
                var rental = db.SplitBillings.First(r => r.RentalId == RentalId);
                rental.OverrideWizardLock = bool.Parse(Checked);
                db.Entry(rental).State = EntityState.Modified;
                db.SaveChanges();

                return "Success";
            }
            catch
            {
                return "Error";
            }
        }

        [HttpPost]
        public string UpdateTimestamp(long RentalId, string Timestamp)
        {
            try
            {
                var rental = db.SplitBillings.First(r => r.RentalId == RentalId);
                rental.Timestamp = DateTime.Parse(Timestamp);
                rental.SentToWizard = false;
                db.Entry(rental).State = EntityState.Modified;
                db.SaveChanges();

                return "Success";
            }
            catch
            {
                return "Error";
            }
        }

        [HttpPost]
        public JsonResult GetAVAddress(string AccountNumber)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SBAIR3Context"].ConnectionString);

            try
            {
                //call stored procedure
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                DataSet dta = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = comm;
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "[dbo].[sp_GetAVAccountAddress]";
                comm.Parameters.Add(new SqlParameter("@AccountNumber", AccountNumber));
                conn.Open();
                adapter.Fill(dta);
                conn.Close();

                if (dta.Tables[0].Rows.Count > 0)
                {
                    var row = dta.Tables[0].Rows[0];
                    return Json(new { Success = "true", Billing_Name = row["Billing_Name"], Billing_Address1 = row["Billing_Address_1"], Billing_Address2 = row["Billing_Address_2"], Billing_Address3 = row["Billing_Address_3"] });
                }
                else 
                {
                    return Json(new { Success = "noupdate" });
                }
            }
            catch
            {
                return Json(new { Success = "false" });
            }
            finally
            {
                conn.Close();
            }

        }

        private List<string> PublicInstancePropertiesEqual<T>(T self, T to, params string[] ignore) where T : class
        {

            List<string> PropertiesDifferent = new List<string>();

            if (self != null && to != null)
            {
                Type type = typeof(T);
                List<string> ignoreList = new List<string>(ignore);
                foreach (PropertyInfo pi in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    if (!ignoreList.Contains(pi.Name))
                    {
                        object selfValue = type.GetProperty(pi.Name).GetValue(self, null);
                        object toValue = type.GetProperty(pi.Name).GetValue(to, null);
                        if (selfValue != null)
                        {
                            if (!selfValue.Equals(toValue))
                                PropertiesDifferent.Add(pi.Name);
                        }
                        else if (toValue != null)
                            PropertiesDifferent.Add(pi.Name);
                    }
                }
            }
            return PropertiesDifferent;
        }
    }

}