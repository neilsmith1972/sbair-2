﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace avis.sbair.Controllers
{
    public class InvoiceController : Controller
    {
        //
        // GET: /Invoice/

        SBAIR3Context db = new SBAIR3Context();

        ViewResult Main(Int64 invoiceid)
        {

            var invoice = db.Invoices.FirstOrDefault(i => i.InvoiceId == invoiceid);
            ViewBag.Type = "Customer";
            if (invoice.CustomerType == "I") ViewBag.Type = "Insurance";
            if(invoice.InvoiceType == "Credit") ViewBag.Credit = "true";
            var ra = db.SplitBillings.FirstOrDefault(sb => sb.RentalId == invoice.RentalId).RentalAgreementNumber;

            var allRecs = db.SplitBillings.Where(r => r.RentalAgreementNumber == ra).OrderByDescending(o => o.Timestamp).ToList();
            Entities.SplitBilling currentRec = null;
            Entities.SplitBilling previousRec = null;

            if (invoice.InvoiceType == "Correction" || invoice.InvoiceType == "Credit" || invoice.InvoiceType == "FC" || invoice.InvoiceType == "DOC")
            {
                var takeNext = false;
                foreach(var r in allRecs)
                {
                    if(takeNext) { previousRec = r; break; }
                    if(r.RentalId == invoice.RentalId) { currentRec = r; takeNext = true; }
                }

                if (invoice.InvoiceType == "Credit" || invoice.InvoiceType == "FC") currentRec.RentalDamageNumber = previousRec.RentalDamageNumber;

            }
            else
            {
                currentRec = db.SplitBillings.Where(s => s.RentalId == invoice.RentalId).FirstOrDefault();
            }

            if (invoice.InvoiceType == "FC") { currentRec = previousRec; } //invoice.InvoiceType = "Credit"; }

            var bill = currentRec;
            if (bill.PriceSplit == -1) ViewBag.Void = true;
            ViewBag.VATCustomerPays = bill.VATCustomerPays;
            ViewBag.VATInsurancePays = bill.VATTotal - bill.VATCustomerPays;

            Classes.Calculations calc = new Classes.Calculations();

            if (bill.SuperCDWCustomerPays > 0 && bill.SuperCDWTotal == 0) { bill.SuperCDWTotal = 0; }//bill.SuperCDWCustomerPays; }
            if (bill.RAICustomerPays > 0 && bill.RAITotal == 0) { bill.RAITotal = 0;} // bill.RAICustomerPays; }
            if (bill.SuperTPCustomerPays > 0 && bill.SuperTPTotal == 0) { bill.SuperTPTotal = 0;} // bill.SuperTPCustomerPays; }

            if (bill.KMCheckout == null)
            {
                if (bill.KMCheckin > bill.KMDriven) { bill.KMCheckout = bill.KMDriven; bill.KMDriven = bill.KMCheckin - bill.KMCheckout; }
            }
            ViewBag.CheckOutMileage = bill.KMCheckout;
            bill.KMDriven = bill.KMCheckin - bill.KMCheckout;

            calc.Calculate(bill);

            ViewBag.hasPrevious = false;

            if(previousRec != null)
            {
                var previousCalc = new Classes.Calculations();
                if(previousRec.SuperCDWCustomerPays > 0 && previousRec.SuperCDWTotal == 0) { previousRec.SuperCDWTotal = previousRec.SuperCDWCustomerPays; }
                if(previousRec.RAICustomerPays > 0 && previousRec.RAITotal == 0) { previousRec.RAITotal = previousRec.RAICustomerPays; }
                if (previousRec.KMCheckout == null || previousRec.KMCheckout == 0)
                {
                    //if (previousRec.KMCheckin > previousRec.KMDriven) { 
                        previousRec.KMCheckout = previousRec.KMDriven; previousRec.KMDriven = previousRec.KMCheckin - previousRec.KMCheckout; 
                    //}
                }
                previousRec.KMDriven = previousRec.KMCheckin - previousRec.KMCheckout;
                previousCalc.Calculate(previousRec);
                if(previousRec.SuperTPTotal == null) previousRec.SuperTPTotal = 0;
                if(previousRec.SuperTPCustomerPays == null) previousRec.SuperTPCustomerPays = 0;
                if(previousRec.RentalALIPerDayAmount == null) previousRec.RentalALIPerDayAmount = 0;
                if(previousRec.RentalRenterInvoiceFeeAmount == null) previousRec.RentalRenterInvoiceFeeAmount = 0;

                ViewBag.C_Total_Prev = Decimal.Round(previousCalc.C_TimeAmt + previousCalc.C_SuperCDWAmt + previousCalc.C_SuperTPAmt + previousCalc.C_RAIAmt + previousCalc.C_VRFAmt + previousCalc.C_SplittedVATAmt + previousCalc.C_AirportFeeAmt + previousCalc.C_RefuelingAmt + previousCalc.C_DeliveryFeeAmt + previousCalc.C_ReturnFeeAmt + previousCalc.C_OneWayFeeAmt + previousCalc.C_MiscAmt + previousCalc.C_OthersAmt + (decimal)previousRec.RentalRenterInvoiceFeeAmount - previousRec.AlreadyPaidByCustomerAmount, 2);
                ViewBag.I_Total_Prev = Decimal.Round(previousCalc.I_TimeAmt + previousCalc.I_SuperCDWAmt + previousCalc.I_SuperTPAmt + previousCalc.I_RAIAmt + previousCalc.I_VRFAmt + previousCalc.I_SplittedVATAmt + previousCalc.I_AirportFeeAmt + previousCalc.I_RefuelingAmt + previousCalc.I_DeliveryFeeAmt + previousCalc.I_ReturnFeeAmt + previousCalc.I_OneWayFeeAmt + previousCalc.I_MiscAmt + previousCalc.I_OthersAmt - previousRec.AlreadyPaidByInsuranceAmount, 2);
                ViewBag.previousRec = previousRec;
                ViewBag.previousCalc = previousCalc;
                ViewBag.hasPrevious = true;
                ViewBag.previousDays = Classes.Calculations.CalculateNbDays(previousRec.RentalCheckoutDateTime.Value, previousRec.RentalCheckinDateTime.Value);
                // Remove 'ther' fee
                //ViewBag.C_TaxableAmt_Prev = previousCalc.C_TimeAmt + previousRec.AirportFeeCustomerPays + previousRec.DeliveryCustomerPays + previousRec.InvoiceFeeCustomerPays + previousRec.MiscCostsCustomerPays + previousRec.OneWayCustomerPays + previousRec.OtherFeeCustomerPays + previousRec.RefuellingCustomerPays + previousRec.ReturnCustomerPays + previousRec.VRFCustomerPays;
                //ViewBag.I_TaxableAmt_Prev = previousCalc.I_TimeAmt - previousRec.PriceSplitCustomerPays + previousRec.AirportFeeTotal - previousRec.AirportFeeCustomerPays+ previousRec.DeliveryTotal - previousRec.DeliveryCustomerPays+ previousRec.InvoiceFeeTotal - previousRec.InvoiceFeeCustomerPays+ previousRec.MiscCostsTotal - previousRec.MiscCostsCustomerPays+ previousRec.OneWayTotal - previousRec.OneWayCustomerPays+ previousRec.OtherFeeTotal - previousRec.OtherFeeCustomerPays+ previousRec.RefuellingTotal - previousRec.RefuellingCustomerPays+ previousRec.ReturnTotal - previousRec.ReturnCustomerPays+ previousRec.SuperCDWTotal - previousRec.SuperCDWCustomerPays+ previousRec.SuperTPTotal - previousRec.SuperTPCustomerPays+ previousRec.RAITotal - previousRec.RAICustomerPays;
                ViewBag.C_TaxableAmt_Prev = previousCalc.C_TimeAmt + previousRec.AirportFeeCustomerPays + previousRec.DeliveryCustomerPays + previousRec.InvoiceFeeCustomerPays + previousRec.MiscCostsCustomerPays + previousRec.OneWayCustomerPays + previousRec.RefuellingCustomerPays + previousRec.ReturnCustomerPays + previousRec.VRFCustomerPays;
                ViewBag.I_TaxableAmt_Prev = previousCalc.I_TimeAmt - previousRec.PriceSplitCustomerPays + previousRec.AirportFeeTotal - previousRec.AirportFeeCustomerPays + previousRec.DeliveryTotal - previousRec.DeliveryCustomerPays + previousRec.InvoiceFeeTotal - previousRec.InvoiceFeeCustomerPays + previousRec.MiscCostsTotal - previousRec.MiscCostsCustomerPays + previousRec.OneWayTotal - previousRec.OneWayCustomerPays + previousRec.RefuellingTotal - previousRec.RefuellingCustomerPays + previousRec.ReturnTotal - previousRec.ReturnCustomerPays + previousRec.SuperCDWTotal - previousRec.SuperCDWCustomerPays + previousRec.SuperTPTotal - previousRec.SuperTPCustomerPays + previousRec.RAITotal - previousRec.RAICustomerPays;

            }
            else
            {
                ViewBag.C_Total_Prev = 0;
                ViewBag.I_Total_Prev = 0;
                ViewBag.previousRec = new Entities.SplitBilling();
            }

            Entities.Invoice cr = invoice; //db.Invoices.Where(r => r.RentalId == rentalId).FirstOrDefault();

            if (cr != null)
            {

                bill.RentalInsuranceBillingName = cr.BillingName;
                bill.RentalInsuranceBillingCompany = cr.BillingCompany;
                bill.RentalInsuranceBillingAddress1 = cr.BillingAddress1;
                bill.RentalInsuranceBillingAddress2 = cr.BillingAddress2;
                bill.RentalInsuranceBillingAddress3 = cr.BillingAddress3;

            }

            if (bill.SuperTPTotal == null) bill.SuperTPTotal = 0;
            if (bill.SuperTPCustomerPays == null) bill.SuperTPCustomerPays = 0;
            if (bill.RentalALIPerDayAmount == null) bill.RentalALIPerDayAmount = 0;

            ViewBag.Days = Classes.Calculations.CalculateNbDays(bill.RentalCheckoutDateTime.Value, bill.RentalCheckinDateTime.Value);
            ViewBag.Calc = calc;

            var desc = db.PriceSplits.Where(v => v.ID == bill.PriceSplit).FirstOrDefault().Description;
            if (bill.PriceSplit == 6) desc = desc.Replace("SEK X", "SEK " + decimal.Round((decimal)bill.AmountCoveredByInsurance,2));
            if (bill.PriceSplit == 5) desc = desc.Replace("X dagar", bill.DaysCoveredByInsurance + " dagar")
                .Replace("Y km", bill.KmCoveredByInsurance + " km");

            ViewBag.PriceSplit = desc;

            ViewBag.VATSplit = db.VATSplits.Where(v => v.ID == bill.VATSplit).FirstOrDefault().Description;

            // Remove 'other' fee
            //ViewBag.C_TaxableAmt = calc.C_TimeAmt + bill.AirportFeeCustomerPays + bill.DeliveryCustomerPays + bill.InvoiceFeeCustomerPays + bill.MiscCostsCustomerPays + bill.OneWayCustomerPays + bill.OtherFeeCustomerPays + bill.RefuellingCustomerPays + bill.ReturnCustomerPays + bill.VRFCustomerPays;
            //ViewBag.C_Total = bill.VATCustomerPays + bill.PriceSplitCustomerPays + bill.AirportFeeCustomerPays + bill.DeliveryCustomerPays + bill.InvoiceFeeCustomerPays + bill.MiscCostsCustomerPays + bill.OneWayCustomerPays + bill.OtherFeeCustomerPays + bill.RefuellingCustomerPays + bill.ReturnCustomerPays + bill.SuperCDWCustomerPays + bill.SuperTPCustomerPays + bill.RAICustomerPays;
            ViewBag.C_TaxableAmt = calc.C_TimeAmt + bill.AirportFeeCustomerPays + bill.DeliveryCustomerPays + bill.InvoiceFeeCustomerPays + bill.MiscCostsCustomerPays + bill.OneWayCustomerPays + bill.RefuellingCustomerPays + bill.ReturnCustomerPays + bill.VRFCustomerPays;
            ViewBag.C_Total = bill.VATCustomerPays + bill.PriceSplitCustomerPays + bill.AirportFeeCustomerPays + bill.DeliveryCustomerPays + bill.InvoiceFeeCustomerPays + bill.MiscCostsCustomerPays + bill.OneWayCustomerPays + bill.RefuellingCustomerPays + bill.ReturnCustomerPays + bill.SuperCDWCustomerPays + bill.SuperTPCustomerPays + bill.RAICustomerPays;
            //

            ViewBag.I_TaxableAmt = calc.I_TimeAmt + 
                + bill.AirportFeeTotal - bill.AirportFeeCustomerPays
                + bill.DeliveryTotal - bill.DeliveryCustomerPays
                + bill.InvoiceFeeTotal - bill.InvoiceFeeCustomerPays
                + bill.MiscCostsTotal - bill.MiscCostsCustomerPays
                + bill.OneWayTotal - bill.OneWayCustomerPays
                //+ bill.OtherFeeTotal - bill.OtherFeeCustomerPays
                + bill.RefuellingTotal - bill.RefuellingCustomerPays
                + bill.ReturnTotal - bill.ReturnCustomerPays
                + bill.SuperCDWTotal - bill.SuperCDWCustomerPays
                + bill.SuperTPTotal - bill.SuperTPCustomerPays
                + bill.RAITotal - bill.RAICustomerPays;


            ViewBag.I_Total = bill.VATTotal - bill.VATCustomerPays
                + bill.PriceSplitTotal - bill.PriceSplitCustomerPays
                + bill.AirportFeeTotal - bill.AirportFeeCustomerPays
                + bill.DeliveryTotal - bill.DeliveryCustomerPays
                + bill.InvoiceFeeTotal - bill.InvoiceFeeCustomerPays
                + bill.MiscCostsTotal - bill.MiscCostsCustomerPays
                + bill.OneWayTotal - bill.OneWayCustomerPays
                //+ bill.OtherFeeTotal - bill.OtherFeeCustomerPays
                + bill.RefuellingTotal - bill.RefuellingCustomerPays
                + bill.ReturnTotal - bill.ReturnCustomerPays
                + bill.SuperCDWTotal - bill.SuperCDWCustomerPays
                + bill.SuperTPTotal - bill.SuperTPCustomerPays
                + bill.RAITotal - bill.RAICustomerPays;

            ViewBag.RentalRenterInvoiceNumber = cr.InvoiceNumber;
            ViewBag.RentalInsuranceInvoiceNumber = cr.InvoiceNumber;
            ViewBag.RentalRenterInvoiceFeeAmount = bill.InvoiceFeeCustomerPays;
            ViewBag.InvoiceDate = cr.InvoiceDate.ToShortDateString().Replace(".", "/");

            ViewBag.RenterDueDate = cr.DueDate.ToShortDateString().Replace(".", "/");
            ViewBag.InsuranceDueDate = cr.DueDate.ToShortDateString().Replace(".", "/");

            ViewBag.RentalRenterBillingName = cr.BillingName;
            ViewBag.RentalInsuranceBillingName = cr.BillingName;
            ViewBag.RentalCompanyName = cr.BillingCompany;

            ViewBag.RentalRenterAddress1Text = cr.BillingAddress1;
            ViewBag.RentalRenterAddress2Text = cr.BillingAddress2;
            ViewBag.RentalRenterAddressCityText = cr.BillingAddress3;
            ViewBag.RentalInsuranceBillingCompany = cr.BillingCompany;
            ViewBag.RentalInsuranceBillingAddress1 = cr.BillingAddress1;
            ViewBag.RentalInsuranceBillingAddress2 = cr.BillingAddress2;
            ViewBag.RentalInsuranceBillingAddress3 = cr.BillingAddress3;
            //ViewBag.CheckInMileage = bill.KMCheckout + bill.KMDriven; // cr.CheckInMileage;
            //ViewBag.CheckOutMileage = bill.KMCheckout;

            ViewBag.KMDriven = bill.KMDriven; //cr.CheckInMileage - cr.CheckOutMileage;
            if (bill.PriceSplit == 5)
            {
                ViewBag.I_KM = bill.KmCoveredByInsurance;
                ViewBag.C_KM = bill.KMDriven - bill.KmCoveredByInsurance;

                ViewBag.I_Days = bill.DaysCoveredByInsurance;
                ViewBag.C_Days = ViewBag.Days - bill.DaysCoveredByInsurance;
            }

            if (ViewBag.KMDriven == null) ViewBag.KMDrive = bill.KMCheckin - bill.KMCheckout;

            ViewBag.AvisCompanyName = cr.AvisCompanyName;
            ViewBag.AvisCompanyAddress1 = cr.AvisCompanyAddress1;
            ViewBag.AvisCompanyAddress2 = cr.AvisCompanyAddress2;
            ViewBag.AvisCompanyAddress3 = cr.AvisCompanyAddress3;
            ViewBag.VatRegNumber = cr.VatRegNumber;
            ViewBag.RentalVehicleLicensePlateNumber = bill.RentalVehicleLicensePlateNumber;
            ViewBag.RentalMiscChargeAmount = bill.RentalMiscChargeAmount;
            ViewBag.RentalCDWPerDayAmount = bill.RentalCDWPerDayAmount;
            ViewBag.RentalALIPerDayAmount = bill.RentalALIPerDayAmount;
            ViewBag.RentalPAIPerDayAmount = bill.RentalPAIPerDayAmount;

            ViewBag.RentalRenterCreditClubCode = bill.RentalRenterCreditClubCode + bill.RentalRenterCreditCardNumber;
            ViewBag.RentalInsuranceCreditClubCode = bill.RentalInsuranceCreditClubCode + bill.RentalInsuranceCreditCardNumber;

            if (bill.MiscCode != null)
            {
                var mc = db.MiscCodes.FirstOrDefault(m => m.MiscCodeLetter == bill.MiscCode);
                if(mc != null) ViewBag.MiscDescription = mc.MiscCodeDescription.Substring(3, mc.MiscCodeDescription.Length-3);
            }


            if (invoice.InvoiceType == "NI" || invoice.InvoiceType == "FC") ViewBag.hasPrevious = false;

            ViewBag.StationPhone = GetStationTelephoneNumber(currentRec.RentalCheckoutLocationText).telephoneNumber;

            //if (bill.PriceSplit == 6) return View("MainSplit6", bill);

            return View("Main", bill);

        }

        public ActionResult Customer(Int64 id)
        {
            ViewBag.Type = "Customer";
            return Main(id);
        }

        public ActionResult Insurance(Int64 id)
        {
            ViewBag.Type = "Insurance";
            return Main(id);
        }



        public ActionResult Generate(Int64 id)
        {

            var i = db.Invoices.Where(x => x.InvoiceId == id).FirstOrDefault();
            var s = db.SplitBillings.Where(y => y.RentalId == i.RentalId).FirstOrDefault();

            switch(i.CustomerType)
            {
                case "C":
                    WebRequest request = WebRequest.Create(Request.Url.OriginalString.ToLower().Replace("generate","customer")); 
                    WebResponse response = request.GetResponse(); 
                    Stream data = response.GetResponseStream(); 
                    string html = String.Empty; 
                    using (StreamReader sr = new StreamReader(data)) 
                    { 
                        html = sr.ReadToEnd(); 
                    }

                    var filename = "customer_Invoice_" + s.RentalAgreementNumber + "_" + i.InvoiceNumber + ".pdf";
                    var ctr = 0;
                    while (db.Invoices.Any(iv => iv.InvoiceFileName == filename))
                    {
                        ctr += 1;
                        filename = "customer_Invoice_" + s.RentalAgreementNumber + "_" + i.InvoiceNumber + "_" + ctr + ".pdf";
                    }

                    string cus = Classes.InvoiceGen.CreatePdfFile(ConfigurationManager.AppSettings["InvoiceStore"] + filename, html);
                    try
                    {
                        System.IO.File.Copy(ConfigurationManager.AppSettings["InvoiceStore"] + cus, ConfigurationManager.AppSettings["InvoiceStoreLocal"] + cus, true);
                    }
                    catch { }
                    return Content(cus);// Json(new { filename = cus }, JsonRequestBehavior.AllowGet); //RedirectToAction("edit", "listing", new { @id = id, @type="closed" });

                    

                case "I":
                    WebRequest request1 = WebRequest.Create(Request.Url.OriginalString.ToLower().Replace("generate", "insurance"));
                    WebResponse response1 = request1.GetResponse();
                    Stream data1 = response1.GetResponseStream();
                    string html1 = String.Empty;
                    using (StreamReader sr = new StreamReader(data1))
                    {
                        html1 = sr.ReadToEnd();
                    }
                    var ifilename = "insurance_Invoice_" + s.RentalAgreementNumber + "_" + i.InvoiceNumber + ".pdf";
                    var ictr = 0;
                    while (db.Invoices.Any(iv => iv.InvoiceFileName == ifilename))
                    {
                        ictr+=1;
                        ifilename = "insurance_Invoice_" + s.RentalAgreementNumber + "_" + i.InvoiceNumber + "_" + ictr + ".pdf";
                    }
                    string ins = Classes.InvoiceGen.CreatePdfFile(ConfigurationManager.AppSettings["InvoiceStore"] + ifilename, html1);
                    try
                    {
                        System.IO.File.Copy(ConfigurationManager.AppSettings["InvoiceStore"] + ins, ConfigurationManager.AppSettings["InvoiceStoreLocal"] + ins, true);
                    }
                    catch { }

                    return Content(ins);// Json(new { filename = ins }, JsonRequestBehavior.AllowGet); //RedirectToAction("edit", "listing", new { @id = id, @type="closed" });

                    
            }

            return Json("no match");
        }

        public FileResult Invoice(string path, string invoicenumber)
        {
            return File(@path, "application/pdf", invoicenumber + ".pdf");
        }

        public StationDetail GetStationTelephoneNumber(string stationName)
        {
            var detail = new StationDetail();
            var conn = new SqlConnection(ConfigurationManager.AppSettings["LDB"]);
            var command = new SqlCommand("select * from LDB where (LDB_STATION_NAME = '" + stationName + "')", conn);
            var data = new DataSet();
            var adapter = new SqlDataAdapter();
            command.CommandType = CommandType.Text;
            adapter.SelectCommand = command;
            if (conn.State != ConnectionState.Open) conn.Open();
            adapter.Fill(data);
            conn.Close();

            if (data.Tables[0].Rows.Count > 0)
            {
                DataRow row = data.Tables[0].Rows[0];
                detail.licensee = row["LDB_CORP_LICENSEE_IND"].ToString() == "L" ? true : false;
                detail.telephoneNumber = row["LDB_TELEPHONE_NUMBER"].ToString();
            }
            

            return detail; 
        }
    }

    public class StationDetail 
    {
        public string telephoneNumber { get; set; }
        public bool licensee { get; set; }
    }

}
