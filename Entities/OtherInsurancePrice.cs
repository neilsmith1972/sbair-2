using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class OtherInsurancePrice
	{
		public int ID { get; set; }
		public string CarGroup { get; set; }
		public decimal Price { get; set; }
		public Nullable<int> OtherInsurancePriceVersion_ID { get; set; }
	}
}

