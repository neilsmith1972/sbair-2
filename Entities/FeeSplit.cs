using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class FeeSplit
	{
		public int ID { get; set; }
		public string Description { get; set; }
		public decimal PCentCustomerPart { get; set; }
		public Nullable<int> CountryCode_ID { get; set; }
		public Nullable<int> FeeType_ID { get; set; }
	}
}

