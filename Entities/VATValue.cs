using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class VATValue
	{
		public int ID { get; set; }
		public string FeeType { get; set; }
		public System.DateTime FromDate { get; set; }
		public System.DateTime ToDate { get; set; }
		public decimal pcent { get; set; }
		public string Comment { get; set; }
		public Nullable<int> CountryCode_ID { get; set; }
	}
}

