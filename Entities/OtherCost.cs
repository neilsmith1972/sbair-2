using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class OtherCost
	{
		public int ID { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
	}
}

