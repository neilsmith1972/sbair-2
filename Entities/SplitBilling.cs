using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
    public class SplitBilling : Interfaces.IRental
	{
		public long RentalId { get; set; }
		public System.DateTime Timestamp { get; set; }
		public Nullable<int> Source { get; set; }
		public string RentalAgreementNumber { get; set; }
		public Nullable<int> RentalCheckInYear { get; set; }
		public string RentalTransactionType { get; set; }
		public string RentalReservationNumber { get; set; }
		public Nullable<System.DateTime> RentalCheckoutDateTime { get; set; }
		public string RentalCheckoutLocationCode { get; set; }
		public string RentalCheckoutLocationText { get; set; }
		public string RentalCheckoutCorporateLicenseeIndicator { get; set; }
		public Nullable<System.DateTime> RentalCheckinDateTime { get; set; }
		public string RentalCheckinLocationCode { get; set; }
		public string RentalCheckinLocationText { get; set; }
		public string RentalCheckinCorporateLicenseeIndicator { get; set; }
		public string RentalRateCode { get; set; }
		public string RentalIATANumber { get; set; }
		public string RentalAWDNumber { get; set; }
		public string RentalACTONumber { get; set; }
		public string RentalWizardOfAvisNumber { get; set; }
		public string RentalDamageNumber { get; set; }
		public string RentalRemarks { get; set; }
		public string RentalLocalContactText { get; set; }
		public string RentalCustomerName { get; set; }
		public string RentalCompanyName { get; set; }
		public string RentalRenterBillingName { get; set; }
		public string RentalRenterAddress1Text { get; set; }
		public string RentalRenterAddress2Text { get; set; }
		public string RentalRenterAddressCityText { get; set; }
		public string RentalRenterBillingCountryCode { get; set; }
		public string RentalRenterCreditClubCode { get; set; }
		public string RentalRenterCreditCardNumber { get; set; }
		public Nullable<int> RentalRenterInvoiceNumber { get; set; }
		public Nullable<decimal> RentalRenterTotalAmount { get; set; }
		public string RentalInsuranceBillingCompany { get; set; }
		public string RentalInsuranceBillingName { get; set; }
		public string RentalInsuranceBillingAddress1 { get; set; }
		public string RentalInsuranceBillingAddress2 { get; set; }
		public string RentalInsuranceBillingAddress3 { get; set; }
		public string RentalInsuranceBillingCountryCode { get; set; }
		public string RentalInsuranceCreditClubCode { get; set; }
		public string RentalInsuranceCreditCardNumber { get; set; }
		public Nullable<int> RentalInsuranceInvoiceNumber { get; set; }
		public Nullable<decimal> RentalInsuranceTotalAmount { get; set; }
		public string RentalVehicleLicensePlateNumber { get; set; }
		public string RentalVehicleDescription { get; set; }
		public string RentalOwningCorpLicIndicator { get; set; }
		public string RentalVehicleClassChargedCode { get; set; }
		public string RentalConfirmedVehicleClassCode { get; set; }
		public string RentalPreferredVehicleClassCode { get; set; }
		public Nullable<long> KMDriven { get; set; }
        public Nullable<long> KMCheckin { get; set; }
		public Nullable<decimal> RentalTotalRentalChargesAmount { get; set; }
		public Nullable<decimal> NetTimeDistanceAmount { get; set; }
		public Nullable<decimal> RentalOneWayFeeAmount { get; set; }
		public Nullable<decimal> RentalSurchargeAmount { get; set; }
		public Nullable<decimal> RentalDeliveryChargeAmount { get; set; }
		public Nullable<decimal> RentalCollectionChargeAmount { get; set; }
		public Nullable<decimal> RentalGasChargesAmount { get; set; }
		public Nullable<decimal> RentalMiscChargeAmount { get; set; }
		public Nullable<decimal> RentalCDWAmount { get; set; }
        public Nullable<decimal> RentalTPAmount { get; set; }
		public Nullable<decimal> RentalPAIAmount { get; set; }
		public Nullable<decimal> RentalTPIAmount { get; set; }
		public Nullable<decimal> RentalContractFeeAmount { get; set; }
		public string RentalMiscChargeCode { get; set; }
		public Nullable<decimal> RentalTaxNumber { get; set; }
		public Nullable<long> RentalMilesChargedCount { get; set; }
		public Nullable<int> RentalFreeMileageCount { get; set; }
		public Nullable<decimal> RentalMileageRateAmount { get; set; }
		public Nullable<decimal> RentalHourlyRateAmount { get; set; }
		public Nullable<decimal> RentalDailyRateAmount { get; set; }
		public Nullable<decimal> RentalWeeklyRateAmount { get; set; }
		public Nullable<decimal> RentalMonthlyRateAmount { get; set; }
		public int RentalAdditionalDaysAmount { get; set; }
		public int RentalAdditionalHoursAmount { get; set; }
		public int RentalCDWLiabilityAmount { get; set; }
		public Nullable<decimal> RentalCDWPerDayAmount { get; set; }
		public Nullable<decimal> RentalPAIPerDayAmount { get; set; }
		public Nullable<decimal> RentalALIPerDayAmount { get; set; }
		public Nullable<decimal> RentalPEPPerDayAmount { get; set; }
		public int RentalFreeDaysNumber { get; set; }
		public int RentalNetRateCDWAmount { get; set; }
		public int RentalRNetRateCDWAmount { get; set; }
		public int RentalNetRatePAIAmount { get; set; }
		public int RentalRNetRatePAIAmount { get; set; }
		public int RentalNetRatePEPAmount { get; set; }
		public int RentalRNetRatePEPAmount { get; set; }
		public int RentalNetRateALIAmount { get; set; }
		public int RentalRNetRateALIAmount { get; set; }
		public int RentALInclusiveNetRateInd { get; set; }
		public int RentalGasCreditAmount { get; set; }
		public Nullable<int> Country { get; set; }
		public Nullable<int> PaymentMethod { get; set; }
		public Nullable<int> Status { get; set; }
		public Nullable<int> PriceSplit { get; set; }
		public Nullable<int> VATSplit { get; set; }
		public Nullable<int> CDWSplit { get; set; }
        public Nullable<int> SuperTPSplit { get; set; }
		public Nullable<int> RAISplit { get; set; }
		public Nullable<int> AirportFeeSplit { get; set; }
		public Nullable<int> ReturnFeeSplit { get; set; }
		public Nullable<int> DeliveryFeeSplit { get; set; }
		public Nullable<int> RefuelingSplit { get; set; }
		public Nullable<int> OtherFeeSplit { get; set; }
		public Nullable<int> VRFSplit { get; set; }
		public Nullable<int> OneWayFeeSplit { get; set; }
		public Nullable<int> MiscCostsSplit { get; set; }
        public string MiscCostRemarks { get; set; }
		public Nullable<int> InvoiceFeeSplit { get; set; }
		public decimal PriceSplitTotal { get; set; }
		public decimal PriceSplitCustomerPays { get; set; }
		public decimal VATTotal { get; set; }
		public decimal VATCustomerPays { get; set; }
		public decimal SuperCDWTotal { get; set; }
		public decimal SuperCDWCustomerPays { get; set; }
        public Nullable<decimal> SuperTPTotal { get; set; }
        public Nullable<decimal> SuperTPCustomerPays { get; set; }
		public decimal RAITotal { get; set; }
		public decimal RAICustomerPays { get; set; }
		public decimal AirportFeeTotal { get; set; }
		public decimal AirportFeeCustomerPays { get; set; }
		public decimal OneWayTotal { get; set; }
		public decimal OneWayCustomerPays { get; set; }
		public decimal RefuellingTotal { get; set; }
		public decimal RefuellingCustomerPays { get; set; }
		public decimal DeliveryTotal { get; set; }
		public decimal DeliveryCustomerPays { get; set; }
		public decimal ReturnTotal { get; set; }
		public decimal ReturnCustomerPays { get; set; }
		public decimal VRFTotal { get; set; }
		public decimal VRFCustomerPays { get; set; }
		public decimal InvoiceFeeTotal { get; set; }
		public decimal InvoiceFeeCustomerPays { get; set; }
		public decimal MiscCostsTotal { get; set; }
		public decimal MiscCostsCustomerPays { get; set; }
		public decimal OtherFeeTotal { get; set; }
		public decimal OtherFeeCustomerPays { get; set; }
		public Nullable<int> KmCoveredByInsurance { get; set; }
		public Nullable<int> DaysCoveredByInsurance { get; set; }
        public decimal AlreadyPaidByCustomerAmount { get; set; }
        public decimal AlreadyPaidByInsuranceAmount { get; set; }
        public decimal InvoiceAmountInsurance { get; set; }
        public decimal InvoiceAmountCustomer { get; set; }
		public string Remarks { get; set; }
		public string InsuranceCompanyContact { get; set; }

        public decimal RentalVATAmount;
        public decimal RentalInvoiceFeeAmount;
        public decimal RentalOtherCosts;
        public int NumKMDriven;
        public int NumDaysRental;
        public Nullable<decimal> RentalRenterInvoiceFeeAmount { get; set; }

        public Nullable<decimal> AmountCoveredByInsurance { get; set; }
        public string MVANumber { get; set; }

        public DateTime? AmendedDate { get; set; }
        public bool? NewInvoiceRequired { get; set; }

        public DateTime? DateArchived { get; set; }
        public bool? IsArchive { get; set; }

        public decimal? OverrideDaysAmount { get; set; }
        public decimal? OverrideKMAmount { get; set; }

        public Int64? KMCheckout { get; set; }

        public bool? SentToWizard {get; set;}
        public bool? WizardSuccess { get; set; }
        public bool? VoucherValueUpdate { get; set; }
        public bool? TotalUpdate { get; set; }

        public bool? CCIUpdated { get; set; }
        public bool? CustomerAddressUpdated { get; set; }

        public string MiscCode { get; set; }
        public Int64? ParentRentalId { get; set; }
        public string AVISComments { get; set; }
        public string OnTheRoadCode { get; set; }
        public bool? ManualInvoice { get; set; }

        public string UpdatedBy { get; set; }

        public bool? Void { get; set; }
        public bool? OverrideWizardLock { get; set; }

        //public decimal? TMDiscount { get; set; }

        public decimal? MiscCostsInsurancePays { get; set; }

        public string Output_Type_I { get; set; }
        public string Output_Type_C { get; set; }
    }
}

