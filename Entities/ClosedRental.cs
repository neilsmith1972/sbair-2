using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class xClosedRental : Interfaces.IRental
	{
		public long RentalId { get; set; }
		public System.DateTime Timestamp { get; set; }
		public Nullable<int> Source { get; set; }
		public string RentalAgreementNumber { get; set; }
		public Nullable<int> RentalCheckInYear { get; set; }
		public string RentalTransactionType { get; set; }
		public string RentalReservationNumber { get; set; }
		public Nullable<System.DateTime> RentalCheckoutDateTime { get; set; }
		public string RentalCheckoutLocationCode { get; set; }
		public string RentalCheckoutLocationText { get; set; }
		public string RentalCheckoutCorporateLicenseeIndicator { get; set; }
		public Nullable<System.DateTime> RentalCheckinDateTime { get; set; }
		public string RentalCheckinLocationCode { get; set; }
		public string RentalCheckinLocationText { get; set; }
		public string RentalCheckinCorporateLicenseeIndicator { get; set; }
		public string RentalRateCode { get; set; }
		public string RentalIATANumber { get; set; }
		public string RentalAWDNumber { get; set; }
		public string RentalACTONumber { get; set; }
		public string RentalWizardOfAvisNumber { get; set; }
		public string RentalDamageNumber { get; set; }
		public string RentalRemarks { get; set; }
		public string RentalLocalContactText { get; set; }
		public string RentalCustomerName { get; set; }
		public string RentalCompanyName { get; set; }
		public string RentalRenterBillingName { get; set; }
		public string RentalRenterAddress1Text { get; set; }
		public string RentalRenterAddress2Text { get; set; }
		public string RentalRenterAddressCityText { get; set; }
		public string RentalRenterBillingCountryCode { get; set; }
		public string RentalRenterCreditClubCode { get; set; }
		public string RentalRenterCreditCardNumber { get; set; }
		public Nullable<int> RentalRenterInvoiceNumber { get; set; }
		public Nullable<decimal> RentalRenterTotalAmount { get; set; }
		public string RentalInsuranceBillingCompany { get; set; }
		public string RentalInsuranceBillingName { get; set; }
		public string RentalInsuranceBillingAddress1 { get; set; }
		public string RentalInsuranceBillingAddress2 { get; set; }
		public string RentalInsuranceBillingAddress3 { get; set; }
		public string RentalInsuranceBillingCountryCode { get; set; }
		public string RentalInsuranceCreditClubCode { get; set; }
		public string RentalInsuranceCreditCardNumber { get; set; }
		public Nullable<int> RentalInsuranceInvoiceNumber { get; set; }
		public Nullable<decimal> RentalInsuranceTotalAmount { get; set; }
		public string RentalVehicleLicensePlateNumber { get; set; }
		public string RentalVehicleDescription { get; set; }
		public string RentalOwningCorpLicIndicator { get; set; }
		public string RentalVehicleClassChargedCode { get; set; }
		public string RentalConfirmedVehicleClassCode { get; set; }
		public string RentalPreferredVehicleClassCode { get; set; }
		public Nullable<long> KMDriven { get; set; }
		public Nullable<decimal> RentalTotalRentalChargesAmount { get; set; }
		public Nullable<decimal> NetTimeDistanceAmount { get; set; }
		public Nullable<decimal> RentalOneWayFeeAmount { get; set; }
		public Nullable<decimal> RentalSurchargeAmount { get; set; }
		public Nullable<decimal> RentalDeliveryChargeAmount { get; set; }
		public Nullable<decimal> RentalCollectionChargeAmount { get; set; }
		public Nullable<decimal> RentalGasChargesAmount { get; set; }
		public Nullable<decimal> RentalMiscChargeAmount { get; set; }
		public Nullable<decimal> RentalCDWAmount { get; set; }
		public Nullable<decimal> RentalPAIAmount { get; set; }
		public Nullable<decimal> RentalTPIAmount { get; set; }
		public Nullable<decimal> RentalContractFeeAmount { get; set; }
		public string RentalMiscChargeCode { get; set; }
		public Nullable<decimal> RentalTaxNumber { get; set; }
		public Nullable<long> RentalMilesChargedCount { get; set; }
		public Nullable<int> RentalFreeMileageCount { get; set; }
		public Nullable<decimal> RentalMileageRateAmount { get; set; }
		public Nullable<decimal> RentalHourlyRateAmount { get; set; }
		public Nullable<decimal> RentalDailyRateAmount { get; set; }
		public Nullable<decimal> RentalWeeklyRateAmount { get; set; }
		public Nullable<decimal> RentalMonthlyRateAmount { get; set; }
		public decimal RentalAdditionalDaysAmount { get; set; }
		public decimal RentalAdditionalHoursAmount { get; set; }
		public decimal RentalCDWLiabilityAmount { get; set; }
		public Nullable<decimal> RentalCDWPerDayAmount { get; set; }
		public Nullable<decimal> RentalPAIPerDayAmount { get; set; }
		public Nullable<decimal> RentalALIPerDayAmount { get; set; }
		public Nullable<decimal> RentalPEPPerDayAmount { get; set; }
		public int RentalFreeDaysNumber { get; set; }
		public decimal RentalNetRateCDWAmount { get; set; }
		public decimal RentalRNetRateCDWAmount { get; set; }
		public decimal RentalNetRatePAIAmount { get; set; }
		public decimal RentalRNetRatePAIAmount { get; set; }
		public decimal RentalNetRatePEPAmount { get; set; }
		public decimal RentalRNetRatePEPAmount { get; set; }
		public decimal RentalNetRateALIAmount { get; set; }
		public decimal RentalRNetRateALIAmount { get; set; }
		public int RentALInclusiveNetRateInd { get; set; }
		public decimal RentalGasCreditAmount { get; set; }
        //public string InsuranceInvoiceNumber { get; set; }
        //public string RenterInvoiceNumber { get; set; }
        public Nullable<decimal> RentalRenterInvoiceFeeAmount { get; set; }


	    public System.DateTime InvoiceDate { get; set; }
	    public System.DateTime RenterDueDate { get; set; }
	    public System.DateTime InsuranceDueDate { get; set; }
	    public long CheckInMileage { get; set; }
        public long CheckOutMileage { get; set; }

        public string AvisCompanyName { get; set; }
	    public string AvisCompanyAddress1 { get; set; }
	    public string AvisCompanyAddress2 { get; set; }
	    public string AvisCompanyAddress3 { get; set; }
        public string VatRegNumber { get; set; }

        public string MVANumber { get; set; }

        public DateTime? AmendedDate { get; set; }
        public bool? NewInvoiceRequired { get; set; }

    }
}

