﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avis.sbair.Entities
{
    public class Invoice
    {
        public Int64 InvoiceId { get; set; }
        public int RentalId { get; set; }
        public DateTime InsertedTimestamp { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime DueDate { get; set; }
        public string InvoiceType { get; set; }
        public string PaymentType { get; set; }
        public string BillingName { get; set; }
        public string BillingCompany { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingAddress3 { get; set; }
        public string BillingCountryCode { get; set; }
        public string AvisCompanyName { get; set; }
        public string AvisCompanyAddress1 { get; set; }
        public string AvisCompanyAddress2 { get; set; }
        public string AvisCompanyAddress3 { get; set; }
        public string VatRegNumber { get; set; }
        public bool Printed { get; set; }
        public DateTime? PrintedTimestamp { get; set; }
        public string CustomerType { get; set; }
        public string InvoiceFileName { get; set; }

    }
}