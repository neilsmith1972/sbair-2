using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class FeePrice
	{
		public int ID { get; set; }
		public string CarGroup { get; set; }
		public decimal Price { get; set; }
		public string Information { get; set; }
		public Nullable<int> FeeVersion_ID { get; set; }
	}
}

