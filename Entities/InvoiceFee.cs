using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class InvoiceFee
	{
		public int ID { get; set; }
		public System.DateTime FromDate { get; set; }
		public System.DateTime ToDate { get; set; }
		public decimal Price { get; set; }
		public Nullable<int> CountryCode_ID { get; set; }
	}
}

