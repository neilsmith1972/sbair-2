using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class FeeType
	{
		public int ID { get; set; }
		public string FeeCode { get; set; }
		public string Name { get; set; }
	}
}

