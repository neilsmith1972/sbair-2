﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avis.sbair.Entities
{
    public class InsuranceCompany
    {
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Country { get; set; }
        public string AwdNumber { get; set; }
        public string RateCode { get; set; }
        public string Account { get; set; }
        public string WizardNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string e_billingindicator { get; set; }
        public string emailaddress { get; set; }

    }
}