﻿using System;
using System.Collections.Generic;


namespace avis.sbair.Entities
{
    public class BaseRental : Interfaces.IRental
    {
        public long Id { get; set; }
        public string RentalAgreementNumber { get; set; }
        public Nullable<System.DateTime> RentalCheckoutDateTime { get; set; }
        public string RentalCheckoutLocationText { get; set; }
        public Nullable<System.DateTime> RentalCheckinDateTime { get; set; }
        public string RentalCheckinLocationText { get; set; }
        public string RentalVehicleClassChargedCode { get; set; }
        public string RentalVehicleLicensePlateNumber { get; set; }
        public string RentalTransactionType { get; set; }

        public string RentalDamageNumber { get; set; }
        public string RentalCustomerName { get; set; }
        public string RentalCompanyName { get; set; }
        public string RentalRenterAddress1Text { get; set; }
        public string RentalRenterAddress2Text { get; set; }
        public string RentalRenterAddressCityText { get; set; }
        public Nullable<decimal> RentalMileageRateAmount { get; set; }
        public Nullable<decimal> RentalHourlyRateAmount { get; set; }
        public Nullable<decimal> RentalDailyRateAmount { get; set; }
        public Nullable<decimal> RentalWeeklyRateAmount { get; set; }
        public Nullable<decimal> RentalMonthlyRateAmount { get; set; }

        public string type { get; set; }
        public string MVANumber { get; set; }

        public string RentalId { get; set; }

        public DateTime TimeStamp { get; set; }

    }
}