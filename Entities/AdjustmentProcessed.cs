﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Entities
{
    public class AdjustmentProcessed
    {
        [Key]
        public int AdjustmentId {get; set;}
	    public int SplitBillingId {get; set;}
	    public DateTime TimeStamp {get; set;}
	    public string RentalAgreementNumber {get; set;}
	    public int RentalCheckinYear {get; set;}
	    public int Processed {get; set;}
        public int OK { get; set; }
    }
}