using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class audit
	{
		public int ID { get; set; }
		public System.DateTime TimeChanged { get; set; }
		public string Comment { get; set; }
		public string RentalNumber { get; set; }
		public Nullable<int> Status_ID { get; set; }
	}
}

