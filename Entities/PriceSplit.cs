using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class PriceSplit
	{
		public int ID { get; set; }
		public string Description { get; set; }
		public string PrintingDescription { get; set; }
		public decimal PCentCustomerPartTime { get; set; }
		public decimal PCentCustomerPartMileage { get; set; }
		public decimal PercentageCustomerPays { get; set; }
		public bool MileageByUnit { get; set; }
		public bool TimeByUnit { get; set; }
		public bool InsurancePayAFixedAmount { get; set; }
		public bool FixedAmountIsVATIncluded { get; set; }
	}
}

