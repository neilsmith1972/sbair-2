using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class InvoicedRental
	{
		public int InvoicedRentalId { get; set; }
		public int Source { get; set; }
		public string RateCode { get; set; }
		public string AvisRANumber { get; set; }
		public string RANumber { get; set; }
		public string RANumberPrefix { get; set; }
		public Nullable<int> CheckInYear { get; set; }
		public string SplitBillingInd { get; set; }
		public Nullable<int> AgressoInvoiceNo { get; set; }
		public Nullable<decimal> TotalAmount { get; set; }
		public System.DateTime InsertedTimestamp { get; set; }
		public int Processed { get; set; }
		public int OK { get; set; }
        public string MVANumber { get; set; }

	}
}

