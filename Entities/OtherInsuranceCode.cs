using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class OtherInsuranceCode
	{
		public int ID { get; set; }
		public string Insurance_Code { get; set; }
		public string Name { get; set; }
	}
}

