using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class FeeVersion
	{
		public int ID { get; set; }
		public System.DateTime FromDate { get; set; }
		public Nullable<System.DateTime> ToDate { get; set; }
		public Nullable<int> CountryCode_ID { get; set; }
		public Nullable<int> FeeCode_ID { get; set; }
	}
}

