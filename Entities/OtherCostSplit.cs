using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class OtherCostSplit
	{
		public int ID { get; set; }
		public string Description { get; set; }
		public decimal PercentCustomerPays { get; set; }
		public Nullable<int> CountryCode_ID { get; set; }
		public Nullable<int> Code_ID { get; set; }
	}
}

