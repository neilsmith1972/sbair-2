using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class Country
	{
		public int ID { get; set; }
		public string CountryCode { get; set; }
		public string Name { get; set; }
	}
}

