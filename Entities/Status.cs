using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class Status
	{
		public int ID { get; set; }
		public string StatusText { get; set; }
	}
}

