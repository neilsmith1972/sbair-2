﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using avis.sbair.InvoiceService;

namespace avis.sbair.Entities
{
    public class InvoiceViewModel
    {
        public string RaNumber { get; set; }
        public DtoDocumentSearchResult SearchResult { get; set; }

        public string InvoiceNumber { get; set; }
    }

}