using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class PaymentMethod
	{
		public int ID { get; set; }
		public string PaymentMethodName { get; set; }
		public decimal CustomerPays { get; set; }
		public decimal InsurancePays { get; set; }
	}
}

