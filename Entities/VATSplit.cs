using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class VATSplit
	{
		public int ID { get; set; }
		public string Description { get; set; }
		public string PrintingDescription { get; set; }
		public decimal PercentageCustomerPays { get; set; }
		public string AppliedTo { get; set; }
	}
}

