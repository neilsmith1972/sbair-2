using System;
using System.Collections.Generic;

namespace avis.sbair.Entities
{
	public class OtherInsurancePriceVersion
	{
		public int ID { get; set; }
		public int MappedID { get; set; }
		public System.DateTime FromDate { get; set; }
		public Nullable<System.DateTime> ToDate { get; set; }
		public string Informaton { get; set; }
		public decimal VAT_Pcent { get; set; }
		public Nullable<int> OtherInsuranceCode_ID { get; set; }
		public Nullable<int> CountryCode_ID { get; set; }
	}
}

