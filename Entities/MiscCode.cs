using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Entities
{
	public class MiscCode
	{
        [Key]
		public string MiscCodeLetter { get; set; }
		public string MiscCodeDescription { get; set; }
		
	}
}

