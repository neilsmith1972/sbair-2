using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace avis.sbair.Entities
{
	public class ExpenseCode
	{
        [Key]
		public string ExpenseCodeLetter { get; set; }
		public string ExpenseCodeDescription { get; set; }
		
	}
}

